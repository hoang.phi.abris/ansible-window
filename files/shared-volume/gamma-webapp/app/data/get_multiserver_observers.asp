<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="include/JSON.asp"-->
	<!--#include file="include/get_db_data.asp"-->
	<!--#include file="include/controller_groups.asp"-->
	<!--#include file="security/security.asp"-->
	<!--#include file="include/json2.asp"-->

	<%
try {
	var host_id = Number(Request("host_id"));
	if(CheckRight(SEC_ROLE_LOGIN, null) && FindRights([SEC_ROLE_ENVMAN_CONFIG, SEC_ROLE_ENVMAN_VIEW]))
	{
		var result = {}
		result.observers = []
		result.instances = []

		var sql= "SELECT name FROM Configuration.ObserverClass WHERE is_multi_server = 1"
		var rowsA = runSQL(sql,cAcc,"fulltable");
		for(var i=0; i<rowsA.length; i++) {
			result.observers.push(rowsA[i].name);
		}
		
		var sql= "SELECT i.id as iid, di.id as diid, dh.id, dh.name as dhname FROM Configuration.Hosts h "
		sql+=    "	INNER JOIN Configuration.Instances i ON h.name = i.host "
		sql+=    "	INNER JOIN Configuration.Environments e ON i.environment = e.name "
		sql+=    "	INNER JOIN Configuration.Hosts dh ON e.default_host = dh.id "
		sql+=    "	INNER JOIN Configuration.Instances di ON di.environment = e.name AND di.host = dh.name "
		sql+=    "WHERE h.id = " + host_id + " AND (i.environment <> di.environment OR i.host <> di.host)"

		var rowsA = runSQL(sql,cAcc,"fulltable");
		for(var i=0; i<rowsA.length;i++) {
			var instance = {
				iid : rowsA[i].iid,
				diid : rowsA[i].diid,
				dhost_id : rowsA[i].id,
				dhname : rowsA[i].dhname,
				observers : []
			}
			sql = "SELECT theFile From Configuration.ConfigFiles WHERE host = '" + rowsA[i].dhname + "' AND STATUS = 0 AND filetype_id = 1";
			var rowsB = runSQL(sql,cAcc,"fulltable");

			var xmlDoc = Server.CreateObject("MSXML2.DOMDocument");
			xmlDoc.loadXML(rowsB[0].theFile)
			var arrayNodes = ["INSTANCE", "OBSERVER", "BATCH", "EXCEPTION", "PHANTOM", "ALLOWED", "DENIED", "EXPRESSION", "INTERLOCK", "MQ",
							  "FILTER", "USER_GROUP", "PROCESS", "APP_LEVEL", "REC_LEVEL", "OFS_PARAM", "TC_ADAPTER", "TSA_AGENT", "PHANTOM", "BUSINESS",
							  "STRING", "JOB", "TC_HOST", "LISTENER", "FILE", "CODE", "PRINTER", "FILE_PATH", "SERVLET", "TOCF_HOST", "TOCF_QUEUE", "PARAM", 
							  "DISK", "EXCLUDE_DISK", "PROG", "SETTING", "BROWSER_SERVER","COMMAND","EXCLUDE_EXP", "SERVICE_TO_MONITOR", "USER", "APPSERVER", "WAS_ITEM", "ATTR",
							  "HTTP", "TWS", "TSM"
							  ];

			var json = xml2json(xmlDoc.documentElement, arrayNodes);

			for (var j=0; j<json.INSTANCES.INSTANCE.length; j++) {
				if (json.INSTANCES.INSTANCE[j]["@attributes"].id == instance.diid) {
					var observers = json.INSTANCES.INSTANCE[j].OBSERVER;
					if (observers != undefined) {
						for(var k=0;k<observers.length;k++) {
							if (observers[k]["@attributes"].multiserver == "true" && observers[k]["@attributes"].enabled == "true")
								instance.observers.push(observers[k])	
						}
					}
				}
			}
				
			result.instances.push(instance)
		}
		var data = JSON.stringify(result, null, 2);

		Response.Write(data);
	}
}
catch (err) {
  Response.Write('Error: ' + err.message);
   logData('get_multiserver_observer.asp: ' + err.message);
}

function checkIss(issFullArray, issToCheck) {
    for(var i=0; i<issFullArray.length; i++) {
        if(issFullArray[i]==issToCheck) {
            return true;
        }
    }
    return false;
}
function xml2json(currNode, arrayNodes) {
	var json = {}

	if (currNode.nodeType == 1) {
		if (currNode.attributes.length > 0) {
			json["@attributes"] = {}
			for(var i=0; i<currNode.attributes.length; i++) {
				json["@attributes"][currNode.attributes[i].name] = currNode.attributes[i].value
			}
		}
	}
	else if (currNode.nodeType == 3) {
		json = currNode.nodeValue
	}

	for (var i=0; i<currNode.childNodes.length; i++) {
	  var item = currNode.childNodes.item(i);
      var nodeName = item.nodeName;
      if (typeof(json[nodeName]) == "undefined")
      {
        if (checkIss(arrayNodes, nodeName)) {
          json[nodeName] = new Array();
          json[nodeName][0] = xml2json(item, arrayNodes);
        }
        else {
          json[nodeName] = xml2json(item, arrayNodes);
		}
      }
      else
      {
        if (typeof(json[nodeName].length) == "undefined")
        {
          var old = json[nodeName];
          json[nodeName] = new Array();
          json[nodeName][json[nodeName].length] = old;
        }

        json[nodeName][json[nodeName].length] = xml2json(item, arrayNodes);
      }
	}
	return json;
}


%>