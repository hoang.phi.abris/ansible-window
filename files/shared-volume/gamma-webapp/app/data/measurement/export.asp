<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

<%
var fileName = isset(Request("filename")) ? String(Request("filename")) : "Measurement export"
Response.ContentType = "application/vnd.ms-excel" 
Response.AddHeader("Content-disposition", "attachment; filename=" + fileName +".xls");	
%><?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
	<Title>GAMMA export</Title>
	<Author>GAMMA</Author> 
	<Created><%=Response.Write(Date())%></Created>
  <Version>1.0</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
   <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>9000</WindowHeight>
  <WindowWidth>20750</WindowWidth>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Hyperlink" ss:Name="Hyperlink">
   <Font ss:FontName="Helvetica" ss:Size="12" ss:Color="#0000D4" ss:Underline="Single"/>
</Style>

<Style ss:ID="MeasurementTitle">
	<Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1" ss:Indent="0"/>
	<Borders>
		<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#dcddde"/>
	</Borders>
	<Font ss:Bold="1" ss:Color="#616a76" ss:FontName="Tahoma" ss:Size="8"/>
	<Interior ss:Color="#dddddd" ss:Pattern="Solid"/>
</Style>
<Style ss:ID="MeasurementText">
	<Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/>
	<Borders>
		<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#dcddde"/>
	</Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#948B54"/>
	<Interior ss:Color="#eeeeee" ss:Pattern="Solid"/>
</Style>

<Style ss:ID="header">
	<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
	<Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#DCDDDE"/> </Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8"  ss:Color="#616A76" ss:Bold="1"/>
	<Interior ss:Color="#DBE5F1" ss:Pattern="Solid"/>
</Style>
<Style ss:ID="default">
	<Alignment ss:Vertical="Center" ss:WrapText="1"/>
	<Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/></Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#215867"/>
	<Interior ss:Color="#FAFAFA" ss:Pattern="Solid"/>
</Style>
<Style ss:ID="defaultcenter">
	<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
	<Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/></Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#215867"/>
	<Interior ss:Color="#FAFAFA" ss:Pattern="Solid"/>
</Style>
<Style ss:ID="defaultleft">
	<Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/>
	<Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/></Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#215867"/>
	<Interior ss:Color="#FAFAFA" ss:Pattern="Solid"/>
</Style>
<Style ss:ID="defaultright">
	<Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
	<Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/></Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#215867"/>
	<Interior ss:Color="#FAFAFA" ss:Pattern="Solid"/>
</Style>
<Style ss:ID="green">
	<Alignment ss:Vertical="Center" ss:WrapText="1"/>
	<Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/></Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#0D6B24"/>
	<Interior ss:Color="#FAFAFA" ss:Pattern="Solid"/>
</Style>
<Style ss:ID="greencenter">
	<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
	<Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/></Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#0D6B24"/>
	<Interior ss:Color="#FAFAFA" ss:Pattern="Solid"/>
</Style>
<Style ss:ID="red">
	<Alignment ss:Vertical="Center" ss:WrapText="1"/>
	<Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/></Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#E8051F"/>
	<Interior ss:Color="#FAFAFA" ss:Pattern="Solid"/>
</Style>
<Style ss:ID="redcenter">
	<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
	<Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/></Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#E8051F"/>
	<Interior ss:Color="#FAFAFA" ss:Pattern="Solid"/>
</Style>
<Style ss:ID="encrypted">
	<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
	<Borders>
		<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/>
		<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/>
	</Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#215867"/>
	<Interior ss:Color="#FAFAFA" ss:Pattern="Solid"/>
</Style>
 </Styles>
 <Worksheet ss:Name='Sheet1'>
<!--#include file="../include/JSON.asp"-->
<!--#include file="../include/get_db_data.asp"-->
<!--#include file="../include/util.asp"-->
<!--#include file="../security/security.asp"--> 

<%
/* configre extra url request header properties */
try{


    if(CheckRight(SEC_ROLE_LOGIN, null)) {
        var table = String(Request("table"));
        var record = eval('('+getData()+')');
        Response.Write(record.data.table);
        Response.Write('</Worksheet>')
        Response.Write('</Workbook>')
    }
    }catch (err) 
        {
              logData('measurement/exportWithDetail.asp: ' + err.message);
              Response.Write("{success:false,error:'" + err.message + "'}");
        } 
        
    
        function getData() {
            var sOut = '';
    
            if(Request.TotalBytes > 0) {
                var lngBytesCount = Request.TotalBytes;
    
                var stream = Server.CreateObject("Adodb.Stream");
                stream.type = 1;
                stream.open;
                stream.write(Request.BinaryRead(lngBytesCount));
                stream.position = 0;
                stream.type = 2;
                stream.charset = "utf-8";
                sOut = stream.readtext();
                stream.close;
            }
    
            return sOut;
        }
    %>