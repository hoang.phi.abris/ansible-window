<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<%
 Response.AddHeader("Pragma","public");
 Response.AddHeader("Expires","0");
 Response.AddHeader("Cache-Control","must-revalidate, post-check=0, pre-check=0");
 Response.AddHeader("Content-Type","application/force-download");
 Response.AddHeader("Content-Type","application/vnd.ms-excel");
 Response.AddHeader("Content-Disposition", "attachment; filename=export2excel.xls");

%>



		<!--#include file="../include/JSON.asp"-->
		<!--#include file="../include/get_db_data.asp"-->
		<!--#include file="../security/security.asp"-->

		<%
try{
/* configre extra url request header properties */
	if(CheckRight(SEC_ROLE_LOGIN, null) ) {
		var thetime = String(Request("thetime"));
		var start = parseFloat(isset(Request("start")) ? Request("start") : "0")
		var pageSize = parseFloat(isset(Request("pageSize")) ? Request("pageSize") : "50")
		var pieceOfItems = parseFloat(isset(Request("pieceOfItems")) ? Request("pieceOfItems") : "250")

		var severity = isset(Request("severity")) ? String(Request("severity")).replace(/^\s*/, "").replace(/\s*$/, "").replace(",","") : ""


		var severityfilter = severity.length > 0 ? " and severity in (" + severity + ")"  : " and severity in ('')";

		var observer = isset(Request("observer")) ? String(Request("observer")).replace(/^\s*/, "").replace(/\s*$/, "").replace(",","") : ""
		var host = isset(Request("host")) ? String(Request("host")) : ""

		var tfilter = isset(Request("timefilter")) ? String(Request("timefilter")) : ""
		var timefilter = tfilter.length > 0 ? " and timestamp <= '" + tfilter + "'" : ""
		var newerFilter = isset(Request("newerFilter")) ? " and timestamp > '" + String(Request("newerFilter")) + "'" : ""



		var observerfilter = observer.length > 0 ? " and observer = '" + observer + "'"  : "";
		var hostfilter = host.length > 0 ? " and m.host = '" + host + "'"  : "";

		var enddate = ""
		var whereline = ""

		sql  = "SELECT top "
		sql +=	pieceOfItems
		sql += "	timestamp "
		sql += "	,severity "
		sql += "	,m.environment"
		sql += "	,m.host as 'host' "
		sql += "	,observer "
		sql += "	,o.display_name as 'display' "
		sql += "	,m.id as 'id' "
		sql += "	,observation_duration_in_milisecond  as 'duration' "
		sql += "FROM "
		sql += "	measurements m  "
		sql += "	inner join 	Configuration.ObserverClass o ON m.observer = o.name "
		sql += "	LEFT JOIN (select master_environment_name, name, multicountry from Configuration.Environments) e  on e.name = '" + env + "' and e.multicountry = 1 "
		sql += "WHERE "
		sql += "	o.is_service = 0 "
		sql += "	and (environment = '" + env + "' OR environment=e.master_environment_name) "
		sql += observerfilter +  timefilter + severityfilter + hostfilter + newerFilter
		sql += " order by timestamp desc "




		//Response.Write(sql)

		//var pre = [];
		//var rowsA = [{"major":0,"warning":0}]
		rowsA = runSQL(sql,cAcc,"fulltable");

		ind				= 0;
		pagedRows = [];
		pageSize 	= (pageSize==0) ? rowsA.length : start + pageSize;
		pageSize 	= (pageSize > rowsA.length) ? rowsA.length : pageSize;
		if (rowsA.length >0)
		{
			//pagedRows = rowsA;
			for(ii=start;ii<pageSize; ii++)
			{
				pagedRows[ind++] = rowsA[ii];
			}
		}
		else
		{
			pagedRows = rowsA;
		}

		var myJSON = new JSON();
		data 		= myJSON.toJSON(null, pagedRows, false);
		fileds 	= myJSON.generateFields(rowsA)
		Response.Write( '{ "metaData": {	"totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fileds + '}, "total":"' + rowsA.length + '","results":' + data + '}');
	}
}catch (err)
	{
 		 logData('measurement/measurement.asp: ' + err.message);
 		 Response.Write("{success:false,error:'" + err.message + "'}");
	}
%>