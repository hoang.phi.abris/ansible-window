<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<%
var fileName = isset(Request("filename")) ? String(Request("filename")) : "Measurement export"
Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader("Content-disposition", "attachment; filename=" + fileName +".xls");
%>
		<?xml version="1.0"?>
		<?mso-application progid="Excel.Sheet"?>
			<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel"
			 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40">
				<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
					<Title>GAMMA export</Title>
					<Author>GAMMA</Author>
					<Created>
						<%=Response.Write(Date())%>
					</Created>
					<Version>1.0</Version>
				</DocumentProperties>
				<OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
					<AllowPNG/>
				</OfficeDocumentSettings>
				<ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
					<WindowHeight>9000</WindowHeight>
					<WindowWidth>20750</WindowWidth>
					<ProtectStructure>False</ProtectStructure>
					<ProtectWindows>False</ProtectWindows>
				</ExcelWorkbook>
				<Styles>
					<Style ss:ID="Hyperlink" ss:Name="Hyperlink">
						<Font ss:FontName="Helvetica" ss:Size="12" ss:Color="#0000D4" ss:Underline="Single"/>
					</Style>

					<Style ss:ID="MeasurementTitle">
						<Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1" ss:Indent="0"/><Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#dcddde"/></Borders><Font ss:Bold="1" ss:Color="#616a76" ss:FontName="Tahoma" ss:Size="8"/><Interior ss:Color="#dddddd" ss:Pattern="Solid"/>
					</Style>
					<Style ss:ID="MeasurementText">
						<Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/><Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#dcddde"/></Borders><Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#948B54"/><Interior ss:Color="#eeeeee" ss:Pattern="Solid"/>
					</Style>

					<Style ss:ID="header">
						<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/><Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#DCDDDE"/></Borders><Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#616A76" ss:Bold="1"/><Interior ss:Color="#DBE5F1" ss:Pattern="Solid"/>
					</Style>
					<Style ss:ID="default">
						<Alignment ss:Vertical="Center" ss:WrapText="1"/><Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/></Borders><Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#215867"/><Interior ss:Color="#FAFAFA" ss:Pattern="Solid"/>
					</Style>
					<Style ss:ID="defaultcenter">
						<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/><Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/></Borders><Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#215867"/><Interior ss:Color="#FAFAFA" ss:Pattern="Solid"/>
					</Style>
					<Style ss:ID="defaultleft">
						<Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/><Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/></Borders><Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#215867"/><Interior ss:Color="#FAFAFA" ss:Pattern="Solid"/>
					</Style>
					<Style ss:ID="defaultright">
						<Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/><Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/></Borders><Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#215867"/><Interior ss:Color="#FAFAFA" ss:Pattern="Solid"/>
					</Style>
					<Style ss:ID="green">
						<Alignment ss:Vertical="Center" ss:WrapText="1"/><Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/></Borders><Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#0D6B24"/><Interior ss:Color="#FAFAFA" ss:Pattern="Solid"/>
					</Style>
					<Style ss:ID="greencenter">
						<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/><Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/></Borders><Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#0D6B24"/><Interior ss:Color="#FAFAFA" ss:Pattern="Solid"/>
					</Style>
					<Style ss:ID="red">
						<Alignment ss:Vertical="Center" ss:WrapText="1"/><Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/></Borders><Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#E8051F"/><Interior ss:Color="#FAFAFA" ss:Pattern="Solid"/>
					</Style>
					<Style ss:ID="redcenter">
						<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/><Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/></Borders><Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#E8051F"/><Interior ss:Color="#FAFAFA" ss:Pattern="Solid"/>
					</Style>
					<Style ss:ID="encrypted">
						<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/><Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/><Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/></Borders><Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#215867"/><Interior ss:Color="#FAFAFA" ss:Pattern="Solid"/>
					</Style>
				</Styles>
				<Worksheet ss:Name='Measurements list '>
					<WorksheetOptions xmlns='urn:schemas-microsoft-com:office:excel'>
						<PageSetup>
							<Header x:Data='&amp;B&amp;S04+000GAMMA&amp;S000000 &amp;S06-023export&#10;Measurements&amp;K&amp;S04+000modelbank'
							/>
						</PageSetup>
						<Unsynced/>
						<Print>
							<ValidPrinterInfo/>
							<PaperSizeIndex>9</PaperSizeIndex>
							<HorizontalResolution>600</HorizontalResolution>
							<VerticalResolution>600</VerticalResolution>
						</Print>
						<Selected/>
						<Panes>
							<Pane>
								<Number>3</Number>
								<ActiveRow>4</ActiveRow>
								<ActiveCol>1</ActiveCol>
							</Pane>
						</Panes>
						<ProtectObjects>False</ProtectObjects>
						<ProtectScenarios>False</ProtectScenarios>
					</WorksheetOptions>
					<!--#include file="../include/JSON.asp"-->
					<!--#include file="../include/get_db_data.asp"-->
					<!--#include file="../include/util.asp"-->
					<!--#include file="../security/security.asp"-->

					<%
/* configre extra url request header properties */
try{
var env = String(Request("env"));
var type = String(Request("type"));
var dTitle = "Measurement details"
var observerSeverityFilter = ""
if (type=="alert") {
	dTitle = "Alert details"
	observerSeverityFilter = " and severity<>'normal'"
}

if(CheckRight(SEC_ROLE_LOGIN, null)) {
	var observer = "";

	var id = String(Request("id"));
	var hasRight = String(HasRight(SEC_ROLE_VIEW_SENSITIVE_DATA,env));

	var cm = observer.replace(/\./g, "")
	//Response.Write(eval(columns)

		sql  = "SELECT  "
		sql += "	timestamp "
		sql += "	,severity "
		sql += "	,m.environment "
		sql += "	,m.host "
		sql += "	,observer "
		sql += "	,o.display_name as 'display' "
		sql += "	,m.id "
		sql += "FROM "
		sql += "	measurements m  "
		sql += "	inner join 	Configuration.ObserverClass o ON m.observer = o.name "
		sql += "WHERE "
		sql += "	o.is_service = 0 "
		sql += "	and m.id = '" + id + "' "


	rowsA = runSQL(sql,cAcc,"fulltable");


	if (rowsA.length > 0) {
		observer = rowsA[0].observer
		columnsSql = "select * from Configuration.[Observer.Columns] where observer = '" + observer + "' order by sort"
		columns = runSQL(columnsSql,cAcc,"fulltable");

		Response.Write('<Table x:FullRows="1" x:FullColumns="1" ss:ExpandedColumnCount="' + (columns.length + 2 ) + '" ss:ExpandedRowCount="1000">');
		Response.Write('<Column ss:Width="70"/>');		//severity

		for (i=0; i<columns.length;i++)	{
			if (columns[i].dataIndex != 'severity') {
				Response.Write('<Column ss:Width="' + columns[i].width + '"/>');
			}
		}

	}
	else {
		Response.Write('<Table x:FullRows="1" x:FullColumns="1" ss:ExpandedColumnCount="10" ss:ExpandedRowCount="1000">');
		Response.Write('<Column ss:Width="70"/>');		//severity

	}
	Response.Write('<Row ss:AutoFitHeight="0" ss:Height="15">');
	Response.Write('	<Cell ss:MergeAcross="3" ss:StyleID="default">')
	Response.Write('		<ss:Data ss:Type="String" xmlns="http://www.w3.org/TR/REC-html40"><Font html:Color="#666699"><B>GAMMA </B> export </Font><Font html:Color="#666699">' + dTitle + '</Font></ss:Data>');
	Response.Write('	</Cell>');
	Response.Write('</Row>');

	setDateFormat()

	for (i=0; i<rowsA.length;i++) {

		var d = new Date(rowsA[i].timestamp);
		var options = {
			weekday: "long", year: "numeric", month: "short",
			day: "2-digit", hour: "2-digit", minute: "2-digit"
		};

		Response.Write('<Row ss:AutoFitHeight="0" ss:Height="22">');
		Response.Write('<Cell ss:StyleID="MeasurementTitle"><Data ss:Type="String">Severity</Data></Cell>');
		Response.Write('<Cell ss:StyleID="MeasurementText"><Data ss:Type="String">' + rowsA[i].severity + '</Data></Cell>');
		Response.Write('</Row>')
		Response.Write('<Row ss:AutoFitHeight="0" ss:Height="22">');
		Response.Write('<Cell ss:StyleID="MeasurementTitle"><Data ss:Type="String">Environment</Data></Cell>');
		Response.Write('<Cell ss:StyleID="MeasurementText"><Data ss:Type="String">' + rowsA[i].environment + '</Data></Cell>');
		Response.Write('</Row>')
		Response.Write('<Row ss:AutoFitHeight="0" ss:Height="22">');
		Response.Write('<Cell ss:StyleID="MeasurementTitle"><Data ss:Type="String">Observer</Data></Cell>');
		Response.Write('<Cell ss:StyleID="MeasurementText"><Data ss:Type="String">' + rowsA[i].display + '</Data></Cell>');
		Response.Write('</Row>')
		Response.Write('<Row ss:AutoFitHeight="0" ss:Height="22">');
		Response.Write('<Cell ss:StyleID="MeasurementTitle"><Data ss:Type="String">Timestamp</Data></Cell>');
		//Response.Write('<Cell ss:StyleID="MeasurementText"><Data ss:Type="String">' + d.toLocaleDateString("en-US",options) + '</Data></Cell>');
		Response.Write('<Cell ss:StyleID="MeasurementText"><Data ss:Type="String">' +  new Date(rowsA[i].timestamp).format("yyyy-mm-dd HH:MM:ss") + '</Data></Cell>');
		Response.Write('</Row>')
		Response.Write('<Row ss:AutoFitHeight="0" ss:Height="22">');
		Response.Write('<Cell ss:StyleID="MeasurementTitle"><Data ss:Type="String">Host</Data></Cell>');
		Response.Write('<Cell ss:StyleID="MeasurementText"><Data ss:Type="String">' + rowsA[i].host + '</Data></Cell>');
		Response.Write('</Row>')
	}

	columnsSql = "select * from Configuration.[Observer.Columns] where observer = '" + observer + "' order by sort"

	columns = runSQL(columnsSql,cAcc,"fulltable");
	var myJSON = new JSON();

	Response.Write('<Row>')
	Response.Write('</Row>');


	sql= "SELECT  *  from observer.[" + observer + "] where measurement_id = '" + id + "' " + observerSeverityFilter
	observer = runSQL(sql,cAcc,"fulltable");

	if (observer.length > 0) {
		Response.Write('<Row ss:AutoFitHeight="0" ss:Height="22">');
		for (i=0; i<columns.length;i++)	{

			if(hasRight == "false"){
				if(columns[i].sensitive != 1) {
					Response.Write('<Cell ss:StyleID="header"><Data ss:Type="String">' + columns[i].header + '</Data></Cell>');
				}
			}
			else{
				Response.Write('<Cell ss:StyleID="header"><Data ss:Type="String">' + columns[i].header + '</Data></Cell>');
			}


		}
		Response.Write('</Row>')
	}
	else {
		Response.Write('<Row ss:AutoFitHeight="0" ss:Height="22">');
		Response.Write('<Cell ss:StyleID="header"><Data ss:Type="String">Key</Data></Cell>');
		Response.Write('<Cell ss:StyleID="header"><Data ss:Type="String">Value</Data></Cell>');
		Response.Write('</Row>')
		sql= "SELECT  *  from dbo.error where measurement_id = '" + id + "' "
		observer = runSQL(sql,cAcc,"fulltable");
		columns = [];
		columns.push({dataIndex:'key',width:100})
		columns.push({dataIndex:'value',width:300})
	}
	for (i=0; i<observer.length;i++) {
		Response.Write('<Row ss:AutoFitHeight="0" ss:Height="40">');
		if (observer[i].respdata) {
			var obj = eval ("(" + observer[i].respdata + ")");
			for(var pName in obj) {
				observer[i][pName.toLowerCase()] = obj[pName]
			}
		}
		for (ind=0; ind<columns.length;ind++)
		{
			var tStr = (observer[i][columns[ind].dataIndex]) ? observer[i][columns[ind].dataIndex].toString() : '';
			var tStr = tStr.replace(/<BR>/g, "");
			var tStr = tStr.replace(/\</g, "&lt;");
			var tStr = tStr.replace(/\>/g, "&gt;");
			var tStr = tStr.replace(/\&/g, "&amp;");

			if(hasRight == "false") {
				if(columns[ind].sensitive != 1){
					Response.Write(writeData(columns[ind].sensitive));
				}
			} else{
				Response.Write(writeData(columns[ind].sensitive));
			}


			function writeData(sensitive) {
				if(sensitive==1) {
					return '<Cell ss:StyleID="encrypted"><Data ss:Type="String">' + tStr + '</Data></Cell>\n';
				} else {
					return '<Cell ss:StyleID="defaultcenter"><Data ss:Type="String">' + tStr + '</Data></Cell>\n';
				}
			};


		}
		Response.Write('</Row>');
	}

	Response.Write('</Table>')
	Response.Write('</Worksheet>')
	Response.Write('</Workbook>')
}
}catch (err)
	{
 		 logData('measurement/exportWithDetail.asp: ' + err.message);
 		 Response.Write("{success:false,error:'" + err.message + "'}");
	}
%>