<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>
<%
var fileName = isset(Request("filename")) ? String(Request("filename")) : "Measurement export"
Response.Buffer = true;
Response.ContentType = "application/vnd.ms-excel" 
Response.AddHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");	
%><?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
	<Title>GAMMA export</Title>
	<Author>GAMMA</Author> 
	<Created><%=Response.Write(Date())%></Created>
  <Version>1.0</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
   <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>9000</WindowHeight>
  <WindowWidth>20750</WindowWidth>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
<Style ss:ID="header">
	<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
	<Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#DCDDDE"/> </Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8"  ss:Color="#616A76" ss:Bold="1"/>
	<Interior ss:Color="#DBE5F1" ss:Pattern="Solid"/>
</Style>
<Style ss:ID="defaultcenter">
	<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
	<Borders>
		<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/>
		<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/>
	</Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#215867"/>
	<Interior ss:Color="#FAFAFA" ss:Pattern="Solid"/>
</Style>
<Style ss:ID="defaultcenterRightBorder">
	<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
	<Borders>
		<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/>
		<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#215867"/>
	</Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#215867"/>
	<Interior ss:Color="#FAFAFA" ss:Pattern="Solid"/>
</Style>
<Style ss:ID="defaultcenterUpperAndRightBorder">
	<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
	<Borders>
		<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#215867"/>
		<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/>
		<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#215867"/>
	</Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#215867"/>
	<Interior ss:Color="#FAFAFA" ss:Pattern="Solid"/>
</Style>
<Style ss:ID="defaultcenterUpperBorder">
	<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
	<Borders>
		<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#215867"/>
		<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/>
		<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/>
	</Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#215867"/>
	<Interior ss:Color="#FAFAFA" ss:Pattern="Solid"/>
</Style>
<Style ss:ID="defaultcenterSecondLine">
	<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
	<Borders>
		<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/>
		<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/>
	</Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#215867"/>
	<Interior ss:Color="#F1F1F1" ss:Pattern="Solid"/>
</Style>
<Style ss:ID="defaultcenterSecondLineRightBorder">
	<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
	<Borders>
		<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/>
		<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#215867"/>
	</Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#215867"/>
	<Interior ss:Color="#F1F1F1" ss:Pattern="Solid"/>
</Style>
<Style ss:ID="defaultcenterSecondLineUpperAndRightBorder">
	<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
	<Borders>
		<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#215867"/>
		<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/>
		<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#215867"/>
	</Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#215867"/>
	<Interior ss:Color="#F1F1F1" ss:Pattern="Solid"/>
</Style>
<Style ss:ID="defaultcenterSecondLineUpperBorder">
	<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
	<Borders>
		<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#215867"/>
		<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/>
		<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/>
	</Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#215867"/>
	<Interior ss:Color="#F1F1F1" ss:Pattern="Solid"/>
</Style>
<Style ss:ID="defaultcritical">
	<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#868A08"/>
	<Interior ss:Color="#D9D9D9" ss:Pattern="Solid"/>
</Style>
<Style ss:ID="defaultcriticalTOP">
	<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
	<Borders>
		<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#215867"/>
		<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/>
	</Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#868A08"/>
	<Interior ss:Color="#D9D9D9" ss:Pattern="Solid"/>
</Style>
<Style ss:ID="defaultcriticalTOPRIGHT">
	<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
	<Borders>
		<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#215867"/>
		<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#215867"/>
	</Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#868A08"/>
	<Interior ss:Color="#D9D9D9" ss:Pattern="Solid"/>
</Style>
<Style ss:ID="defaultcriticalRIGHT">
	<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
	<Borders>
		<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#215867"/>
	</Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#868A08"/>
	<Interior ss:Color="#D9D9D9" ss:Pattern="Solid"/>
</Style>
 </Styles>
<!--#include file="../include/JSON.asp"-->
<!--#include file="../include/get_db_data.asp"-->
<!--#include file="../include/util.asp"-->
<!--#include file="../security/security.asp"-->

<%
/* configre extra url request header properties */
var baseTimeout = Server.ScriptTimeout
Server.ScriptTimeout=900
try{

var type = isset(Request("type")) ? String(Request("type")) : "NO TYPE"									

var severity = isset(Request("severity")) ? String(Request("severity")).replace(/^\s*/, "").replace(/\s*$/, "").replace(",","") : ""  		
var severityfilter = severity.length > 0 ? " and m.severity in (" + severity + ")"  : " and severity in ('')";
		
var myobserver = isset(Request("observer")) ? String(Request("observer")).replace(/^\s*/, "").replace(/\s*$/, "").replace(",","") : ""   
var observerfilter = myobserver.length > 0 ? " and m.observer = '" + myobserver + "' "  : "";

var host = isset(Request("host")) ? String(Request("host")) : ""
var hostfilter = host.length > 0 ? " and m.host = '" + host + "'"  : "";
		

var fromdate = isset(Request("fromdate")) ? new Date(Request("fromdate")) : ""
var todate = isset(Request("todate")) ? new Date(Request("todate")) : ""	

	var fromdates = fromdate.toString().split(' ');
	var fromdate = fromdates[1] + ' ' + fromdates[2] + ' ' + fromdates[3] + ' ' + fromdates[5] ;
	
	var todates = todate.toString().split(' ');
	var todate = todates[1] + ' ' + todates[2] + ' ' + todates[3] + ' ' + todates[5];

var fromFilter = " and Cast(m.timestamp as datetime) >= Cast('" +  fromdate + "' as datetime)";
var toFilter = " and Cast(m.timestamp as datetime) <= Cast('" +  todate + "' as datetime)";

var dTitle = "Measurement details"
var observerSeverityFilter = ""

if (type=="alert") {
	dTitle = "Alert details"
	observerSeverityFilter = " and severity<>'normal'"
}

if(CheckRight(SEC_ROLE_LOGIN, null)) {
	var observer = "";
	var xmlText = '';
	
	var env = String(Request("env"));	
	var env = isset(Request("env")) ? String(Request("env")) : ""
	var envfilter = env.length > 0 ? " and m.environment = '" + env + "'"  : "";
		
	
	sql  = "SELECT "
		sql += "	timestamp "
		sql += "	,severity "
		sql += "	,m.environment "
		sql += "	,m.host "
		sql += "	,observer "
		sql += "	,o.display_name as 'display' "
		sql += "	,m.id as 'id' "
		sql += "FROM "
		sql += "	measurements m  "
		sql += "	inner join 	Configuration.ObserverClass o ON m.observer = o.name  "
		sql += "WHERE "
		sql += "	o.is_service = 0 "
		sql +=   	envfilter 
		sql += 		observerfilter +  fromFilter + toFilter + severityfilter + hostfilter
	sql += "  Order by timestamp desc;"
	rowsA = runSQL(sql,cAcc,"fulltable");

	var	osql  = "SELECT  Distinct"
		osql += "	observer "
		osql += "FROM "
		osql += "	measurements m  "
		osql += "	inner join 	Configuration.ObserverClass o ON m.observer = o.name "
		osql += "WHERE "
		osql += "	o.is_service = 0 "
		osql +=   	envfilter 
		osql += 	observerfilter +  fromFilter + toFilter + severityfilter + hostfilter
	osql += "  Order by observer asc;"

	var observers = runSQL(osql,cAcc,"fulltable");
	var lineNumber = 0; 
	var hasRight = String(HasRight(SEC_ROLE_VIEW_SENSITIVE_DATA,env));
		for(var observerindex = 0; observerindex < observers.length; observerindex++){
			if (rowsA.length > 0) {
				var observer = observers[observerindex].observer;
				var columnsSql = "select * from Configuration.[Observer.Columns] where observer = '" + observer + "' order by sort"
				var columns = runSQL(columnsSql,cAcc,"fulltable");
				
				subloop: for (i=0; i<rowsA.length;i++) {
					if(rowsA[i].observer == observers[observerindex].observer){
						xmlText +=  ('<Worksheet ss:Name="' + rowsA[i].display + '">')
						xmlText +=  ('\n<WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel"> <PageSetup><Header x:Data="&amp;B&amp;S04+000GAMMA&amp;S000000 &amp;S06-023export&#10;Measurements&amp;K&amp;S04+000modelbank"/></PageSetup><Unsynced/> <Print><ValidPrinterInfo/><PaperSizeIndex>9</PaperSizeIndex><HorizontalResolution>600</HorizontalResolution> <VerticalResolution>600</VerticalResolution></Print><Selected/><Panes><Pane><Number>3</Number><ActiveRow>4</ActiveRow><ActiveCol>1</ActiveCol></Pane></Panes><ProtectObjects>False</ProtectObjects>   <ProtectScenarios>False</ProtectScenarios>  </WorksheetOptions> ') 
						break subloop;
					}
				}
				xmlText +=  ('\n<Table x:FullRows="1" x:FullColumns="1" ss:ExpandedColumnCount="100" ss:ExpandedRowCount="1000000">');
				xmlText +=  ('\n<Column ss:Width="70"/>');	
				xmlText +=  ('\n<Column ss:Width="70"/>');
				xmlText +=  ('\n<Column ss:Width="70"/>');					
				
				for (i=0; i<columns.length;i++)	{
					if (columns[i].dataIndex != 'severity') {
						xmlText +=  ('\n<Column ss:Width="' + columns[i].width + '"/>');
					}
				}
			
			}
			else {
				xmlText +=  ('\n<Table x:FullRows="1" x:FullColumns="1" ss:ExpandedColumnCount="100" ss:ExpandedRowCount="1000000">');
				//xmlText +=  ('\n<Column ss:Width="70"/>');		

			}

			var firstInThisObserver = true;
			var firstCriticalInThisObserver = true;
			var firstLine = false;
			var upperboard = true;
			
			for (index=0; index<rowsA.length;index++) {
				if(rowsA[index].observer == observers[observerindex].observer){
					firstLine = !firstLine;
					var isCritical = false;
					setDateFormat()
					observer = "";
					observer = observers[observerindex].observer;
					
					Response.Write(xmlText);
					xmlText = '';
					Response.Flush;
					
					columnsSql = "select * from Configuration.[Observer.Columns] where observer = '" + observer + "' order by sort"

					columns = runSQL(columnsSql,cAcc,"fulltable");
					var myJSON = new JSON();
					
					var d = new Date(rowsA[index].timestamp);
					var options = {
						weekday: "long", year: "numeric", month: "short",
						day: "2-digit", hour: "2-digit", minute: "2-digit"
					};
				
					sql= "SELECT  *  from observer.[" + observer + "] where measurement_id = '" + rowsA[index].id + "' " + observerSeverityFilter 
					observer = "";
					observer = runSQL(sql,cAcc,"fulltable");
					

					if (observer.length > 0 && firstInThisObserver) {
						firstInThisObserver = false;
						xmlText +=  ('\n<Row ss:AutoFitHeight="0" ss:Height="22">');
						lineNumber++;
						xmlText +=  ('<Cell ss:StyleID="header"><Data ss:Type="String">Host</Data></Cell>');
						//xmlText +=  ('<Cell ss:StyleID="header"><Data ss:Type="String">Severity</Data></Cell>');	
						xmlText +=  ('\n<Cell ss:StyleID="header"><Data ss:Type="String">Timestamp</Data></Cell>');
							
						for (i=0; i<columns.length;i++)	{
							if(columns[i].header == '' && i == 0)
								xmlText +=  ('\n<Cell ss:StyleID="header"><Data ss:Type="String">Severity</Data></Cell>');
							else {
								if(columns[i].sensitive != 1 || hasRight=="true") {
									xmlText +=  ('\n<Cell ss:StyleID="header"><Data ss:Type="String">' + columns[i].header + '</Data></Cell>');
								}
							}
						}
						xmlText +=  ('</Row>')
					}
					else if (observer.length <= 0) {
						if(firstInThisObserver &&  firstCriticalInThisObserver){
							firstCriticalInThisObserver = false;
							lineNumber++;
							xmlText +=  ('\n<Row ss:AutoFitHeight="0" ss:Height="22">');
							xmlText +=  ('<Cell ss:StyleID="header"><Data ss:Type="String">Host</Data></Cell>');								
							xmlText +=  ('\n<Cell ss:StyleID="header"><Data ss:Type="String">Timestamp</Data></Cell>');
							xmlText +=  ('<Cell ss:StyleID="header"><Data ss:Type="String">Severity</Data></Cell>');
							xmlText +=  ('\n<Cell ss:StyleID="header"><Data ss:Type="String">Key</Data></Cell>');
							xmlText +=  ('\n<Cell ss:StyleID="header"><Data ss:Type="String">Value</Data></Cell>');
							xmlText +=  ('\n</Row>')
						}
						isCritical = true;
						firstLine = !firstLine;
						sql= "SELECT  *  from dbo.error where measurement_id = '" + rowsA[index].id + "' " 
						observer = runSQL(sql,cAcc,"fulltable");
						columns = [];
						columns.push({dataIndex:'key',width:100})
						columns.push({dataIndex:'value',width:300})
					}
					var styleId = 'defaultcenter';
					for ( i = 0; i < observer.length; i++ ) {
						if(isCritical){	
							styleId = 'defaultcritical';	
							if( i == 0 )
							{
								styleId = 'defaultcriticalTOP';
							}
						}
						else if(firstLine){
							styleId = 'defaultcenter';
							if( i == 0 )
							{
								styleId = 'defaultcenterUpperBorder';
							}
						}
						else if(!firstLine && !isCritical)
						{
							styleId = 'defaultcenterSecondLine';
							if( i == 0 )
							{
								styleId = 'defaultcenterSecondLineUpperBorder';
							}
						}
						lineNumber++;
						xmlText +=  ('\n<Row ss:AutoFitHeight="0" ss:Height="40">');
						xmlText +=  ('<Cell ss:StyleID="' + styleId + '"><Data ss:Type="String">' + rowsA[index].host + '</Data></Cell>');						
						xmlText +=  ('\n<Cell ss:StyleID="' + styleId + '"><Data ss:Type="String">' +  new Date(rowsA[index].timestamp).format("yyyy-mm-dd HH:MM:ss") + '</Data></Cell>');		
						if(isCritical){							
							xmlText +=  ('\n<Cell ss:StyleID="' + styleId + '"><Data ss:Type="String">' + rowsA[index].severity + '</Data></Cell>');
						}
						if (observer[i].respdata) {			
							var obj = eval ("(" + observer[i].respdata + ")");
							for(var pName in obj) {
								observer[i][pName.toLowerCase()] = obj[pName]
							}			
						}
						columnsloop: for (ind=0; ind < columns.length; ind++ ) 
						{		
							var tStr = (observer[i][columns[ind].dataIndex]) ? observer[i][columns[ind].dataIndex].toString() : '';
							
							var tStr = tStr.replace(/<BR>/g, "");
							var tStr = tStr.replace(/\</g, "&lt;");
							var tStr = tStr.replace(/\>/g, "&gt;");
							var tStr = tStr.replace(/\&/g, "&amp;");
							var sensitive = false;
							
							if(columns[ind].sensitive == 1) {
								sensitive = true;
							}
							if(hasRight == "false" && sensitive){
								continue;
							}
							if(sensitive) {
								tStr = '@@@'+tStr+'@@@';
							}
							if(isCritical){	
								styleId = 'defaultcritical';
								var tStr2 = tStr;
								tStr = ' ';
								tStr2 += (observer[i][columns[ind + 1].dataIndex]) ? observer[i][columns[ind + 1].dataIndex].toString() : '';
								var tStr2 = tStr2.replace(/<BR>/g, "");
								var tStr2 = tStr2.replace(/\</g, "&lt;");
								var tStr2 = tStr2.replace(/\>/g, "&gt;");
								var tStr2 = tStr2.replace(/\&/g, "&amp;");
								
								if( i == 0 )
								{
									styleId = 'defaultcriticalTOP';
								}
								
								xmlText +=  ('\n<Cell ss:StyleID="' + styleId + '"> <Data ss:Type="String">' + tStr + '</Data></Cell>')
								
								styleId = 'defaultcriticalRIGHT';
								if( ind == columns.length - 2 && i == 0)
								{
									styleId = 'defaultcriticalTOPRIGHT';
								}								
								
								xmlText +=  ('\n<Cell ss:StyleID="' + styleId + '"> <Data ss:Type="String">' + tStr2 + '</Data></Cell>')
								
								break columnsloop;
							}
							else if(firstLine){
								styleId = 'defaultcenter';
								if( i == 0 )
								{
									styleId = 'defaultcenterUpperBorder';
								}
								if(ind == columns.length - 1)
								{
									styleId = 'defaultcenterRightBorder';
								}
								if( ind == columns.length - 1 && i == 0)
								{
									styleId = 'defaultcenterUpperAndRightBorder';
								}
								xmlText +=  ('\n<Cell ss:StyleID="' + styleId + '"> <Data ss:Type="String">' + tStr + '</Data></Cell>')
							}							
							else
							{
								styleId = 'defaultcenterSecondLine';
								if( i == 0 )
								{
									styleId = 'defaultcenterSecondLineUpperBorder';
								}
								if(ind == columns.length - 1)
								{
									styleId = 'defaultcenterSecondLineRightBorder';
								}
								if( ind == columns.length - 1 && i == 0)
								{
									styleId = 'defaultcenterSecondLineUpperAndRightBorder';
								}
								xmlText +=  ('\n<Cell ss:StyleID="' + styleId + '"> <Data ss:Type="String">' + tStr + '</Data></Cell>')
							}
						}
						//xmlText +=  ('\n<Cell ss:StyleID="' + styleId + '"> <Data ss:Type="String">' + 'SOR:' + lineNumber + '</Data></Cell>')
						xmlText +=  ('\n</Row>');	
					}	
				}
			}
			xmlText +=  ('\n</Table>')
			xmlText +=  ('\n</Worksheet>')
		}

		xmlText +=  ('\n</Workbook>')
		Response.Write(xmlText)
	}
}catch (err) 
	{
 		 logData('measurement/exportAllWithDetails.asp: ' + err.message);
 		 Response.Write( xmlText + "\n{success:false,error: '" + err.message + "'}");
	} 
	Server.ScriptTimeout = baseTimeout
%>
