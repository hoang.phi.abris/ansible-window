<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

<!--#include file="include/JSON.asp"-->
<!--#include file="include/get_db_data.asp"-->
<!--#include file="security/security.asp"-->

<%
try {
	if(CheckRight(SEC_ROLE_LOGIN, null)) {
		var sql= "SELECT token_type, sub_type, user_name, browser_id, status, DATEDIFF(minute, timestamp, GETDATE()) as duration, ask_user FROM Configuration.TokenData";
		rowsA = runSQL(sql,cAcc,"fulltable");
		
		var myJSON = new JSON();
		var data = ''
		var fields = ''
		if (rowsA.length)  {
			data = myJSON.toJSON(null, rowsA, false); 
			fields = myJSON.generateFields(rowsA)
		}
		else {
			data = '[]'
			fields = '[]'
		}

		Response.Write( '{ "metaData": {	"totalProperty" : "total", "root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '", "results":' + data + '}');
	}
} 
catch (err) {
  Response.Write('Error: ' + err.message);
   logData('list_config_token.asp: ' + err.message);
}

%>
