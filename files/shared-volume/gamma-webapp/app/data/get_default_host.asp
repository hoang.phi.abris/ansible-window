<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="include/JSON.asp"-->
	<!--#include file="include/get_db_data.asp"-->
	<!--#include file="security/security.asp"-->

	<%
try {
	if(CheckRight(SEC_ROLE_LOGIN, null)) {
		var environment_id = String(Request("environment_id"));
		
		var sql= "SELECT default_host FROM Configuration.Environments WHERE id = " + environment_id;
		rowsA = runSQL(sql, cAcc, "fulltable");
		
		var myJSON = new JSON();
		var data = ''
		var fields = ''
		if (rowsA.length)  {
			data = rowsA[0].default_host;
		}
		else {
			data = ''
		}

		Response.Write( data);
	}
} 
catch (err) {
  Response.Write('Error: ' + err.message);
    logData('get_default_host.asp: ' + err.message);
}

%>