<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

  <!--#include file="include/JSON.asp"-->
  <!--#include file="include/get_db_data.asp"-->

  <%

	var adCmdStoredProc = 4
    var adParamInput = 1
    var adParamOutput = 2
    var adInteger = 3
    var adBStr = 8
    var adVarWChar = 202
	var adVarBinary = 204
	var adStateClosed = 0
		
	var rowsA = []
  try {
  

		
		
    var inputterid = Request("inputterid");
    var commandid = Request("commandid");
    var host = Request("host");
    var environment = Request("environment");
    var parameter = Request("parameter");
    var instance = Request("instance");

   
    var cmd = Server.CreateObject('ADODB.Command');
    cmd.ActiveConnection = cAcc;
    cmd.CommandType = adCmdStoredProc
    cmd.CommandText = "dbo.CommandHandler";
    
    cmd.Parameters.Append(cmd.CreateParameter("@inputter", adInteger, adParamInput, -1, inputterid));
    cmd.Parameters.Append(cmd.CreateParameter("@commandid", adInteger, adParamInput, -1, commandid));
    cmd.Parameters.Append(cmd.CreateParameter("@host", adVarWChar, adParamInput, -1, host));
    cmd.Parameters.Append(cmd.CreateParameter("@environment", adVarWChar, adParamInput, -1, environment));
    cmd.Parameters.Append(cmd.CreateParameter("@parameter", adVarWChar, adParamInput, -1, parameter));
    cmd.Parameters.Append(cmd.CreateParameter("@instance", adVarWChar, adParamInput, -1, instance));
    cmd.Parameters.Append(cmd.CreateParameter("@status", adVarWChar, adParamOutput, 5));

	



   var rsTEMP = cmd.Execute();

	if(rsTEMP.State != adStateClosed) {
		var ii,jj,arg1,tempO;
		jj=0; // returned rows counter
		while (!rsTEMP.EOF) { // -- ROWS
			var tempO = new Object();
			for (ii=0; ii<rsTEMP.Fields.Count; ii++){
				colName = rsTEMP.Fields(ii).Name;
				if(colName == null || colName =="") // give it a name
					colName = "xyzcol"+ii;
				tempO[colName] = rsTEMP.Fields(ii).Value;
				arg1+= tempO[colName] + ", ";
			}
			rowsA[jj]=tempO;
			tempO=null;
			jj++;
			rsTEMP.MoveNext();
		}  // -- next ROW
		rsTEMP.Close();
		rsTEMP=null;
	} 
		
var myJSON = new JSON();
		data = myJSON.toJSON(null, rowsA, false); 
		fields = myJSON.generateFields(rowsA);
		count = rowsA.length;

		Response.Write( '{ "metaData": {	"totalProperty" : "total", "root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + count + '", "results":' + data + '}');
	
  } 
  catch (err) {
    Response.Write('Error: ' + err.message);
  }
%>