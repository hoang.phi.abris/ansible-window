<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

<!--#include file="include/JSON.asp"-->
<!--#include file="include/get_db_data.asp"-->
<!--#include file="security/security.asp"-->

<%
try {
	if(CheckRight(SEC_ROLE_LOGIN, null)) {
		var host_id = String(Request("host_id"));

		var sql= "SELECT h.id, h.name FROM Configuration.Instances i "
		sql += "	INNER JOIN Configuration.Environments e ON i.environment = e.name "
		sql += "	INNER JOIN Configuration.Hosts h ON i.host = h.name "
		sql += "WHERE environment IN ( "
		sql += "  	SELECT environment FROM Configuration.Hosts h "
		sql += "		INNER JOIN Configuration.Instances i ON h.name = i.host "
		sql += "	WHERE h.id = " + host_id
		sql += ") AND i.is_multi_server = 1 AND h.id <> " + host_id

		rowsA = runSQL(sql,cAcc,"fulltable");

		var myJSON = new JSON();
		var data = ''
		var fields = ''
		if (rowsA.length)  {
			data = myJSON.toJSON(null, rowsA, false); 
			fields = myJSON.generateFields(rowsA)
		}
		else {
			data = '[]'
			fields = '[]'
		}

		Response.Write( '{ "metaData": {	"totalProperty" : "total", "root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '", "results":' + data + '}');
	}
} 
catch (err) {
	logData('get_multi_server_hosts error : ' + err.message);
	Response.Write('Error: ' + err.message + ' ' + sql);
}

%>
