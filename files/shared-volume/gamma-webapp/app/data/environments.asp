<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="include/JSON.asp"-->
	<!--#include file="include/get_db_data.asp"-->
	<!--#include file="security/security.asp"-->

	<%
/* configre extra url request header properties */
try{
	if(CheckRight(SEC_ROLE_LOGIN, null))
	{
		sql	 = "SELECT 	name, master_environment_name, "
		sql += "				multicountry,"
		sql += "				description,"
		sql += "				isnull((select top 1 dbo.getSeverityNumber(m.severity) as 'severity_number' from alerts a inner join measurements m on m.id = a.measurement_id WHERE status < 2 and environment in (name, master_environment_name) order by severity_number desc),0) as 'severity',"
		sql += "				state, "
		sql += "				date, "
		sql += "				id, "
		sql += "				CONVERT(nvarchar(30), cob_date, 120) as cob_date, "
		sql += "				cob_start, "
		sql += "				cob_stage, t24_version, "
		sql += "				(select stage from configuration.[cob.stages] where id = cob_stage) as cob_stagename, "
		sql += "                default_host "
		sql += "FROM Configuration.Environments "
		sql += "WHERE state is not null"

		rowsA = runSQL(sql,cAcc,"fulltable");

		var myJSON = new JSON();
		data 		= myJSON.toJSON(null, rowsA, false);
		fileds 	= myJSON.generateFields(rowsA)

		//Response.Write( '({ "sql":"' + sql + '", "metaData": {	"totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fileds + '}, "total":"' + rowsA.length + '","results":' + data + '})');
		Response.Write( '{ "results":' + data + '}');
	}
}
catch (err) {
  Response.Write('Error: ' + err.message);
  logData('environments.asp: ' + err.message);
}
%>