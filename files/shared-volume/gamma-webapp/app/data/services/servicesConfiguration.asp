<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
/* configre extra url request header properties */
try{
if(CheckRight(SEC_ROLE_LOGIN, null) ) {
	var component = String(Request("component"));							
	var addComponent = isset(Request("add")) ? String(Request("add")).replace(/^\s*/, "").replace(/\s*$/, "") : ""
	var deleteComponent = isset(Request("delete")) ? String(Request("delete")).replace(/^\s*/, "").replace(/\s*$/, "") : ""
	var task = String(Request("task"));							

	if (task=="dataview") {
		sql   = "SELECT c.name, id, icon, class, description, business_impact, leaf, parent_id,observer, measurement, item, c.environment, c.host FROM service.[services] s "
		sql  += " 	inner join service.components c on c.id = s.component_id "
		sql  += " WHERE s.parent_id = '" + component + "' 	and (environment is null OR environment = '" + env + "')"
		sql  += " ORDER By leaf, name"	
	}

		
	if (task=="chart") 		{
		sql  = "WITH component_list( [Level] , parent_id, component_id, id, p_id) "
		sql += "AS "
		sql += "( "
		sql += "	SELECT CONVERT( INT , 0 ) [Level] "
		sql += "    	 , parent_id " 
		sql += "     	, component_id " 
		sql += "     	,convert(nvarchar(50), parent_id) + '-' +  convert(nvarchar(max), component_id) as id "
		sql += "     	,convert(nvarchar(max), parent_id) as p_id "
		sql += "	FROM service.[services] "
		sql += "	WHERE parent_id = '" + component + "' "
		sql += "	UNION ALL "
		sql += "	SELECT CONVERT( INT , ( ct.[Level] + 1 ) ) [Level] "
		sql += "    	, pc.parent_id "
		sql += "    	, pc.component_id "
		sql += "    	,convert(nvarchar(max), ct.id) + '-' +  convert(nvarchar(50), pc.component_id) as id "
		sql += "    	,convert(nvarchar(max), id) as p_id "
		sql += "	FROM service.[services]     pc "
		sql += " 	JOIN component_list    ct ON pc.Parent_id = ct.component_id "
		sql += ")" 
		if (String(env).length>1) { 
			sql += "SELECT (service.getComponentSeverity(component_id,'" + env + "')) as 'severity', name, clist.id, clist.p_id, icon, class, description, business_impact, leaf, parent_id, observer, measurement, item FROM component_list clist inner join service.components c on c.id = clist.component_id  and (c.environment is null OR c.environment = '" + env + "')"
		}
		else {
			sql += "SELECT 'null' as 'severity', name, clist.id, clist.p_id, icon, class, description, business_impact, leaf, parent_id FROM component_list clist inner join service.components c on c.id = clist.component_id  and (c.environment is null OR c.environment = '" + env + "')"	
		}
	}

	if (addComponent.length > 0 && CheckRight(SEC_ROLE_SERVICECONFIG, env) ) {				
		insertSql  = "INSERT INTO service.[services] (component_id,parent_id) "	
		insertSql += " VALUES " 
		insertSql += "     ('" + addComponent + "','" + component + "')"
		runSQL(insertSql,cAcc,"cmd");	
	}

	if (deleteComponent.length > 0 && CheckRight(SEC_ROLE_SERVICECONFIG, env) ) {	
		deleteSql  = "DELETE service.[services] "	
		deleteSql += " WHERE  component_id = '" + deleteComponent + "' and parent_id = '" + component + "'"
		runSQL(deleteSql,cAcc,"cmd");			
	}
	var rowsA = []

	var myJSON = new JSON();
	rowsA 	= runSQL(sql,cAcc,"fulltable");
	
	for (i=0; i<rowsA.length;i++) {
		rowsA[i].style = (rowsA[i].leaf) ? 'component' : 'service'
	}
	
	data 		= myJSON.toJSON(null, rowsA, false); 
	fileds 	= myJSON.generateFields(rowsA)

	Response.Write('{"results": '+ data + '}');
}
	}catch(err) {
	Response.Write( "{success:failed, error:'" + err.message + "'}");
	logData('services/servicesConfiguration.asp: ' + err.message);
}
function buildPropertyGridElement(arr)
{
	for (i=0; i<arr.length;i++)
	{
  Response.Write("name: " + arr[i].name + "<br>")
	

	}
}
%>