<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
/* configre extra url request header properties */
try{
if(CheckRight(SEC_ROLE_LOGIN, null)) 
{
	// && FindRights([SEC_ROLE_SERVICECONFIG, SEC_ROLE_SERVICEVIEW])
	sql =  "SELECT  "
	sql += "	name "
	sql += "	,id "
	sql += "	,class "
	sql += "	,description "
	sql += "	,leaf "
	sql += "	,business_impact "
	sql += "	,observer "
	sql += "	,measurement "
	sql += "	,item "
	sql += "	,icon "
	sql += "	,environment "		
	sql += "	,host "	
	sql += "FROM "
	sql += "	service.components "
	sql += "WHERE "
	sql += "	id <> 'FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF' "
	sql += "ORDER BY "
	sql += "	name "

	var rowsA = []

	var myJSON = new JSON();
	rowsA 	= runSQL(sql,cAcc,"fulltable");
	data 		= myJSON.toJSON(null, rowsA, false); 
	fileds 	= myJSON.generateFields(rowsA)

	Response.Write( '{"results": ' + data + '}');
}


}catch(err) {
	Response.Write( "{success:failed, error:'" + err.message + "'}");
	logData('services/availableComponents.asp: ' + err.message);
}

%>