<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
try{
var fromArchive	= (isset(Request("fromArchive"))) ? true : false
if(CheckRight(SEC_ROLE_LOGIN, null)) {

	if (fromArchive) {
		sql = "SELECT "
		sql += "	name as 'observer' "
		sql += "	,display_name "
		sql += "FROM "
		sql += "	Configuration.ObserverClass "
		sql += "WHERE "
		sql += "	enabled = 1 "
		sql += "	and name not in (select name FROM configuration.archive ) "
		sql += "ORDER By display_name "			
	}
	else
		sql = "select name as 'observer', display_name from Configuration.ObserverClass WHERE enabled = 1 and is_service = 0  ORDER by display_name";
		
	rowsA = runSQL(sql,cAcc,"fulltable");
	var myJSON = new JSON();
	data 		= myJSON.toJSON(null, rowsA, false);
	fileds 	= myJSON.generateFields(rowsA);
	Response.Write( '{ "metaData": {	"totalProperty" : "total", 	"root" : "results", 	"fields" : ' + fileds + '}, "total":"' + rowsA.length + '","results":' + data + '}');
}
}catch(err) {
	Response.Write( "{success:failed, error:'" + err.message + "'}");
	logData('services/observerList.asp: ' + err.message);
}
%>