<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
/* configre extra url request header properties */

try{
if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_SERVICECONFIG, env) ) {

	var task 			= String(Request("task"));
	var componentId 	= String(Request("id"));
	var source 	= eval ("(" + String(Request("source")) + ")"); 
	if (source.environment)
		source.environment = (source.environment.replace(/^\s+|\s+$/g,'') != "") ? "'" + source.environment.replace(/^\s+|\s+$/g,'') + "'" : null
	else
		source.environment = null
		
	var name = Array(Request("name"));

	verbose=0
	vprint(1,"JSON: processing task:"+task+":");

	var myJSON = new JSON();


	if (task=="read") {
		var pre_config_data = 
		[
			{"name"	: "name"			, "value" : ""},
			{"name" : "environment"		, "value" : ""},
			{"name" : "observer"		, "value" : ""},
			{"name" : "measurement"		, "value" : ""},
			{"name" : "item"			, "value" : ""},
			{"name"	: "class"			, "value" : ""},
			{"name" : "business_impact"	, "value" : ""},
			{"name" : "icon"			, "value" : "component.png"}
		]
			
			data 		=  myJSON.toJSON(null, pre_config_data, false); 
			Response.Write( '{  "metaData": { "description":"jobdesc", "totalProperty" : "total", 	"root" : "results", "id" : "id", 	fields:[{"name":"name"},{"name":"value"}]}, "total":"' + pre_config_data.length + '" ,"results":' + data	 + '}');
	}

	if (task=="insert") {
		Response.Write(insertComponent())
	}

	if (task=="update") {
			Response.Write(updateComponent())
	}
}
}catch(err) {
	Response.Write( "{success:failed, error:'" + err.message + "'}");
	logData('services/createComponent.asp: ' + err.message);
}

function updateComponent()
{
try{
	var sql;

	
	if (source.name.replace(/^\s+|\s+$/g,'') == "")
	{
		return '{"success":false, "error":"' + ((source.leaf) ? 'Service' : 'Component') + ' name is null"}';	
	}
	
	sql  =  "UPDATE [service].[components] SET "
	sql  += "	[observer] = '" + source.observer + "' "
	sql  += "	,[measurement] = '" + source.measurement + "' "
	sql  += "	,[environment] = " + source.environment + " "
	sql  += "	,[host] = '" + source.host + "' "
	sql  += "	,[item] = '" + source.item + "' "
	sql  += "	,[leaf] = '" + !source.leaf + "' "
	sql  += "	,[class] = '" + source["class"] + "' "
	sql  += "	,[description] = '" + source.description + "' "
	sql  += "	,[business_impact] = '" + source.business_impact + "' "
	sql  += "	,[name] = '" + source.name + "' "
	sql  += "	,[icon] = '" + source.icon + "' "
	sql  += "WHERE "
	sql  += "	id = '" + componentId + "'"

	//Response.Write(sql)
	
	runSQL(sql,cAcc,"cmd");
	return '{"success":true}';
}catch(err) {
	Response.Write( "{success:failed, error:'" + err.message + "'}");
	logData('services/createComponent.asp: ' + err.message);
}	
}

function insertComponent()
{
try{
	if (source.name.replace(/^\s+|\s+$/g,'') !="") 
	{
		sql = "select * from  service.[components] where name = '" + source.name + "'"
		var rowsA = runSQL(sql,cAcc,"fulltable");
		if (rowsA.length > 0)
		{
			return '{"success":false,"error":"' + ((source.leaf) ? 'Service' : 'Component') + ' name already exists"}';	
		}
	}
	
	if (source.leaf)
	{
		if (source.name == "")
		{
			return '({success:false,error:"Service name is null"})';	
		}

		sql  = "INSERT INTO service.[components] ([leaf],[class],[description],[business_impact],[name],[icon],[environment]) "
		sql += "     VALUES" 
		sql += "     (0,'" + source["class"] + "','" + source.description + "','" + source.business_impact + "','" + source.name + "','" + source.icon + "'," + source.environment + ")"

	}
	else
	{
		if (source.name == "")
		{
			return '{"success": false, "error": "Component name is null"}';	
		}
		if (source.observer == "")
		{
			return '{"success": false, "error": "Observer name is null"}';	
		}
		if (source.measurement == "")
		{
			return '{"success": false, "error": "Measurement type name is null"}';	
		}
		if (source.item == "")
		{
			source.item = '%'
		}
		
		sql  = "INSERT INTO service.[components] ([observer],[measurement],[item],[leaf],[class],[description],[business_impact],[name],[icon],[environment],[host]) "
		sql += "     VALUES" 
		sql += "     ('" + source.observer + "','" + source.measurement + "','" + source.item + "',1,'" + source["class"] + "','" + source.description + "','" + source.business_impact + "','" + source.name + "','" + source.icon + "'," + source.environment + ", '"+source.host+"')"
		}
	runSQL(sql,cAcc,"cmd");
	return '{"success":true}';	
}catch(err) {
	Response.Write( "{success:failed, error:'" + err.message + "'}");
	logData('services/createComponent.asp: ' + err.message);
}
}

%>