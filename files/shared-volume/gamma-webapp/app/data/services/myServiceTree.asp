﻿<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
/* configre extra url request header properties */
try{
if(CheckRight(SEC_ROLE_LOGIN, null)) {

	var node = String(Request("node"))=="id" ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF"  : String(Request("node"));							
	componentArray = node.split("/")
	var component = componentArray[componentArray.length-1]


	sql   = "SELECT "
	sql  += "	 c.name "
	sql  += "	,id "
	sql  += "	,icon as 'img' "
	sql  += "	,icon "
	sql  += "	,'myServiceTree-icon' as 'iconCls' "
	sql  += "	,class "
	sql  += "	,description "
	sql  += "	,business_impact "
	sql  += "	,leaf "
	sql  += "	,parent_id "
	sql  += "	,observer "
	sql  += "	,item "
	sql  += "	,measurement "
	sql  += "	,service.getComponentSeverity(component_id,'" + env + "') as severity "
	sql  += "	,c.environment "
	sql  += "	,c.host "
	sql  += "FROM "
	sql  += "	service.[services] s "
	sql  += " 	inner join service.components c on c.id = s.component_id  and (c.environment is null OR c.environment = '" + env + "') "
	sql  += "WHERE "
	sql  += "	s.parent_id = '" + component + "'"


	var rowsA = []

	var myJSON = new JSON();
	rowsA 	= runSQL(sql,cAcc,"fulltable");
	rowsA = addExtraProperties(rowsA)
	data 		= myJSON.toJSON(null, rowsA, false); 
	fileds 	= myJSON.generateFields(rowsA)

	Response.Write(  data );
}
	}catch(err) {
	Response.Write( "{success:failed, error:'" + err.message + "'}");
	logData('services/myServiceTree.asp: ' + err.message);
}

function addExtraProperties(arr) {
	for (i=0; i<arr.length;i++) {
		if(!arr[i].leaf) {
			arr[i].icon = "../gamma_icons/services/32/" + arr[i].icon
		} else {
			arr[i].icon = "resources/img/services/observers/" + arr[i].icon
		}
		arr[i].id = node + "/" + arr[i].id	
	}
	return arr
}
%>