<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="include/JSON.asp"-->
	<!--#include file="include/get_db_data.asp"-->
	<!--#include file="security/security.asp"-->

	<%
try {
	var environment = String(Request("environment"));
	if(CheckRight(SEC_ROLE_LOGIN, null) && (CheckRight(SEC_ROLE_CONTROLLER_VIEW, environment) || CheckRight(SEC_ROLE_CONTROLLER_CONFIG, environment)))
	{
		var sql= "select cf.id as fid, e.id as envid, cf.name as fname, e.name envname from Configuration.ControllerFunction cf inner join Configuration.Environments e on cf.environment_id = e.id"

		rowsA = runSQL(sql,cAcc,"fulltable");

		var myJSON = new JSON();
		var data = ''
		var fields = ''
		if (rowsA.length)  {
			data = myJSON.toJSON(null, rowsA, false); 
			fields = myJSON.generateFields(rowsA)
		}
		else {
			data = '[]'
			fields = '[]'
		}

		Response.Write( '{ "metaData": {	"totalProperty" : "total", "root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '", "results":' + data + '}');
	}
} 
catch (err) {
  Response.Write('Error: ' + err.message);
  logData('get_controller_functions.asp: ' + err.message);
}

%>