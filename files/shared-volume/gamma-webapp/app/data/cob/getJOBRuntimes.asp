<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
try{
if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_COBAN_VIEW, env)) {
	//get the appropiate parts of the request string for this type
	var jobid	= String(Request("jobid"));
	var cobid 	= isset(Request("cobid")) ? ", @cob_id='" + String(Request("cobid")) + "'" : ""  
	var start = parseFloat(isset(Request("start")) ? Request("start") : "0")
	var pageSize = parseFloat(isset(Request("pageSize")) ? Request("pageSize") : "0")  

	sql = "exec COB.getJOBRuntimes @job_id = '" + jobid + "' " + cobid
	
	rowsA = runSQL(sql,cAcc,"fulltable");

	ind				= 0;
	pagedRows = [];
	pageSize 	= (pageSize==0) ? rowsA.length : start + pageSize;
	pageSize 	= (pageSize > rowsA.length) ? rowsA.length : pageSize;
	if (rowsA.length >0) {
		for(ii=start;ii<pageSize; ii++)  {
			pagedRows[ind++] = rowsA[ii];
		}
	}
	else { 
		pagedRows = rowsA;
	}

	var myJSON = new JSON();//instantiate new json object
	data = myJSON.toJSON(null, pagedRows, false); //encode the data in json format
	fileds = myJSON.generateFields(rowsA)
	Response.Write( '{ "total":"' + rowsA.length + '", "results": ' + data + '}');
}
}catch (err) {
  Response.Write('Error: ' + err.message);
  logData('cob/getJOBRuntimes.asp: ' + err.message);
}
%>