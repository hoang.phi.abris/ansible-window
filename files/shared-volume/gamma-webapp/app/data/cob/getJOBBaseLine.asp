<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
try{
if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_COBAN_VIEW, env)) {
	//get the appropiate parts of the request string for this type
	var action 	= String(Request("action"));
	var job_id 	= String(Request("job_id"));
	var baseline= String(Request("baseline"));
	var job_name 	= String(Request("job_name"));
	var modified_by = String(Request("modified_by"));
	if (baseline == 'undefined')
		baseline = 0
	if (action=='insert') {	
		sql  = "exec COB.setCOBBaseLine @env='" + env + "', @baseline=" + baseline + ", @hardday_id=0, @job_id=" + job_id 
		rowsA = runSQL(sql,cAcc,"fulltable");
		/*Audit*/
		audit(86,41, job_name  )
	}
	if (action=='remove') {	
		sql  = "exec COB.setCOBBaseLine @remove=1, @env='" + env + "', @baseline=" + baseline + ", @hardday_id=0, @job_id=" + job_id 
		rowsA = runSQL(sql,cAcc,"fulltable");
		/*Audit*/
		audit(86,42,job_name  )
	}

	sql = "exec COB.getJOBBaseLine @env='" + env + "'";	
	rowsA = runSQL(sql,cAcc,"fulltable");
	var myJSON = new JSON();
	data =  myJSON.toJSON(null, rowsA, false); 
	fileds = myJSON.generateFields(rowsA)
	Response.Write( '{"metaData": {	"totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fileds + '}, "total":"' + rowsA.length + '","results":' + data + '}');			

}
}catch (err) {
  Response.Write('Error: ' + err.message);
  logData('cob/getJOBBaseLine.asp: ' + err.message);
}

%>