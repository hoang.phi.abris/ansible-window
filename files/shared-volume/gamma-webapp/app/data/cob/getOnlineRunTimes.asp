<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->


	<%
try{
if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_COBAN_VIEW, env)) {
		
	//get the appropiate parts of the request string for this type
	var cob_id 			= isset(Request("cobid")) ? Request("cobid") : '';
	var env 			= isset(Request("env")) ? Request("env") : '';
	var filter = isset(Request("filter")) ? String(Request("filter")) : ""  
	var filterstring  = filter.length > 0 ? " ,@filter='" + filter + "'" : ""
	var start		= parseInt(Request("start"))
	var limit		= parseInt(Request("limit"))
	var minruntime	=  (parseInt(Request("minruntime")) ? ',@minruntime=' + Request("minruntime") : "")

	sql = "execute  [Online].[getOnlineRunTimes] @cob_id = '" + cob_id +"', @env = '" + env + "'  " +  minruntime + filterstring

	rowsA = runSQL(sql,cAcc,"fulltable");
	var result = [];

	for(var i=start;i<rowsA.length && i<(start+limit); i++){
		result.push(rowsA[i])
	}

	var myJSON = new JSON();//instantiate new json object
	data = myJSON.toJSON(null, result, false); //encode the data in json format


	fileds = myJSON.generateFields(rowsA)
	Response.Write( '{ "metaData": {	"totalProperty" : "total", 	"root" : "results", "id" : "id"	}, "total":"' + rowsA.length + '","results":' + data + '}');
}
}catch (err) {
  Response.Write('Error: ' + err.message);
  logData('cob/getOnlineRunTimes.asp: ' + err.message);
}	
%>