<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
var env		= String(Request("env"));

try{
if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_COBAN_VIEW, env)) {
		
	//get the appropiate parts of the request string for this type
	var start		= String(Request("start"));
	var end			= String(Request("end"));
	var day			= String(Request("day"));
	var harddays 	= String(Request("harddays"));
	var env_id		= String(Request("env_id"));
	var gen_value 	= String(Request("gen_value"));

	var action	= String(Request("action"));

	if (action=="update")
	{
		sql = "exec COB.setHardDaysInCalendar '" + day + "', " + harddays	+ " , '" + gen_value + "' , "+env_id
		runSQL(sql,cAcc,"cmd");	
		audit(86,6,day )
	}

	var pre_data = []

	sql = "exec COB.getCOBCalendar '" + env + "', '" + start + "' , '" + end + "'"
	rowsA = runSQL(sql,cAcc,"fulltable");
	
	var id = 0
	for (i=0; i<rowsA.length;i++)
	{
		tablestr = "<table width='100%' border=0><tr><td>runtime</td><td align=right><b>" + rowsA[i].cob + "<b></td></tr>  <tr><td>open</td><td align=right><b>" + rowsA[i].cob_stop_time + "</b></td></tr></table>"		
		cid=5			
		if ((rowsA[i].COB_runtime > rowsA[i].cob_avg) && (rowsA[i].cob_avg > 1))
		{
			cid=6
		}	
		pre_data.push(
		{
			 //"id"		: "cob" + id++
			"id"		: "cob" + rowsA[i].closed_day	
			,"cid"		: cid
			,"cobid"	: rowsA[i].id
			,"cobtype"	: rowsA[i].isLastCOBonWeek
			//,"title"	: tablestr
			,"title"	: rowsA[i].cob
			,"notes"	: rowsA[i].cob_stop_time
			,"start"	: rowsA[i].closed_day
			,"end"		: rowsA[i].closed_day
			,"ad"		: true
			,"IsAllDay"	: true
		})
	}

	sql = "exec COB.getHarddayCalendar '" + env + "', '" + start + "' , '" + end + "'"
	rowsA = runSQL(sql,cAcc,"fulltable");
	var id = 0
	for (i=0; i<rowsA.length;i++)
	{
		pre_data.push(
		{
			 "id"		: "harddays" + id++
			,"cid"		: 0
			,"title"	: "harddays"
			,"start"	: rowsA[i].dt
			,"end"		: rowsA[i].dt
			,"ad"		: true
			,"IsAllDay"	: true
			,"HardDays"	: rowsA[i].value
		});
	}

	var myJSON = new JSON();
	data 	=  myJSON.toJSON(null, pre_data, false); 

	Response.Write( data);
}
}catch (err) {
  Response.Write('Error: ' + err.message);
  logData('cob/getCOBCalendar.asp: ' + err.message);
}


%>