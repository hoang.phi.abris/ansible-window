
<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

<!--#include file="../include/JSON.asp"-->
<!--#include file="../include/get_db_data.asp"-->
<!--#include file="../security/security.asp"-->

<%
try{
//	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_COBAN_VIEW, env) && CheckRight(SEC_ROLE_COBAN_JOBSTATISTICS, env)) {
if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_COBAN_VIEW, env)) {

	//get the appropiate parts of the request string for this type

	var task = String(Request("task"));
	var observer = String(Request("observer"));
	var id = String(Request("id"));
	var thetime = String(Request("thetime"));
	var whereline = String(Request("whereline"));

	vprint(1,"JSON: processing task:"+task+":");
	cobdate = isset(Request("cobdate")) ? Request("cobdate") : '';
	view_job_statistic_per_cob(cobdate, env)
}
}catch (err) {
  Response.Write('Error: ' + err.message);
  logData('cob/getJobStatistics.asp: ' + err.message);
}	

function view_job_statistic_per_cob(cobdate, env)
{
	try{
	sql= "exec cob.getStatistic_jobs '" + cobdate + "','" + env + "'"
	rowsA = runSQL(sql,cAcc,"fulltable");
	var myJSON = new JSON();//instantiate new json object
	data = myJSON.toJSON(null, rowsA, false); //encode the data in json format
	fileds = myJSON.generateFields(rowsA)
	Response.Write( '{"metaData": {	"totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fileds + '}, "total":"' + rowsA.length + '","results":' + data + '}');
}catch (err) {
  Response.Write('Error: ' + err.message);
  logData('cob/getJobStatistics.asp: ' + err.message);
}	
}

%>
