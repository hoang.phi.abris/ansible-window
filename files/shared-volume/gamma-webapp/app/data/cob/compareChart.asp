<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
try{
	var env = String(Request("destenv"));				
	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_COBAN_VIEW, env) ) {
		var job_id = String(Request("job_id"));		
		var fromdate = String(Request("fromdate"));
		var enddate = String(Request("enddate"));
		var harddays = String(Request("harddays"));
		var hconfig  = String(Request("hconfig"));
		var weekendstr  = String(Request("weekend"));
		var islastcobstr  = String(Request("cobtype"));
		var target = isset(Request("target")) ? String(Request("target")) : ""

		var weekend = 1
		var islastcob=0
		if (hconfig=='any')
		{
			if (harddays>0)
			{
					harddays = harddays *- 1
			}
		}

		if (hconfig=='none')
		{
			harddays = null

		}


		if (weekendstr=='false')
		{
			weekend = 0
		}

		
		if (islastcobstr=='true'){
			islastcob = 1
		}


		if (target!="") {
		sql = "exec [COB].[compareChart_stage] "
			sql += "	 @target = '" + target + "'"
			sql += "	,@env = '" + env + "'"
			sql += "	,@fromdate = '" + fromdate + "'"
			sql += "	,@enddate = '" + enddate + "'"
			sql += "	,@harddays = " + harddays
			sql += "	,@ftype = '" + hconfig + "'"
			sql += "	,@weekend = " + weekend + " "
			sql += "	,@islastcob = " + islastcob + " "
		
		}
		else {
			sql = "exec [COB].[compareChart_job] "
			sql += "	 @job_id = '" + job_id + "'"
			sql += "	,@env = '" + env + "'"
			sql += "	,@fromdate = '" + fromdate + "'"
			sql += "	,@enddate = '" + enddate + "'"
			sql += "	,@harddays = " + harddays
			sql += "	,@ftype = '" + hconfig + "'"
			sql += "	,@weekend = " + weekend + " "
			sql += "	,@islastcob = " + islastcob + " "


		}
		//Response.Write( sql)
		rowsA = runSQL(sql,cAcc,"fulltable");

		var myJSON = new JSON();
		data = myJSON.toJSON(null, rowsA, false); 
		fileds = myJSON.generateFields(rowsA)
		Response.Write( '{ "metaData": {	"totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fileds + '}, "total":"' + rowsA.length + '","results":' + data + '}');
	}
}catch (err) {
  Response.Write('Error: ' + err.message);
  logData('cob/compareChart.asp: ' + err.message);
}

%>