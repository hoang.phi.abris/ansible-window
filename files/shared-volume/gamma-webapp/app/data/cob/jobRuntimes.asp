
<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

<!--#include file="../include/JSON.asp"-->
<!--#include file="../include/get_db_data.asp"-->
<!--#include file="../security/security.asp"-->

<%
try{
if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_COBAN_VIEW, env)) {

	//get the appropiate parts of the request string for this type
	var jobid	= String(Request("jobid"));
	var start = parseFloat(isset(Request("start")) ? Request("start") : "0")
	var pageSize = parseFloat(isset(Request("pageSize")) ? Request("pageSize") : "0")  



	//sql = "SELECT jt.*, dbo.GetJobNote(jt.job_azon) as jobnote, b.stage, b.batch_name, b.job_order, b.job_name, note.text, note.azon as 'nazon', note.gamma FROM JOB_TIMES jt inner join batches b on jt.job_azon = b.azon inner join cob on jt.cob_azon = cob.azon left join note on jt.job_azon = note.job_azon and jt.cob_azon = note.cob_azon  WHERE cob.azon = '" + cobazon + "' and (b.job_name like '%" + jobstr + "%' or b.batch_name like '%" + jobstr + "%' or b.stage like '%" + jobstr + "%')"

	sql  = "SELECT "
	sql += "	jt.* "
	sql += "	,h.value as harddays "
	sql += "	,c.closed_day "
	sql += "	,COB.isLastCOBOnWeek(c.id) cobtype "
	sql += "	,n.text "
	sql += "	,n.gamma "
	sql += "FROM "
	sql += "	cob.jobtimes jt "
	sql += "	inner join COB.cob c on c.id = jt.cob_id "
	sql += "	left join COB.[HardDays] h on c.closed_day = h.dt "
	sql += "	left join COB.note n on jt.job_id = n.job_id and jt.cob_id = n.cob_id  "
	sql += "WHERE " 
	sql += "	jt.job_id = '" + jobid + "' "
	sql += "ORDER by " 
	sql += "	c.closed_day desc " 

	rowsA = runSQL(sql,cAcc,"fulltable");

	ind				= 0;
	pagedRows = [];
	pageSize 	= (pageSize==0) ? rowsA.length : start + pageSize;
	pageSize 	= (pageSize > rowsA.length) ? rowsA.length : pageSize;
	if (rowsA.length >0)
	{
		for(ii=start;ii<pageSize; ii++)
	  {
			pagedRows[ind++] = rowsA[ii];
		}
	}
	else
	{ pagedRows = rowsA;}

	var myJSON = new JSON();//instantiate new json object
	data = myJSON.toJSON(null, pagedRows, false); //encode the data in json format
	fileds = myJSON.generateFields(rowsA)
	Response.Write( '{ "metaData": { start:' + start + ', pageSize:' + pageSize + ',	"totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fileds + '}, "total":"' + rowsA.length + '","results":' + data + '}');
}
}catch (err) {
  Response.Write('Error: ' + err.message);
  logData('cob/jobRuntimes.asp: ' + err.message);
}	
%>
