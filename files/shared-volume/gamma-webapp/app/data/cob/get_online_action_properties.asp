
<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

<!--#include file="../include/JSON.asp"-->
<!--#include file="../include/get_db_data.asp"-->
<!--#include file="../security/security.asp"-->

<%

try{
	var task 	= String(Request("task"));
	var jobid 	= String(Request("job"));
	var bankdate = String(Request("bankdate"));
	var state 	= String(Request("state"));
	var env 	= String(Request("env"));

			
	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_COBAN_VIEW, env)) {        
		var cobid 	= isset(Request("cobid")) ? Request("cobid") : '';
		var jobdes = ""
		vprint(1,"JSON: processing task:"+task+":");


			var pre_config_data = [
			{
				"id"		: "action_name",
				"name"		: "Action name",
				"value"		: "",
				"editor"	: "",
				"group"		: "General property",
				"editable"	: false,
				"status"	: false,
				"renderer"	: ""
			}, {
				"id"		: "avg",
				"name"		: "AVG runtime",
				"value"		: "",
				"editor"	: "",
				"group"		: "General property",
				"editable"	: false,
				"status"	: false,
				"renderer"	: ""
			}, {
				"id"		: "max",
				"name"		: "Max runtime",
				"value"		: "",
				"editor"	: "",
				"group"		: "General property",
				"editable"	: false,
				"status"	: false,
				"renderer"	: ""
			}, {
				"id"		: "min",
				"name"		: "Min runtime",
				"value"		: "",
				"editor"	: "",
				"group"		: "General property",
				"editable"	: false,
				"status"	: false,
				"renderer"	: ""
			}];

		

		var sql = "select CONVERT(varchar, DATEADD(s, AVG(t1.run_time) over (partition by t2.action_name) , 0), 108) avg,  CONVERT(varchar, DATEADD(s,MIN(t1.run_time) over (partition by t2.action_name) , 0), 108) min,  CONVERT(varchar, DATEADD(s,MAX(t1.run_time) over (partition by t2.action_name) , 0), 108) max, t2.action_name from Online.ActionTimes t1 join online.actions t2 on t1.action_id = t2.id where t1.action_id = '"+jobid+"'";
        
        buildPropertyGridElement(runSQL(sql,cAcc,"fulltable"));
		var myJSON = new JSON();
		data 	=  myJSON.toJSON(null, pre_config_data, false); 
		Response.Write( '{ "success": true, "metaData": { "description":"jobdesc", "totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields": [{"name":"id"},{"name":"name"},{"name":"value"},{"name":"editor"},{"name":"group"},{"name":"editable"},{"name":"status"},{"name":"renderer"}]}, "total":"' + pre_config_data.length + '","jobdesc":"' + jobdes + '","results":' + data	 + '}');
		
	}
} catch (err) {
  Response.Write('Error: ' + err.message);
  logData('cob/get_online_action_properties.asp: ' + err.message);
}
	
function buildPropertyGridElement(arr)
{
	for (i=0; i<arr.length;i++)
	{
	
		for (item in arr[i])
		{
			
			for (j=0; j<pre_config_data.length;j++)
			{
				if (pre_config_data[j]["id"] == item)
				{					
					pre_config_data[j]["value"] =  arr[i][item];

				}
			}
			
		}
	}
}


%>
