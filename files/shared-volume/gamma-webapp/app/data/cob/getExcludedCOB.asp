<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
try{
if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_COBAN_VIEW, env)) {	
		//get the appropiate parts of the request string for this type
	
		var action 	= String(Request("action"));
		var cob 	= String(Request("cob"));
		var modified_by = String(Request("modified_by"));
		var success = true;
		var errorText = "";

		if (action=='update')
		{
			sql  = "exec COB.setExcludedCOB @env = '" + env  + "', @closed_day='" + cob  + "', @hidestatistic = 1" 
			rowsA = runSQL(sql,cAcc,"fulltable");
			
				if (rowsA[0].error)
				{
					success = false;
					errorText = rowsA[0].error
				}
				else {
					/*Audit*/
					audit(86,22, cob  )
				}
			

		}	
		if (action=='remove')
		{
			sql  = "exec COB.setExcludedCOB @env = '" + env  + "', @closed_day='" + cob  + "', @hidestatistic = 0" 
			rowsA = runSQL(sql,cAcc,"fulltable");
			try
			{
				if (rowsA[0].error)
				{
					success = false;
					errorText = rowsA[0].error
				}
				else {
					audit(86,23,cob )
				}

			}
			catch (e) {Response.Write(errorText)}
		}						
		
		sql= "COB.getExcludedCOB @env = '" + env  + "'";	
		rowsA = runSQL(sql,cAcc,"fulltable");
		

		
		var myJSON = new JSON();
		data 	=  myJSON.toJSON(null, rowsA, false); 
		fileds = myJSON.generateFields(rowsA)
		Response.Write( '{ "saveSuccess": ' + success + ', "text": "' + errorText + '", "metaData": {	"totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fileds + '}, "total":"' + rowsA.length + '","results":' + data + '}');
		
		//Response.Write( '{ "success" : false}');
	}
} catch (err) {
  Response.Write('Error: ' + err.message);
  logData('cob/getExcludedCOB.asp: ' + err.message);
}

%>