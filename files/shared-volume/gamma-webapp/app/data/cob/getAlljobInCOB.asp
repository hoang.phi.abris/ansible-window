<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->
	<!--#include file="../include/util.asp"-->

	<%
try{
	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_COBAN_VIEW, env)) {
		/* configre extra url request header properties */

		
		var cobid		= String(Request("cobid"));
		var start		= (parseInt(Request("start")) ? ',@start=' + Request("start") : "")
		var limit		= (parseInt(Request("limit")) ? ',@limit=' + Request("limit") : "")
		var minruntime	=  (parseInt(Request("minruntime")) ? ',@minruntime=' + Request("minruntime") : "")
		var filter = isset(Request("filter")) ? String(Request("filter")) : ""  
		var filterstring  = filter.length > 0 ? " ,@filter='" + filter + "'" : ""		
		var exportSwitch = isset(Request("exportSwitch")) ? String(Request("exportSwitch")) : "" 
		var vhtml = String(Request("html"));
		var title = (String(Request("title")) != "undefined")  ? String(Request("title")) : "GAMMAExport";
		if ((exportSwitch) && (exportSwitch!="")){
			setDateFormat()
			//limit = ' ,@limit=20 '
			Response.ContentType = "application/vnd.ms-excel" 
			Response.AddHeader("Content-disposition", "attachment; filename=" + title + ".xls");	
			var respXML = decodeURIComponent(vhtml)
			var respRec =  ''; 
			sql= "exec cob.getAlljobInCOB @cob_id='" + cobid + "'" + start + limit + minruntime + filterstring			
			rowsA = runSQL(sql,cAcc,"fulltable");
						if (rowsA) {
				for (i = 0; i < rowsA.length; i++) {
					respRec +=  '<Row>\n' +  
					'<Cell ss:StyleID="defaultleft"><Data ss:Type="String">' + rowsA[i].stage + '</Data></Cell>\n' +
					'<Cell ss:StyleID="defaultleft"><Data ss:Type="String">' + rowsA[i].batch_name + '</Data></Cell>\n' +
					'<Cell ss:StyleID="defaultcenter"><Data ss:Type="String">' + rowsA[i].job_index + '</Data></Cell>\n' +
					'<Cell ss:StyleID="defaultleft"><Data ss:Type="String">' + rowsA[i].job_name + '</Data></Cell>\n' +
					'<Cell ss:StyleID="defaultcenter"><Data ss:Type="String">' + new Date(rowsA[i].start_time).format("yyyy-mm-dd HH:MM:ss") + '</Data></Cell>\n' +
					'<Cell ss:StyleID="defaultcenter"><Data ss:Type="String">' + new Date(rowsA[i].stop_time).format("yyyy-mm-dd HH:MM:ss") + '</Data></Cell>\n' +
					'<Cell ss:StyleID="greencenter"><Data ss:Type="String">' + secondsToTime(rowsA[i].run_time) + '</Data></Cell>\n' +
					//TODO GAMMA Alert
					'<Cell ss:StyleID="red"><Data ss:Type="String"></Data></Cell>\n' +
					'</Row>\n';
				}
				
			}
			respXML = respXML.replace("<!--INSERTCOUNTHERE-->", (rowsA.length + 4).toString())
			Response.Write(respXML.replace("<!--INSERTHERE-->", respRec))
		}
		else {
			
			sql= "exec cob.getAlljobInCOB @cob_id='" + cobid + "'" + start + limit + minruntime + filterstring
			sqlCount= "exec cob.getAlljobInCOB_count @cob_id='" + cobid + "'" + start + limit + minruntime + filterstring

			rowsA = runSQL(sql,cAcc,"fulltable");
			rowsCount = runSQL(sqlCount,cAcc,"fulltable");
			
			var myJSON = new JSON();
			data 		= myJSON.toJSON(null, rowsA, false); 
				
			Response.Write( '{ "metaData": {	"totalProperty" : "total", 	"root" : "results", "id" : "id"}, "total":"' + rowsCount[0].count + '","results":' + data + '}');
		}
	}
}catch (err) {
  Response.Write('Error: ' + err.message);
  logData('cob/getAlljobInCOB.asp: ' + err.message);
}

function secondsToTime(timeInSeconds) {

	var sec = timeInSeconds
	if (timeInSeconds<0) {
		var d = parseInt(Math.abs(timeInSeconds) / 86400 )										
		sec = (86400*(++d))+timeInSeconds
	}
	
	var hours = Math.floor(sec / (60 * 60));
	var divisor_for_minutes = sec % (60 * 60);
	var minutes = Math.floor(divisor_for_minutes / 60);
	var divisor_for_seconds = divisor_for_minutes % 60;
	var seconds = Math.ceil(divisor_for_seconds);
	if(hours<10) hours = '0'+hours;
	if(minutes<10) minutes = '0'+minutes;
	if(seconds<10) seconds = '0'+seconds;
	if (timeInSeconds<0) {
		return  '-' + d + ' day ' + hours+':'+minutes+':'+seconds;   
	}
	return hours+':'+minutes+':'+seconds;   	
}; 
	
%>