<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../include/util.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
	var tran = '';
	try {
		var action = Request("action");
		var table = Request("table");
		var sProc = String(Request("sProc"));
		var columns = String(Request("columns"));
		var insertProc = String(Request("insertProc"));
		var updateProc = String(Request("updateProc"));
		var deleteProc = String(Request("deleteProc"));
		var where = (String(Request("environment_id")) != "undefined") ? " WHERE environment_id = '" + String(Request("environment_id")) + "'" : "";
		var order = (String(Request("order")) != "undefined") ? " ORDER BY " + String(Request("order")) : "";

		if(CheckDbTableRights(table, action)) {
			if(action == "read") {
				if (sProc != "undefined") {
					var sql= "EXEC " + sProc;
				}
				else {
					var sql= "SELECT " + columns + " FROM " + table + where + order;
				}
				rowsA = runSQL(sql, cAcc, "fulltable");


			var myJSON = new JSON();
			var data = '';
			var fields = '';

			if (rowsA.length) {
				data = myJSON.toJSON(null, rowsA, false);
				fields = myJSON.generateFields(rowsA);
			} else {
				var columnsarray = columns.split(',');
				fields = '[';
				for (var i = 0; i < columnsarray.length; i++) {
					fields += (i ? ',' : '') + '{"name": "' + columnsarray[i] + '"}';
				}
				fields += ']';
				data = '[]';
			}

				Response.Write('{"metaData": {"totalProperty" : "total", "root" : "results", "id" : "id", "fields" : ' + fields + '}, "total":"' + rowsA.length + '", "results":' + data + '}');
			} else {
				var firstComaPos = columns.indexOf(',');
				var idColumn = columns.substr(0, firstComaPos);
				var columnsWithoutID = columns.substr(firstComaPos+1);
				var record = eval('('+getData()+')');

				if (record.data.length == undefined) {
					doOperation(action, record.data, cAcc, idColumn, columnsWithoutID);
				} else {
					tran = beginTransaction(cAcc);
					for (var i = 0; i < record.data.length; i++) {
						doOperation(action, record.data[i], tran, idColumn, columnsWithoutID);
					}
				}
				Response.Write('');
			}
		}
	}
	catch (err) {
		if (tran) {
			rollbackTransaction(tran);
		}
		Response.Write('Error: ' + err.message)
//		Response.Write("({ error: '" + err.message.replace(/'/g, '"') + "'})"); EZ LENNE A HELYES EREDMENY, DE A FELULET MEG ROSSZUL KEZELI, MAJD ILYENRE KELL ALAKITANI
		logData('db_table_store.asp: ' + err.message);
	}

	function doOperation(action, data, conn, idColumn, columnsWithoutID) {

		try {
			var columnsarray = columnsWithoutID.split(',');
			var sql = '';

			if (action == 'create') {

				var values = '';
				for (var i = 0; i < columnsarray.length; i++) {
					var colName = columnsarray[i];
					values += (i ? ', ' : '') + (data[colName.replace(' ','')] == null ? "null" : "'" + data[colName.replace(' ','')] + "'");
				}
				sql = "INSERT INTO " + table + " (" + columnsWithoutID + ")  VALUES(" + values + ")";
				runSQL(sql, conn ,"fulltable");

			} else if (action == 'update') {

				var sets = '';
				for (var i = 0; i < columnsarray.length; i++) {
					var colName = columnsarray[i];
					sets += (i  ? ', ' : '') + colName + " = " + (data[colName.replace(' ','')] == null ? "null" : "'" + data[colName.replace(' ','')] + "'");
				}
				sql = "UPDATE " + table + " SET " + sets + " WHERE " + idColumn + " = '" + data[idColumn] + "'";
				runSQL(sql, conn, "fulltable");

			} else if (action == 'destroy') {

				sql = "DELETE FROM " + table + " WHERE " + idColumn + " = " + data[idColumn];
				runSQL(sql, conn, "cmd");

			}

			return;
		}
		catch (err) {
			Response.Write(sql);
			Response.Write('Error: ' + err.message);
			logData('db_table_store.asp: ' + err.message);
		}
	}

	function getData() {
		var sOut = '';

		if(Request.TotalBytes > 0) {
			var lngBytesCount = Request.TotalBytes;

			var stream = Server.CreateObject("Adodb.Stream");
			stream.type = 1;
			stream.open;
			stream.write(Request.BinaryRead(lngBytesCount));
			stream.position = 0;
			stream.type = 2;
			stream.charset = "iso-8859-1";
			sOut = stream.readtext();
			stream.close;
		}

		return sOut;
	}
%>