<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
try{
	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_COBAN_VIEW, env)) {

		sql = "exec COB.chart_online '" + env + "'"
		rowsA = runSQL(sql,cAcc,"fulltable");

		for ( item in rowsA)
		{
			rowsA[item].closed_day = convertDateToHighChart(rowsA[item].closed_day)
			delete rowsA[item].closed_days;
			delete rowsA[item].text;
			delete rowsA[item].title;
		}

		var myJSON = new JSON();
		data 		= myJSON.toJSON(null, rowsA, false);
		fields 	= myJSON.generateFields(rowsA)
		Response.Write( '{ "metaData": { "totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '", "results":' + data + '  }');
	}
}catch (err) {
  Response.Write('Error: ' + err.message);
  logData('cob/chart_online.asp: ' + err.message);
}

function formatclosed_dayToHighChart(year, month, day)
{

	var tdate = new Date(year, month, day)
	return Date.UTC(tdate.getYear(),tdate.getMonth(),tdate.getDate());
}

%>