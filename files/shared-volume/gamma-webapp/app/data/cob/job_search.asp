<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
try{
	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_COBAN_VIEW, env)) {
		//get the appropiate parts of the request string for this type
		var cobid	= String(Request("cobid"));
		var jobstr	= String(Request("jobstr"));
		var start = parseFloat(isset(Request("start")) ? Request("start") : "0")
		var pageSize = parseFloat(isset(Request("limit")) ? Request("limit") : "0")  

		//sql = "SELECT jt.*, dbo.GetJobNote(jt.job_azon) as jobnote, b.stage, b.batch_name, b.job_order, b.job_name, note.text, note.azon as 'nazon', note.gamma FROM JOB_TIMES jt inner join batches b on jt.job_azon = b.azon inner join cob on jt.cob_azon = cob.azon left join note on jt.job_azon = note.job_azon and jt.cob_azon = note.cob_azon  WHERE cob.azon = '" + cobazon + "' and (b.job_name like '%" + jobstr + "%' or b.batch_name like '%" + jobstr + "%' or b.stage like '%" + jobstr + "%')"

		sql  = "SELECT jt.* "
		sql += "	,cob.GetJobNote(jt.job_id) as jobnote "
		sql += "	,jobs.stage, jobs.batch_name, jobs.job_index, jobs.job_name, note.text, note.id as 'nazon', note.gamma "
		sql += "FROM "
		sql += "	cob.getCobJobs('" + cobid + "')  jt "
		sql += "	inner join COB.getCOBJobsName() jobs on jt.job_id = jobs.id "
		sql += "	left join COB.note note on jt.job_id = note.job_id and note.cob_id = '" + cobid + "' "  
		sql += "WHERE " 
		sql += "	start_time is not null " 
		sql += "	and (jobs.job_name like '%" + jobstr + "%' or jobs.batch_name like '%" + jobstr + "%' or jobs.stage like '%" + jobstr + "%') "
		sql += "ORDER By "
		sql += "	start_time"
		
		rowsA = runSQL(sql,cAcc,"fulltable");

		ind				= 0;
		pagedRows = [];
		pageSize 	= (pageSize==0) ? rowsA.length : start + pageSize;
		pageSize 	= (pageSize > rowsA.length) ? rowsA.length : pageSize;
		if (rowsA.length >0)
		{
		//Response.Write(start + "<br>")
			for(ii=start;ii<pageSize; ii++)
		  {
				pagedRows[ind++] = rowsA[ii];
			}
		}
		else
		{ pagedRows = rowsA;}

		var myJSON = new JSON();//instantiate new json object
		data = myJSON.toJSON(null, pagedRows, false); //encode the data in json format
		fileds = myJSON.generateFields(rowsA)
		Response.Write( '{"metaData": { start:' + start + ', pageSize:' + pageSize + ',	"totalProperty" : "total", 	"root" : "results", "id" : "id"}, "total":"' + rowsA.length + '","results":' + data + '}');
	}
}catch (err) {
  Response.Write('Error: ' + err.message);
  logData('cob/job_search.asp: ' + err.message);
}	
%>