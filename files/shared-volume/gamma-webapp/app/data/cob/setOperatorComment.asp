<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_COBAN_VIEW, env)) 
{
	try 
	{
		var jobid	= String(Request("jobid"));
		var cobid 	= String(Request("cobid"));
		var vtext	= String(Request("text"));
		var todo	= String(Request("saverecord"));
		var stage	= String(Request("stage"));
		var env	    = String(Request("env"));

		//get the appropiate parts of the request string for this type

		if(stage == "ON" || stage == "ONLINE"){
			if (todo=="jobtext")
			{
				sql= "exec Online.setOperatorCommentForJob '" + jobid + "', '" + vtext + "'";		
			}
			if (todo=="text")
			{
				sql= "exec Online.setOperatorCommentForJobAtCOB '" + cobid + "', '" + jobid + "', '" + env + "', '" + vtext + "'";
			}
		}
		else{
			if (todo=="jobtext")
			{
				sql= "exec COB.setOperatorCommentForJob '" + jobid + "', '" + vtext + "'";		
			}
			if (todo=="text")
			{
				sql= "exec COB.setOperatorCommentForJobAtCOB '" + cobid + "', '" + jobid + "','" + vtext + "'";
			}
		}
	
        rowsA = runSQL(sql,cAcc,"cmd");
		Response.Write( '{ "success" : true}');
	}
	catch (err) {
		logData('cob/setOperatorComment.asp: ' + err.message);
		Response.Write('{success:false,error:"SendCommand(' + modified_by + ',' + env + ', ' + host +',26, '+ parameter + ',' + inputter + ') ::' + err.message + '"}');  
	}
}
%>