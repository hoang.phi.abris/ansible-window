<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>


	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
try{
	var env			= String(Request("destenv"));
	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_COBAN_VIEW, env) ) {
		//get the appropiate parts of the request string for this type
		var cobid		= String(Request("cobid"));
		var fromdate	= String(Request("fromdate"));
		var enddate		= String(Request("enddate"));	
		var closed_day	= String(Request("cobdate"));
		var harddays	= String(Request("harddays"));
		var hconfig		= String(Request("hconfig"));
		var weekendstr  = String(Request("weekend"));
		var islastcobstr  = String(Request("cobtype"));
		var weekend = 1
		var islastcob=0
		if (hconfig=='any')
		{
			if (harddays>0)
			{
					harddays = harddays *- 1
			}
		}

		if (hconfig=='none')
		{
			harddays = null
		}


		if (weekendstr=='false'){
			weekend = 0
		}
		if (islastcobstr=='true'){
			islastcob = 1
		}
		


		//sql =  "exec COB.compareStages @cob_id='" + cobid + "', @fromdatum = '" + fromdate + "', @enddatum = '" + enddate + "', @env = '" + denv + "', @minimalrun_time = 5 " 

		sql  = "exec [COB].[compareJOB_destination] "
		sql += " 	 @env = '" + env + "'"	
		sql += "	,@fromdate = '" + fromdate + "'"
		sql += "	,@enddate = '" + enddate + "'"
		sql += "	,@harddays = " + harddays 
		sql += "	,@ftype = '" + hconfig + "'"
		sql += "	,@weekend = " + weekend + " " 
		sql += "	,@islastcob = " + islastcob + " "

		//sql =  "exec COB.compareJobs @cob_id='" + cobid + "', @fromdatum = '" + datefrom + "', @enddatum = '" + dateto + "', @env = '" + denv + "', @minimalrun_time = 5 " 

		//Response.Write(sql)
		rowsA = runSQL(sql,cAcc,"fulltable");
		var myJSON = new JSON();
		data = myJSON.toJSON(null, rowsA, false); 
		fileds = myJSON.generateFields(rowsA)
		Response.Write( '{ "metaData": {	"totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fileds + '}, "total":"' + rowsA.length + '","results":' + data + '}');
	}
}catch (err) {
  Response.Write('Error: ' + err.message);
  logData('cob/compareJOB_destination.asp: ' + err.message);
}

%>