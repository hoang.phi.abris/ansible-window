<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
try{
if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_COBAN_VIEW, env)) {
	var cobid = String(Request("cobid"));
	var environment = String(Request("env")); 
	var chart = String(Request("chart")); 
	var type = String(Request("type")); 
	//var job_id = String(Request("job_id")); 
	var year = String(Request("year")); 
	var month = String(Request("month")); 
	var job_name = String(Request("job_name")); 
	if (type == "monthly") {			
		var physicalPath = Server.MapPath("../../../reports") + "\\" +  environment  + "\\" + type
		var objWShell = new ActiveXObject("WScript.shell");
		var nextOK = true;
		phantomjs = Server.MapPath("/export/phantomjs/bin")
		
		function sleep() {
  		var e = new Date().getTime() + (25);
  		while (new Date().getTime() <= e) {}
		}

		try {
		var objCmd = objWShell.Exec("cmd.exe /c mkdir " + physicalPath);	
		sleep()
		objWShell.CurrentDirectory = physicalPath;
		}
		catch(err) {
			Response.Write('{"success":false, "error":"Failed create ' + physicalPath + ' folder for image export!"}')
			nextOK = false;
		}

		if (nextOK == true) {			
			sql = "exec [COB].[getJOBBaseLine] @env='" + environment + "'"		
			jobsA = runSQL(sql,cAcc,"fulltable");
			for (ind=0; ind<jobsA.length;ind++)	{
				callJobReport(environment,jobsA[ind].job_id, jobsA[ind].batch_name + " " + jobsA[ind].job_name, jobsA[ind].job_id) 
			}
			callJobReport(environment,'COB', "T24 COB runtime", "COB") 
		}
		if (nextOK == true) {
			Response.Write('{"success":true}')
		}		
	}
	else
		Response.Write('{"success":true}')		
}
}catch (err) {
  Response.Write('Error: ' + err.message);
  logData('cob/report_createPicture.asp: ' + err.message);
}	

function callJobReport(env,jobid,title,fileName) {

	if (jobid=='COB')
		sql = "exec [COB].[job_report_monthly] @env = '" + env + "', @year=" + year + ", @month=" + month
	else
		sql = "exec [COB].[job_report_monthly] @env = '" + env + "', @job_id='" + jobid + "', @year=" + year + ", @month=" + month
	//fileName = fileName.replace("/", "-");
	fileName = fileName.replace(/({|})/g, "")
	//Response.Write(sql)
	rowsA = runSQL(sql,cAcc,"fulltable");
	var closed_day = []
	var run_time = []
	var base_line = []
	var romai = [".I",".II",".III",".IV",".V",".VI",".VII",".VIII",".IX",".X",".XI",".XII"]
	var avg_run_time = 0
	for (i=0; i<rowsA.length;i++)	{		
		var tdate = new Date(convertDateToHighChart(rowsA[i].closed_day))			
		closed_day.push(tdate.getDate() + romai[tdate.getMonth()])
		run_time.push(rowsA[i].run_time)
		avg_run_time = rowsA[i].avg_run_time
		if (rowsA[i].baseline)
			base_line.push((rowsA[i].baseline * 60 ))
	}
	
	var myJSON = new JSON();
	xAxis = myJSON.toJSON(null, closed_day, false); 
	data = myJSON.toJSON(null, run_time, false); 
	baseline = myJSON.toJSON(null, base_line, false); 
	xAxis = xAxis.replace(/"/g, "'")
	data = data.replace(/"/g, "'")
	baseline = baseline.replace(/"/g, "'")

	//phantomjs.exe demo.js xAxis data avg_run_time title filename
	if (jobid=='COB')
		command = "cmd.exe /c " + phantomjs + "/phantomjs.exe " + phantomjs + "/cob.js " + xAxis + " " + data + " " + " " + baseline + " " + avg_run_time + " \"" + title + "\" \""  + month + "." + fileName + "\""
	else
		command = "cmd.exe /c " + phantomjs + "/phantomjs.exe " + phantomjs + "/job.js " + xAxis + " " + data + " " + avg_run_time + " \"" + title + "\" \""  + month + "." + fileName + "\""
	//Response.Write(command)

	try {
		var objCmd    = objWShell.Exec(command)		
	}
	catch(err) {
		Response.Write("{success:false, error:'Image export failed to created folder'}")
		nextOK = false;
	}
	errorString = objCmd.StdErr.Readall();
	if (errorString != '') {
		nextOK = false;
		Response.Write("{success:false, error:'Image export failed to created folder'}")
		//Response.Write("Error: " + objCmd.StdErr.Readall() + "<br>StdOut: "  + objCmd.StdOut.Readall())	
	}
	var objCmd = null

}
	
%>