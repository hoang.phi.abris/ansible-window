<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
try{
if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_COBAN_VIEW, env)) {

	var cobid 	= String(Request("cobid"));
	var cobdate 	= String(Request("cobdate"));
	sql= "exec COB.gantChart @cob_id='" + cobid +"'";	

	rowsA = runSQL(sql,cAcc,"fulltable");
	
	if (rowsA[0].error == "ERROR")	{
		Response.Write( '({ "success":"false"})')
	}
	else{
			
		var myJSON = new JSON();
					
		var stageName = {}
		var stage = {}
		for (i=0; i<rowsA.length;i++)	{			
			if (!stageName[rowsA[i].stagename]) {
				stageName[rowsA[i].stagename] = {						
					Name		: rowsA[i].stagename
					,id			: rowsA[i].stagename
					,StartDate	: ""
					,EndDate	: ""
					,PercentDone: "100"
					,run_time	: "0"
					,cls		: "COBPart"
					,children	: []
				}
			}
			if (!stage[rowsA[i].stagename + rowsA[i].stage]) {
				stage[rowsA[i].stagename + rowsA[i].stage] = {						
					Name		:rowsA[i].stage
					,id			: rowsA[i].stagename + rowsA[i].stage
					,StartDate	: ""
					,EndDate	: ""
					,PercentDone: "100"
					,run_time	: "0"
					,cls		: "stage"
					,children	: []
				}
			}				
		}
						
		var batch = {}
		for (i=0; i<rowsA.length;i++)	{
			
			var avgs = []
			if (rowsA[i].avg_run_time)
				avgs = rowsA[i].avg_run_time.split(';')	
			if (!batch[rowsA[i].stagename+rowsA[i].stage+rowsA[i].batch_name]) {
				batch[rowsA[i].stagename + rowsA[i].stage + rowsA[i].batch_name] = {						
					Name		: rowsA[i].batch_name
					,id			: rowsA[i].stagename + rowsA[i].stage + rowsA[i].batch_name
					,stage		: rowsA[i].stage
					,StartDate	: ""
					,EndDate	: ""
					,PercentDone: "100"
					,run_time	: "0"
					,cls		: "batch"
					,children	: [{
						Name		: rowsA[i].job_name
						,id 		:rowsA[i].stagename + rowsA[i].stage + rowsA[i].batch_name + rowsA[i].job_name + rowsA[i].job_index
						,StartDate	: rowsA[i].start_time
						,EndDate	: rowsA[i].stop_time
						,BaselineStartDate :  avgs[0]
						,BaselineEndDate	:  avgs[1]
						//,BaselineStartDate :  rowsA[i].start_time
						//,BaselineEndDate	:  rowsA[i].stop_time
						
						,PercentDone: "100"
						,run_time	: rowsA[i].run_time
						,leaf		: "true"
						,cls		: "job"
						,orig_start_time : rowsA[i].orig_start_time
						,orig_stop_time	: rowsA[i].orig_stop_time

					}]
				}
			}
			else {
				batch[rowsA[i].stagename + rowsA[i].stage + rowsA[i].batch_name].children.push({
					Name		: rowsA[i].job_name
					,id 		:rowsA[i].stagename + rowsA[i].stage + rowsA[i].batch_name + rowsA[i].job_name + rowsA[i].job_index
					,StartDate	: rowsA[i].start_time
					,EndDate	: rowsA[i].stop_time
					,BaselineStartDate :  avgs[0]
					,BaselineEndDate	:  avgs[1]
					//,BaselineStartDate :  rowsA[i].start_time
					//,BaselineEndDate	:  rowsA[i].stop_time

					,PercentDone: "100"
					,run_time	: rowsA[i].run_time
					,leaf		: "true"
					,cls		: "job"
					,orig_start_time : rowsA[i].orig_start_time
					,orig_stop_time	: rowsA[i].orig_stop_time
				})
			}
		}
			
		batch = calculatetimes( batch)	
		for (var s in stage) {
			for (var b in batch) {
				 if (b.indexOf(s) == 0){
					stage[s].children.push(batch[b])
				 }										
			}		
		}

		stage = calculatetimes( stage)
		for (var sn in stageName) {
			for (var s in stage) {
				 if (s.indexOf(sn) == 0){
					stageName[sn].children.push(stage[s])
				 }										
			}		
		}
			
		stageName = calculatetimes( stageName)
		var id = 1
		var result = {
			 Id 		: "root"
			,Name 		: "Root"
			,PercentDone: 100
			,StartDate	: ""
			,EndDate	: ""
			,children	: [{
		
			 Id 		: id++
			,Name 		: "COB"
			,cls 		: "COB"
			,PercentDone: 100
			,StartDate	: ""
			,EndDate	: ""
			,children	: []
		}]
		}
		
		for (var sn in stageName) {
			result.children[0].children.push(stageName[sn])
		}
		result.children[0] = calculatetimes( result.children[0])			
		data 	=  myJSON.toJSON(null, result, false); 
	}
   Response.Write(data);
}
}catch (err) {
  Response.Write('Error: ' + err.message);
  logData('cob/gantchart.asp: ' + err.message);
}


function calculatetimes( object) {
	if (!object.Name) {
		for (var key in object) {	
			object[key] = doitCalculateTimes(object[key])
		}
	}
	else {
		object = doitCalculateTimes( object)
	}
	return object
}

function doitCalculateTimes( obj) {
	var start = new Date(obj.children[0].StartDate)
	var stop = new Date(obj.children[0].EndDate)
	var orig_start = new Date(obj.children[0].orig_start_time)
	var orig_stop = new Date(obj.children[0].orig_stop_time)
	var bstart = new Date(obj.children[0].BaselineStartDate)
	var bstop = new Date(obj.children[0].BaselineEndDate)

	for (var child in obj.children) {
		start = (start > new Date(obj.children[child].StartDate)) ? new Date(obj.children[child].StartDate) : start
		stop = (stop < new Date(obj.children[child].EndDate)) ? new Date(obj.children[child].EndDate) : stop
		orig_start = (orig_start > new Date(obj.children[child].orig_start_time)) ? new Date(obj.children[child].orig_start_time) : orig_start
		orig_stop = (orig_stop < new Date(obj.children[child].orig_stop_time)) ? new Date(obj.children[child].orig_stop_time) : orig_stop
		bstart = (bstart > new Date(obj.children[child].BaselineStartDate)) ? new Date(obj.children[child].BaselineStartDate) : bstart
		bstop = (bstop < new Date(obj.children[child].BaselineEndDate)) ? new Date(obj.children[child].BaselineEndDate) : bstop

	}
	obj.StartDate		= start.toString()
	obj.EndDate			= stop.toString()
	obj.BaselineStartDate = bstart.toString()
	obj.BaselineEndDate		= bstop.toString()
	obj.orig_start_time	= orig_start.toString()
	obj.orig_stop_time	= orig_stop.toString()				
	obj.run_time = (orig_stop - orig_start) / 1000;
	return obj		
}	

%>