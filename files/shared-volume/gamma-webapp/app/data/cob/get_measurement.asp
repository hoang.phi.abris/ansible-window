<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
try {
	var env = String(Request("env"));

	if(CheckRight(SEC_ROLE_LOGIN, null)) {
	
		/* configre extra url request header properties */
		var thetime = String(Request("thetime"));
		var observer = String(Request("observer"));
		var id = String(Request("id"));
		var caller = String(Request("caller"));
		var filterstr = "";
		if (caller == 'alerts')
		{
			filterstr = " and severity <> 'normal'";
		}
		
		var observerfilter = "";
		var renderSeverity = "renderSeverity";
		var cm = observer.replace(/\./g, "");
		columnsSql = "select * from Configuration.[Observer.Columns] where observer = '" + observer + "' order by sort";
		rowsA = runSQL(columnsSql,cAcc,"fulltable");
		var myJSON = new JSON();
		for (i=0; i<rowsA.length;i++)
		{
			rowsA[i].renderer = '"' + rowsA[i].renderer + '"';
			rowsA[i].id = env + "-" + caller + "-"  + rowsA[i].id;
		}
		columns = myJSON.toJSON(null, rowsA, false);
		Response.Write(buildResponse());
	}
} catch (err)
{
	logData('measurement/get_measurement.asp: ' + err.message);
	Response.Write("{success:false,error:'" + err.message + "'}");
}

function buildResponse()
{
	sql= "SELECT * from observer.[" + observer + "] where measurement_id = '" + id + "' " + filterstr;
	
	rowsA = runSQL(sql,cAcc,"fulltable");
	var myJSON = new JSON(); // instantiate new json object
	data = myJSON.toJSON(null, rowsA, false); // encode the data in json format
	fields 	= myJSON.generateFields(rowsA);

	if (rowsA.length == 0)
	{
		sql= "SELECT 'critical' as 'severity', *  from [ERROR] where measurement_id = '" + id + "'";
		rowsA = runSQL(sql,cAcc,"fulltable");

	if (rowsA.length != 0)
		{


		for ( item in rowsA) {
			rowsA[item].value = '"' + rowsA[item].value + '"';
		}
		var myJSON = new JSON(); // instantiate new json object
		data = myJSON.toJSON(null, rowsA, false);
		fields = myJSON.generateFields(rowsA);
		
		return '{'+
			'"success": true,'+
			'"metaData": {'+
				'"columns": ['+
					'{'+
						'"dataIndex": "severity",'+
						'"header": "",'+
						'"menuDisabled": true,'+
						'"width": 30,'+
						'"renderer": "renderSeverity"'+
					'},'+
					'{'+
						'"dataIndex": "key",'+
						'"header": "Key",'+
						'"menuDisabled": true,'+
						'"width": 120'+
					'},'+
					'{'+
						'"dataIndex": "value",'+
						'"header": "Value",'+
						'"width": 520,'+
						'"menuDisabled": true,'+
						'"flex": 1'+
					'}'+
				'],'+
				'"totalProperty": "total",'+
				'"root": "results",'+
				'"id": "id",'+
				'"fields": ['+
					'{'+
						'"name": "key"'+
					'},'+
					'{'+
						'"name": "severity"'+
					'},'+
					'{'+
						'"name": "value"'+
					'}'+
				']'+
			'},'+
			'"total": "' + rowsA.length + '",'+
			'"results": ' + data + ''+
		'}';
		}else
		{

		return '{'+
				'"success": true,'+
				'"metaData": {'+
					'"columns": ['+
						'{'+
							'"dataIndex": "severity",'+
							'"header": "",'+
							'"sortable": false,'+
							'"menuDisabled": true,'+
							'"width": 30,'+
							'"renderer": "renderSeverity"'+
						'},'+
						'{'+
							'"dataIndex": "value",'+
							'"header": "Value",'+
							'"menuDisabled": true,'+
							'"sortable": true,'+
							'"width": 520,'+
							'"flex": 1'+
						'}'+
					'],'+
					'"totalProperty": "total",'+
					'"root": "results",'+
					'"id": "id",'+
					'"fields": ['+
						'{'+
							'"name": "severity"'+
						'},'+
						'{'+
							'"name": "value"'+
						'}'+
					']'+
				'},'+
				'"total": "' + 1 + '",'+
				'"results":   [{"severity": "warning","value": "ERROR : Cannot read the measurement details from the database!"}] '+
			'}';
		}
	}
	else
	{
		return '{'+
			'"success": true,'+
			'"metaData": {'+
				'"columns": ' + columns + ','+
				'"totalProperty": "total",'+
				'"root": "results",'+
				'"id": "id",'+
				'"fields": ' + fields + ''+
			'},'+
			'"total": "' + rowsA.length + '",'+
			'"results": ' + data + ''+
		'}';
	}
}

%>