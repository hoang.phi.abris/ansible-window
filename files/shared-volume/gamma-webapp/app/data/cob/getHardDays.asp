<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
try{
if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_COBAN_VIEW, env)) {

	//get the appropiate parts of the request string for this type
	var environment_id 	= String(Request("environment_id"));
	var day 	= String(Request("day"));
	var action 	= String(Request("action"));
	var value 	= String(Request("value"));
	var name 	= String(Request("name"));
	var icon 	= String(Request("icon"));
	var description 	= String(Request("description"));
	var modified_by = String(Request("modified_by"));
	var record = eval('('+getData()+')');		

	if (action=='create')
	{
		sql  = "exec COB.setHardDays @value=0 , @name='" + record.data.name + "', @icon='" + record.data.icon + "', @description='" + record.data.description + "'"
		rowsA = runSQL(sql,cAcc,"fulltable");
		if (rowsA[0].error != "ERROR") {
			/*Audit*/
			audit(86,3,"new hardday type: " + record.data.name )
			Response.Write( '({ "success":"true","action":"' + action +'"})')
		}
	}
		
	if (action=='destroy')
	{
		sql  = "exec COB.setHardDays " + value + ", 'COB-HARDDAY-DELETE', '" + icon + "', '" + description + "'"
		rowsA = runSQL(sql,cAcc,"fulltable");
		if (rowsA[0].error == "ERROR") 	
			Response.Write( '({ "success":"false","action":"' + action +'"})')
		else {
			/*Audit*/
			audit(86,3,"del hardday type:" + record.data.name )
			Response.Write( '({ "success":"true","action":"' + action +'"})')
		}
	}
	if (action=='update') {		
	
		if (record.data.length == undefined) {
			sql  = "exec COB.setHardDays @value=" + record.data.value + ", @name='" + record.data.name + "', @icon='" + record.data.icon + "', @description='" + record.data.description + "'"
			rowsA = runSQL(sql,cAcc,"fulltable");
		}
		else {
			for(var i = 0; i < record.data.length; i++) {
				sql  = "exec COB.setHardDays @value=" + record.data[i].value + ", @name='" + record.data[i].name + "', @icon='" + record.data[i].icon + "', @description='" + record.data[i].description + "'"
				rowsA = runSQL(sql,cAcc,"fulltable");
			}
		}

		if (rowsA[0].error != "ERROR") {
			/*Audit*/
			audit(86,3,"update hardday type: " + record.data.name )

			Response.Write( '({ "success":"true","action":"' + action +'"})')
		}
	}
	if (action=='read') {	
		if (day != 'undefined' && environment_id !='undefined')
			sql= "exec COB.getHardDays '" + day + "', "+environment_id;	
		else
			sql= "exec COB.getHardDays";	
		
		rowsA = runSQL(sql,cAcc,"fulltable");
		if (rowsA[0].error == "ERROR") 	{
			Response.Write( '{ "success":"false","action":"' + action +'"}')
		}
		else {
			var myJSON = new JSON();
			data 	=  myJSON.toJSON(null, rowsA, false); 
			fileds = myJSON.generateFields(rowsA)
			Response.Write( '{ "metaData": {	"totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fileds + '}, "total":"' + rowsA.length + '","results":' + data + '}');
		}
	}
}
}catch (err) {
  Response.Write('Error: ' + err.message);
  logData('cob/getHarDays.asp: ' + err.message);
}

function getData() {
 var sOut = null
 if(Request.TotalBytes > 0){
    var lngBytesCount = Request.TotalBytes

    var stream = Server.CreateObject("Adodb.Stream")
    stream.type = 1
    stream.open
    stream.write(Request.BinaryRead(lngBytesCount))
    stream.position = 0
    stream.type = 2
    stream.charset = "iso-8859-1"
    sOut = stream.readtext()
    stream.close
  }
  return sOut;
}

%>