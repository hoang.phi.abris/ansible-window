
<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

<!--#include file="../include/JSON.asp"-->
<!--#include file="../include/get_db_data.asp"-->
<!--#include file="../security/security.asp"-->

<%


function CheckRightNoWrite(right, environment) 
{
	if (Session("username") == undefined || Session("username") == "")
	{
		var path = String(Request.ServerVariables ("PATH_INFO")).split("/app/data/")[0];
		Server.Execute(path+"/app/data/security/get_logged_in_user.asp")
		Response.Clear();
		if (Session("username") != "") {
			Server.Execute(path+"/app/data/security/get_security_roles.asp")
			Response.Clear();
		}
	}
	
	var id = right + "-" + environment;
	var res = (Session(id) == "ok");
	return res;
}
try{
var configuration = [];
	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_COBAN_VIEW, env)) {
		//get the appropiate parts of the request string for this type
		sql= "select top 10 COB.islastcobonweek(id) as islastcobonweek,COB.getDateString(closed_day) as 'closed_day', id as 'cob_azon'  from cob.cob where environment = '" + env + "' order by closed_day desc" 
		
		observerSql  = 'SELECT '
		observerSql += '	POWER (CAST(2 AS BIGINT), CAST((ROW_NUMBER() OVER(ORDER BY id)-1) AS BIGINT)) as id '
		observerSql += '	,name '
		observerSql += '	,display_name as title '
		observerSql += 'FROM '
		observerSql += '	[Configuration].ObserverClass '
		observerSql += 'WHERE is_service = 0 '
		
		rowsA = runSQL(sql,cAcc,"fulltable");
		
		var myJSON = new JSON();//instantiate new json object
		var myJSON2 = new JSON();//instantiate new json object

		
		
		data = myJSON.toJSON(null, rowsA, false); //encode the data in json format
		
		
		securityRoles = CheckRightNoWrite(SEC_ROLE_COBAN_CONFIG, env)
		
		fileds = myJSON.generateFields(rowsA)
		
		rowsB = runSQL(observerSql,cAcc,"fulltable");
		observers = myJSON2.toJSON(null, rowsB, false); //encode the data in json format
		
		
		

		ConfigurationSql  = "exec COB.[configGlobal] @env = '" + env + "'"

		rowsC = runSQL(ConfigurationSql,cAcc,"fulltable");
		configuration = myJSON2.toJSON(null, rowsC, false); //encode the data in json format
		
		Response.Write( '{ "metaData": {	"totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fileds + '}, "total":"' + rowsA.length + '","results":' + data + ',"observers":' + observers + ',"securityRoles":' + securityRoles + ',"configuration":' + configuration + '}');
	}
}catch (err) {
  Response.Write('Error: ' + err.message);
  logData('cob/getOnlineJOBRuntimes.asp: ' + err.message);
}	
%>
