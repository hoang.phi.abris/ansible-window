<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
try{

		/*Audit*/
/*		var log_type = 85
		var auditSql = "INSERT INTO Audit.LogDetails ([user], log_type, timestamp, env, param1, param2) VALUES('" + modified_by + "', " + log_type + " ,GETDATE(),'" + env + "', 4, 'new Special job:" + job_name +"')";
		runSQL(auditSql,cAcc,"cmd");

*/

	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_COBAN_VIEW, env)) {
		//get the appropiate parts of the request string for this type
		
		
		var day 	= String(Request("day"));
		var action 	= String(Request("action"));
		var value 	= String(Request("value"));
		var name 	= String(Request("name"));
		var icon 	= String(Request("icon"));
		var baseline= String(Request("baseline"));

		var record = eval('('+getData()+')');		
		
		var auditText = '';
		if (action=='update') {		
		
			if (record.data.length == undefined) {
				sql  = "exec COB.setCOBBaseLine @env=" + env + ", @baseline=" + record.data.baseline + ", @hardday_id=" + record.data.value 
				auditText = record.data.name + ' set to ' + record.data.baseline				
				rowsA = runSQL(sql,cAcc,"fulltable");
				
			}
			else {
				for(var i = 0; i < record.data.length; i++) {
					sql  = "exec COB.setCOBBaseLine @env=" + env + ", @baseline=" + record.data[i].baseline + ", @hardday_id=" + record.data[i].value
					auditText = record.data[0].name + ' set to ' + record.data[0].baseline
					rowsA = runSQL(sql,cAcc,"fulltable");
				}
			}

			if (rowsA[0]) {
				if (rowsA[0].error != "ERROR") {
					audit(86,5,auditText )
					Response.Write( '({ "success":"true"})')
				}
				else 
					Response.Write( '({ "success":"false"})')
			}
			else
				Response.Write( '({ "success":"false"})')
				
		}
		if (action=='read') {	
			sql= "exec COB.getCOBBaseLine @env='" + env + "'";	
			rowsA = runSQL(sql,cAcc,"fulltable");
			if (rowsA[0].error == "ERROR") 	{
				Response.Write( '({ "success":"false"})')
			}
			else {
				var myJSON = new JSON();
				data 	=  myJSON.toJSON(null, rowsA, false); 
				fileds = myJSON.generateFields(rowsA)
				Response.Write( '{ "metaData": {	"totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fileds + '}, "total":"' + rowsA.length + '","results":' + data + '}');
			}
		}
	}
}catch (err) {
  Response.Write('Error: ' + err.message);
  logData('cob/getCOBBaseLine.asp: ' + err.message);
}


function getData() {
 var sOut = null
 if(Request.TotalBytes > 0){
    var lngBytesCount = Request.TotalBytes

    var stream = Server.CreateObject("Adodb.Stream")
    stream.type = 1
    stream.open
    stream.write(Request.BinaryRead(lngBytesCount))
    stream.position = 0
    stream.type = 2
    stream.charset = "iso-8859-1"
    sOut = stream.readtext()
    stream.close
  }
  return sOut;
}

%>