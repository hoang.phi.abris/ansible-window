<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
try{
//	if(CheckRight(SEC_ROLE_LOGIN, null) && FindRights([SEC_ROLE_COBAN_VIEW, SEC_ROLE_COBAN_CHARTS, SEC_ROLE_PORTAL_MANAGEMENT])) {
if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_COBAN_VIEW, env)) {	
		
	/* configre extra url request header properties */
	 
	sql = "SELECT top 1 "
	sql+= "		closed_day, "
	sql+= "		application_run_time, "
	sql+= "		online_run_time, "
	sql+= "		report_run_time, "
	sql+= "		system_run_time, "
	sql+= "		startofday_run_time " 
	sql+= "	FROM " 
	sql+= "		COB.cob "
	sql+= "WHERE "
	sql+= "		environment = '" + env + "' "
	sql+= "ORDER by "
	sql+= "		closed_day desc "

	var fields = []
	var data = []
	var postRows = []
	var closed_day = ""
	
	rowsA = runSQL(sql,cAcc,"fulltable");
	postRows.push({'stage':'application','runtime':rowsA[0].application_run_time})
	postRows.push({'stage':'system','runtime':rowsA[0].system_run_time})
	postRows.push({'stage':'reporting','runtime':rowsA[0].report_run_time})
	postRows.push({'stage':'startofday','runtime':rowsA[0].startofday_run_time })
	postRows.push({'stage':'online','runtime':rowsA[0].online_run_time})

	closed_day = rowsA[0].closed_day

	var myJSON = new JSON();
	data 		= myJSON.toJSON(null, postRows, false); 
	fields 	= myJSON.generateFields(postRows)

	Response.Write( '{ "metaData": {"totalProperty" : "total", 	"root" : "results", "id" : "id", "closed_day" : "' + closed_day + '", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');
}
}catch (err) {
  Response.Write('Error: ' + err.message);
  logData('cob/cobstageschart.asp: ' + err.message);
}

%>