
<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

<!--#include file="../include/JSON.asp"-->
<!--#include file="../include/get_db_data.asp"-->
<!--#include file="../security/security.asp"-->

<%
try{
	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_COBAN_VIEW, env) ) {
		var cobid = String(Request("cobid"));
		var env = String(Request("env"));
		var fromdate = String(Request("fromdate"));
		var enddate = String(Request("enddate"));
		var harddays = String(Request("harddays"));
		var hconfig  = String(Request("hconfig"));
		var weekendstr  = String(Request("weekend"));
		var weekend = 1
		if (hconfig=='any')
		{
			if (harddays>0)
			{
					harddays = harddays *- 1
			}
		}
		if (hconfig=='none')
		{
			harddays = null
		}
		if (weekendstr=='false')
		{
			weekend = 0
		}

		//get the appropiate parts of the request string for this type


		sql = "exec [COB].[compareListFilteredCOBList] "
		sql += "	 @cob_id = '" + cobid + "'"
		sql += " 	,@env = '" + env + "'"	
		sql += "	,@fromdate = '" + fromdate + "'"
		sql += "	,@enddate = '" + enddate + "'"
		sql += "	,@harddays = " + harddays 
		sql += "	,@ftype = '" + hconfig + "'"
		sql += "	,@weekend = " + weekend + " " 
		 
		//Response.Write( sql)
		rowsA = runSQL(sql,cAcc,"fulltable");
		var myJSON = new JSON();
		data = myJSON.toJSON(null, rowsA, false); 
		fileds = myJSON.generateFields(rowsA)
		Response.Write( '{"metaData": {	"totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fileds + '}, "total":"' + rowsA.length + '","results":' + data + '}');
	}
}catch (err) {
  Response.Write('Error: ' + err.message);
  logData('cob/compareListFilteredCOBList.asp: ' + err.message);
}

	
%>
