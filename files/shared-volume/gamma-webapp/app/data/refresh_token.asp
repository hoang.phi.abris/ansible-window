<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="include/JSON.asp"-->
	<!--#include file="include/get_db_data.asp"-->
	<!--#include file="security/security.asp"-->

	<%
try {
	if(CheckRight(SEC_ROLE_LOGIN, null)) {
		var keepInUse = String(Request("keepInUse"));
		var browser_id = String(Request("browser_id"));
		var keepInUseList = keepInUse.split(String.fromCharCode(252));

		var conn = beginTransaction(cAcc);
		for (var i=0;i<keepInUseList.length-1;i++) {
			var data = keepInUseList[i].split('/');
		
			sql = "UPDATE Configuration.TokenData SET timestamp = GETDATE() WHERE token_type = " + data[0] + " AND sub_type = " + data[1] + " AND browser_id = '" + browser_id + "'";
			runSQL(sql,conn,"cmd");
		}
		commitTransaction(conn);		
	}
} 
catch (err) {
  Response.Write('Error: ' + err.message);
   logData('refresh_token.asp: ' + err.message);
}

%>