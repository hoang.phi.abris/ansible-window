<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="include/JSON.asp"-->
	<!--#include file="include/get_db_data.asp"-->
	<!--#include file="security/security.asp"-->
	<!--#include file="include/util.asp"-->

	<%
try {
	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_ADMIN_WINDOW, null)) {
		
		var sql = String(Request("input"));
		var rowsA = _runSQL_columnIndex(sql, cAcc, "fulltable", true);
		
		var myJSON = new JSON();
		var data = myJSON.toJSON(null, rowsA, false);
		var fields = myJSON.generateFields(rowsA);
		
		Response.Write('{"metaData" : {"totalProperty" : "total", "root" : "results", "fields" : ' + fields + '}, "total" : "' + rowsA.length + '", "results" : ' + data + '}');
	}
	else {
		Response.Write('{"metaData" : {"totalProperty" : "total", "root" : "results"}, "total" : "0", "error" : "You have no rights to execute SQL queries."}');
	}
} 
catch (err) {
	logData('get_query.asp: ' + err.message);
	Response.Write('{"metaData" : {"totalProperty" : "total", "root" : "results"}, "total" : "0", "error" : "' + err.message.replace(/"/g, '\\"') + '"}');
}
%>