<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="include/JSON.asp"-->
	<!--#include file="include/get_db_data.asp"-->
	<!--#include file="include/controller_groups.asp"-->
	<!--#include file="security/security.asp"-->
	<!--#include file="include/json2.asp"-->

	<%

try {
	var environment_id = Number(Request("environment_id"));
	if(CheckRight(SEC_ROLE_LOGIN, null) && FindRights([SEC_ROLE_ENVMAN_CONFIG, SEC_ROLE_ENVMAN_VIEW]))
	{
		var fileName = ''
		var environment = {}
		environment.id = environment_id;

		var sql= "SELECT name, description, multicountry, master_environment_name, t24_version FROM Configuration.Environments WHERE id = " + environment_id;
		var rowsA = runSQL(sql,cAcc,"fulltable");
		if (rowsA.length > 0) {
			environment.name = rowsA[0].name;
			environment.description = rowsA[0].description;
			environment.multicountry = rowsA[0].multicountry;
			environment.master_environment_name = rowsA[0].master_environment_name;
			environment.t24_version = rowsA[0].t24_version;
			var fileName = environment.name + '.gec'
			
			environment.instances = []
			sql= "SELECT id, host FROM Configuration.Instances WHERE environment = '" + environment.name + "'"
			var rowsA = runSQL(sql,cAcc,"fulltable");
			for(var i=0; i<rowsA.length; i++) {
				var instance = {}
				instance.id = rowsA[i].id;
				instance.host = rowsA[i].host;
				
				sql = "SELECT theFile From Configuration.ConfigFiles WHERE host = '" + instance.host + "' AND STATUS = 0 AND filetype_id = 1";
				var rowsB = runSQL(sql,cAcc,"fulltable");
				instance.error = ''
				
				var xmlDoc = Server.CreateObject("MSXML2.DOMDocument");
				xmlDoc.loadXML(rowsB[0].theFile)
				var arrayNodes = ["INSTANCE", "OBSERVER", "BATCH", "EXCEPTION", "PHANTOM", "ALLOWED", "DENIED", "EXPRESSION", "INTERLOCK", "MQ",
				"FILTER", "USER_GROUP", "PROCESS", "APP_LEVEL", "REC_LEVEL", "OFS_PARAM", "TC_ADAPTER", "TSA_AGENT", "PHANTOM", "BUSINESS",
				"STRING", "JOB", "TC_HOST", "LISTENER", "FILE", "CODE", "PRINTER", "FILE_PATH", "SERVLET", "TOCF_HOST", "TOCF_QUEUE", "PARAM",
				"DISK", "EXCLUDE_DISK", "PROG", "SETTING", "BROWSER_SERVER","COMMAND","EXCLUDE_EXP", "SERVICE_TO_MONITOR", "USER", "APPSERVER", "WAS_ITEM", "ATTR",
				"HTTP", "TWS", "TSM"
			];
			
			var json = xml2json(xmlDoc.documentElement, arrayNodes);
			
			for (var j=0; j<json.INSTANCES.INSTANCE.length; j++) {
				if (json.INSTANCES.INSTANCE[j]["@attributes"].id == instance.id) {
					instance.config = json.INSTANCES.INSTANCE[j];
					if (instance.config.USER_GROUP_LIST.USER_GROUP == undefined)
					instance.config.USER_GROUP_LIST.USER_GROUP = []
					if (instance.config.OBSERVER == undefined)
					instance.config.OBSERVER = []
					break;
				}
			}
			
			instance.observers = []
			sql = "SELECT observer_id, obs_cba, isnull(critical_block_alert, '') as critical_block_alert, obs_mail, isnull(mail_subject, '') as mail_subject, obs_sms, isnull(sms_text, '') as sms_text, obs_interface, isnull(interface_text, '') as interface_text, obs_message_group, isnull(message_group, '') as message_group, obs_action FROM Configuration.HostObserver WHERE instance_id = " + instance.id + " AND host = '" + instance.host + "'";
			var rowsB = runSQL(sql,cAcc,"fulltable");
			for(var j=0; j<rowsB.length; j++) {
				var obs = {}
				obs.observer_id = rowsB[j].observer_id;
				obs.obs_cba = rowsB[j].obs_cba;
				obs.critical_block_alert = rowsB[j].critical_block_alert;
				obs.obs_mail = rowsB[j].obs_mail;
				obs.mail_subject = rowsB[j].mail_subject;
				obs.obs_sms = rowsB[j].obs_sms;
				obs.sms_text = rowsB[j].sms_text;
				obs.obs_interface = rowsB[j].obs_interface;
				obs.interface_text = rowsB[j].interface_text;
				obs.obs_message_group = rowsB[j].obs_message_group;
				obs.message_group = rowsB[j].message_group;
				obs.obs_action = rowsB[j].obs_action;
				instance.observers.push(obs)
			}
			
			instance.alertactions = []
			sql = "SELECT observer_id, age, repeat_after, mail_target, mail_to, sms_target, sms_to, int_target, interface FROM Configuration.AlertActions WHERE host = '" + instance.host + "'";
			var rowsB = runSQL(sql,cAcc,"fulltable");
			for(var j=0; j<rowsB.length; j++) {
				var aa = {}
				var found = false;
				for(var k=0; k<instance.observers.length; k++)
				if (instance.observers[k].observer_id == rowsB[j].observer_id) {
					found = true;
					break;
				}
				if (found) {
					aa.observer_id = rowsB[j].observer_id;
					aa.age = rowsB[j].age;
					aa.repeat_after = rowsB[j].repeat_after;
					aa.mail_target = rowsB[j].mail_target;
					aa.int_target = rowsB[j].int_target;
					aa.mail_to = rowsB[j].mail_to;
					aa.sms_target = rowsB[j].sms_target;
					aa.sms_to = rowsB[j].sms_to;
					aa.interface = rowsB[j].interface;
					aa.int_target = rowsB[j].int_target;
					instance.alertactions.push(aa);
				}
			}
			
			sql = "SELECT theFile From Configuration.ConfigFiles WHERE host = '" + instance.host + "' AND STATUS = 0 AND filetype_id = 6";
			var rowsB = runSQL(sql,cAcc,"fulltable");
			
			xmlDoc.loadXML(rowsB[0].theFile)
			arrayNodes = ["GROUP", "STATE"];
			instance.host_ep_spec = {}
			instance.host_ep_spec.EB_PHANTOMS = xml2json(xmlDoc.documentElement, arrayNodes);
			
			sql = "SELECT theFile From Configuration.ConfigFiles WHERE host = '" + instance.host + "' AND STATUS = 0 AND filetype_id = 7";
			var rowsB = runSQL(sql,cAcc,"fulltable");
			xmlDoc.loadXML(rowsB[0].theFile)
			arrayNodes = ["GROUP", "STATE"];
			instance.host_tsa_spec = {}
			instance.host_tsa_spec.TSA_AGENTS = xml2json(xmlDoc.documentElement, arrayNodes);
			
			sql = "SELECT theFile From Configuration.ConfigFiles WHERE host = '" + instance.host + "' AND STATUS = 0 AND filetype_id = 8";
			var rowsB = runSQL(sql,cAcc,"fulltable");
			xmlDoc.loadXML(rowsB[0].theFile)
			arrayNodes = ["SCHEDULER"];
			instance.host_sched_spec = {}
			instance.host_sched_spec.SCHEDULERS = xml2json(xmlDoc.documentElement, arrayNodes);
			
			environment.instances.push(instance);
		}
		
		environment.functions = []
		var sql= "SELECT id, name, position FROM Configuration.ControllerFunction where environment_id = " + environment_id + " ORDER BY position"
		var rowsC = runSQL(sql,cAcc,"fulltable");
		for(var k=0; k<rowsC.length; k++) {
			var funct = {}
			funct.name = rowsC[k].name;
			var fid = rowsC[k].id;
			getIs_cob(funct, rowsC[k].id);
			getGroups(funct, rowsC[k].id);
			getControllerScheds(funct, fid);
			getControllerCommands(funct, fid);
			environment.functions.push(funct);
		}
	}	
	var data = JSON.stringify(environment, function (key, value) {
                return value === null ? "" : value ;
            }, 2);
			
			Response.ContentType = "text/plain";
			Response.AddHeader("Content-disposition", "attachment; filename=" + fileName);
			
			Response.Write(data);
		}
	}
	catch (err) {
		Response.Write('Error: ' + err.message);
		logData('get_environment_config.asp: ' + err.message);
}

function checkIss(issFullArray, issToCheck) {
    for(var i=0; i<issFullArray.length; i++) {
        if(issFullArray[i]==issToCheck) {
            return true;
        }
    }
    return false;
}
function xml2json(currNode, arrayNodes) {
	var json = {}

	if (currNode.nodeType == 1) {
		if (currNode.attributes.length > 0) {
			json["@attributes"] = {}
			for(var i=0; i<currNode.attributes.length; i++) {
				json["@attributes"][currNode.attributes[i].name] = currNode.attributes[i].value
			}
		}
	}
	else if (currNode.nodeType == 3) {
		json = currNode.nodeValue
	}

	for (var i=0; i<currNode.childNodes.length; i++) {
	  var item = currNode.childNodes.item(i);
      var nodeName = item.nodeName;
      if (typeof(json[nodeName]) == "undefined")
      {
        if (checkIss(arrayNodes, nodeName)) {
          json[nodeName] = new Array();
          json[nodeName][0] = xml2json(item, arrayNodes);
        }
        else {
          json[nodeName] = xml2json(item, arrayNodes);
		}
      }
      else
      {
        if (typeof(json[nodeName].length) == "undefined")
        {
          var old = json[nodeName];
          json[nodeName] = new Array();
          json[nodeName][json[nodeName].length] = old;
        }

        json[nodeName][json[nodeName].length] = xml2json(item, arrayNodes);
      }
	}
	return json;
}

%>