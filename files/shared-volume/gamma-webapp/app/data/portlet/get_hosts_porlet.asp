<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->
	<!--#include file="../include/util.asp"-->

	<%
try {
	var obs = String(Request("obs"));
	var env = String(Request("env"));

	if(CheckRight(SEC_ROLE_LOGIN, null)) {

		sql = "exec portlet.getHosts '"+obs+"',  '"+env+"'";

		rowsA = runSQL(sql,cAcc,"fulltable");
		var myJSON = new JSON();
		
		data = myJSON.toJSON(null, rowsA, false); 	
		fields = myJSON.generateFields(rowsA);
		count = rowsA.length;
		Response.Write( '{ "total":"' + count + '", "results":' + data + '}');
	}
} 
catch (err) {
	logData('portlet/get_hosts_portlet.asp: ' + err.message);
  Response.Write('Error: ' + err.message);
}

%>