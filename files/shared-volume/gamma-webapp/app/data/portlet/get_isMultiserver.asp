<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->
	<!--#include file="../include/util.asp"-->

	<%
try {
	var observerName = String(Request("observer"));
	var valueField = String(Request("valueField"));
	var value = String(Request("value"));
	var env = String(Request("env"));
	var timestamp = String(Request("time"));

	if(CheckRight(SEC_ROLE_LOGIN, null)) {
		
		 
		sql = "Select name From [Configuration].[ObserverClass] Where display_name = '" + observerName + "' "
		rowsA = runSQL(sql,cAcc,"fulltable");
		var observer = rowsA[0].name;
		
		sql=" Select TOP 1 measurements.host AS host, "
		sql+="			environment  "
		sql+=" From observer.[" + observer + "] epo, measurements "
		sql+=" Where id = measurement_id "
		sql+="	AND measurements.observer = '" + observer + "' "
		sql+="	AND timestamp = '" + timestamp + "' "
		sql+="	AND environment = '" + env + "' "
		
		if(valueField != null && value != null && valueField != 'undefined' && value != 'undefined')
		{
			sql+="  AND " + valueField + " = " + value
		}
		
		rowsA = runSQL(sql,cAcc,"fulltable");
		var myJSON = new JSON();
		
		data = myJSON.toJSON(null, rowsA, false); 	
		fields = myJSON.generateFields(rowsA);
		count = rowsA.length;
		
		Response.Write('{ "metaData": { "totalProperty" : "total",  "root" : "results", "id" : "id",    "fields" : ' + fields + '}, "total":"' + count + '","results":' + data + '}');
	}
} 
catch (err) {
	logData('data/portlet/get_isMultiserver.asp: ' + err.message );
  Response.Write('Error: ' + err.message+ ' || ' + valueField);
}

%>