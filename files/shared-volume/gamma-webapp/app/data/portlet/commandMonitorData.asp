
<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

<!--#include file="../include/JSON.asp"-->
<!--#include file="../include/get_db_data.asp"-->
<!--#include file="../security/security.asp"-->

<%
try{
	if(CheckRight(SEC_ROLE_LOGIN, null))
	{	
		var commandName = String(Request("command_name"));
		
		sql	 = "select env_name, env_number, command_status, last_mod_date FROM("
		sql += "	select e.name as env_name, e.env_number, e.id as env_id, s.value as command_status, cc.name as command_name, a.id, a.modified_at as last_mod_date, "
		sql += "		ROW_NUMBER() OVER (PARTITION BY e.name ORDER BY a.modified_at DESC, a.id DESC) as date_rank"
		sql += "		from Configuration.ControllerAction a"
		sql += "		inner join Configuration.ControllerGroup g on g.id=a.group_id"
		sql += "		inner join Configuration.ControllerFunction f on f.id=g.function_id"
		sql += "		inner join (select *, ROW_NUMBER() OVER ( ORDER BY id) as env_number from Configuration.Environments) e on e.id=f.environment_id"
		sql += "		inner join  Configuration.ControllerCommand cc "
		sql += "			on (a.run_command=cc.id and (a.host_id = cc.host_id or (a.host_id = 0 and e.default_host = cc.host_id)))"
		sql += "		inner join Param.ActionStatus s on s.value=a.status"
		sql += "		where cc.name like '"+commandName+"') as commands"
		sql += " where date_rank = 1"
		sql += " order by env_number"

		rowsA = runSQL(sql,cAcc,"fulltable");

		var myJSON = new JSON();
		data 		= myJSON.toJSON(null, rowsA, false);
		fileds 	= myJSON.generateFields(rowsA)
		
		Response.Write( '{ "results":' + data + '}');
	}
}
catch (err) {
  Response.Write('Error: ' + err.message);
  logData('commandMonitorData.asp: ' + err.message);
}
%>
