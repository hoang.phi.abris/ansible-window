﻿<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
/* configre extra url request header properties */
try{
if(CheckRight(SEC_ROLE_LOGIN, null))  {
		
	var env = String(Request("env"));
	var start = String(Request("start"));
	var end = String(Request("end"));

	var rowsD = []		
	var start_bankDate = ''
	var stop_bankDate = ''
	var start_timestamp = ''
	var stop_timestamp = ''
	var days = 0
	var wdays = 0
	var minutes = 0
	
	sql = "exec [service].[getDatePeriod] '" + env + "', '" + start + "','" + end + "'"

	rowsD 	= runSQL(sql,cAcc,"fulltable");
	var treeData = [];
	if ((rowsD.length > 0) && (rowsD[0].start_timestamp !='0')) {
		start_bankDate = rowsD[0].start_bankdate
		stop_bankDate = rowsD[0].end_bankdate
		start_timestamp = rowsD[0].start_timestamp
		stop_timestamp = rowsD[0].end_timestamp
		days = rowsD[0].days
		wdays = rowsD[0].wdays
		minutes = rowsD[0].minutes
		sql   = "exec service.SLAReport '" + env + "', '" + start_timestamp + " " + "','" + stop_timestamp + "'"

		var rowsA = []	
		rowsA 	= runSQL(sql,cAcc,"fulltable");
		var sum = 0;

		treeData.push(
		{
			"name" : "T24 SLA"
			,"priority" : "1"
			,"sla_priority" : "1"
			,"icon" : "resources/img/icons/16/sla.png"
			,"leaf": false
			,"expanded": true
			,"startBankDate":  start_bankDate
			,"endBankDate":  stop_bankDate
			,"startDate":  start_timestamp
			,"endDate":  stop_timestamp
			,"days"	: days
			,"wdays" : wdays
			,"minutes" : minutes
			,"children":  getChild()
		})
		
		treeData[0].sla = sum;
	}
	
	var myJSON = new JSON();
	data 		= myJSON.toJSON(null, treeData, false); 	
	Response.Write(  data );
}		
} 
catch (err) {
	logData('portlet/SLAServiceTree.asp: ' + err.message);
  Response.Write('Error: ' + err.message);
}

function getChild()
{
	var child = []
	for (var i=0; i<rowsA.length; i++)
	{
		child.push(
		{
			"name" : rowsA[i].SLAService
			,"id" : rowsA[i].id
			,"icon" : rowsA[i].icon
			,"sla"	: rowsA[i].sla
			,"priority" : rowsA[i].sla_width_priority
			,"sla_priority" : rowsA[i].priority
			,"leaf": true
		})
			sum += rowsA[i].sla_width_priority
	}
	return child;
}
%>