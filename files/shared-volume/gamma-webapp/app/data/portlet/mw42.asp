<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->
	<!--#include file="../include/util.asp"-->

	<%
try {
	var inputter = String(Request("inputter"));
	var host = String(Request("host"));
	var parameters = String(Request("parameters"));

	if(CheckRight(SEC_ROLE_LOGIN, null)) {
		//sql= "execute observer.getmw42 @env='" + env + "', @host='" + host + "' "

		sql = " Select * "
		sql += " From observer.[MW42.ANALYSER] "
		sql += " Where measurement_id IN  "
		sql += " 	( Select top 1 id "
		sql += " 	From measurements m2, observer.[MW42.ANALYSER] epo "
		sql += " 	WHERE observer = 'MW42.ANALYSER' "
		sql += " 		AND id = measurement_id "
		sql += " 		AND environment IN ('" + env + "',  (select master_environment_name from Configuration.Environments where name = '"+env+"') )"
		sql += " 		AND (m2.host = '" + host + "' OR (m2.host = 'MULTISERVER' AND epo.host = '" + host + "' )) "
		sql += " 	Order by timestamp Desc ) AND (host =  '"+host+"' or host is null) AND (port is not null OR pid is not null )"


		rowsA = runSQL(sql,cAcc,"fulltable");
		var myJSON = new JSON();

		data = myJSON.toJSON(null, rowsA, false);
		fields = myJSON.generateFields(rowsA);
		count = rowsA.length;
		Response.Write( '{ "total":"' + count + '", "results":' + data + '}');
	}
}
catch (err) {
	logData('portlet/mw42.asp: ' + err.message);
  Response.Write('Error: ' + err.message);
}

%>