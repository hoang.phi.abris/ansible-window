<%

// ObjectStateEnum values
var adStateClosed = 0;

//CommandTypeEnum values
var adCmdStoredProc = 4;

// DataTypeEnum values
var adInteger = 3;
var adBoolean = 11;
var adVarWChar = 202;
var adLongVarWChar = 203;

// ParameterDirectionEnum values
var adParamInput = 1;
var adParamOutput = 2;

function createStoredProcCmd(activeConnection, commandText, parameters) {
	
	var cmd = Server.CreateObject('ADODB.Command');
	cmd.ActiveConnection = activeConnection;
	cmd.CommandType = adCmdStoredProc;
	cmd.CommandText = commandText;
	cmd.CommandTimeout = 40000;
	
	if (parameters) {
		for (var i = 0; i < parameters.length; i++) {
			var parameter = parameters[i];
			cmd.Parameters.Append(cmd.CreateParameter(parameter.name, parameter.type, parameter.direction, -1, parameter.value));
		}
	}
	
	return cmd;
}

function executeCmdAndProcessResult(cmd) {
	
	var rowsA = new Array();
	var rsTEMP = cmd.Execute();
	
	if(rsTEMP.State != adStateClosed) {
		var ii, jj, arg1, tempO;
		jj = 0; // returned rows counter
		while (!rsTEMP.EOF) { // -- ROWS
			var tempO = new Object();
			for (ii = 0; ii < rsTEMP.Fields.Count; ii++) {
				colName = rsTEMP.Fields(ii).Name;
				if(colName == null || colName == "") // give it a name
					colName = "xyzcol" + ii;
				tempO[colName] = rsTEMP.Fields(ii).Value;
				arg1 += tempO[colName] + ", ";
			}
			rowsA[jj] = tempO;
			tempO = null;
			jj++;
			rsTEMP.MoveNext();
		} // -- next ROW
		rsTEMP.Close();
		rsTEMP = null;
	}
	
	return rowsA;
}

function StoreConfigFile(host, thefile, filetypeid, connection) {

	var cmd = createStoredProcCmd(connection, "Configuration.StoreConfigFile", [
		{name: "@host", type: adVarWChar, direction: adParamInput, value: host},
		{name: "@theFile", type: adLongVarWChar, direction: adParamInput, value: thefile},
		{name: "@type", type: adInteger, direction: adParamInput, value: filetypeid},
		{name: "@resID", type: adInteger, direction: adParamOutput}
	]);
	
	var rsTEMP = cmd.Execute();
}

function InitHostFiles(id, connection, procName, newRow) {
	var fs, f, t, x;
	fs = Server.CreateObject("Scripting.FileSystemObject");
	var currentdirectorypath = Server.MapPath(".");
	
	t = fs.OpenTextFile(currentdirectorypath + "\\defaults\\config.xml", 1, false);
	res = t.ReadAll();
	t.close();
	StoreConfigFile (newRow[0]["name"], res, 1, connection);
	
	t = fs.OpenTextFile(currentdirectorypath + "\\defaults\\Ebp.xml", 1, false);
	res = t.ReadAll();
	t.close();
	StoreConfigFile (newRow[0]["name"], res, 6, connection);
	
	t = fs.OpenTextFile(currentdirectorypath + "\\defaults\\read_tsa_config.xml", 1, false);
	res = t.ReadAll();
	t.close();
	StoreConfigFile (newRow[0]["name"], res, 7, connection);
	
	t = fs.OpenTextFile(currentdirectorypath + "\\defaults\\scheduler.xml", 1, false);
	res = t.ReadAll();
	t.close();
	StoreConfigFile (newRow[0]["name"], res, 8, connection);
}

function RunStoredProcWithId(id, connection, procName, newRow) {
	switch (procName) {
		case "InitHostFiles":
			InitHostFiles(id, connection, procName, newRow);
			break;
		default:
			var cmd = createStoredProcCmd(connection, procName, [
				{name: "@id", type: adInteger, direction: adParamInput, value: id}
			]);
			var rsTEMP = cmd.Execute();
			break;
	}
}

function GetControllerCommands(inputter, environment) {
	
	var cmd = createStoredProcCmd(cAcc, "Configuration.GetControllerCommands", [
		{name: "@inputter", type: adInteger, direction: adParamInput, value: inputter},
		{name: "@environment", type: adVarWChar, direction: adParamInput, value: environment}
	]);
	
	return executeCmdAndProcessResult(cmd);
}

function GetT24Data(inputter, environment, host, command, parameters) {
	
	var cmd = createStoredProcCmd(cAcc, "Configuration.GetT24Data", [
		{name: "@inputter", type: adInteger, direction: adParamInput, value: inputter},
		{name: "@environment", type: adVarWChar, direction: adParamInput, value: environment},
		{name: "@host", type: adVarWChar, direction: adParamInput, value: host},
		{name: "@command", type: adInteger, direction: adParamInput, value: command},
		{name: "@parameters", type: adVarWChar, direction: adParamInput, value: parameters}
	]);
	
	return executeCmdAndProcessResult(cmd);
}

function SendGammaInfo(inputter, environment, parameters, download) {
	
	var cmd = createStoredProcCmd(cAcc, "dbo.SendGammaInfo", [
		{name: "@inputter", type: adInteger, direction: adParamInput, value: inputter},
		{name: "@environment", type: adVarWChar, direction: adParamInput, value: environment},
		{name: "@download", type: adInteger, direction: adParamInput, value: download}
	]);
	
	return executeCmdAndProcessResult(cmd);
}

function SendCommand(inputter, environment, host, commandID, param, runServer) {
	
	var cmd = createStoredProcCmd(cAcc, "dbo.SendCommand", [
		{name: "@inputter", type: adVarWChar, direction: adParamInput, value: inputter},
		{name: "@environment", type: adVarWChar, direction: adParamInput, value: environment},
		{name: "@host", type: adVarWChar, direction: adParamInput, value: host},
		{name: "@commandID", type: adInteger, direction: adParamInput, value: commandID},
		{name: "@parameters", type: adVarWChar, direction: adParamInput, value: param},
		{name: "@@runbyserver", type: adBoolean, direction: adParamInput, value: runServer}
	]);
	
	cmd.CommandTimeout = 0;
	
	return executeCmdAndProcessResult(cmd);
}

function cloneObject(obj) {
	if (obj === null || typeof obj !== 'object') {
		return obj;
	}
	
	var temp = obj.constructor(); // give temp the original obj's constructor
	for (var key in obj) {
		temp[key] = cloneObject(obj[key]);
	}
	
	return temp;
}

function setDateFormat() {
var dateFormat = function () {
	var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (val, len) {
			val = String(val);
			len = len || 2;
			while (val.length < len) val = "0" + val;
			return val;
		};
	
	// Regexes and supporting functions are cached through closure
	return function (date, mask, utc) {
		var dF = dateFormat;
		
		// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
		if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
			mask = date;
			date = undefined;
		}
		
		// Passing date through Date applies Date.parse, if necessary
		date = date ? new Date(date) : new Date;
		if (isNaN(date)) throw SyntaxError("invalid date");
		
		mask = String(dF.masks[mask] || mask || dF.masks["default"]);
		
		// Allow setting the utc argument via the mask
		if (mask.slice(0, 4) == "UTC:") {
			mask = mask.slice(4);
			utc = true;
		}
		
		var _ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
				d:    d,
				dd:   pad(d),
				ddd:  dF.i18n.dayNames[D],
				dddd: dF.i18n.dayNames[D + 7],
				m:    m + 1,
				mm:   pad(m + 1),
				mmm:  dF.i18n.monthNames[m],
				mmmm: dF.i18n.monthNames[m + 12],
				yy:   String(y).slice(2),
				yyyy: y,
				h:    H % 12 || 12,
				hh:   pad(H % 12 || 12),
				H:    H,
				HH:   pad(H),
				M:    M,
				MM:   pad(M),
				s:    s,
				ss:   pad(s),
				l:    pad(L, 3),
				L:    pad(L > 99 ? Math.round(L / 10) : L),
				t:    H < 12 ? "a"  : "p",
				tt:   H < 12 ? "am" : "pm",
				T:    H < 12 ? "A"  : "P",
				TT:   H < 12 ? "AM" : "PM",
				Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
				o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
				S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};
		
		return mask.replace(token, function ($0) {
			return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
		});
	};
}();

// Some common format strings
dateFormat.masks = {
	"default":      "ddd mmm dd yyyy HH:MM:ss",
	shortDate:      "m/d/yy",
	mediumDate:     "mmm d, yyyy",
	longDate:       "mmmm d, yyyy",
	fullDate:       "dddd, mmmm d, yyyy",
	shortTime:      "h:MM TT",
	mediumTime:     "h:MM:ss TT",
	longTime:       "h:MM:ss TT Z",
	isoDate:        "yyyy-mm-dd",
	isoTime:        "HH:MM:ss",
	isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
	isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
	dayNames: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
	monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
	return dateFormat(this, mask, utc);
};}

function getTimestampFromDate(datetime){
	var parts = datetime.split(' ');
	var dateparts = parts[0].split('.');
	var timeparts = parts[1].split(':');
	return formatdatestampToHighChart(Number(dateparts[0]), Number(dateparts[1]-1), Number(dateparts[2]),  Number(timeparts[0]), Number(timeparts[1]), Number(timeparts[2]));
}
		

function formatdatestampToHighChart(year, month, day, hour, minutes, seconds)
{
	var tdate = new Date(year, month, day, hour, minutes, seconds);
	return Date.UTC(tdate.getYear(),tdate.getMonth(),tdate.getDate(), tdate.getHours(), tdate.getMinutes(), tdate.getSeconds());
}

%>