<!-- #include virtual="/config.inc" -->
<%
//<script>
Response.Charset = "UTF-8";
/***************************************************************
//  General subroutine for getting the data from the database
//  Request items:
//      verbose     -
//      source      -
//      environment -
***************************************************************/

// set verbose mode
var verbose=String(Request("verbose"));
if (verbose=="undefined") verbose=0;

var debug=String(Request("debug"));
if(debug=="undefined") debug=0;

/* request header properties */
var cobdate = isset(Request("cobdate")) ? Request("cobdate") : '';
var env = isset(Request("env")) ? Request("env") : '';

// Uid=myUsername; Pwd=myPassword;
var cAcc = MM_connAccess_STRING_GAMMA;
var cAccArch = MM_connAccess_STRING_GAMMA_ARC;

/****************************************
* Functions
*****************************************/

function debugPrint(level, t) {
	if(debug >= level)
	Response.Write("<br>" + level + ": " + t);
	}

function beginTransaction(myConn) {
	var CM_updateConnection = Server.CreateObject('ADODB.Connection');
	CM_updateConnection.Open(myConn);
	CM_updateConnection.BeginTrans();
	return CM_updateConnection;
}

function commitTransaction(CM_updateConnection) {
	CM_updateConnection.CommitTrans();
	CM_updateConnection.close();
}

function rollbackTransaction(CM_updateConnection) {
	CM_updateConnection.RollbackTrans();
	CM_updateConnection.close();
}

/**
 * Executes SQL statement with error handling.
 * Catches exceptions, writes to response.
 */
function runSQL(theSQL, myConn, rollup) {
    try{
        return _runSQL(theSQL, myConn, rollup);
    }
    catch (err) {
        logData('include/get_db_data.asp: ' + err.message);
        
        // invalid JSON string because message can contain simple quote characters
        Response.Status = '500 ' + err.message;
        Response.Write('{"error":"' + err.message.replace(/"/g, "'") + '"}');
    }
} // end runSQL

/**
 * Executes SQL statement without error handling.
 * Can throw exception, does not write to response.
 */
function _runSQL(theSQL, myConn, rollup) {
	return _runSQL_columnIndex(theSQL, myConn, rollup, false);
} // end _runSQL


/**
 * Executes SQL statement without error handling.
 * Adds column index to column name if required. (Makes unique column names.)
 * Can throw exception, does not write to response.
 */
function _runSQL_columnIndex(theSQL, myConn, rollup, addIndexToColumn) {
	
		var ii, jj, zz, arg1, argA, itemp, tempO;
		vprint(1, "runSQL called:" + theSQL + ":");

		var Conn1 = Server.CreateObject('ADODB.Connection');
		var rsTEMP = Server.CreateObject('ADODB.Recordset');

		Conn1.CommandTimeout = 0;
		Conn1.Open(myConn);
		
		if (rollup != null) {
			arg1 = "";
			if (rollup == "fulltable" ) { // pack an array of objects

				rsTEMP = Conn1.Execute(theSQL, 0, 0x10);
				var closed = false;
				
				var oShell = Server.CreateObject("WScript.Shell")
				var path = Server.mappath("/")

				while(rsTEMP.State == 4){
					// not so ugly hack to sleep a few ms
					oShell.run("cscript "+path+"/sleep.vbs 50", 1, true)				

					if(!Response.IsClientConnected){
						Conn1.Cancel();
						closed = true;
					}
				}

				if(!closed){
					argA = new Array();
					jj = 0; // returned rows counter
					zz = 0; // actual counter
					
					try {
						while (!rsTEMP.EOF) { // -- ROWS

							if(!Response.IsClientConnected){
								return([]);
							}
							var tempO = new Object();
							for (ii=0; ii < rsTEMP.Fields.Count; ii++) {
								colName = rsTEMP.Fields(ii).Name;
								if (colName == null) {
									colName = "";
								}
								
								if (addIndexToColumn) {
									// add index to column name
									colName += ("_" + ii);
								} else {
									if(colName == "") {
										// give it a name
										colName = "xyzcol" + ii;
									}
								}
								
								tempO[colName] = rsTEMP.Fields(ii).Value;
								arg1 += tempO[colName] + ", ";
								vprint(2, "runSQL row[" + jj + "] col[" + ii + "]:name:" + colName + ": is: " + tempO[colName]);
							}
							argA[jj] = tempO;
							tempO = null;
							jj++;
							zz++; // row counter
							rsTEMP.MoveNext();
						} // -- next ROW

					} 
					catch (err) {
						logData('include/get_db_data.asp: ' + err.message);
						Response.Write('Error: ' + err.message + "\n");
						return([]);
					}

					vprint(1, "runSQL, Total records: " + zz);
					
					// global
					total_records = zz;
					rsTEMP.Close();
				}
				
			} // end fulltable
			
			else { // expect no return values
				// params are: numReturnedItems, SQLstorProParamas, Options:adExecuteNoRecords
				var argA = Conn1.Execute(theSQL, null, 0x00000080);
				vprint(1, "runSQL command mode nothing to return");
			} // end command
			
		} // end rollup not null

		Conn1.Close();

		return(argA);
	
	
} // end _runSQL_columnIndex

function isDate (x) {
	return (null != x) && !isNaN(x) && ("undefined" !== typeof x.getDate);
}

function formatDateToHighChart(value) {
	var tdate = new Date(value)
	return Date.UTC(tdate.getYear(), tdate.getMonth(), tdate.getDate());
}

function convertDateToHighChart(value) {
	
	var isoExp = /^\s*(\d{4})-(\d\d)-(\d\d)\s*$/;
	var parts = isoExp.exec(value);
    var dateFormat2Exp = /^\s*(\d\d?)\/(\d\d?)\/(\d{4})\s*$/;
    var parts2 = dateFormat2Exp.exec(value);
    var date;
    if(parts) {
        date = new Date((Date.UTC((+parts[1]), (+parts[2])-1, (+parts[3]), 0, 0, 0, 0)));
    } else if(parts2) {
        date = new Date((Date.UTC((+parts2[3]), (+parts2[2])-1, (+parts2[1]), 0, 0, 0, 0)));
    } else {
        date = new Date(value);
	}
	return date.getTime()
}

function formatDateTimeToHighChart(value) {
	var tdate = new Date(value)
	return tdate.getTime()
}

function getLeadingZero(val) {
    if (val < 10) {
        val = "0" + val;
    }

    return val;
}


function convertDateTime(value) {
	var date = new Date(value);
	
    var year = date.getFullYear();
    var month = getLeadingZero(date.getMonth() + 1);
    var day = getLeadingZero(date.getDate());
    var hours = getLeadingZero(date.getHours());
    var minutes = getLeadingZero(date.getMinutes());
    var seconds = getLeadingZero(date.getSeconds());

    var timestamp = year + "-" + month + "-" + day + " " + hours + ":" + minutes;
    return timestamp;
}

function logData(toLog) {
	var adCmdStoredProc = 4
	var adParamInput = 1
	var adLongVarWChar = 203
	
	var cmd = Server.CreateObject('ADODB.Command');
	cmd.ActiveConnection = cAcc;
	cmd.CommandType = adCmdStoredProc
	cmd.CommandText = "dbo.StoreWorkingLog"
	
	cmd.Parameters.Append(cmd.CreateParameter("@toLog", adLongVarWChar, adParamInput, -1, toLog));
	
	var rsTEMP = cmd.Execute();
}

function audit(log_type, param1, param2) {
	try {
		var auditEnvironment = '';
		if ((env != 'undefined') || (env != ''))
			auditEnvironment = env;
			
		if ((auditEnvironment == 'undefined') || (auditEnvironment == '')) {
			Response.Write("audit error: set environment variable");
			return;
		}
		
		var user = Session("username");
		var auditSql = "INSERT INTO Audit.LogDetails ([user], log_type, timestamp, env, param1, param2) VALUES('" + user + "', " + log_type + " ,GETDATE(),'" + auditEnvironment  + "', " + param1 +", '" + param2 + "')";
		runSQL(auditSql, cAcc, "cmd");
	}
	catch (err) {
		logData('include/get_db_data.asp: ' + err.message);
		Response.Write("{success:false,error:'" + err.message + "'}");
	}
}
%>
