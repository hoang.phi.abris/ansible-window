﻿<%
//**************************************************************************************************************
//* GAB_LIBRARY Copyright (C) 2003 - This file is part of GAB_LIBRARY  
//* For license refer to the license.txt          
//**************************************************************************************************************

//**************************************************************************************************************

//' @CLASSTITLE:  JSON
//' @CREATOR:  Michal Gabrukiewicz (gabru at grafix.at), Michael Rebec
//' @CONTRIBUTORS: - Cliff Pruitt (opensource at crayoncowboy.com)
//'     - Sylvain Lafontaine
//'     - Craig Mandsager (www.wildwebwidget.com)
//' @CREATEDON:  2007-04-26 12:46
//' @CDESCRIPTION: Comes up with functionality for JSON (http://json.org) to use within ASP.
//'      Correct escaping of characters, generating JSON Grammer out of ASP datatypes && structures
//' @REQUIRES:  -
//' @OPTIONEXPLICIT: yes
//' @VERSION:  1.5
//' @HISTORY   :
//' 20080501 Craig Mandsager conversion of VB flavor to JSP ASP

//**************************************************************************************************************
function JSON(){

 //private members;
 var output="", inner=0;
 var doEscapes;  // configuration var for testing to inhibit escapes (set true for live system)

 //public members;
 var toResponse;  //'[bool] should generated results be directly written to the response? default = false;
 // make a global reference
 var thisJSON=this;

 //**********************************************************************************************************;
 //* constructor ;
 //**********************************************************************************************************;
 this.class_initialize = class_initialize;
  function class_initialize(doEscapes,toResponse){
		newGeneration();
		thisJSON.doEscapes  = (doEscapes==null)?true:doEscapes;
    thisJSON.toResponse = (toResponse==null)?false:toResponse;
    vprint(1,"doEscapes:"+thisJSON.doEscapes+" toResponse:"+thisJSON.toResponse);
	 }
 // run it!
 thisJSON.class_initialize();

 //******************************************************************************************;
 //' @SDESCRIPTION: STATIC! takes a given string && makes it JSON valid;
 //' @DESCRIPTION: all characters which needs to be escaped are beeing replaced by their;
 //'     unicode representation according to the ;
 //'     RFC4627#2.5 - http://www.ietf.org/rfc/rfc4627.txt?number=4627;
 //' @PARAM:   val [string]: value which should be escaped;
 //' @RETURN:   [string] JSON valid string;
 //******************************************************************************************;
 this.uescape = uescape;
 function uescape(val){
	var ret_val="";
	if(!thisJSON.doEscapes)
		return(val);
	val=String(val);
	// preserve any real percents before doing escape
	ret_val=val.replace(/\%/g,"_PER9846CENT_");
	// JSP builtin escape function
	ret_val=escape(ret_val);
	// convert back % if changes were made
	if(ret_val != val){
		// builtin function returns form %22 just replace the % with \u00 to match JSON format
		ret_val=ret_val.replace(/\%/g,"\\u00");
		ret_val=ret_val.replace(/\u00u/g,"u");
		
		ret_val=ret_val.replace(/_PER9846CENT_/g,"%");
		}

	/*******ORIGINAL VB code, JSCRIPT provides escape function 
		var cDoubleQuote, cRevSolidus, cSolidus;
		cDoubleQuote = 0x0022;
		cRevSolidus = 0x005C;
		cSolidus = 0x002F;
	
		var i, currentDigit;
		for (i = 0; i<val.length; i++){
		 currentDigit = val.substr(i, 1);
		 if(val.charCodeAt(i) > 0x0000 && val.charCodeAt(i) < 0x001F){
			currentDigit = escapequence(currentDigit);
		 }else if(val.charCodeAt(i) >=  0xC280 && val.charCodeAt(i) <=  0xC2BF){
			currentDigit = "\\"+"u00" + right(padLeft(parseInt(val.charCodeAt(i) - 0xC200).toString(16), 2, 0), 2);
		 }else if(val.charCodeAt(i) >= 0xC380 && val.charCodeAt(i) <= 0xC3BF){
			currentDigit = "\\"+"u00" + right(padLeft(parseInt(val.charCodeAt(i) - 0xC2C0).toString(16), 2, 0), 2);
		 }else{
			switch(val.charCodeAt(i)){
			 case cDoubleQuote: currentDigit = escapequence(currentDigit); 
						break;
			 
			 case cRevSolidus: currentDigit = escapequence(currentDigit);
						break;

			 case cSolidus: currentDigit = escapequence(currentDigit);
						break;
			}
		 }
		 ret_val = ret_val + currentDigit;
		} // end for
	  ***************************	*/
		vprint(3,"uescape IN:"+val+" OUT:"+ret_val+" loops:"+val.length);
		return(ret_val);
	 }

 //******************************************************************************************************************
 //' @SDESCRIPTION: generates a representation of a name value pair in JSON grammer;
 //' @DESCRIPTION: the generation is done fully recursive so the value can be a complex datatype as well. e.g.;
 //'     toJSON("n", array(array(), 2, true), false) || toJSON("n", array(RS, dict, false), false), etc.;
 //' @PARAM:   name [string]: name of the value (accessible with javascript afterwards). leave empty to get just the value;
 //' @PARAM:   val [variant], [int], [float], [array], [object], [dictionary], [recordset]: value which needs;
 //'     to be generated. Conversation of the data types (ASP datatype -> Javascript datatype)
 //'     null, NULL -> null, ARRAY -> array, BOOL -> bool, OBJECT -> name of the type, 
 //'     MULTIDIMENSIONAL ARRAY -> generates a 1 dimensional array (flat) with all values of the multivar array;
 //'     DICTIONARY -> valuepairs. each key is accessible as property afterwards
 //'     RECORDSET -> array where each row of the recordset represents a field in the array. 
 //'     fields have properties named after the column names of the recordset (LOWERCASED!)
 //'     e.g. generate(RS) can be used afterwards r[0].ID
 //'     INT, FLOAT -> number
 //'     OBJECT with reflect() method -> returned as object which can be used within JavaScript
 //' @PARAM:   nested [bool]: is the value pair already nested within another? if(yes){ the {} are left out.
 //' @RETURN:   [string] returns a JSON representation of the given name value pair
 //'     (if(toResponse is on){ the return is written directly to the response && null is returned)
 //******************************************************************************************************************
  
	this.toJSON = toJSON;
	function toJSON(name, val, nested){
		if ((name=="renderer") && (val.length > 0)) {var retVal = val; val = function() {return retVal}}		
		if ((name=="respdata") && (val.length > 0)) {var retVal = val; val = function() {return retVal}}		
		var ret_val;
		if(! nested && ! isEmpty(name)) write("{");
		if(! isEmpty(name)) write('"' + thisJSON.uescape(name) + '": '); //
		generateValue(val);
		if(! nested && ! isEmpty(name)) write("}");
		ret_val= output;
	
		if(inner== 0)
			newGeneration();
		return(ret_val);
		}
 //******************************************************************************************************************
 //' @SDESCRIPTION: converts JSON grammer back to ASP(Jscript) data types
 //' @DESCRIPTION: the conversion returns a mix of array and hash datatypes, custom classes are not handled.
 //' @PARAM:   data [string]: the JSON data string;
 //' @PARAM:   conv2class [class]: not implemented, future use is to populate an existing class from JSON data.
 //' @RETURN:   [Object/Array] returns Jscript representation of the data
 //******************************************************************************************************************
  
	this.fromJSON = fromJSON;
	function fromJSON(data, conv2class){
  	var key;
		if(conv2class !=null)vprint(0,"Warning fromJSON was passed non-implemented arg conv2class, ignoring it");
    data=String(data);
		// try to decide on array vs object
    if(data.search(/^\s*{/) < 0){ //maybe an array like [45,55,67]
    	vprint(1,"ARRAY:");
      data = data.replace(/\[/,"");
      data = data.replace(/\]/,"");
			//200803 when passing arrays like [23,45] works fine but it single element [23] makes an array of size 23
    	if(data.search(/,/) < 0) //single element
      	data = 'new Array("'+data+'")';
			else
      	data = "new Array("+data+")";
    	var ret_val = eval(data);
      }
    else{
      vprint(1,"OBJECT:");
			var ret_val = new Object(data);
      }
		vprint(1,"fromJSON("+data+") returning type: "+typeof(ret_val)+", length:"+ret_val.length+"! ");
    if(ret_val.length != null)
    	for(ii=0;ii<ret_val.length;ii++)
    		vprint(2,"["+ii+"] value:"+ret_val[ii]);
    for(key in ret_val)
    	vprint(2,"key:"+key+" value:"+ret_val[key]);
		return(ret_val);
		}
 //******************************************************************************************************************;
 //* generate ;
 //******************************************************************************************************************;
  this.generateValue=generateValue;
	function generateValue(val){
	vprint(2,"generateValue typeof is:"+typeof(val)+":");
		if(isNull(val)){
		 write("null");
		}else if(isArray(val)){
		 vprint(2,"generateValue type is ARRAY");
		 generateArray(val);
		}else if(isClass(val)){
		 vprint(2,"generateValue type is CLASS");
		 generateObject(val);
		}else if(isObject(val)){
		 vprint(2,"generateValue type is OBJECT, type:"+val.type+" name:"+val.name);
		 if(val==null){
			write("null");
		 }else if(val.type == "Dictionary"){
			generateDictionary(val);
		 }else if(val.type == "Recordset"){
			generateRecordset(val);
		 }else{
		 	generateDictionary(val);
		 }
		}else{
		 //bool;
		 if(typeof(val)=="boolean"){
		  vprint(2,"generateValue type is BOOLEAN");
			if(val) write("true"); else write("false");
		 //int, long, byte; //single, double, currency;
		 }else if(typeof(val)=="number"){
		 	vprint(2,"generateValue type is NUMBER");
			write(val);
		 }else if(typeof(val)=="function"){
		 	vprint(2,"generateValue type is NUMBER");
			write(val());
		 }
		 else{
		  vprint(2,"generateValue type is OTHER");
			write('"' + thisJSON.uescape(val ) + '"');
		 }
		}
		vprint(1,"---generateValue returning:"+output+":");
		return( output);
	 }

 //******************************************************************************************************************;
 //* generateArray ;
 //******************************************************************************************************************;
  this.generateArray=generateArray;
	function generateArray(val){
		var item, i;
		write("[");
		i = 0;
		//the for each allows us to support also multi dimensional arrays;
		for ( item in val){
			if(i > 0)
			write(",");
			generateValue(val[item]);
			i = i + 1;
			}
		write("]");
	 }


 //******************************************************************************************************************;
 //* generateFields
 //* Gyorfi Jeno
 //******************************************************************************************************************;
  this.generateFields=generateFields;
	function generateFields(val){		
	output = "";
		write("[");
		if ( ! val.eof){
			i = 0;
			for (key in val[0]){
				if(i > 0)
					write(",");
				write("{");
				if (key=="respdata") 
					write(toJSON ("name", "login", true) + "," + toJSON ("mapping", "respdata.Login", true) + "," + toJSON ("type", "int", true) + "},{" + toJSON ("name", "operation", true) + "," + toJSON ("mapping", "respdata.Operation", true) + "," + toJSON ("type", "int", true) );
				else
					write(toJSON ("name", key, true));
				write("}");
			  i = i + 1;
			}
		}
		write("]");
		return output ;
	 }




 //******************************************************************************************************************;
 //* generateDictionary ;
 //******************************************************************************************************************;
  this.generateDictionary=generateDictionary;
	function generateDictionary(val){
		var key, i;
		inner= inner+ 1;
		write("{");
		i=0;
		for (key in val){
		 if(i > 0) write(",");
		 toJSON( key, val[key], true);
		 i++;
		}
		write("}");
		inner= inner- 1;
	 }

 //******************************************************************************************************************;
 //* generateRecordset ; SKIPPING for JSCRIPT
 //******************************************************************************************************************;
  this.generateRecordset=generateRecordset;
	function generateRecordset(val){
		var i;
		write("[");
		while ( ! val.eof){
		 inner= inner+ 1;
		 write("{");
		 for (i = 0; i< val.fields.count - 1; i++){
			if(i > 0) write(",");
			toJSON (val.fields(i.toLowerCase().name), val.fields(i).value, true);
		 }
		 write("}");
		 val.movenext();
		 if(! val.eof) write(",");
		 inner= inner- 1;
		} // end while
		write("]");
	 }

 //******************************************************************************************************************;
 //* generateObject ; for classes, needs a reflect method
 //******************************************************************************************************************;
  this.generateObject=generateObject;
	function generateObject(val){
		var props;
		
	 props = val.reflect();
		if(err == 0){
		 inner= inner+ 1;
		 toJSON (null, props, true);
		 inner= inner- 1;
		}else{
		 write('"' + thisJSON.uescape(typename(val)) + '"');
		}
	 }

 //******************************************************************************************************************;
 //* newGeneration ;
 //******************************************************************************************************************;
  this.newGeneration=newGeneration;
	function newGeneration(){
		output = "";
		inner= 0;
	 }

 //******************************************************************************************;
 //* JsonEscapeSquence ;
 //******************************************************************************************;
  this.escapequence=escapequence;
	function escapequence(digit){
		escapequence = "\\"+"u00" + right(padLeft(parseInt(String.fromCharCode(digit)).toString(16), 2, 0), 2);
	 }

 //******************************************************************************************;
 //* padLeft ;
 //******************************************************************************************;
  this.padLeft=padLeft;
	function padLeft(value, totalLength, paddingChar){
		padLeft = right(clone(paddingChar, totalLength) + value, totalLength);
	 }

 //******************************************************************************************;
 //* clone ;
 //******************************************************************************************;
  this.clone=clone;
	function clone(str, n){
		var i,ret_val="";
		for (i = 0; i< n ;i++) 
			ret_val = ret_val + str;
		return(ret_val);
	 }

 //******************************************************************************************;
 //* write ;
 //******************************************************************************************;
  this.write=write;
	function write(val){
		if(toResponse){
		 response.write(val);
		}else{
		 output = output + val;
		}
	 }

} //end class JSON

//
// UTILITY - utility functions easing conversion from vbscript to jscript
//
function isEmpty(invar){
	return( (invar==null)?true:false );
	}
function isNull(invar){
	return( (invar==null)?true:false );
	}
function isset(invar){
	return( (String(invar)!="undefined")?true:false );
	}
function isArray(invar){
	var ret_val=false;
	if(invar !=null)
		if(typeof(invar)=="object")
			if (invar.length != null)
				ret_val=true;
	return( ret_val );
	}
function isObject(invar){
	var ret_val=false;
	if(invar !=null)
		if(typeof(invar)=="object" && invar.reflect==null)
			ret_val=true;
	return( ret_val );
	}
function isClass(invar){
	var ret_val=false;
	if(invar !=null)
		if(typeof(invar)=="function" && invar.reflect!=null)
			ret_val=true;
	return( ret_val );
	}
// turn debug prints on/off with global int verbose
function vprint(level,stuff){
	if(verbose >=level)
	Response.write("\n<br>" + stuff );
	}
// ============================================================================
// This code converted from VBScript to Javascript by the ScriptConverter tool.
// Use freely.  Please do not redistribute without permission.
// Copyright 2003 Rob Eberhardt - scriptConverter@slingfive.com.
// it missed all class stuff
// sub -> function
// for each item in val;  -> for (item in val)
// for i = 0 to uBound(keys); -> for(i=0; i<keys; i++){
// missed function return values
// ============================================================================
%>