<%

function getItem(list, id) {
	for(var i=0; i<list.length; i++) {
		if (list[i].id == id)
			return list[i];
	}
	return null;
}

function getParamListIdFromDb(conn, host_id, name) {
	sql = "SELECT id FROM Configuration.ParamList WHERE name = '" + name + "' AND host_id = " + host_id;
	 var plrows = runSQL(sql,conn,"fulltable");
	return plrows.length == 0 ? -1 : plrows[0].id;
}

function addNewParamList(conn, host_id, paramList) {
	sql = "INSERT INTO Configuration.ParamList (id, host_id, version, name) OUTPUT INSERTED.id, INSERTED.param_list_id ";
	sql += "VALUES((SELECT ISNULL(max(id),0)+1 FROM Configuration.ParamList  where host_id = "+host_id+"), " + host_id + ", 1, '" + paramList.name + "') ";

 	var plrows = runSQL(sql,conn,"fulltable");
	if (plrows.length > 0) {
		for(var i=0; i<paramList.items.length;i++) {
			sql = "INSERT INTO Configuration.ParamListItem (param_list_id, item, wm_key, wm_status, [order]) ";
			sql += "VALUES(" + plrows[0].param_list_id + ", '" + paramList.items[i].value +"', '"+ paramList.items[i].wm_key + "', '"+ paramList.items[i].wm_status + "', " + (i+1) + ") ";
			var r = runSQL(sql,conn,"cmd");
		}
		if (plrows.length > 0) {
			return plrows[0].id;
		}
	}
	return -1;
}

function getShellIdFromDb(conn, host_id, name) {
	sql = "SELECT id FROM Configuration.Shell WHERE name = '" + name + "' AND host_id = " + host_id;
	 var srows = runSQL(sql,conn,"fulltable");
	return srows.length == 0 ? -1 : srows[0].id;
}

function addNewShell(conn, host_id, shell) {
	sql = "INSERT INTO Configuration.Shell (id, host_id, version, name, shell_cmd) OUTPUT INSERTED.id, INSERTED.shell_id ";
	sql += "VALUES((SELECT ISNULL(max(id),0)+1 FROM Configuration.Shell where host_id = "+host_id+"), " + host_id + ", 1, '" + shell.name + "', '" + shell.shell_cmd + "') ";

 	var srows = runSQL(sql,conn,"fulltable");
	if (srows.length > 0) {
		for(var i=0; i<shell.commands.length;i++) {
			sql = "INSERT INTO Configuration.ShellItem (shell_id, command, [order]) ";
			sql += "VALUES(" + srows[0].shell_id + ", '" + shell.commands[i] + "', " + (i+1) + ") ";
			var r = runSQL(sql,conn,"cmd");
		}
		if (srows.length > 0) {
			return srows[0].id;
		}
	}
	return -1;
}

function handleParamList(command, host_id, paramLists, conn) {
	var paramList = getItem(paramLists, command.param_list_id);
	if (paramList != null) {
		var id = getParamListIdFromDb(conn, host_id, paramList.name);
		if (id < 0) {
			id = addNewParamList(conn, host_id, paramList);
		}
		command.param_list_id = id;
	}
}

function handleShell(command, fieldName, host_id, shells, conn) {
	var parts = command[fieldName].split('"');
	if (parts.length > 1) {
		var shell = getItem(shells, parts[1]);
		if (shell != null) {
			var id = getShellIdFromDb(conn, host_id, shell.name);
			if (id < 0) {
				id = addNewShell(conn, host_id, shell);
			}

			command[fieldName] = parts[0] + '"' + id;
			for(var i=2;i<parts.length;i++)
				command[fieldName] += '"' + parts[i];
		}
	}
}

%>