<%

function getGroups(funct, fid) {
try{
	funct.groups = []
	var sql= "SELECT g.id, g.name, startable, email, dependent_id, isnull(s.name, '') as " + '"scheduler_name"' + ", autostart FROM Configuration.ControllerGroup g"
	sql += " LEFT OUTER JOIN Configuration.ControllerScheduler s ON s.id = g.group_scheduler_id "
	sql += " WHERE function_id = " + fid + " ORDER BY position"
	var rowsA = runSQL(sql,cAcc,"fulltable");

	sql = "SELECT a.id, group_id, a.name, run_command, dependent_id, host_id, command_type, email_onsuccess, email_onerror, re_runnable, stoppable, isnull(s.name, '') as " + '"scheduler_name"' + ",[cob_time_type] FROM Configuration.ControllerAction a"
	sql += " LEFT OUTER JOIN Configuration.ControllerScheduler s ON s.id = a.scheduler_id "
	sql += "WHERE group_id IN (SELECT id FROM Configuration.ControllerGroup WHERE function_id = " + fid + ") "
	sql += "ORDER BY group_id, position"

	var rowsB = runSQL(sql,cAcc,"fulltable");

	for (var i=0; i<rowsA.length; i++) {
		var group = {
			id : rowsA[i].id,
			name : rowsA[i].name,
			startable : rowsA[i].startable,
			email : rowsA[i].email,
			dependent_id : rowsA[i].dependent_id,
			scheduler_name : rowsA[i].scheduler_name,
			autostart : rowsA[i].autostart,
			position : i,
			actions : []
		}
		for (var j=0; j<rowsB.length; j++) {
			if (rowsB[j].group_id == rowsA[i].id) {
				var action = {
					id : rowsB[j].id,
					name : rowsB[j].name,
					run_command : rowsB[j].run_command,
					position : j,
					dependent_id : rowsB[j].dependent_id,
					command_type : rowsB[j].command_type,
					host_id : rowsB[j].host_id,
					email_onsuccess : rowsB[j].email_onsuccess,
					email_onerror : rowsB[j].email_onerror,
					re_runnable : rowsB[j].re_runnable,
					stoppable : rowsB[j].stoppable,
					scheduler_name : rowsB[j].scheduler_name,
					cob_time_type : rowsB[j].cob_time_type
				}
				group.actions.push(action)
			}
		}
		funct.groups.push(group);
	}
	}catch (err)
	{
 		 logData('include/controller_groups.asp: ' + err.message);
 		 Response.Write("{success:false,error:'" + err.message + "'}");
	}
}

function getControllerScheds(funct, fid) {
try{
	funct.schedulers = []
	var sql= 'SELECT id, name, CONVERT(nvarchar(30), startdate, 120) as startdate, frequency, [type] as "stype", param, is_automatic, CONVERT(nvarchar(30), start_at, 8) as start_at, timeout, day_type, t24_state, date_type  FROM Configuration.ControllerScheduler WHERE environment_id IN (SELECT environment_id FROM Configuration.ControllerFunction where id = ' + fid + ')';
	var rowsA = runSQL(sql,cAcc,"fulltable");

	for (var i=0; i<rowsA.length; i++) {
		var sched = {
			id : rowsA[i].id,
			name : rowsA[i].name,
			startdate : rowsA[i].startdate,
			frequency : rowsA[i].frequency,
			stype : rowsA[i].stype,
			param : rowsA[i].param,
			is_automatic : rowsA[i].is_automatic,
			start_at : rowsA[i].start_at,
			timeout : rowsA[i].timeout,
			day_type : rowsA[i].day_type,
			date_type : rowsA[i].date_type,
			t24_state : rowsA[i].t24_state
		}
		funct.schedulers.push(sched);
	}
	}catch (err)
	{
 		 logData('include/controller_groups.asp: ' + err.message);
 		 Response.Write("{success:false,error:'" + err.message + "'}");
	}
}

function getControllerCommands(funct, fid) {
try{
	funct.commands = []
	var sql= 'SELECT cc.[id], [version], [type], cc.[name], cc.[description], [cmd_class], ';
	sql += '[cmd_class_param], [command_line], [stop_command_line], [param_list_id], ';
	sql += '[command_param], [check_cmd_class], [check_cmd_class_param], [check_cmd_line], ';
	sql += '[check_cmd_line_param], [status_cmd_class], [status_cmd_class_param], [status_cmd_line], ';
	sql += '[status_cmd_line_param], [check_timeout], [run_in_paused_state], [template_id], wm_status, ';
	sql += '[email_to], [email_cc], [email_subject], [email_body], [email_attachment], [email_auto], [command_line_start], s.host_id, real_host_id, ';
	sql += 'CASE WHEN s.host_id=0 THEN ' + "''" + ' ELSE h.name END AS host_name ';
	sql += 'FROM (SELECT run_command, CASE WHEN ca.host_id=0 THEN e.default_host ELSE host_id END AS real_host_id, ca.host_id FROM Configuration.ControllerFunction cf ';
	sql += 'INNER JOIN Configuration.ControllerGroup cg ON cg.function_id = cf.id ';
	sql += 'INNER JOIN Configuration.ControllerAction ca ON ca.group_id = cg.id ';
	sql += 'INNER JOIN Configuration.Environments e ON cf.environment_id = e.id ';
	sql += 'WHERE function_id = ' + fid + ') s ';
	sql += 'INNER JOIN Configuration.ControllerCommand cc ON cc.host_id = s.real_host_id AND cc.id = s.run_command ';
	sql += 'INNER JOIN Configuration.Hosts h ON h.id = s.real_host_id';

	var rowsA = runSQL(sql,cAcc,"fulltable");
	funct.shells = []
	funct.paramLists = []

	for (var i=0; i<rowsA.length; i++) {
		//not exporting sensitive data
		if (rowsA[i].command_line == "GAMMA.TC.MANAGER" && rowsA[i].command_param != null) {
			var parts = rowsA[i].command_param.split('"');
			parts[3] = '***' + parts[3];
			parts[5] = '***' + parts[5];
			rowsA[i].command_param = '';
			for(var j=0; j<parts.length-1; j++)
				rowsA[i].command_param += parts[j] + '"';
			rowsA[i].command_param += parts[parts.length-1];
		} else if (rowsA[i].cmd_class == "hu.abris.gamma.client.controller.command.SftpMoveFilesCommand") {
			var parts = rowsA[i].cmd_class_param.split('"');
			parts[9] = '***' + parts[9];
			parts[11] = '***' + parts[11];
			parts[13] = '';
			rowsA[i].command_param = '';
			for(var j=0; j<parts.length-1; j++)
				rowsA[i].command_param += parts[j] + '"';
			rowsA[i].cmd_class_param += parts[parts.length-1];
		} else if (rowsA[i].command_line == "GAMMA.BULK.TC.MANAGER") {
			rowsA[i].param_list_id = "";
		} else if (rowsA[i].cmd_class == "hu.abris.gamma.client.controller.command.CurlUploadFileCommand") {
			var parts = rowsA[i].cmd_class_param.split('"');
			parts[5] = '***' + parts[5];
			parts[7] = '***' + parts[7];
			parts[9] = '***' + parts[9];
			parts[11] = '';
			rowsA[i].command_param = '';
			for(var j=0; j<parts.length-1; j++)
				rowsA[i].command_param += parts[j] + '"';
			rowsA[i].cmd_class_param += parts[parts.length-1];
		}
		var command = {
			id : rowsA[i].id,
			version : rowsA[i].version,
			type : rowsA[i].type,
			name : rowsA[i].name,
			description : rowsA[i].description,
			cmd_class : rowsA[i].cmd_class,
			cmd_class_param : rowsA[i].cmd_class_param,
			command_line : rowsA[i].command_line,
			stop_command_line : rowsA[i].stop_command_line,
			param_list_id : rowsA[i].param_list_id,
			command_param : rowsA[i].command_param,
			check_cmd_class : rowsA[i].check_cmd_class,
			check_cmd_class_param : rowsA[i].check_cmd_class_param,
			check_cmd_line : rowsA[i].check_cmd_line,
			check_cmd_line_param : rowsA[i].check_cmd_line_param,
			status_cmd_class : rowsA[i].status_cmd_class,
			status_cmd_class_param : rowsA[i].status_cmd_class_param,
			status_cmd_line : rowsA[i].status_cmd_line,
			status_cmd_line_param : rowsA[i].status_cmd_line_param,
			check_timeout : rowsA[i].check_timeout,
			run_in_paused_state : rowsA[i].run_in_paused_state,
			template_id : rowsA[i].template_id,
			email_to : rowsA[i].email_to,
			email_cc : rowsA[i].email_cc,
			email_subject : rowsA[i].email_subject,
			email_body : rowsA[i].email_body,
			email_attachment : rowsA[i].email_attachment,
			email_auto : rowsA[i].email_auto,
			command_line_start : rowsA[i].command_line_start,
			host_id : rowsA[i].host_id,
			real_host_id : rowsA[i].real_host_id,
			host_name : rowsA[i].host_name,
			wm_status: rowsA[i].wm_status
		}

		if (command.cmd_class != undefined && command.cmd_class.indexOf('ShellCommand') > 0) {
			var parts = command.cmd_class_param.split('"');
			if (parts.length > 1)
				addIfNotIn(funct.shells, parts[1], addShell, command.real_host_id);
		}
		if (command.check_cmd_class != undefined && command.check_cmd_class.indexOf('ShellCheckCommand') > 0) {
			var parts = command.check_cmd_class_param.split('"');
			if (parts.length > 1)
				addIfNotIn(funct.shells, parts[1], addShell, command.real_host_id)
		}
		if (command.status_cmd_class != undefined && command.status_cmd_class.indexOf('ShellStatusCommand') > 0) {
			var parts = command.status_cmd_class_param.split('"');
			if (parts.length > 1)
				addIfNotIn(funct.shells, parts[1], addShell, command.real_host_id)
		}
		if (command.param_list_id != "" && command.param_list_id != null && command.param_list_id != "ALL") { //allos feltetelt kivenni
			addIfNotIn(funct.paramLists, command.param_list_id, addParamList, command.real_host_id)
		}
    if (!findRecord(command, funct.commands, "id")) {
      funct.commands.push(command);
    }
	}
	}catch (err)
	{
 		 logData('include/controller_groups.asp: ' + err.message);
 		 Response.Write("{success:false,error:'" + err.message + "'}");
	}
}

function findRecord(item, list, field) {
  for(var i=0;i<list.length;i++) {
    if (item[field] == list[i][field])
      return true;
  }
  return false;
}

function addIfNotIn(list, id, funct, host_id) {
	for(var i=0; i<list.length; i++) {
		if (list[i].id == id)
			return;
	}
	funct(list, id, host_id);
}

function addShell(list, id, host_id) {
	var sql= 'SELECT * FROM Configuration.Shell WHERE id = ' + id + ' AND host_id = ' + host_id;
	var rowsB = runSQL(sql,cAcc,"fulltable");
	var result = {};
	if (rowsB.length == 1) {
		result.id = rowsB[0].id;
		result.host_id = rowsB[0].host_id;
		result.version = rowsB[0].version;
		result.name = rowsB[0].name;
		result.shell_cmd = rowsB[0].shell_cmd;
		var sql= 'SELECT command FROM Configuration.ShellItem WHERE shell_id = ' + rowsB[0].shell_id + ' order by [order]';
		var rowsC = runSQL(sql,cAcc,"fulltable");
		result.commands = []
		for(var i=0; i < rowsC.length; i++) {
			result.commands.push(rowsC[i].command);
		}
	}
	list.push(result);
}

function addParamList(list, id, host_id) {
	var sql= 'SELECT * FROM Configuration.ParamList WHERE id = ' + id + ' AND host_id = ' + host_id;
	var rowsB = runSQL(sql,cAcc,"fulltable");
	var result = {};
	if (rowsB.length == 1) {
		result.id = rowsB[0].id;
		result.host_id = rowsB[0].host_id;
		result.version = rowsB[0].version;
		result.name = rowsB[0].name;
		var sql= 'SELECT item, wm_status, wm_key FROM Configuration.ParamListItem WHERE param_list_id = ' + rowsB[0].param_list_id + ' order by [order]';
		var rowsC = runSQL(sql,cAcc,"fulltable");
		result.items = []
		for(var i=0; i < rowsC.length; i++) {
			result.items.push({ value: rowsC[i].item, wm_key: rowsC[i].wm_key, wm_status: rowsC[i].wm_status});
		}
	}
	list.push(result);
}

function getIs_cob(funct, fid) {
try{
	funct.is_cob = []
	var sql= 'SELECT is_cob FROM Configuration.ControllerFunction where id = ' + fid;
	var rowsA = runSQL(sql,cAcc,"fulltable");

	for (var i=0; i<rowsA.length; i++) {
		funct.is_cob= rowsA[i].is_cob==1;
	}
	} catch (err)
	{
 		 logData('include/controller_groups.asp: ' + err.message);
 		 Response.Write("{success:false,error:'" + err.message + "'}");
	}
}


%>