<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="include/JSON.asp"-->
	<!--#include file="include/get_db_data.asp"-->
	<!--#include file="security/security.asp"-->
	<!--#include file="include/util.asp"-->

	<%

try {
    if(CheckRight(SEC_ROLE_LOGIN, null) && FindRights([SEC_ROLE_ENVMAN_CONFIG, SEC_ROLE_ENVMAN_VIEW]))
	{
		sql = "SELECT tz.timezone, tz.jbase_timezone, env.name as environment, host.name AS host FROM Configuration.Timezones tz inner join Configuration.Environments env ON tz.environment_id = env.id inner join Configuration.Hosts host ON tz.host_id = host.id";

		rowsA = runSQL(sql,cAcc,"fulltable");

		var myJSON = new JSON();
		var data = ''
		var fields = ''
		if (rowsA.length)  {
			data = myJSON.toJSON(null, rowsA, false); 
			fields = myJSON.generateFields(rowsA)
		}
		else {
			data = '[]'
			fields = '[]'
		}

		Response.Write( '{ "metaData": {	"totalProperty" : "total", "root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '", "results":' + data + '}');
    }
}
catch (err) {
	logData(err.message);
	Response.Write('Error: ' + err.message);
}

%>