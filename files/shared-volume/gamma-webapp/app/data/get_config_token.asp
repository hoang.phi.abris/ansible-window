<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="include/JSON.asp"-->
	<!--#include file="include/get_db_data.asp"-->
	<!--#include file="security/security.asp"-->

	<%
try {
	if(CheckRight(SEC_ROLE_LOGIN, null)) {
		var token_type = String(Request("token_type"));
		var sub_type = String(Request("sub_type"));
		var browser_id = String(Request("browser_id"));
		var timeout = String(Request("timeout"));
		var userID = Session("username");

		var sql= "DELETE FROM Configuration.TokenData WHERE DATEDIFF(minute, timestamp, GETDATE()) > " + timeout + " AND token_type = " + token_type + " AND sub_type = " + sub_type;
		rowsA = runSQL(sql,cAcc,"cmd");

		sql= "SELECT user_name FROM Configuration.TokenData WHERE token_type = " + token_type + " AND sub_type = " + sub_type;
		rowsA = runSQL(sql,cAcc,"fulltable");

		try
		{
			if(rowsA.length == 0) {
				sql = "INSERT INTO Configuration.TokenData (token_type, sub_type, user_name, browser_id, timestamp, status) VALUES(" + token_type + ", " + sub_type + ", '" + userID + "', '" + browser_id + "', GETDATE(), 0)";
				runSQL(sql,cAcc,"cmd");
			}
		} 
		catch (err) {Response.Write('Error: ' + err.message);}

		var sql= "SELECT user_name, browser_id, status FROM Configuration.TokenData WHERE token_type = " + token_type + " AND sub_type = " + sub_type;
		rowsA = runSQL(sql,cAcc,"fulltable");
		
		var myJSON = new JSON();
		var data = ''
		var fields = ''
		if (rowsA.length)  {
			data = myJSON.toJSON(null, rowsA, false); 
			fields = myJSON.generateFields(rowsA)
		}
		else {
			data = '[]'
			fields = '[]'
		}

		Response.Write( '{ "metaData": {	"totalProperty" : "total", "root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '", "results":' + data + '}');
	}
} 
catch (err) {
  Response.Write('Error: ' + err.message);
    logData('get_config_token.asp: ' + err.message);
}

%>