
<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT" %>

<!--#include file="../include/JSON.asp"-->
<!--#include file="../include/get_db_data.asp"-->
<!--#include file="../include/util.asp"-->
<!--#include file="../security/security.asp"-->
<!--#include file="../diagram/statisticsUtil.asp"-->

<%
/* configre extra url request header properties */
try{
	if(CheckRight(SEC_ROLE_LOGIN, null) && FindRight(SEC_ROLE_PORTAL_STATISTICS)) {

		var host = Request("host");
		var env = Request("environment");
		var observer = "DISK.USAGE.CHECK";
		var min_time_stamp = isset(Request("min_time_stamp")) ? Request("min_time_stamp") : "";
		var max_time_stamp = isset(Request("max_time_stamp")) ? Request("max_time_stamp") : "";
		var	rec_count = Request("rec_count");
		var	archive_db = Request("archive_db");
		var loadSteps = isset(Request("loadSteps")) ? parseInt(Request("loadSteps")) : 0;
		var useArchive = isset(Request("useArchive")) ? parseInt(Request("useArchive")) : 0;



		sql = "select top 1 CONVERT(VARCHAR(19),timestamp,120) as min_time from measurements where observer = '"+observer+"' order by timestamp asc";

		var db = "";
		var minDates = {liveDbMinTime: null, archDbMinTime: null};
		minDates = getMinDatesLiveAndArch(sql);
		var intarvalInArchive = false;
		if(loadSteps == 0){
			if(minDates.liveDbMinTime == null){
				useArchive = true;
			}
		}
		if(useArchive){
			db =  archive_db + ".";
		} else  if(getTimestampFromDate(String(min_time_stamp)) < minDates['liveDbMinTime']) {
			intarvalInArchive=true
		}


		var div_seconds = getDivSec(min_time_stamp, max_time_stamp, rec_count, useArchive,observer);

		sql = "SELECT datestamp, yvalue, REPLACE(volume,'.','') as volume  "
			+" FROM ("
			+" 	SELECT datestamp, avg(CAST( SUBSTRING(capacity, 0, LEN(capacity)) AS INT)) as 'yvalue', volume      "
			+" 	FROM ("
			+" 		SELECT  dbo.Timestamp_to_interval_time( "
			+"			m.timestamp, "
			+"			'" + min_time_stamp + "', "
			+"			'" + max_time_stamp + "', "
			+"			" + div_seconds + ") AS 'datestamp', "
			+"		id, host FROM " + db + "dbo.measurements m"
			+" 		left join (select master_environment_name, name, multicountry from Configuration.Environments) e"
			+" 		on name = '" + env + "' and e.multicountry = 1"
			+" 		WHERE (environment = '" + env + "' OR environment=e.master_environment_name) and observer = 'DISK.USAGE.CHECK'"
			+"		and m.timestamp between '" + min_time_stamp + "' and '" + max_time_stamp + "'"
			+" 	  ) m2 "
			+" 	INNER JOIN " + db + "observer.[DISK.USAGE.CHECK] epo ON epo.measurement_id = m2.id "
			+" 	WHERE (m2.host = '" + host + "' OR (m2.host = 'MULTISERVER' AND epo.host = '" + host + "'))"
			+" 	GROUP BY datestamp, volume"
			+"   ) m3 "
			+"   LEFT JOIN Configuration.events e on m3.datestamp = convert(varchar, e.timestamp, 102)"
			+" where volume is not null ORDER BY datestamp ASC";


		rowsA = runSQL(sql,cAcc,"fulltable");



		var rowsarch = [];
		if(intarvalInArchive && rowsA.length < 100) {
			db =  archive_db + ".";
			sql = "SELECT datestamp, yvalue, REPLACE(volume,'.','') as volume  "
				+" FROM ("
				+" 	SELECT datestamp, avg(CAST( SUBSTRING(capacity, 0, LEN(capacity)) AS INT)) as 'yvalue', volume      "
				+" 	FROM ("
				+" 		SELECT  dbo.Timestamp_to_interval_time( "
				+"			m.timestamp, "
				+"			'" + min_time_stamp + "', "
				+"			'" + max_time_stamp + "', "
				+"			" + div_seconds + ") AS 'datestamp', "
				+"		id, host FROM " + db + "dbo.measurements m"
				+" 		left join (select master_environment_name, name, multicountry from Configuration.Environments) e"
				+" 		on name = '" + env + "' and e.multicountry = 1"
				+" 		WHERE (environment = '" + env + "' OR environment=e.master_environment_name) and observer = 'DISK.USAGE.CHECK'"
				+"		and m.timestamp between '" + min_time_stamp + "' and '" + max_time_stamp + "'"
				+" 	  ) m2 "
				+" 	INNER JOIN " + db + "observer.[DISK.USAGE.CHECK] epo ON epo.measurement_id = m2.id "
				+" 	WHERE (m2.host = '" + host + "' OR (m2.host = 'MULTISERVER' AND epo.host = '" + host + "'))"
				+" 	GROUP BY datestamp, volume"
				+"   ) m3 "
				+"   LEFT JOIN Configuration.events e on m3.datestamp = convert(varchar, e.timestamp, 102)"
				+" ORDER BY datestamp ASC";

				rowsarch = runSQL(sql,cAcc,"fulltable");
			}
			while(rowsarch.length>0&&rowsA.unshift(rowsarch.pop())<100);



		var flags = [],
            volumes = [],
            times = [],
            i;

		for ( i in rowsA)
		{
			if(rowsA[i].datestamp != ""){
				rowsA[i].datestamp = getTimestampFromDate(rowsA[i].datestamp);
			}

			if (rowsA[i].volume.indexOf(":\\") >= 0)
                        rowsA[i].volume = rowsA[i].volume.substring(0, rowsA[i].volume.indexOf(":\\"))
            if (flags[rowsA[i].volume])
            {}
            else
            {
                flags[rowsA[i].volume] = true;
                volumes.push(rowsA[i].volume);
            }
            if (flags[rowsA[i].datestamp])
            {}
            else
            {
                flags[rowsA[i].datestamp] = true;
                var tdate = new Date(rowsA[i].datestamp);
                times.push(tdate);
            }
		}


		var rowsB = rowsA;

        for (var xx = 0; xx < times.length; xx++)
        {
            for (var j = 0; j < volumes.length; j++)
            {
                rowsB[xx][volumes[j]] = 0;
            }

            var c = new Date(times[xx])
            c = c.getTime();

            for (var a = 0; a < rowsA.length; a++)
            {
                var d = new Date(rowsA[a].datestamp);
                d = d.getTime();


                if (c === d)
                {
                    rowsB[xx][rowsA[a].volume] = parseInt(rowsA[a].yvalue);
                }
            }

            rowsB[xx].datestamp = c;

        }


        rowsA = [];
        rowsA = rowsB.splice(0, times.length);

		var myJSON = new JSON();
		var data = myJSON.toJSON(null, rowsA, false);
		var fields = myJSON.generateFields(rowsA);

		//fields = myJSON.toJSON(null, fields, false);

		Response.Write( '{ "metaData": { "totalProperty" : "total", 	"root" : "results", "id" : "id",  "liveDbMinTime" : "' + minDates.liveDbMinTime + '", "archDbMinTime" : "' + minDates.archDbMinTime + '", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');

	}
}catch (err)
{
    Response.Write("{success:failed, error:'" + err.message +  "' } " + sql);
    logData('app/data/diagram/getDiskUsageStat.asp: ' + err.message);
}
%>
