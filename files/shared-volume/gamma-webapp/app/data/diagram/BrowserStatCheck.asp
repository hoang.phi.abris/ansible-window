<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->
	<!--#include file="../include/util.asp"-->
	<!--#include file="../diagram/statisticsUtil.asp"-->

	<%
try{
	Response.Buffer = false;
	var rec_count = Request("rec_count");
	var ctype = Request("ctype");
	var bcid = isset(Request("targetId")) ? String(Request("targetId")) : ""
	var observer = "BROWSER.CONNECTIVITY";

	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_PORTAL_STATISTICS, env)) {
		var data = [];
		var rowsA = [];
		var instances = [];
		var configData = [];
		var config = [];

		getInstances = "select host, name from Configuration.Instances where environment = '" + env + "'"
		instances = runSQL(getInstances,cAcc,"fulltable");

		for(var i=0; i < instances.length; i++) {
			getConfigSQL = "exec [observer].[getConfig.BROWSER.CONNECTIVITY] @host='" + instances[i].host + "', @instance='" + instances[i].name + "'"
			rowsA = runSQL(getConfigSQL,cAcc,"fulltable");
			for(var c=0; c < rowsA.length; c++) {
				configData.push(rowsA[c]);
			}
		}
		var myJSON = new JSON();
		config 	= myJSON.toJSON(null, configData, false);

		if ((configData.length > 0 ) && bcid == "")
		   bcid = configData[0].desc


		var min_time_stamp = isset(Request("min_time_stamp")) ? String(Request("min_time_stamp")): '' ;
		var max_time_stamp = isset(Request("max_time_stamp")) ? String(Request("max_time_stamp")): '' ;
		var	rec_count = Request("rec_count");
		var	archive_db = Request("archive_db");
		var	loadSteps = Request("loadSteps");
		var loadSteps = isset(Request("loadSteps")) ? parseInt(Request("loadSteps")) : 0;
		var useArchive = isset(Request("useArchive")) ? parseInt(Request("useArchive")) : 0;

		sql = "select top 1 CONVERT(VARCHAR(19),timestamp,120) as min_time from measurements where observer = '"+observer+"' order by timestamp asc";

		var db = "";
		var minDates = {liveDbMinTime: null, archDbMinTime: null};
		minDates = getMinDatesLiveAndArch(sql);
		var intarvalInArchive = false;
		if(loadSteps == 0){
			if(minDates.liveDbMinTime == null){
				useArchive = true;
			}
		}
		if(useArchive){
			db =  archive_db + ".";
		} else  if(getTimestampFromDate(String(min_time_stamp)) < minDates['liveDbMinTime']) {
			intarvalInArchive=true
		}

		var div_seconds = getDivSec(min_time_stamp, max_time_stamp, rec_count, useArchive,observer);

		sql  = "SELECT datestamp, total, text, title "
		sql += "FROM ( "
		sql += "	SELECT datestamp, avg(total) as 'total' "
		sql += "	FROM ( "
		sql += " 		SELECT  dbo.timestamp_to_interval_time( "
		sql += "			m.timestamp, "
		sql += "			'" + min_time_stamp + "', "
		sql += "			'" + max_time_stamp + "', "
		sql += "			" + div_seconds + ") AS 'datestamp', "
		sql += "		 id FROM " + db + "dbo.measurements m left join (select master_environment_name, name, multicountry from Configuration.Environments) e  on name = '" + env + "' and e.multicountry = 1"
		sql += "		WHERE (environment = '" + env + "' OR environment=e.master_environment_name) AND observer = 'BROWSER.CONNECTIVITY'"
		sql += "		and m.timestamp between '" + min_time_stamp + "' and '" + max_time_stamp + "'"
		sql += "	  ) m2 "
		sql += "	INNER JOIN " + db + "observer.[BROWSER.CONNECTIVITY] epo "
		sql += "	ON epo.measurement_id = m2.id "
		sql += "	WHERE epo.[description] = '" + bcid + "' "
		sql += "	GROUP BY datestamp "
		sql += "  ) m3 "
		sql += "  LEFT JOIN Configuration.events e on m3.datestamp = convert(varchar, e.timestamp, 102) "
		sql += "ORDER BY datestamp ASC "


		rowsA = runSQL(sql,cAcc,"fulltable");
		var myJSON = new JSON();

		var rowsB = [];
		if(intarvalInArchive && rowsA.length < 100) {
			db =  archive_db + ".";
			sql  = "SELECT datestamp, total, text, title "
			sql += "FROM ( "
			sql += "	SELECT datestamp, avg(total) as 'total' "
			sql += "	FROM ( "
			sql += " 		SELECT  dbo.timestamp_to_interval_time( "
			sql += "			m.timestamp, "
			sql += "			'" + min_time_stamp + "', "
			sql += "			'" + max_time_stamp + "', "
			sql += "			" + div_seconds + ") AS 'datestamp', "
			sql += "		 id FROM " + db + "dbo.measurements m left join (select master_environment_name, name, multicountry from Configuration.Environments) e  on name = '" + env + "' and e.multicountry = 1"
			sql += "		WHERE (environment = '" + env + "' OR environment=e.master_environment_name) AND observer = 'BROWSER.CONNECTIVITY'"
			sql += "		and m.timestamp between '" + min_time_stamp + "' and '" + max_time_stamp + "'"
			sql += "	  ) m2 "
			sql += "	INNER JOIN " + db + "observer.[BROWSER.CONNECTIVITY] epo "
			sql += "	ON epo.measurement_id = m2.id "
			sql += "	WHERE epo.[description] = '" + bcid + "' "
			sql += "	GROUP BY datestamp "
			sql += "  ) m3 "
			sql += "  LEFT JOIN Configuration.events e on m3.datestamp = convert(varchar, e.timestamp, 102) "
			sql += "ORDER BY datestamp ASC "

			rowsB = runSQL(sql,cAcc,"fulltable");
		}
		while(rowsB.length>0&&rowsA.unshift(rowsB.pop())<100);


		var series = []

		for ( item in rowsA)
		{
			if(rowsA[item].datestamp != ""){
				rowsA[item].datestamp = getTimestampFromDate(rowsA[item].datestamp);
			}
		}


		var myJSON = new JSON();
		data 		= myJSON.toJSON(null, rowsA, false);
		fields 	= myJSON.generateFields(rowsA)

		Response.Write( '{ "metaData": { "totalProperty" : "total", 	"root" : "results", "id" : "id", "liveDbMinTime" : "' + minDates.liveDbMinTime + '", "archDbMinTime" : "' + minDates.archDbMinTime + '", "config":' + config +'}, "total":"' + rowsA.length + '", "results":' + data + '  }');

	}
}catch (err)
{
 logData('diagram/BrowserStatCheck.asp: ' + err.message);
  Response.Write("{success:false,error:'" + err.message + "'}");
}




%>