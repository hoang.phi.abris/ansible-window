<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
try{
	var rec_count = Request("rec_count");
	var environment = Request("environment");
	var name = Request("name");

	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_PORTAL_REALTIME, environment)) {
		sql  = "SELECT timestamp, count(*) as 'yvalue' FROM observer.[MQ.DEPTH] md INNER JOIN measurements m "
		sql += "ON md.measurement_id = m.id "
		sql += "WHERE measurement_id IN ( "
		sql += "	SELECT top " + rec_count + " id "
		sql += "	FROM measurements left join (select master_environment_name, name, multicountry from Configuration.Environments) e  on name = '" + environment + "' and e.multicountry = 1 "
		sql += "	WHERE (environment = '" + environment + "' OR environment=e.master_environment_name) AND observer = 'MQ.DEPTH.OBSERVER' "
		sql += "	ORDER BY timestamp DESC) "
		if (name != 'All channels')
			sql += "	AND md.mq_id = '" + name + "' "
		sql += "GROUP BY timestamp "
		sql += "ORDER BY timestamp ASC "

		var fields = []
		var data = []
		var rowsA = []

		rowsA = runSQL(sql,cAcc,"fulltable");

		for ( item in rowsA)
		{
			rowsA[item].timestamp = formatDateTimeToHighChart(rowsA[item].timestamp)
		}

		var myJSON = new JSON();
		data 		= myJSON.toJSON(null, rowsA, false); 
		fields 	= myJSON.generateFields(rowsA)

		//logData('({ "metaData": { "sql":"' + sql + '",	"totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '})')

		Response.Write( '{ "metaData": { "totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');
	}
}catch (err) 
{
 logData('diagram/mq_chennel.asp: ' + err.message);
  Response.Write("{success:false,error:'" + err.message + "'}");
} 
%>