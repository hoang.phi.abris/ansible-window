<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
try{
	var rec_count = Request("rec_count");
	var environment = Request("environment");
	var host = Request("host");
	var min_time_stamp = isset(Request("min_time_stamp")) ? Request("min_time_stamp") : "";
	var max_time_stamp = isset(Request("max_time_stamp")) ? Request("max_time_stamp") : "";

	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_PORTAL_REALTIME, environment)) {


		var sort = "desc";

		if(min_time_stamp != ""){
			min_time_stamp = " and timestamp > '"+min_time_stamp+"' ";
			sort = "asc";
		}

		if(max_time_stamp != ""){
			max_time_stamp = " and timestamp < '"+max_time_stamp+"' ";
			sort = "desc";
		}

		sql  = "SELECT timestamp, isnull(occurence,0) as 'yvalue' FROM observer.[TSA.RESTART.ANALYSER] epo INNER JOIN measurements m "
		sql += "ON epo.measurement_id = m.id "
		sql += "WHERE measurement_id IN ( "
		sql += "	SELECT top " + rec_count + " id "
		sql += "	FROM measurements left join (select master_environment_name, name, multicountry from Configuration.Environments) e  on name = '" + environment + "' and e.multicountry = 1"
		sql += "	WHERE (environment = '" + environment + "' OR environment=e.master_environment_name) " + max_time_stamp + min_time_stamp + " AND observer = 'TSA.RESTART.ANALYSER' "
		sql += "	ORDER BY timestamp "+sort+") "
		sql += "  AND ((m.host = 'MULTISERVER' AND epo.host = '" + host + "') OR (m.host = '" + host + "')) "
		sql += "ORDER BY timestamp ASC "

		var fields = []
		var data = []
		var rowsA = []


		rowsA = runSQL(sql,cAcc,"fulltable");

		for ( item in rowsA)
		{
			rowsA[item].timestamp = formatDateTimeToHighChart(rowsA[item].timestamp)
		}

		var myJSON = new JSON();
		data 		= myJSON.toJSON(null, rowsA, false); 
		fields 	= myJSON.generateFields(rowsA)

		Response.Write( '{ "metaData": { "totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');
	}
}catch (err) 
{
  logData('diagram/tsa_restart_analyser.asp: ' + err.message);
  Response.Write("{success:false,error:'" + err.message + "'}");
} 
%>