<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

<!--#include file="../include/JSON.asp"-->
<!--#include file="../include/get_db_data.asp"-->
<!--#include file="../security/security.asp"-->
<%
//<script>
try{
	var environment = Request("environment");
    var lastDate = String(Request("lastDate"));
    var order = String(Request("order"));

    
	
	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_PORTAL_REALTIME, environment)) {
        sqlOrder = (order=='prev') ? 'DESC' : 'ASC';
        sql = "SELECT  state, (SELECT name FROM Security.[User] WHERE id = user_id) as user_name, [timestamp], [text], user_id, closed_day, environment, next_day " +
            " FROM Cob.note cn, cob.cob cc "+
            " WHERE cc.id = cn.cob_id and job_id is null and "+
            " environment = '" + environment + "' and "+
            " closed_day = ";
        if(lastDate=='')
            lastDaySql = "SELECT top 1 closed_day FROM cob.cob "+
                " WHERE environment = '" + environment + "' ORDER BY closed_day DESC"; 
        else
            lastDaySql = " SELECT top 1 closed_day FROM cob.cob "+
                " WHERE environment = '" + environment + "' and closed_day "+(sqlOrder=='DESC'? '<': '>')+" '" + lastDate + "' ORDER BY closed_day "+ sqlOrder;
        
        sql += '(' + lastDaySql + ') ORDER BY timestamp desc';
        rowsA = runSQL(sql,cAcc,"fulltable");

        if(rowsA.length == 0) {
            rowsA = runSQL(lastDaySql,cAcc,"fulltable");
            sql = lastDaySql
            if(sqlOrder=='ASC'&&rowsA.length==0) {
                sql = "SELECT * ,(SELECT name FROM Security.[User] WHERE id = user_id) as user_name"+
		" FROM OCM.Comment "+
		" WHERE "+
            " environment =  '"+environment+"' ORDER BY timestamp desc";
            rowsA = runSQL(sql,cAcc,"fulltable");
            }
        }
        var myJSON = new JSON();
        data = myJSON.toJSON(null, rowsA, false);		
        Response.Write( '{ "metaData": { "sql":"' + sql + '",	"totalProperty" : "total", 	"root" : "results", "id" : "id"}, "total":"' + rowsA.length + '","results":' + data + '}');
    }
} catch (err) {
  Response.Write("{success:false,error:'" + err.message + "'}");
}

%>
