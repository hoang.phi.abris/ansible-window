<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

    <!--#include file="../include/JSON.asp"-->
    <!--#include file="../include/get_db_data.asp"-->
    <!--#include file="../security/security.asp"-->

    <script runat=server language=JavaScript>
        try {
            if (CheckRight(SEC_ROLE_LOGIN, null) && FindRight(SEC_ROLE_PROTOCOL_VIEW)) {
                var environment = String(Request("environment"));
                var rec_count = isset(Request("rec_count")) ? "  top " + Request("rec_count") + " " : " ";
                var host = String(Request("host"));

                var min_time_stamp = isset(Request("min_time_stamp")) ? Request("min_time_stamp") : "";
                var max_time_stamp = isset(Request("max_time_stamp")) ? Request("max_time_stamp") : "";
                var process = isset(Request("process")) ? "  AND process = '" + String(Request("process")) + "' " : '';
                var sql = "";
                var sort = "desc";

                if (min_time_stamp != "") {
                    min_time_stamp = " and timestamp > '" + min_time_stamp + "' ";
                    sort = "asc";
                }
                if (max_time_stamp != "") {
                    max_time_stamp = " and timestamp < '" + max_time_stamp + "' ";
                    sort = "desc";
                }
                if (process == '' || process == "  AND process = '" + '' + "' ") {

                    sql = "SELECT DISTINCT process_name AS process, mhost AS host ";
                    sql += "FROM ( ";
                    sql += "SELECT TOP 1000 o.*, m.host AS mhost ";
                    sql += "FROM measurements m ";
                    sql += "JOIN observer.[SERVER.PERFORMANCE.MONITOR] o ON m.id = o.measurement_id ";
                    sql += "WHERE m.environment IN ( ";
                    sql += "SELECT '" + environment + "' ";
                    sql += "UNION ";
                    sql += "SELECT master_environment_name ";
                    sql += "FROM configuration.environments ";
                    sql += "WHERE NAME = '" + environment + "' ";
                    sql += ") ";
                    sql += "AND m.observer = 'SERVER.PERFORMANCE.MONITOR' ";
                    sql += "ORDER BY TIMESTAMP DESC ";
                    sql += ") t ";
                    sql += "WHERE process_name IS NOT NULL ";
                    sql += "ORDER BY process";



                    var rowsA = runSQL(sql, cAcc, "fulltable");
                    var myJSON = new JSON();

                    var hosts = host.split("'")
                    for (i = 0; i < rowsA.length; i++) {
                        rowsA[i].host = rowsA[i].host.toLowerCase();
                    }
                    for (var ind = 1; ind < hosts.length - 1; ind += 2) {
                        var tmp = {
                            process: 'ALL',
                            host: hosts[ind].toLowerCase()
                        }
                        rowsA.unshift(tmp);
                    }

                } else {
                    sql = null;
                    sql = " Select cpu, process_name as process, timestamp, measurements.host as host "
                    sql += "   From measurements INNER JOIN observer.[SERVER.PERFORMANCE.MONITOR] ON id = measurement_id  "
                    sql += "           AND measurements.observer = 'SERVER.PERFORMANCE.MONITOR'      "
                    sql += "   Where timestamp IN    "
                    sql += "           (SELECT " + rec_count + "  timestamp from ( SELECT DISTINCT timestamp   "
                    sql += "            From observer.[SERVER.PERFORMANCE.MONITOR], measurements    "
                    sql += "            WHERE id = measurement_id    "
                    sql += "               AND environment IN (SELECT '" + environment + "' "
                    sql += "                            UNION "
                    sql += "                            SELECT master_environment_name "
                    sql += "                            FROM   configuration.environments "
                    sql += "                            WHERE  name = '" + environment + "') "
                    sql += "              AND ((measurements.host = 'MULTISERVER' AND observer.[SERVER.PERFORMANCE.MONITOR].host = '" + host + "') OR (measurements.host = '" + host + "')) "
                    sql += "              AND measurements.observer = 'SERVER.PERFORMANCE.MONITOR'"
                    sql += max_time_stamp
                    sql += min_time_stamp
                    sql += "         ) t  ORDER BY timestamp " + sort
                    sql += " )  "
                    sql += "       AND environment IN (SELECT '" + environment + "' "
                    sql += "                            UNION "
                    sql += "                            SELECT master_environment_name "
                    sql += "                            FROM   configuration.environments "
                    sql += "                            WHERE  name = '" + environment + "') "
                    sql += "      AND ((measurements.host = 'MULTISERVER' AND observer.[SERVER.PERFORMANCE.MONITOR].host = '" + host + "') OR (measurements.host = '" + host + "')) "
                    sql += "   Order by timestamp  asc "

                    var rowsA = runSQL(sql, cAcc, "fulltable");
                    var myJSON = new JSON();

                    var flags = [],
                        processes = [],
                        times = [],
                        i;
                    for (i = 0; i < rowsA.length; i++) {
                        if (flags[rowsA[i].process]) { }
                        else {
                            flags[rowsA[i].process] = true;

                            processes.push(rowsA[i].process);
                        }
                        if (flags[rowsA[i].timestamp]) { }
                        else {
                            flags[rowsA[i].timestamp] = true;
                            var tdate = new Date(rowsA[i].timestamp);
                            times.push(tdate);
                        }

                    }

                    var rowsB = rowsA;

                    for (var xx = 0; xx < times.length; xx++) {
                        for (var j = 0; j < processes.length; j++) {
                            rowsB[xx][processes[j]] = 0;
                        }

                        var c = new Date(times[xx])
                        c = c.getTime();

                        for (var a = 0; a < rowsA.length; a++) {
                            var d = new Date(rowsA[a].timestamp);
                            d = d.getTime();

                            if (c === d) {
                                if (a % 2 == 0)
                                    rowsB[xx][rowsA[a].process] = parseInt(rowsA[a].cpu);
                                else
                                    rowsB[xx][rowsA[a].process] = parseInt(rowsA[a].cpu);
                            }
                        }

                        rowsB[xx].timestamp = c;

                    }
                    rowsA = [];
                    rowsA = rowsB.splice(0, times.length);
                }

                var data = myJSON.toJSON(null, rowsA, false);
                var fields = myJSON.generateFields(rowsA);

                Response.Write('{ "metaData": { "totalProperty" : "total",     "root" : "results", "id" : "id",     "fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');
            }
        } catch (err) {
            Response.Write("{success:failed, error:'" + err.message + sql + "'}");
            logData('app/data/diagram/getCpuMonitor.asp: ' + err.message);
        }
    </script>