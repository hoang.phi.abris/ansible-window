
<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT" %>

<!--#include file="../../include/JSON.asp"-->
<!--#include file="../../include/get_db_data.asp"-->
<!--#include file="../../include/util.asp"-->
<!--#include file="../../security/security.asp"-->
<!--#include file="../../diagram/statisticsUtil.asp"-->

<%
//<script>
try {
	if(CheckRight(SEC_ROLE_LOGIN, null) && FindRight(SEC_ROLE_PORTAL_STATISTICS)) {

		var host = Request("host");
		var env = Request("environment");
		var	rec_count = Request("rec_count");
		var	archive_db = Request("archive_db");
		var loadSteps = isset(Request("loadSteps")) ? parseInt(Request("loadSteps")) : 0;
        var useArchive = isset(Request("useArchive")) ? parseInt(Request("useArchive")) : 0;
        var observer = 'T24.ENVIRONMENT';


		var sql = "select top 1 CONVERT(VARCHAR(19),timestamp,120) as min_time from measurements where observer = '"+observer+"' order by timestamp asc";

		var db = "";
		var minDates = getMinDatesLiveAndArch(sql);

        function getSql(_db) {
            var sqlSelect =  "SELECT m3.timestamp, yvalue, limit  "
			+" FROM ("
			+" 	SELECT timestamp, CAST(logged_on_users AS INT) as 'yvalue', max_t24_users as 'limit' "
			+" 	FROM ("
			+" 		SELECT m.timestamp as timestamp, id, host FROM " + _db + "dbo.measurements m"
			+" 		left join (select master_environment_name, name, multicountry from Configuration.Environments) e"
			+" 		on name = '" + env + "' and e.multicountry = 1"
			+" 		WHERE (environment = '" + env + "' OR environment=e.master_environment_name) and observer = '" + observer + "'"
			+"		and m.timestamp BETWEEN DATEADD(hh, -2, GETDATE()) AND GETDATE() "
			+" 	  ) m2 "
			+" 	INNER JOIN " + _db + "observer.[T24.ENVIRONMENT] epo ON epo.measurement_id = m2.id "
			+"   ) m3 "
			+"   LEFT JOIN Configuration.events e on m3.timestamp = convert(varchar, e.timestamp, 102)"
			+" ORDER BY m3.timestamp ASC";
            return sqlSelect;
        }

		sql = getSql(db);
		rowsA = runSQL(sql,cAcc,"fulltable");

		var flags = [],
            volumes = [],
            times = [],
            i;

		for ( item in rowsA) {
			if(rowsA[item].datestamp != "") {
				rowsA[item].timestamp = Date.parse(rowsA[item].timestamp);
			}
		}

		var myJSON = new JSON();
		var data = myJSON.toJSON(null, rowsA, false);
		var fields = myJSON.generateFields(rowsA);

		Response.Write( '{ "metaData": { "totalProperty" : "total", 	"root" : "results", "id" : "id",  "liveDbMinTime" : "' + minDates.liveDbMinTime + '", "archDbMinTime" : "' + minDates.archDbMinTime + '", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');

	}
} catch (err) {
    Response.Write("{success:failed, error:'" + err.message +  "' } " + sql);
    logData('app/data/diagram/getDiskUsageStat.asp: ' + err.message);
}
%>
