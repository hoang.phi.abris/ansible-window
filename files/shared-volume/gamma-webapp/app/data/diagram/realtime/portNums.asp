<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT" %>

<!--#include file="../../include/JSON.asp"-->
<!--#include file="../../include/get_db_data.asp"-->
<!--#include file="../../include/util.asp"-->
<!--#include file="../../security/security.asp"-->
<!--#include file="../../diagram/statisticsUtil.asp"-->

<%
//<script>
// The whole asp is the same as t24Licenses except the sql string
// I do not want to refactor a lot of asp before we get rid of them but hopefully in Java things like this wont happen
// I could create a getStatisticalData.asp and a statisticalPortalUtil.asp which would have the sql-s
// but even the sql-s have a lot of duplication...
try {
	if(CheckRight(SEC_ROLE_LOGIN, null) && FindRight(SEC_ROLE_PROTOCOL_VIEW)) {

		var host = Request("host");
		var env = Request("environment");
		var	rec_count = Request("rec_count");
		var	archive_db = Request("archive_db");
		var loadSteps = isset(Request("loadSteps")) ? parseInt(Request("loadSteps")) : 0;
		var useArchive = isset(Request("useArchive")) ? parseInt(Request("useArchive")) : 0;
        var observer = 'T24.ENVIRONMENT';

		var sql = "select top 1 CONVERT(VARCHAR(19),timestamp,120) as min_time from measurements where observer = '"+observer+"' order by timestamp asc";

		var db = "";
		var minDates = getMinDatesLiveAndArch(sql);

        function getSql(_db) {
            sqlSelect = " SELECT timestamp, yvalue FROM ( " +
            "    SELECT timestamp, CAST([value] AS INT) as 'yvalue'       " +
            "    FROM ( " +
            "        SELECT m.timestamp as timestamp, id, host FROM " + _db + "dbo.measurements m " +
            "        left join (select master_environment_name, name, multicountry from Configuration.Environments) e " +
            "        on name = '" + env + "' and e.multicountry = 1 " +
            "        WHERE (environment = '" + env + "' OR environment=e.master_environment_name) and observer = 'T24.ENVIRONMENT' " +
            "        and m.timestamp BETWEEN DATEADD(hh, -2, GETDATE()) AND GETDATE() " +
            "        ) m2  " +
            "    INNER JOIN " + _db + "dbo.[measurementsOptions] mo ON mo.measurement_id = m2.id  " +
            "    WHERE   mo.[key] = 'port_num_" + host + "' " +
            "    ) m3  " +
			" ORDER BY timestamp ASC";
            return sqlSelect;
        }

		sql = getSql(db);
		rowsA = runSQL(sql,cAcc,"fulltable");

		var flags = [],
            volumes = [],
            times = [],
            i;

		for ( item in rowsA) {
			if(rowsA[item].timestamp != "") {
				rowsA[item].timestamp = Date.parse(rowsA[item].timestamp);
			}
		}

		var myJSON = new JSON();
		var data = myJSON.toJSON(null, rowsA, false);
		var fields = myJSON.generateFields(rowsA);

		Response.Write( '{ "metaData": { "totalProperty" : "total", 	"root" : "results", "id" : "id",  "liveDbMinTime" : "' + minDates.liveDbMinTime + '", "archDbMinTime" : "' + minDates.archDbMinTime + '", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');

	}
} catch (err) {
    Response.Write("{success:failed, error:'" + err.message +  "' } " );
    logData('app/data/diagram/realtime/portNums.asp: ' + err.message);
}
%>
