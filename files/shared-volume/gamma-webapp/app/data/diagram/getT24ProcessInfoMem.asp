<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

    <!--#include file="../include/JSON.asp"-->
    <!--#include file="../include/get_db_data.asp"-->
    <!--#include file="../security/security.asp"-->

    <script runat=server language=JavaScript>
        try {
            var rec_count = Request("rec_count");
            var environment = Request("environment");
            var host = String(Request("host"));
            var min_time_stamp = isset(Request("min_time_stamp")) ? Request("min_time_stamp") : "";
            var max_time_stamp = isset(Request("max_time_stamp")) ? Request("max_time_stamp") : "";

            if (CheckRight(SEC_ROLE_LOGIN, null) && FindRight(SEC_ROLE_PROTOCOL_VIEW)) {
                var port = isset(Request("port")) ? "  AND port = '" + String(Request("port")) + "' " : '';
                var type = String(Request("type"));

                var sql = "";
                var sort = "desc";

                var rowsA;
                var myJSON;

                var splitNumber = 0;

                if (min_time_stamp != "") {
                    min_time_stamp = " and timestamp > '" + min_time_stamp + "' ";
                    sort = "asc";
                }

                if (max_time_stamp != "") {
                    max_time_stamp = " and timestamp < '" + max_time_stamp + "' ";
                    sort = "desc";
                }


                if (port == '' || port == "  AND port = '" + '' + "' ") {


                    rec_count = 'TOP ' + rec_count

                    sql = " SELECT distinct CAST(port as INTEGER) port, measurements.host as host, observer.[mw42.analyser].host AS meaHost "
                    sql += " FROM   observer.[mw42.analyser]   "
                    sql += "       INNER JOIN measurements   "
                    sql += "               ON id = measurement_id  AND measurements.observer = 'MW42.ANALYSER' "
                    sql += "                  AND (measurements.host in (" + host + ") OR (measurements.host = 'MULTISERVER' AND observer.[mw42.analyser].host in (" + host + ")))"
                    sql += " Where timestamp IN (SELECT DISTINCT " + rec_count + " timestamp   "
                    sql += "                                    FROM   measurements   "
                    sql += "                                    WHERE  environment IN (SELECT '" + environment + "' "
                    sql += "                                                           UNION   "
                    sql += "                                                           SELECT   "
                    sql += "                                           master_environment_name   "
                    sql += "                                                           FROM   "
                    sql += "                                           configuration.environments   "
                    sql += "                                                           WHERE  NAME = '" + environment + "') "
                    sql += "                                           AND measurements.observer =   "
                    sql += "                                               'MW42.ANALYSER'   "
                    sql += max_time_stamp
                    sql += min_time_stamp
                    sql += "                                    ORDER  BY timestamp " + sort + ")   "
                    sql += " AND port is not null ORDER BY PORT ASC"

                    rowsA = runSQL(sql, cAcc, "fulltable");
                    myJSON = new JSON();
                    var s2 = sql;

                    var hosts = host.split("'")
                    for (var ind = 1; ind < hosts.length - 1; ind += 2) {
                        tmp = {
                            port: 'ALL',
                            host: hosts[ind],
                            meaHost: null
                        };
                        rowsA.unshift(tmp);
                    }

                    for (i = 0; i < rowsA.length; i++) {
                        if (rowsA[i].host == 'MULTISERVER') {
                            rowsA[i].host = rowsA[i].meaHost;
                            rowsA[i].meaHost = 'MULTISERVER';
                        }
                    }

                }
                else if (port == "  AND port = 'ALL' ") {

                    rec_count = 'TOP ' + rec_count

                    sql = " SELECT port,  "
                    sql += "       [read], write, [open], mem, cpu,   "
                    sql += "       timestamp,   "
                    sql += "      prog   "
                    sql += " FROM   observer.[mw42.analyser]   "
                    sql += "       INNER JOIN measurements   "
                    sql += "               ON id = measurement_id  AND measurements.observer = 'MW42.ANALYSER' "
                    sql += "                  AND (measurements.host = '" + host + "' OR (measurements.host = 'MULTISERVER' AND observer.[mw42.analyser].host = '" + host + "'))"
                    sql += " Where timestamp IN (SELECT DISTINCT " + rec_count + " timestamp   "
                    sql += "                                    FROM   measurements   "
                    sql += "                                    WHERE  environment IN (SELECT '" + environment + "' "
                    sql += "                                                           UNION   "
                    sql += "                                                           SELECT   "
                    sql += "                                           master_environment_name   "
                    sql += "                                                           FROM   "
                    sql += "                                           configuration.environments   "
                    sql += "                                                           WHERE  NAME = '" + environment + "') "
                    sql += "                                           AND measurements.observer =   "
                    sql += "                                               'MW42.ANALYSER'   "
                    sql += max_time_stamp
                    sql += min_time_stamp
                    sql += "                                    ORDER  BY timestamp " + sort + ")   "
                    sql += " AND port is not null order by timestamp "
                    //" GROUP BY timestamp "
                    //sql += " ORDER  BY timestamp ASC   "


                    rowsA = runSQL(sql, cAcc, "fulltable");


                    var flags = [],
                        ports = [],
                        times = [],
                        i;
                    for (i = 0; i < rowsA.length; i++) {
                        if (flags[rowsA[i].port]) { }
                        else {
                            flags[rowsA[i].port] = true;
                            ports.push(rowsA[i].port);
                        }
                        if (flags[rowsA[i].timestamp]) { }
                        else {
                            flags[rowsA[i].timestamp] = true;
                            var tdate = new Date(rowsA[i].timestamp);
                            times.push(tdate);
                        }

                    }

                    var rowsB = rowsA;

                    for (var xx = 0; xx < times.length; xx++) {

                        rowsB[xx].prog = '';
                        for (var j = 0; j < ports.length; j++) {
                            rowsB[xx][ports[j]] = 0;
                        }

                        var c = new Date(times[xx])
                        c = c.getTime();

                        for (var a = 0; a < rowsA.length; a++) {
                            var d = new Date(rowsA[a].timestamp);
                            d = d.getTime();

                            if (c === d) {

                                switch (type) {
                                    case 'mem':
                                    
                                    	if (rowsA[a].mem =='N/A'){
                                    	 	 rowsB[xx][rowsA[a].port] =0;
                                    	}
                                        else if (rowsA[a].mem.indexOf('K') > -1) {
                                            var val = parseFloat(rowsA[a].mem) * 1024;
                                            if (val == NaN) {
                                                val = 0;
                                            }
                                            rowsB[xx][rowsA[a].port] = val;
                                        }
                                        else if (rowsA[a].mem.indexOf('M') > -1) {
                                            var val = parseFloat(rowsA[a].mem) * 1024 * 1024;
                                            if (val == NaN) {
                                                val = 0;
                                            }
                                            rowsB[xx][rowsA[a].port] = val;
                                        }
                                        else {
                                            var val = parseFloat(rowsA[a].mem);
                                            if (val == NaN) {
                                                val = 0;
                                            }
                                            rowsB[xx][rowsA[a].port] = val;
                                        }
                                        rowsB[xx].prog += rowsA[a].port.toString() + '!! ' + rowsA[a].prog.toString() + '!!'
                                        break;
                                    case 'cpu':
                                        if (rowsA[a].cpu =='N/A'){
                                    	 	 rowsB[xx][rowsA[a].port] =0;                                     	 	 
                                    	}
                                        else if (rowsA[a].cpu.indexOf('m') > -1) {
                                            var val = parseFloat(rowsA[a].cpu) * 60
                                            if (val == NaN) {
                                                val = 0;
                                            }
                                            rowsB[xx][rowsA[a].port] = val;
                                        }
                                        else if (rowsA[a].cpu.indexOf('h') > -1) {
                                            var val = parseFloat(rowsA[a].cpu) * 60 * 60
                                            if (val == NaN) {
                                                val = 0;
                                            }
                                            rowsB[xx][rowsA[a].port] = val;
                                        }
                                        else {
                                            var val = parseFloat(rowsA[a].cpu);
                                            if (val == NaN) {
                                                val = 0;
                                            }
                                        }
                                        rowsB[xx].prog += rowsA[a].port.toString() + '!! ' + rowsA[a].prog.toString() + '!!'
                                        break;
                                    case 'write':
                                    	if (rowsA[a].write =='N/A'){
                                    	 	 rowsB[xx][rowsA[a].port] =0;                                     	 	 
                                    	}
                                        else if (rowsA[a].write.indexOf('K') > -1) {
                                            var val = parseFloat(rowsA[a].write) * 1024;
                                            if (val == NaN) {
                                                val = 0;
                                            }
                                            rowsB[xx][rowsA[a].port] = val;
                                        }
                                        else if (rowsA[a].write.indexOf('M') > -1) {
                                            var val = parseFloat(rowsA[a].write) * 1024 * 1024;
                                            if (val == NaN) {
                                                val = 0;
                                            }
                                            rowsB[xx][rowsA[a].port] = val;
                                        }
                                        else {
                                            var val = parseFloat(rowsA[a].write);
                                            if (val == NaN) {
                                                val = 0;
                                            }
                                            rowsB[xx][rowsA[a].port] = val;
                                        }
                                        rowsB[xx].prog += rowsA[a].port.toString() + '!! ' + rowsA[a].prog.toString() + '!!'
                                        break;
                                    case 'read':
                                    	if (rowsA[a].read =='N/A'){
                                    	 	 rowsB[xx][rowsA[a].port] =0;
                                    	}
                                        else if (rowsA[a].read.indexOf('K') > -1) {
                                            var val = parseFloat(rowsA[a].read) * 1024;
                                            if (val == NaN) {
                                                val = 0;
                                            }
                                            rowsB[xx][rowsA[a].port] = val;
                                        }
                                        else if (rowsA[a].read.indexOf('M') > -1) {
                                            var val = parseFloat(rowsA[a].read) * 1024 * 1024;
                                            if (val == NaN) {
                                                val = 0;
                                            }
                                            rowsB[xx][rowsA[a].port] = val;
                                        }
                                        else {
                                            var val = parseFloat(rowsA[a].read);
                                            if (val == NaN) {
                                                val = 0;
                                            }
                                            rowsB[xx][rowsA[a].port] = val;
                                        }
                                        rowsB[xx].prog += rowsA[a].port.toString() + '!! ' + rowsA[a].prog.toString() + '!!'
                                        break;
                                    case 'open':
                                    	if (rowsA[a].open =='N/A'){
                                    	 	 rowsB[xx][rowsA[a].port] =0;
                                    	}
                                        else if (rowsA[a].open.indexOf('K') > -1) {
                                            var val = parseFloat(rowsA[a].open) * 1024;
                                            if (val == NaN) {
                                                val = 0;
                                            }
                                            rowsB[xx][rowsA[a].port] = val;
                                        }
                                        else if (rowsA[a].open.indexOf('M') > -1) {
                                            var val = parseFloat(rowsA[a].open) * 1024 * 1024;
                                            if (val == NaN) {
                                                val = 0;
                                            }
                                            rowsB[xx][rowsA[a].port] = val;
                                        }
                                        else {
                                            var val = parseFloat(rowsA[a].open);
                                            if (val == NaN) {
                                                val = 0;
                                            }
                                            rowsB[xx][rowsA[a].port] = val;
                                        }
                                        rowsB[xx].prog += rowsA[a].port.toString() + '!! ' + rowsA[a].prog.toString() + '!!'
                                        break;
                                }
                                if (rowsB[xx][rowsA[a].port] == 0) {
                                    rowsB[xx][rowsA[a].port] = 0;
                                }
                            }
                        }

                        rowsB[xx].timestamp = c;

                    }
                    rowsA = [];
                    rowsA = rowsB.splice(0, times.length);

                }

                myJSON = new JSON();
                var data = myJSON.toJSON(null, rowsA, false);

                var fields = myJSON.generateFields(rowsA);

                Response.Write('{ "metaData": { "totalProperty" : "total",  "root" : "results", "id" : "id",    "fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');
            }
        }
        catch (err) {
            Response.Write("{success:failed, error:'" + err.message + " || " + sql + "'}");
            logData('app/data/diagram/getT24ProcessInfoMem.asp: ' + err.message);
        }
    </script>