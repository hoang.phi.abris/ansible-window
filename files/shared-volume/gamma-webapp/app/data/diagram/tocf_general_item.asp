<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
try{
	var environment = Request("environment");
	var min_time_stamp = isset(Request("min_time_stamp")) ? Request("min_time_stamp") : "";
	var max_time_stamp = isset(Request("max_time_stamp")) ? Request("max_time_stamp") : "";
	
	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_PORTAL_REALTIME, environment)) {
		var rec_count = Request("rec_count");

		var sort = "desc";

		if(min_time_stamp != ""){
			min_time_stamp = " and timestamp > '"+min_time_stamp+"' ";
			sort = "asc";
		}

		if(max_time_stamp != ""){
			max_time_stamp = " and timestamp < '"+max_time_stamp+"' ";
			sort = "desc";
		}

		sql  = "SELECT servlet_name, application, version_enquiry "
		sql += "FROM observer.[TOCF.BROWSER.SERVLET] tbs INNER JOIN measurements m "
		sql += "ON tbs.measurement_id = m.id "
		sql += "WHERE servlet_name is not null and measurement_id IN ( "
		sql += "	SELECT top " + rec_count + " id "
		sql += "	FROM measurements left join (select master_environment_name, name, multicountry from Configuration.Environments) e  on name = '" + environment + "' and e.multicountry = 1"
		sql += "	WHERE (environment = '" + environment + "' OR environment=e.master_environment_name) " + max_time_stamp + min_time_stamp + " AND observer = 'TOCF.BROWSER.SERVLET' "
		sql += "	ORDER BY timestamp "+sort+") "

		var fields = []
		var data = []
		var rowsA = []

		rowsA = runSQL(sql,cAcc,"fulltable");

		var i = 0;
		var results = []
		for ( item in rowsA)
		{
			var key = rowsA[item].servlet_name + "###" + rowsA[item].application + "###" + rowsA[item].version_enquiry
			var found = false;
			for( i = 0; i < results.length; i++) {
				if (results[i].key == key) {
					found = true;
					break;
				}
			}
			if (!found) {
				results.push({
					key : key,
					servlet_name : rowsA[item].servlet_name,
					application : rowsA[item].application,
					version_enquiry : rowsA[item].version_enquiry
				})
			}
		}

		for( i = 0; i < results.length; i++) 
			delete results[i].key;

		var length = results.length;
		var myJSON = new JSON();
		results 		= myJSON.toJSON(null, results, false); 
		fields 	= myJSON.generateFields(rowsA)

		Response.Write( '{ "metaData": {"totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + length + '","results":' + results + '}');
	}
}catch (err) 
{
 logData('diagram/tocf_general_item.asp: ' + err.message);
  Response.Write("{success:false,error:'" + err.message + "'}");
} 
%>