<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
//<script>
try{	
	var environment = Request("environment");
	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_PORTAL_REALTIME, environment)) {
		var rec_count = Request("rec_count");
		var phList = String(Request("phList"));
		var min_time_stamp = isset(Request("min_time_stamp")) ? Request("min_time_stamp") : "";
		var max_time_stamp = isset(Request("max_time_stamp")) ? Request("max_time_stamp") : "";

		var phArray = phList.split(String.fromCharCode(252))
		var quotedPhList = "";
		for(var i=0; i<phArray.length; i++) {
			if (i>0)
				quotedPhList += "','"
			quotedPhList += phArray[i]
		}
		
		var fields = []
		var data = []

		var sort = "desc";

		if(min_time_stamp != ""){
			min_time_stamp = " and timestamp > '"+min_time_stamp+"' ";
			sort = "asc";
		}

		if(max_time_stamp != ""){
			max_time_stamp = " and timestamp < '"+max_time_stamp+"' ";
			sort = "desc";
		}

		sql  = "SELECT timestamp, isnull(int_db,0) as 'yvalue', phantom_name FROM observer.[EB.PHANTOM] epo INNER JOIN measurements m "
		sql += "ON epo.measurement_id = m.id "
		sql += "WHERE measurement_id IN ( "
		sql += "	SELECT top " + rec_count + " id  "
		sql += "	FROM measurements left join (select master_environment_name, name, multicountry from Configuration.Environments) e  on name = '" + environment + "' and e.multicountry = 1 "
		sql += "	WHERE (environment = '" + environment + "' OR environment=e.master_environment_name) " + max_time_stamp + min_time_stamp + " AND observer = 'EB.PHANTOM' "
		sql += "	ORDER BY timestamp "+sort+") "
		sql += "	AND phantom_name in ('" + quotedPhList + "') "
		sql += "ORDER BY timestamp ASC "
	
		var rowIndex = '';
		var lastTS = '';
		var rowsB = []
		rowsA = runSQL(sql,cAcc,"fulltable");
		for (var item in rowsA) {
			var phName = rowsA[item].phantom_name
			var timestamp = formatDateTimeToHighChart(rowsA[item].timestamp);
			if (timestamp != lastTS) {
				rowsB.push({});
				lastTS = timestamp;
				rowIndex = rowsB.length - 1
				rowsB[rowIndex].timestamp = timestamp;
			}
			for(var i=0;i<phArray.length;i++) {
				if (phArray[i] == phName)
					break;
			}
			rowsB[rowIndex]['yvalue'+i] = parseInt(rowsA[item].yvalue);
		}
				
		var myJSON = new JSON();
		data 		= myJSON.toJSON(null, rowsB, false); 
		fields 	= myJSON.generateFields(rowsB)

		Response.Write( '{ "metaData": { "totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');
	}
}catch (err) 
{
 logData('diagram/phantom_workload.asp: ' + err.message);
  Response.Write("{success:false,error:'" + err.message + "'}");
} 
%>