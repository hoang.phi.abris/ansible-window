<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
//<script>
try{
	var rec_count = Request("rec_count");
	var environment = Request("environment");
	var host = Request("host");
	var min_time_stamp = isset(Request("min_time_stamp")) ? Request("min_time_stamp") : "";
	var max_time_stamp = isset(Request("max_time_stamp")) ? Request("max_time_stamp") : "";
	var base_time = String(Request("base_time"));
	var max_time;

	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_PORTAL_REALTIME, environment)) {

		var sort = "desc";

		if(min_time_stamp != ""){
			min_time_stamp = " and timestamp > '"+min_time_stamp+"' ";
			sort = "asc";
		}

		if(max_time_stamp != ""){
			max_time_stamp = " and timestamp < '"+max_time_stamp+"' ";
			sort = "desc";
		}


		sql  = "SELECT timestamp, SUM(CASE  WHEN epo.ph_status = 'ACTIVE' THEN 1 ELSE 0 END ) as 'yvalue' FROM observer.[EB.PHANTOM] epo INNER JOIN measurements m "
		sql += "ON epo.measurement_id = m.id "
		sql += "WHERE measurement_id IN ( "
		sql += "	SELECT top " + rec_count + " id "
		sql += "	FROM measurements left join (select master_environment_name, name, multicountry from Configuration.Environments) e  on name = '" + environment + "' and e.multicountry = 1 "
		sql += "	WHERE (environment = '" + environment + "' OR environment=e.master_environment_name) " + max_time_stamp + min_time_stamp + " AND observer = 'EB.PHANTOM' "
		sql += "	ORDER BY timestamp "+sort+") "
		sql += "  AND ((m.host = 'MULTISERVER' AND epo.host = '" + host + "') OR (m.host = '" + host + "')) "
		sql += "GROUP BY timestamp "
		sql += "ORDER BY timestamp ASC "

		var fields = []
		var data = []
		var rowsA = []

		rowsA = runSQL(sql,cAcc,"fulltable");

		for ( item in rowsA)
		{
			var tdate = rowsA[item].timestamp;
			max_time = convertDateTime(rowsA[item].timestamp);
			rowsA[item].timestamp = formatDateTimeToHighChart(tdate);
		}

		if(base_time!="" && base_time!="undefined"  && base_time!==undefined) {

			var arr = base_time.split(',');
			for(var index = 0; index<arr.length; index++) {

				if(arr[index].indexOf("d")==0) {	//Dynamic

						max_time_stamp = " and dateadd(day,"+arr[index].substring(1)+",timestamp) <= '"+max_time+"' ";
						sort = "desc";

				} else { //Static

						max_time_stamp = " and timestamp < '"+arr[index].substring(1)+"' ";
						sort = "desc";
				}


				sql  = "SELECT timestamp, SUM(CASE  WHEN epo.ph_status = 'ACTIVE' THEN 1 ELSE 0 END ) as 'yvalue' FROM observer.[EB.PHANTOM] epo INNER JOIN measurements m "
				sql += "ON epo.measurement_id = m.id "
				sql += "WHERE measurement_id IN ( "
				sql += "	SELECT top " + rec_count + " id "
				sql += "	FROM measurements left join (select master_environment_name, name, multicountry from Configuration.Environments) e  on name = '" + environment + "' and e.multicountry = 1 "
				sql += "	WHERE (environment = '" + environment + "' OR environment=e.master_environment_name) " + max_time_stamp + min_time_stamp + " AND observer = 'EB.PHANTOM' "
				sql += "	ORDER BY timestamp "+sort+") "
				sql += "  AND ((m.host = 'MULTISERVER' AND epo.host = '" + host + "') OR (m.host = '" + host + "')) "
				sql += "GROUP BY timestamp "
				sql += "ORDER BY timestamp ASC "

				var column = 'yvalue' + (index+1)
				rowsB = runSQL(sql,cAcc,"fulltable");
				for(var j = 0; j<rowsA.length; j++)
					rowsA[j][column] = 0;

				for(var a=0, b=0; a<rowsA.length && b<rowsB.length; a++, b++)
					rowsA[a][column] = parseInt(rowsB[b].yvalue);



			}
		}

		var myJSON = new JSON();
		data 		= myJSON.toJSON(null, rowsA, false); 
		fields 	= myJSON.generateFields(rowsA)

		Response.Write( '{ "metaData": { "totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');
	}
}catch (err) 
{
 logData('diagram/eb_phantom_count.asp: ' + err.message);
  Response.Write("{success:false,error:'" + err.message + "'}");
} 
%>