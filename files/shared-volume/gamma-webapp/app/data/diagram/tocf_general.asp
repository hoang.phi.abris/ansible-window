
<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

<!--#include file="../include/JSON.asp"-->
<!--#include file="../include/get_db_data.asp"-->
<!--#include file="../security/security.asp"-->

<%
try{
	var environment = Request("environment");
	var min_time_stamp = isset(Request("min_time_stamp")) ? Request("min_time_stamp") : "";
	var max_time_stamp = isset(Request("max_time_stamp")) ? Request("max_time_stamp") : "";
	
	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_PORTAL_REALTIME, environment)) {
		var rec_count = Request("rec_count");
		var servlet_name = Request("servlet_name");
		var application = Request("application");
		var version_enquiry = Request("version_enquiry");

		var sort = "desc";

		if(min_time_stamp != ""){
			min_time_stamp = " and timestamp > '"+min_time_stamp+"' ";
			sort = "asc";
		}

		if(max_time_stamp != ""){
			max_time_stamp = " and timestamp < '"+max_time_stamp+"' ";
			sort = "desc";
		}

		sql  = "SELECT timestamp, isnull( cast( cast (total_avg as float) as int) ,0) as 'total_avg', isnull( cast( cast (time_in_servlet_avg as float) as int),0)  as 'time_in_servlet_avg', "
		sql += "isnull( cast( cast (xml_parse_time_avg as float) as int),0)  as 'xml_parse_time_avg', isnull( cast( cast (xml_transform_time_avg as float) as int),0)  as 'xml_transform_time_avg', "
		sql += "isnull( cast( cast (time_in_connector_avg as float) as int),0)  as 'time_in_connector_avg', isnull( cast( cast (time_in_ofs_avg as float) as int),0)  as 'time_in_ofs_avg' "
		sql += "FROM observer.[TOCF.BROWSER.SERVLET] tbs INNER JOIN measurements m "
		sql += "ON tbs.measurement_id = m.id "
		sql += "WHERE measurement_id IN ( "
		sql += "	SELECT top " + rec_count + " id "
		sql += "	FROM measurements left join (select master_environment_name, name, multicountry from Configuration.Environments) e  on name = '" + environment + "' and e.multicountry = 1"
		sql += "	WHERE (environment = '" + environment + "' OR environment=e.master_environment_name) " + max_time_stamp + min_time_stamp + " AND observer = 'TOCF.BROWSER.SERVLET' "
		sql += "	ORDER BY timestamp "+sort+") "
		sql += "	AND tbs.servlet_name = '" + servlet_name + "' "
		sql += "	AND tbs.application = '" + application + "' "
		sql += "	AND tbs.version_enquiry = '" + version_enquiry + "' "
		sql += "ORDER BY timestamp ASC "

		var fields = []
		var data = []
		var rowsA = []

		rowsA = runSQL(sql,cAcc,"fulltable");

		for ( item in rowsA)
		{
			rowsA[item].timestamp = formatDateTimeToHighChart(rowsA[item].timestamp)
		}

		var myJSON = new JSON();
		data 	= myJSON.toJSON(null, rowsA, false); 
		fields 	= myJSON.generateFields(rowsA)

		//logData('({ "metaData": { "sql":"' + sql + '",	"totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '})')

		Response.Write( '{ "metaData": { "totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');
	}
}catch (err) 
{
 logData('diagram/tocf_general.asp: ' + err.message);
  Response.Write("{success:false,error:'" + err.message + "'}");
} 
%>
