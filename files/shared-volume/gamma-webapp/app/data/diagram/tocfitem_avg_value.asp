<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->
	<!--#include file="../include/util.asp"-->
	<!--#include file="../diagram/statisticsUtil.asp"-->

	<%
/* configre extra url request header properties */
try{
	var environment = Request("environment");

	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_PORTAL_STATISTICS, environment)) {

		var name = Request("name");
		var servlet_name = Request("servlet_name");
		var application = Request("application");
		var version_enquiry = Request("version_enquiry");
		var observer = "TOCF.BROWSER.SERVLET";
		var min_time_stamp = Request("min_time_stamp");
		var	max_time_stamp = Request("max_time_stamp");
		var	rec_count = Request("rec_count");
		var	archive_db = Request("archive_db");
		var loadSteps = isset(Request("loadSteps")) ? parseInt(Request("loadSteps")) : 0;
		var useArchive = isset(Request("useArchive")) ? parseInt(Request("useArchive")) : 0;

		sql = " SELECT min(CONVERT(VARCHAR(19),datestamp,120)) as min_time FROM ( "
			+ "SELECT datestamp 	FROM ( 	"
			+ "SELECT m.timestamp AS 'datestamp', id, host FROM dbo.measurements m "
			+ "left join (select master_environment_name, name, multicountry from Configuration.Environments) e "
			+ "on name = '" + environment + "' and e.multicountry = 1 "
			+ "WHERE (environment = '" + environment + "' OR environment=e.master_environment_name) "
			+ "and observer = 'TOCF.BROWSER.SERVLET' ) m2  "
			+ "INNER JOIN observer.[TOCF.BROWSER.SERVLET] tbs ON tbs.measurement_id = m2.id  "
			+ "	WHERE tbs.servlet_name = '" + servlet_name + "' "
			+ "	AND tbs.application = '" + application + "' "
			+ "	AND tbs.version_enquiry = '" + version_enquiry + "' "
			+ " GROUP BY datestamp   ) m3 ";


		var db = "";
		var minDates = {liveDbMinTime: null, archDbMinTime: null};
		minDates = getMinDatesLiveAndArch(sql);
		var intarvalInArchive = false;
		if(loadSteps == 0){
			if(minDates.liveDbMinTime == null){
				useArchive = true;
			}
		}
		if(useArchive){
			db =  archive_db + ".";
		} else  if(getTimestampFromDate(String(min_time_stamp)) < minDates['liveDbMinTime']) {
			intarvalInArchive=true
		}


		var div_seconds = getDivSec(min_time_stamp, max_time_stamp, rec_count, useArchive,observer);

		sql  = "SELECT datestamp, yvalue, text, title "
		sql += "FROM ( "
		sql += "	SELECT datestamp, avg(isnull( cast( cast (" + name + " as float) as bigint) ,0)) as 'yvalue' "
		sql += "	FROM ( "
		sql += " 		SELECT  dbo.timestamp_to_interval_time( "
		sql += "			m.timestamp, "
		sql += "			'" + min_time_stamp + "', "
		sql += "			'" + max_time_stamp + "', "
		sql += "			" + div_seconds + ") AS 'datestamp', "
		sql += " 		id FROM " + db + "dbo.measurements m left join (select master_environment_name, name, multicountry from Configuration.Environments) e  on name = '" + environment + "' and e.multicountry = 1"
		sql += "		WHERE (environment = '" + environment + "' OR environment=e.master_environment_name) AND observer = 'TOCF.BROWSER.SERVLET'"
		sql += "		and m.timestamp between '" + min_time_stamp + "' and '" + max_time_stamp + "'"
		sql += "	  ) m2 "
		sql += "	INNER JOIN " + db + "observer.[TOCF.BROWSER.SERVLET] tbs "
		sql += "	ON tbs.measurement_id = m2.id "
		sql += "	WHERE tbs.servlet_name = '" + servlet_name + "' "
		sql += "	AND tbs.application = '" + application + "' "
		sql += "	AND tbs.version_enquiry = '" + version_enquiry + "' "
		sql += "	GROUP BY datestamp "
		sql += "  ) m3 "
		sql += "  LEFT JOIN Configuration.events e on m3.datestamp = convert(varchar, e.timestamp, 102) "
		sql += "ORDER BY datestamp ASC "

		rowsA = runSQL(sql,cAcc,"fulltable");

		var rowsB = [];
		if(intarvalInArchive && rowsA.length < 100) {
			db =  archive_db + ".";
			sql  = "SELECT datestamp, yvalue, text, title "
			sql += "FROM ( "
			sql += "	SELECT datestamp, avg(isnull( cast( cast (" + name + " as float) as bigint) ,0)) as 'yvalue' "
			sql += "	FROM ( "
			sql += " 		SELECT  dbo.timestamp_to_interval_time( "
			sql += "			m.timestamp, "
			sql += "			'" + min_time_stamp + "', "
			sql += "			'" + max_time_stamp + "', "
			sql += "			" + div_seconds + ") AS 'datestamp', "
			sql += " 		id FROM " + db + "dbo.measurements m left join (select master_environment_name, name, multicountry from Configuration.Environments) e  on name = '" + environment + "' and e.multicountry = 1"
			sql += "		WHERE (environment = '" + environment + "' OR environment=e.master_environment_name) AND observer = 'TOCF.BROWSER.SERVLET'"
			sql += "		and m.timestamp between '" + min_time_stamp + "' and '" + max_time_stamp + "'"
			sql += "	  ) m2 "
			sql += "	INNER JOIN " + db + "observer.[TOCF.BROWSER.SERVLET] tbs "
			sql += "	ON tbs.measurement_id = m2.id "
			sql += "	WHERE tbs.servlet_name = '" + servlet_name + "' "
			sql += "	AND tbs.application = '" + application + "' "
			sql += "	AND tbs.version_enquiry = '" + version_enquiry + "' "
			sql += "	GROUP BY datestamp "
			sql += "  ) m3 "
			sql += "  LEFT JOIN Configuration.events e on m3.datestamp = convert(varchar, e.timestamp, 102) "
			sql += "ORDER BY datestamp ASC "

			rowsB = runSQL(sql,cAcc,"fulltable");
		}
		while(rowsB.length>0&&rowsA.unshift(rowsB.pop())<100);


		for ( item in rowsA)
		{
			if(rowsA[item].datestamp != ""){
				rowsA[item].datestamp = getTimestampFromDate(rowsA[item].datestamp);
			}
		}

		var myJSON = new JSON();

		data 		= myJSON.toJSON(null, rowsA, false);
		fields 	= myJSON.generateFields(rowsA)
		Response.Write( '{ "metaData": {"totalProperty" : "total", 	"root" : "results", "id" : "id",  "liveDbMinTime" : "' + minDates.liveDbMinTime + '", "archDbMinTime" : "' + minDates.archDbMinTime + '", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');

	}
} catch (err) 
{
  logData('diagram/tocfitem_avg_value.asp: ' + err.message);
  Response.Write("{success:false,error:'" + err.message + "'}");
}
%>