<%

	function getMinDatesLiveAndArch(sql){

		var retVal = {liveDbMinTime: null, archDbMinTime: null};

		var resultLive = runSQL(sql,cAcc,"fulltable");

		if(resultLive[0].min_time != null){
			var min_time_live = resultLive[0].min_time.split("-").join(".");
			retVal.liveDbMinTime = getTimestampFromDate(min_time_live);
		}

		var resultArch = runSQL(sql,cAccArch,"fulltable");

		if(resultArch && resultArch[0] && resultArch[0].min_time != null){
		var min_time_arch = resultArch[0].min_time.split("-").join(".");
			retVal.archDbMinTime = getTimestampFromDate(min_time_arch);
		}

        return retVal;
    }

	function getDivSec(min_time_stamp, max_time_stamp, rec_count, useArchive,observer){

		var div_sec = null;

		var sql = "SELECT COUNT(1) as measure_count FROM [dbo].[measurements] WHERE observer = '"+observer+"' and [timestamp] >= '" + min_time_stamp + "' AND [timestamp] <= '" + max_time_stamp + "'";
		if(!useArchive){
			result = runSQL(sql,cAcc,"fulltable");
		}
		else{
			result = runSQL(sql,cAccArch,"fulltable");
		}

		var measure_count = result[0].measure_count;

		if(measure_count < rec_count){
			rec_count = measure_count;
		}

		if(rec_count == 0){
			div_sec = 0;
		}
		else{
			var min_date = new Date(getTimestampFromDate(min_time_stamp + ""));
			var max_date = new Date(getTimestampFromDate(max_time_stamp + ""));

			var diff_date = (max_date.getTime() - min_date.getTime())/1000;

			div_sec = Math.round(diff_date/rec_count);
		}

		return div_sec;

	}

%>