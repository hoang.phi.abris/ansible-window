<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
//<script>
try{

	var rec_count = String(Request("rec_count"));
	var environment = Request("environment");
	var name = Request("name");
	var host = Request("host");
	var min_time_stamp = isset(Request("min_time_stamp")) ? Request("min_time_stamp") : "";
	var max_time_stamp = isset(Request("max_time_stamp")) ? Request("max_time_stamp") : "";
	var base_time = String(Request("base_time"));
	var max_time;

	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_PORTAL_REALTIME, environment)) {
		var tableName = 'TSA.SERVICE.MONITOR'
		var objName = 'agent'
		
		var fields = []
		var data = []
		var rowsA = []
		var rowsB = []


		var sort = "desc";
		
		if(name != 'all' && name != 'ALL' && name != null && name != ' ')
		{
			name =  " AND agent = '" + name + "' "
		}
		else
		{
			name = '';
		}

		if(min_time_stamp != ""){
			min_time_stamp = " and timestamp > '"+min_time_stamp+"' ";
			sort = "asc";
		}

		if(max_time_stamp != ""){
			max_time_stamp = " and timestamp < '"+max_time_stamp+"' ";
			sort = "desc";
		}
		
		
				sql =  " Select * From "
                sql += "  (SELECT top " + rec_count + " Cast(Isnull(int_db, 0) AS INT) AS 'yvalue', "
                sql += "  			 timestamp  "
                sql += "  FROM   observer.[TSA.SERVICE.MONITOR],  "
                sql += "  	   measurements  "
                sql += "  WHERE  id = measurement_id  "
                sql += "  	   AND environment IN (SELECT '" + environment + "' "
				sql += "							UNION "
				sql += "							SELECT master_environment_name "
				sql += "							FROM   configuration.environments "
				sql += "							 WHERE  name = '" + environment + "') "
				sql += "  		AND ((measurements.host = 'MULTISERVER' AND observer.[TSA.SERVICE.MONITOR].host = '" + host + "') OR (measurements.host = '" + host + "')) "
				sql += 			name
                sql += 			max_time_stamp + min_time_stamp
				sql += "  ORDER BY timestamp " + sort
                sql += "  ) r "
                sql += "  ORDER BY timestamp ASC"

			rowsA = runSQL(sql,cAcc,"fulltable");
			for ( item in rowsA) {
				max_time = convertDateTime(rowsA[item].timestamp);
				rowsA[item].timestamp = formatDateTimeToHighChart(rowsA[item].timestamp)
			}

			if(base_time!="" && base_time!="undefined"  && base_time!==undefined) {

				var arr = base_time.split(',');
				for(var index = 0; index<arr.length; index++) {

					if(arr[index].indexOf("d")==0) {	//Dynamic

							max_time_stamp = " and dateadd(day,"+arr[index].substring(1)+",timestamp) <= '"+max_time+"' ";
							sort = "desc";

					} else { //Static

							max_time_stamp = " and timestamp < '"+arr[index].substring(1)+"' ";
							sort = "desc";
					}


					sql =  " Select * From "
					sql += "  (SELECT top " + rec_count + " Cast(Isnull(int_db, 0) AS INT) AS 'yvalue', "
					sql += "  			 timestamp  "
					sql += "  FROM   observer.[TSA.SERVICE.MONITOR],  "
					sql += "  	   measurements  "
					sql += "  WHERE  id = measurement_id  "
					sql += "  	   AND environment IN (SELECT '" + environment + "' "
					sql += "							UNION "
					sql += "							SELECT master_environment_name "
					sql += "							FROM   configuration.environments "
					sql += "							 WHERE  name = '" + environment + "') "
					sql += "  		AND ((measurements.host = 'MULTISERVER' AND observer.[TSA.SERVICE.MONITOR].host = '" + host + "') OR (measurements.host = '" + host + "')) "
					sql += 			name
					sql += 			max_time_stamp + min_time_stamp
					sql += "  ORDER BY timestamp " + sort
					sql += "  ) r "
					sql += "  ORDER BY timestamp ASC"

					var column = 'yvalue' + (index+1)
					rowsB = runSQL(sql,cAcc,"fulltable");
					for(var j = 0; j<rowsA.length; j++)
						rowsA[j][column] = 0;

					for(var a=0, b=0; a<rowsA.length && b<rowsB.length; a++, b++)
						rowsA[a][column] = parseInt(rowsB[b].yvalue);



				}
			}
		

		var myJSON = new JSON();
		data 		= myJSON.toJSON(null, rowsA, false); 
		fields 	= myJSON.generateFields(rowsA)
		
		Response.Write( '{ "metaData": { "totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');

	}
}catch (err) 
{
 logData('diagram/agent_int_db.asp: ' + err.message);
  Response.Write("{success:false,error:'" + err.message + sql + "'}");
} 
%>