<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

    <!--#include file="../include/JSON.asp"-->
    <!--#include file="../include/get_db_data.asp"-->
    <!--#include file="../security/security.asp"-->

    <script runat=server language=JavaScript>
        try {
            if (CheckRight(SEC_ROLE_LOGIN, null) && FindRight(SEC_ROLE_PROTOCOL_VIEW)) {
                var environment = String(Request("environment"));
                var rec_count = isset(Request("rec_count")) ? "  top " + Request("rec_count") + " " : " ";
                var host = String(Request("host"));

                var min_time_stamp = isset(Request("min_time_stamp")) ? Request("min_time_stamp") : "";
                var max_time_stamp = isset(Request("max_time_stamp")) ? Request("max_time_stamp") : "";
                var volume = isset(Request("volume")) ? "  AND volume = '" + String(Request("volume")) + "' " : '';
                var sql = "";
                var sort = "desc";

                if (min_time_stamp != "") {
                    min_time_stamp = " and timestamp > '" + min_time_stamp + "' ";
                    sort = "asc";
                }
                if (max_time_stamp != "") {
                    max_time_stamp = " and timestamp < '" + max_time_stamp + "' ";
                    sort = "desc";
                }
                if (volume == '' || volume == "  AND volume = '" + '' + "' ") {

                    sql = "select distinct  REPLACE(volume,'.','') as volume, mhost as host from (select top 1000 o.*,m.host as mhost from measurements m,  observer.[disk.usage.check] o where m.environment IN(SELECT '" + environment + "'";
                    sql += " UNION SELECT master_environment_name";
                    sql += " FROM configuration.environments";
                    sql += " WHERE NAME = '" + environment + "') and m.id = o.measurement_id and  m.observer = 'DISK.USAGE.CHECK' order by timestamp desc) t where volume is not null order by volume";

                    var rowsA = runSQL(sql, cAcc, "fulltable");
                    var myJSON = new JSON();

                    var hosts = host.split("'")
                    for (i = 0; i < rowsA.length; i++) {
                        if (rowsA[i].volume.indexOf(":\\") >= 0)
                            rowsA[i].volume = rowsA[i].volume.substring(0, rowsA[i].volume.indexOf(":\\"))

                        rowsA[i].volume = rowsA[i].volume;
                        rowsA[i].host = rowsA[i].host.toLowerCase();
                    }
                    for (var ind = 1; ind < hosts.length - 1; ind += 2) {
                        var tmp = {
                            volume: 'ALL',
                            host: hosts[ind].toLowerCase()
                        }
                        rowsA.unshift(tmp);
                    }
                } else {
                    sql = null;
                    sql = " Select capacity, REPLACE(volume,'.','') as volume, timestamp, measurements.host as host "
                    sql += "   From measurements INNER JOIN observer.[DISK.USAGE.CHECK] ON id = measurement_id  "
                    sql += "           AND measurements.observer = 'DISK.USAGE.CHECK'      "
                    sql += "   Where timestamp IN    "
                    sql += "           (SELECT " + rec_count + "  timestamp from ( SELECT DISTINCT timestamp   "
                    sql += "            From observer.[DISK.USAGE.CHECK], measurements    "
                    sql += "            WHERE id = measurement_id    "
                    sql += "               AND environment IN (SELECT '" + environment + "' "
                    sql += "                            UNION "
                    sql += "                            SELECT master_environment_name "
                    sql += "                            FROM   configuration.environments "
                    sql += "                            WHERE  name = '" + environment + "') "
                    sql += "              AND ((measurements.host = 'MULTISERVER' AND observer.[DISK.USAGE.CHECK].host = '" + host + "') OR (measurements.host = '" + host + "')) "
                    sql += "              AND measurements.observer = 'DISK.USAGE.CHECK'"
                    sql += max_time_stamp
                    sql += min_time_stamp
                    sql += "         ) t  ORDER BY timestamp " + sort
                    sql += " )  "
                    //sql += "       AND id = measurement_id    "
                    sql += "       AND environment IN (SELECT '" + environment + "' "
                    sql += "                            UNION "
                    sql += "                            SELECT master_environment_name "
                    sql += "                            FROM   configuration.environments "
                    sql += "                            WHERE  name = '" + environment + "') "
                    sql += "      AND ((measurements.host = 'MULTISERVER' AND observer.[DISK.USAGE.CHECK].host = '" + host + "') OR (measurements.host = '" + host + "')) "
                    // sql += "    and measurements.observer = 'DISK.USAGE.CHECK'"
                    sql += "   Order by timestamp  asc "

                    var rowsA = runSQL(sql, cAcc, "fulltable");
                    var myJSON = new JSON();

                    var flags = [],
                        volumes = [],
                        times = [],
                        i;
                    for (i = 0; i < rowsA.length; i++) {
                        if (rowsA[i].volume.indexOf(":\\") >= 0)
                            rowsA[i].volume = rowsA[i].volume.substring(0, rowsA[i].volume.indexOf(":\\"))
                        if (flags[rowsA[i].volume]) { }
                        else {
                            flags[rowsA[i].volume] = true;

                            volumes.push(rowsA[i].volume);
                        }
                        if (flags[rowsA[i].timestamp]) { }
                        else {
                            flags[rowsA[i].timestamp] = true;
                            var tdate = new Date(rowsA[i].timestamp);
                            times.push(tdate);
                        }

                    }

                    var rowsB = rowsA;

                    for (var xx = 0; xx < times.length; xx++) {
                        for (var j = 0; j < volumes.length; j++) {
                            rowsB[xx][volumes[j]] = 0;
                        }

                        var c = new Date(times[xx])
                        c = c.getTime();

                        for (var a = 0; a < rowsA.length; a++) {
                            var d = new Date(rowsA[a].timestamp);
                            d = d.getTime();

                            if (c === d) {
                                if (a % 2 == 0)
                                    rowsB[xx][rowsA[a].volume] = parseInt(rowsA[a].capacity);
                                else
                                    rowsB[xx][rowsA[a].volume] = parseInt(rowsA[a].capacity);
                            }
                        }

                        rowsB[xx].timestamp = c;
                        rowsB[xx].capacity = parseInt(rowsB[xx].capacity);

                    }
                    rowsA = [];
                    rowsA = rowsB.splice(0, times.length);
                }

                var data = myJSON.toJSON(null, rowsA, false);
                var fields = myJSON.generateFields(rowsA);

                Response.Write('{ "metaData": { "totalProperty" : "total",     "root" : "results", "id" : "id",     "fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');
            }
        } catch (err) {
            Response.Write("{success:failed, error:'" + err.message + sql + "'}");
            logData('app/data/diagram/getDiskUsage.asp: ' + err.message);
        }
    </script>