<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
	var environment = Request("environment");
	
	try {
		if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_PORTAL_REALTIME, environment)) {
			var rec_count = Request("rec_count");
			var min_time_stamp = isset(Request("min_time_stamp")) ? Request("min_time_stamp") : "";
			var max_time_stamp = isset(Request("max_time_stamp")) ? Request("max_time_stamp") : "";
			var host = Request("host");
			var port = Request("port");
			if (port == "")
				port = 0;
			var queue_name = Request("queue_name");


			var sort = "desc";

			if(min_time_stamp != ""){
				min_time_stamp = " and timestamp > '"+min_time_stamp+"' ";
				sort = "asc";
			}

			if(max_time_stamp != ""){
				max_time_stamp = " and timestamp < '"+max_time_stamp+"' ";
				sort = "desc";
			}

			sql  = "SELECT timestamp, isnull( cast( cast (queue_depth as float) as int) ,0) as 'queue_depth', "
			sql += "case when inprocess_msg_count = 'N/A' then 0 else isnull(cast( cast (inprocess_msg_count as float) as int),0) end as 'inprocess_msg_count', "
			sql += "case when processed_count = 'N/A' then 0 else isnull(cast( cast (processed_count as float) as int),0) end as 'processed_count' "
			sql += "FROM observer.[TOCF.QUEUE.CHECK] tq INNER JOIN measurements m "
			sql += "ON tq.measurement_id = m.id "
			sql += "WHERE measurement_id IN ( "
			sql += "	SELECT top " + rec_count + " id "
			sql += "	FROM measurements left join (select master_environment_name, name, multicountry from Configuration.Environments) e  on name = '" + environment + "' and e.multicountry = 1 "
			sql += "	WHERE (environment = '" + environment + "' OR environment=e.master_environment_name) " + max_time_stamp + min_time_stamp + " AND observer = 'TOCF.QUEUE.CHECK' "
			sql += "	ORDER BY timestamp "+sort+") "
			sql += "	AND tq.queue_name = '" + queue_name + "' "
			sql += "	AND tq.host = '" + host + "' "
			sql += "	AND tq.port = " + port + " "
			sql += "ORDER BY timestamp ASC "

			var fields = []
			var data = []
			var rowsA = []

			rowsA = runSQL(sql,cAcc,"fulltable");

			for ( item in rowsA)
			{
				rowsA[item].timestamp = formatDateTimeToHighChart(rowsA[item].timestamp)
			}

			var myJSON = new JSON();
			data 	= myJSON.toJSON(null, rowsA, false); 
			fields 	= myJSON.generateFields(rowsA)

			//logData('({ "metaData": { "sql":"' + sql + '",	"totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '})')

			Response.Write( '{ "metaData": {"totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');
		}
	}
catch (err) 
{
 logData('diagram/tocf_queue.asp: ' + err.message);
  Response.Write("{success:false,error:'" + err.message + "'}");
} 
%>