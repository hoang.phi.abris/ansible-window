<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

    <!--#include file="../include/JSON.asp"-->
    <!--#include file="../include/get_db_data.asp"-->
    <!--#include file="../security/security.asp"-->

    <script runat=server language=JavaScript>
        try {
            var rec_count = Request("rec_count");
            var environment = Request("environment");
            var host = Request("host");
            var base_time = String(Request("base_time"));
            var min_time_stamp = isset(Request("min_time_stamp")) ? Request("min_time_stamp") : "";
            var max_time_stamp = isset(Request("max_time_stamp")) ? Request("max_time_stamp") : "";


            if (CheckRight(SEC_ROLE_LOGIN, null) && FindRight(SEC_ROLE_PROTOCOL_VIEW)) {

                var sort = "desc";

                if (min_time_stamp != "") {
                    min_time_stamp = " and timestamp > '" + min_time_stamp + "' ";
                    sort = "asc";
                }

                if (max_time_stamp != "") {
                    max_time_stamp = " and timestamp < '" + max_time_stamp + "' ";
                    sort = "desc";
                }


                sql = " Select * From "
                sql += "  (SELECT top " + rec_count + " cpu, "
                sql += "  			 memory,  "
                sql += "  			 observer,  "
                sql += "  			 m2.host as host,  "
                sql += "  			 timestamp  "
                sql += "  FROM   observer.[server.performance.monitor] epo,  "
                sql += "  	   measurements m2 "
                sql += "  WHERE  id = measurement_id  "
                sql += "  	   AND environment IN (SELECT '" + environment + "' "
                sql += "							UNION "
                sql += "							SELECT master_environment_name "
                sql += "							FROM   configuration.environments "
                sql += "							 WHERE  name = '" + environment + "') "
                sql += "  	   AND epo.process_name = 'TOTAL' AND (m2.host = '" + host + "' OR (m2.host = 'MULTISERVER' AND epo.host = '" + host + "'))"
                sql += " and m2.observer = 'SERVER.PERFORMANCE.MONITOR'"
                sql += max_time_stamp + min_time_stamp
                sql += "  ORDER BY timestamp " + sort
                sql += "  ) r "
                sql += "  ORDER BY timestamp ASC"



                var rowsA = runSQL(sql, cAcc, "fulltable");
                var myJSON = new JSON();

                for (item in rowsA) {
                    rowsA[item].timestamp = formatDateTimeToHighChart(rowsA[item].timestamp)
                    rowsA[item].memory = parseInt(rowsA[item].memory);
                    rowsA[item].cpu = parseInt(rowsA[item].cpu);
                }

                var data = myJSON.toJSON(null, rowsA, false);
                var fields = myJSON.generateFields(rowsA);


                Response.Write('{ "metaData": { "totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');
            }
        }
        catch (err) {
            Response.Write("{success:failed, error:'" + err.message + sql + "'}");
            logData('app/data/diagram/serverPerformanceMonitor.asp: ' + err.message);
        }
    </script>