<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT" %>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../include/util.asp"-->
	<!--#include file="../security/security.asp"-->
	<!--#include file="../diagram/statisticsUtil.asp"-->

	<%
/* configre extra url request header properties */
try{
	if(CheckRight(SEC_ROLE_LOGIN, null) && FindRight(SEC_ROLE_PORTAL_STATISTICS)) {

		var host = Request("host");

		var env = Request("env");
		var observer = "OFS.MONITOR";
		var min_time_stamp = isset(Request("min_time_stamp")) ? Request("min_time_stamp") : "";
		var max_time_stamp = isset(Request("max_time_stamp")) ? Request("max_time_stamp") : "";
		var	rec_count = Request("rec_count");
		var	archive_db = Request("archive_db");
		var loadSteps = isset(Request("loadSteps")) ? parseInt(Request("loadSteps")) : 0;
		var useArchive = isset(Request("useArchive")) ? parseInt(Request("useArchive")) : 0;

		sql = "select top 1 CONVERT(VARCHAR(19),timestamp,120) as min_time from measurements where observer = '"+observer+"' order by timestamp asc";

		var db = "";
		var minDates = {liveDbMinTime: null, archDbMinTime: null};
		minDates = getMinDatesLiveAndArch(sql);
		var intarvalInArchive = false;
		if(loadSteps == 0){
			if(minDates.liveDbMinTime == null){
				useArchive = true;
			}
		}
		if(useArchive){
			db =  archive_db + ".";
		} else  if(getTimestampFromDate(String(min_time_stamp)) < minDates['liveDbMinTime']) {
			intarvalInArchive=true
		}

		var div_seconds = getDivSec(min_time_stamp, max_time_stamp, rec_count, useArchive,observer);

		sql = "SELECT datestamp, yvalue"//, text, title"
			+" FROM ("
			+" 	SELECT datestamp, avg(avg_total_runtime) as 'yvalue'"
			+" 	FROM ("
			+" 		SELECT  dbo.timestamp_to_interval_time( "
			+"			m.timestamp, "
			+"			'" + min_time_stamp + "', "
			+"			'" + max_time_stamp + "', "
			+"			" + div_seconds + ") AS 'datestamp', "
			+"		id, host FROM " + db + "dbo.measurements m"
			+" 		left join (select master_environment_name, name, multicountry from Configuration.Environments) e"
			+" 		on name = '" + env + "' and e.multicountry = 1"
			+" 		WHERE (environment = '" + env + "' OR environment=e.master_environment_name) and observer = 'OFS.MONITOR'"
			+"		and m.timestamp between '" + min_time_stamp + "' and '" + max_time_stamp + "'"
			+" 	  ) m2 "
			+" 	INNER JOIN " + db + "observer.[OFS.MONITOR] epo ON epo.measurement_id = m2.id "
			+" 	WHERE (m2.host = '" + host + "' OR (m2.host = 'MULTISERVER' AND epo.host = '" + host + "'))"
			+" 	GROUP BY datestamp"
			+"   ) m3 "
			//+"   LEFT JOIN Configuration.events e on LEFT(CONVERT(VARCHAR, datestamp, 120),10) = e.timestamp"
			+" ORDER BY datestamp ASC";


		rowsA = runSQL(sql,cAcc,"fulltable");



		var rowsB = [];
		if(intarvalInArchive && rowsA.length < 100) {
			db =  archive_db + ".";
			sql = "SELECT datestamp, yvalue"//, text, title"
				+" FROM ("
				+" 	SELECT datestamp, avg(avg_total_runtime) as 'yvalue'"
				+" 	FROM ("
				+" 		SELECT  dbo.timestamp_to_interval_time( "
				+"			m.timestamp, "
				+"			'" + min_time_stamp + "', "
				+"			'" + max_time_stamp + "', "
				+"			" + div_seconds + ") AS 'datestamp', "
				+"		id, host FROM " + db + "dbo.measurements m"
				+" 		left join (select master_environment_name, name, multicountry from Configuration.Environments) e"
				+" 		on name = '" + env + "' and e.multicountry = 1"
				+" 		WHERE (environment = '" + env + "' OR environment=e.master_environment_name) and observer = 'OFS.MONITOR'"
				+"		and m.timestamp between '" + min_time_stamp + "' and '" + max_time_stamp + "'"
				+" 	  ) m2 "
				+" 	INNER JOIN " + db + "observer.[OFS.MONITOR] epo ON epo.measurement_id = m2.id "
				+" 	WHERE (m2.host = '" + host + "' OR (m2.host = 'MULTISERVER' AND epo.host = '" + host + "'))"
				+" 	GROUP BY datestamp"
				+"   ) m3 "
				+" ORDER BY datestamp ASC";

			rowsB = runSQL(sql,cAcc,"fulltable");
		}
		while(rowsB.length>0&&rowsA.unshift(rowsB.pop())<100);


		var fields = [{"name": "datestamp"},{"name": "yvalue"}, {"name": "text"}, {"name": "title"}]

		var myJSON = new JSON();

		for ( item in rowsA)
		{
			if(rowsA[item].datestamp != ""){
				rowsA[item].datestamp = getTimestampFromDate(rowsA[item].datestamp);
			}
		}

		data = myJSON.toJSON(null, rowsA, false);
		fields = myJSON.toJSON(null, fields, false);

		Response.Write( '{ "metaData": { "totalProperty" : "total", 	"root" : "results", "id" : "id",  "liveDbMinTime" : "' + minDates.liveDbMinTime + '", "archDbMinTime" : "' + minDates.archDbMinTime + '", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');

	}
}catch (err)
{
 logData('diagram/ofs_avg_availability.asp: ' + err.message);
  Response.Write("{success:false,error:'" + err.message + "'}");
}
%>