
<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT" %>

<!--#include file="../../include/JSON.asp"-->
<!--#include file="../../include/get_db_data.asp"-->
<!--#include file="../../include/util.asp"-->
<!--#include file="../../security/security.asp"-->
<!--#include file="../../diagram/statisticsUtil.asp"-->

<%
//<script>
try {
	if(CheckRight(SEC_ROLE_LOGIN, null) && FindRight(SEC_ROLE_PORTAL_STATISTICS)) {

		var host = Request("host");
		var env = Request("environment");
		var observer = "DISK.USAGE.CHECK";
		var min_time_stamp = isset(Request("min_time_stamp")) ? Request("min_time_stamp") : "";
		var max_time_stamp = isset(Request("max_time_stamp")) ? Request("max_time_stamp") : "";
		var	rec_count = Request("rec_count");
		var	archive_db = Request("archive_db");
		var loadSteps = isset(Request("loadSteps")) ? parseInt(Request("loadSteps")) : 0;
		var useArchive = isset(Request("useArchive")) ? parseInt(Request("useArchive")) : 0;


		sql = "select top 1 CONVERT(VARCHAR(19),timestamp,120) as min_time from measurements where observer = '"+observer+"' order by timestamp asc";

		var db = "";
		var minDates = getMinDatesLiveAndArch(sql);
		var intarvalInArchive = false;
		if(loadSteps == 0){
			if(minDates.liveDbMinTime == null){
				useArchive = true;
			}
		}
		if(useArchive){
			db =  archive_db + ".";
		} else  if(getTimestampFromDate(String(min_time_stamp)) < minDates['liveDbMinTime']) {
			intarvalInArchive=true
		}


		var div_seconds = getDivSec(min_time_stamp, max_time_stamp, rec_count, useArchive,observer);
        function getSql(_db) {
            var sqlSelect =  "SELECT datestamp, yvalue, ymax, limit  "
			+" FROM ("
			+" 	SELECT datestamp, avg(CAST( logged_on_users AS INT)) as 'yvalue', max(CAST(logged_on_users AS INT)) as 'ymax', avg(max_t24_users ) as 'limit' "
			+" 	FROM ("
			+" 		SELECT  dbo.Timestamp_to_interval_time( "
			+"			m.timestamp, "
			+"			'" + min_time_stamp + "', "
			+"			'" + max_time_stamp + "', "
			+"			" + div_seconds + ") AS 'datestamp', "
			+"		id, host FROM " + _db + "dbo.measurements m"
			+" 		left join (select master_environment_name, name, multicountry from Configuration.Environments) e"
			+" 		on name = '" + env + "' and e.multicountry = 1"
			+" 		WHERE (environment = '" + env + "' OR environment=e.master_environment_name) and observer = 'T24.ENVIRONMENT'"
			+"		and m.timestamp between '" + min_time_stamp + "' and '" + max_time_stamp + "'"
			+" 	  ) m2 "
			+" 	INNER JOIN " + _db + "observer.[T24.ENVIRONMENT] epo ON epo.measurement_id = m2.id "

			+" 	GROUP BY datestamp"
			+"   ) m3 "
			+"   LEFT JOIN Configuration.events e on m3.datestamp = convert(varchar, e.timestamp, 102)"
			+" ORDER BY datestamp ASC";
            return sqlSelect;
        }

		sql = getSql(db);
		rowsA = runSQL(sql,cAcc,"fulltable");

		var rowsarch = [];
		if(intarvalInArchive && rowsA.length < 100) {
			db =  archive_db + ".";
            rowsarch = runSQL(getSql(db),cAcc,"fulltable");
        }
        while(rowsarch.length>0&&rowsA.unshift(rowsarch.pop())<100);

		var flags = [],
            volumes = [],
            times = [],
            i;

		for ( item in rowsA) {
			if(rowsA[item].datestamp != "") {
				var ds = rowsA[item].datestamp;
				var newDs = ds.split(' ')[0].split('.').join('/').concat(' ').concat(ds.split(' ')[1])
				rowsA[item].datestamp = Date.parse(newDs);
			}
		}

		var myJSON = new JSON();
		var data = myJSON.toJSON(null, rowsA, false);
		var fields = myJSON.generateFields(rowsA);

		Response.Write( '{ "metaData": { "totalProperty" : "total", 	"root" : "results", "id" : "id",  "liveDbMinTime" : "' + minDates.liveDbMinTime + '", "archDbMinTime" : "' + minDates.archDbMinTime + '", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');

	}
} catch (err) {
    Response.Write("{success:failed, error:'" + err.message +  "' } " + sql);
    logData('app/data/diagram/getDiskUsageStat.asp: ' + err.message);
}
%>
