<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../include/util.asp"-->
	<!--#include file="../security/security.asp"-->
	<!--#include file="../diagram/statisticsUtil.asp"-->

	<%
/* configre extra url request header properties */
try{
	var environment = Request("environment");

	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_PORTAL_STATISTICS, environment)) {

		var name = Request("name");
		var host = Request("host");
		var port = Request("port");
		if (port == "")
			port = 0;
		var queue_name = Request("queue_name");
		var observer = "TOCF.QUEUE.CHECK"

		var min_time_stamp = isset(Request("min_time_stamp")) ? String(Request("min_time_stamp")): '' ;
		var max_time_stamp = isset(Request("max_time_stamp")) ? String(Request("max_time_stamp")): '' ;
		var	rec_count = Request("rec_count");
		var	archive_db = Request("archive_db");
		var loadSteps = isset(Request("loadSteps")) ? parseInt(Request("loadSteps")) : 0;
		var useArchive = isset(Request("useArchive")) ? parseInt(Request("useArchive")) : 0;

		sql = " SELECT min(CONVERT(VARCHAR(19),datestamp,120)) as min_time FROM ( "
			+ "SELECT datestamp 	FROM ( 	"
			+ "SELECT m.timestamp AS 'datestamp', id, host FROM dbo.measurements m "
			+ "left join (select master_environment_name, name, multicountry from Configuration.Environments) e "
			+ "on name = '" + env + "' and e.multicountry = 1 "
			+ "WHERE (environment = '" + env + "' OR environment=e.master_environment_name) "
			+ "and observer = 'TOCF.QUEUE.CHECK' ) m2  "
			+ "INNER JOIN observer.[TOCF.QUEUE.CHECK] tq ON tq.measurement_id = m2.id  "
			+ "	WHERE tq.queue_name = '" + queue_name + "' "
			+ "	AND tq.host = '" + host + "' "
			+ "	AND tq.port = " + port + " "
			+ "GROUP BY datestamp   ) m3 ";


		var db = "";
		var minDates = {liveDbMinTime: null, archDbMinTime: null};
		minDates = getMinDatesLiveAndArch(sql);
		var intarvalInArchive = false;
		if(loadSteps == 0){
			if(minDates.liveDbMinTime == null){
				useArchive = true;
			}
		}
		if(useArchive){
			db =  archive_db + ".";
		} else  if(getTimestampFromDate(String(min_time_stamp)) < minDates['liveDbMinTime']) {
			intarvalInArchive=true
		}

		var div_seconds = getDivSec(min_time_stamp, max_time_stamp, rec_count, useArchive,observer);

		sql  = "SELECT datestamp, yvalue, text, title "
		sql += "FROM ( "
		sql += "	SELECT datestamp, avg(isnull( cast( cast (" + name + " as float) as bigint) ,0)) as 'yvalue' "
		sql += "	FROM ( "
		sql += " 		SELECT  dbo.timestamp_to_interval_time( "
		sql += "			m.timestamp, "
		sql += "			'" + min_time_stamp + "', "
		sql += "			'" + max_time_stamp + "', "
		sql += "			" + div_seconds + ") AS 'datestamp', "
		sql += "		 id FROM " + db + "dbo.measurements m left join (select master_environment_name, name, multicountry from Configuration.Environments) e  on name = '" + environment + "' and e.multicountry = 1"
		sql += "		WHERE (environment = '" + environment + "' OR environment=e.master_environment_name) AND observer = 'TOCF.QUEUE.CHECK'"
		sql += "		and m.timestamp between '" + min_time_stamp + "' and '" + max_time_stamp + "'"
		sql += "	  ) m2 "
		sql += "	INNER JOIN " + db + "observer.[TOCF.QUEUE.CHECK] tq "
		sql += "	ON tq.measurement_id = m2.id "
		sql += "	WHERE tq.queue_name = '" + queue_name + "' "
		sql += "	AND tq.host = '" + host + "' "
		sql += "	AND tq.port = " + port + " "
		sql += "	GROUP BY datestamp "
		sql += "  ) m3 "
		sql += "  LEFT JOIN Configuration.events e on m3.datestamp = convert(varchar, e.timestamp, 102) "
		sql += "ORDER BY datestamp ASC "

		rowsA = runSQL(sql,cAcc,"fulltable");

		var rowsB = [];
		if(intarvalInArchive && rowsA.length < 100) {
			db =  archive_db + ".";
			sql  = "SELECT datestamp, yvalue, text, title "
			sql += "FROM ( "
			sql += "	SELECT datestamp, avg(isnull( cast( cast (" + name + " as float) as bigint) ,0)) as 'yvalue' "
			sql += "	FROM ( "
			sql += " 		SELECT  dbo.timestamp_to_interval_time( "
			sql += "			m.timestamp, "
			sql += "			'" + min_time_stamp + "', "
			sql += "			'" + max_time_stamp + "', "
			sql += "			" + div_seconds + ") AS 'datestamp', "
			sql += "		 id FROM " + db + "dbo.measurements m left join (select master_environment_name, name, multicountry from Configuration.Environments) e  on name = '" + environment + "' and e.multicountry = 1"
			sql += "		WHERE (environment = '" + environment + "' OR environment=e.master_environment_name) AND observer = 'TOCF.QUEUE.CHECK'"
			sql += "		and m.timestamp between '" + min_time_stamp + "' and '" + max_time_stamp + "'"
			sql += "	  ) m2 "
			sql += "	INNER JOIN " + db + "observer.[TOCF.QUEUE.CHECK] tq "
			sql += "	ON tq.measurement_id = m2.id "
			sql += "	WHERE tq.queue_name = '" + queue_name + "' "
			sql += "	AND tq.host = '" + host + "' "
			sql += "	AND tq.port = " + port + " "
			sql += "	GROUP BY datestamp "
			sql += "  ) m3 "
			sql += "  LEFT JOIN Configuration.events e on m3.datestamp = convert(varchar, e.timestamp, 102) "
			sql += "ORDER BY datestamp ASC "

			rowsB = runSQL(sql,cAcc,"fulltable");
		}
		while(rowsB.length>0&&rowsA.unshift(rowsB.pop())<100);


		for ( item in rowsA)
		{
			if(rowsA[item].datestamp != ""){
				rowsA[item].datestamp = getTimestampFromDate(rowsA[item].datestamp);
			}
		}

		var myJSON = new JSON();

		data 		= myJSON.toJSON(null, rowsA, false);
		fields 	= myJSON.generateFields(rowsA)
		Response.Write( '{ "metaData": { "totalProperty" : "total", 	"root" : "results", "id" : "id",  "liveDbMinTime" : "' + minDates.liveDbMinTime + '", "archDbMinTime" : "' + minDates.archDbMinTime + '", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');


	}
}catch (err)
{
 logData('diagram/tocfqueueitem_avg_value.asp: ' + err.message);
  Response.Write("{success:false,error:'" + err.message + "'}");
}
%>