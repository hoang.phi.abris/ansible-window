<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
try{
	var rec_count = Request("rec_count");
	var environment = Request("environment");
	var min_time_stamp = isset(Request("min_time_stamp")) ? Request("min_time_stamp") : "";
	var max_time_stamp = isset(Request("max_time_stamp")) ? Request("max_time_stamp") : "";
	var desc = isset(Request("desc")) ? String(Request("desc")) : ""

	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_PORTAL_REALTIME, environment)) {

		var sort = "desc";

		if(min_time_stamp != ""){
			min_time_stamp = " and timestamp > '"+min_time_stamp+"' ";
			sort = "asc";
		}

		if(max_time_stamp != ""){
			max_time_stamp = " and timestamp < '"+max_time_stamp+"' ";
			sort = "desc";
		}

		var config = [];

		if(desc == ""){

			var instances = [];
			var rowsA = [];
			var configData = [];

			var getInstances = "select host, name from Configuration.Instances where environment = '" + environment + "'"
			var instances = runSQL(getInstances,cAcc,"fulltable");
			
			for(var i=0; i < instances.length; i++) {
				getConfigSQL = "exec [observer].[getConfig.TWS.LOG.ANALYSER] @host='" + instances[i].host + "', @instance='" + instances[i].name + "'"
				rowsA = runSQL(getConfigSQL,cAcc,"fulltable");
				for(var c=0; c < rowsA.length; c++) {
					configData.push(rowsA[c]);
				}
			}
			var myJSON = new JSON();		
			config 	= myJSON.toJSON(null, configData, false); 
			
			if (configData.length > 0)
			desc = configData[0].desc
		}
		else{
			var myJSON = new JSON();
			config = myJSON.toJSON(null, [{desc: desc}], false);
		}


		sql  = "SELECT timestamp, cast(error_count as int) as 'no_of_errors', cast(transaction_count as int) as 'no_of_processed', cast(avg_time as int) as 'avg_time'  FROM observer.[TWS.LOG.ANALYSER.SUMMARY] ord INNER JOIN measurements m "
		sql += "ON ord.measurement_id = m.id "
		sql += "WHERE measurement_id IN ( "
		sql += "	SELECT top " + rec_count + " id "
		sql += "	FROM measurements left join (select master_environment_name, name, multicountry from Configuration.Environments) e  on name = '" + environment + "' and e.multicountry = 1   "
		sql += "	WHERE (environment = '" + environment + "' OR environment=e.master_environment_name) " + max_time_stamp + min_time_stamp + " AND observer = 'TWS.LOG.ANALYSER.SUMMARY' "
		sql += "	ORDER BY timestamp "+sort+") "
		sql += "	AND ord.description = '" + desc + "' "
		sql += "ORDER BY timestamp ASC "

		var fields = []
		var data = []
		var rowsA = []

		rowsA = runSQL(sql,cAcc,"fulltable");

		for ( item in rowsA)
		{
			rowsA[item].timestamp = formatDateTimeToHighChart(rowsA[item].timestamp)
		}

		var myJSON = new JSON();
		data 		= myJSON.toJSON(null, rowsA, false); 
		fields 	= myJSON.generateFields(rowsA)

		Response.Write( '{ "metaData": {"totalProperty" : "total", 	"root" : "results", "id" : "id", "fields" : ' + fields + ', "config":' + config +'}, "total":"' + rowsA.length + '","results":' + data + '}');
	}
}catch (err) 
{
 logData('diagram/tws_txns_check.asp: ' + err.message);
  Response.Write("{success:false,error:'" + err.message + "'}");
} 
%>