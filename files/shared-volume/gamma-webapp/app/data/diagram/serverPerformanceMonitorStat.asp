<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT" %>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../include/util.asp"-->
	<!--#include file="../security/security.asp"-->
	<!--#include file="../diagram/statisticsUtil.asp"-->

	<%
/* configre extra url request header properties */
try{
	if(CheckRight(SEC_ROLE_LOGIN, null) && FindRight(SEC_ROLE_PORTAL_STATISTICS)) {
		var timestamps = "";
		var host = Request("host");
		var env = Request("environment");
		var observer = "server.performance.monitor";
		var min_time_stamp = isset(Request("min_time_stamp")) ? Request("min_time_stamp") : "";
		var max_time_stamp = isset(Request("max_time_stamp")) ? Request("max_time_stamp") : "";
		var	rec_count = Request("rec_count");
		var	archive_db = Request("archive_db");
		var loadSteps = isset(Request("loadSteps")) ? parseInt(Request("loadSteps")) : 0;
		var useArchive = isset(Request("useArchive")) ? parseInt(Request("useArchive")) : 0;

		sql = "select top 1 CONVERT(VARCHAR(19),timestamp,120) as min_time from measurements where observer = '"+observer+"' order by timestamp asc";

		var db = "";
		var minDates = {liveDbMinTime: null, archDbMinTime: null};
		minDates = getMinDatesLiveAndArch(sql);
		var intarvalInArchive = false;
		if(loadSteps == 0){
			if(minDates.liveDbMinTime == null){
				useArchive = true;
			}
		}
		if(useArchive){
			db =  archive_db + ".";
		} else  if(getTimestampFromDate(String(min_time_stamp)) < minDates['liveDbMinTime']) {
			intarvalInArchive=true
		}

		var div_seconds = getDivSec(min_time_stamp, max_time_stamp, rec_count, useArchive,observer);

		sql = "SELECT datestamp, cpu, memory"
			+" FROM ("
			+" 	SELECT datestamp, AVG(CAST([cpu] AS DECIMAL)) AS cpu, AVG(CAST([memory] AS DECIMAL)) AS memory"
			+" 	FROM ("
			+" 		SELECT  dbo.Timestamp_to_interval_time( "
			+"			m.timestamp, "
			+"			'" + min_time_stamp + "', "
			+"			'" + max_time_stamp + "', "
			+"			" + div_seconds + ") AS 'datestamp', "
			+"		id, host FROM " + db + "dbo.measurements m"
			+" 		left join (select master_environment_name, name, multicountry from Configuration.Environments) e"
			+" 		on name = '" + env + "' and e.multicountry = 1"
			+" 		WHERE (environment = '" + env + "' OR environment=e.master_environment_name) and observer = 'server.performance.monitor'"
			+"		and m.timestamp between '" + min_time_stamp + "' and '" + max_time_stamp + "'"
			+" 	  ) m2 "
			+" 	INNER JOIN " + db + "observer.[server.performance.monitor] epo ON epo.measurement_id = m2.id "
			+" 	WHERE epo.process_name = 'TOTAL' AND (m2.host = '" + host + "' OR (m2.host = 'MULTISERVER' AND epo.host = '" + host + "'))"
			+" 	GROUP BY datestamp"
			+"   ) m3 "
			+"   LEFT JOIN Configuration.events e on m3.datestamp = convert(varchar, e.timestamp, 102)"
			+" ORDER BY datestamp ASC";


		rowsA = runSQL(sql,cAcc,"fulltable");

		var rowsB = [];
		if(intarvalInArchive && rowsA.length < 100) {
			db =  archive_db + ".";
			sql = "SELECT datestamp, cpu, memory"
				+" FROM ("
				+" 	SELECT  datestamp, COUNT(cpu) as cpu, COUNT(memory) as memory"
				+" 	FROM ("
				+" 		SELECT  dbo.Timestamp_to_interval_time( "
				+"			m.timestamp, "
				+"			'" + min_time_stamp + "', "
				+"			'" + max_time_stamp + "', "
				+"			" + div_seconds + ") AS 'datestamp', "
				+"		id, host FROM " + db + "dbo.measurements m"
				+" 		left join (select master_environment_name, name, multicountry from Configuration.Environments) e"
				+" 		on name = '" + env + "' and e.multicountry = 1"
				+" 		WHERE (environment = '" + env + "' OR environment=e.master_environment_name) and observer = 'server.performance.monitor'"
				+"		and m.timestamp between '" + min_time_stamp + "' and '" + max_time_stamp + "'"
				+" 	  ) m2 "
				+" 	INNER JOIN " + db + "observer.[server.performance.monitor] epo ON epo.measurement_id = m2.id "
				+" 	WHERE (m2.host = '" + host + "' OR (m2.host = 'MULTISERVER' AND epo.host = '" + host + "'))"
				+" 	GROUP BY datestamp"
				+"   ) m3 "
				+"   LEFT JOIN Configuration.events e on m3.datestamp = convert(varchar, e.timestamp, 102)"
				+" ORDER BY datestamp ASC";


			rowsB = runSQL(sql,cAcc,"fulltable");
		}
		while(rowsB.length>0&&rowsA.unshift(rowsB.pop())<100);

		var fields = [{"name": "datestamp"},{"name": "cpu"}, {"name": "memory"}]

		var myJSON = new JSON();

		for ( item in rowsA)
		{
			if(rowsA[item].datestamp != ""){
				rowsA[item].datestamp = getTimestampFromDate(rowsA[item].datestamp);
			}
			rowsA[item].cpu = parseInt(rowsA[item].cpu);
			rowsA[item].memory = parseInt(rowsA[item].memory);
		}



		data = myJSON.toJSON(null, rowsA, false);
		fields = myJSON.toJSON(null, fields, false);

		Response.Write( '{ "metaData": { "timestamps":"'+timestamps+'",	"totalProperty" : "total", 	"root" : "results", "id" : "id",  "liveDbMinTime" : "' + minDates.liveDbMinTime + '", "archDbMinTime" : "' + minDates.archDbMinTime + '", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');

	}
}catch (err)
{
    Response.Write("{success:failed, error:'" + err.message +  "' } " + sql);
    logData('app/data/diagram/serverPerformanceMonitorStat.asp: ' + err.message);
}
%>