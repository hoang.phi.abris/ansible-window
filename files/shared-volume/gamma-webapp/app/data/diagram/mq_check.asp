<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->
	<!--#include file="../include/util.asp"-->


	<%
try{
/*function contains(item,arr) {
   for (var i=0; i<arr.length;i++){
	if (arr[i] == item)
		return true;
   }
   return false;
}*/
	var rec_count = Request("rec_count");
	var environment = Request("environment");
	var chanelName = String(Request("name"));
	var queueName = String(Request("queueName"));
	var min_time_stamp = isset(Request("min_time_stamp")) ? Request("min_time_stamp") : "";
	var max_time_stamp = isset(Request("max_time_stamp")) ? Request("max_time_stamp") : "";

	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_PORTAL_REALTIME, environment)) {
		var fields = [];
		var fields2 = [];
		var data = [];
		var rowsA = [];
		var rowsB = [];
		var B = {};
		var queues = [];
		var cnt;


		var sort = "desc";

		if(min_time_stamp != ""){
			min_time_stamp = " and timestamp > '"+min_time_stamp+"' ";
			sort = "asc";
		}

		if(max_time_stamp != ""){
			max_time_stamp = " and timestamp < '"+max_time_stamp+"' ";
			sort = "desc";
		}

		var queue = ""

		if(queueName != "all"){
			queue = "AND queue = '" + queueName + "'";
		}

		sql  = "SELECT timestamp, queue, ISNULL(depth,0) depth FROM observer.[MQ.CHECK] tc INNER JOIN measurements m "
			sql += "ON tc.measurement_id = m.id "
			sql += "WHERE measurement_id IN ( "
			sql += "	SELECT top " + rec_count + " id "
			sql += "	FROM measurements left join (select master_environment_name, name, multicountry from Configuration.Environments) e  on name = '" + environment + "' and e.multicountry = 1"
			sql += "	WHERE (environment = '" + environment + "' OR environment=e.master_environment_name) " + max_time_stamp + min_time_stamp + " AND observer = 'MQ.CHECK' "
			sql += "	ORDER BY timestamp "+sort+") "
			sql += "AND channel = '" + chanelName + "' " + queue + " "
			sql += "ORDER BY timestamp, queue ASC "

		rowsA = runSQL(sql,cAcc,"fulltable");

		var i=0;
		var queueReady = false;

		for ( item=0; item < rowsA.length; item++ ) {
			B = {};
			B.timestamp = formatDateTimeToHighChart(rowsA[item].timestamp);
			if (!queueReady){
				B.queue = [];
				B.queue.push('ALL');
			}
			if(item < rowsA.length-1) {
				for ( i=0; formatDateTimeToHighChart(rowsA[item].timestamp) == formatDateTimeToHighChart(rowsA[item+1].timestamp); i++) {
					if (!queueReady)
						B.queue.push(rowsA[item].queue);
					if((queueName == 'all' || rowsA[item].queue.toString() == queueName) /*& (!queueReady || contains(rowsA[item].queue, rowsB[0].queue))*/)
						B['yvalue'+i] = parseInt(rowsA[item].depth);
					item++;
					if (item == rowsA.length-1){
						i++;
						break;
					}
				}
				if (!queueReady)
					B.queue.push(rowsA[item].queue);

				if((queueName == 'all' || rowsA[item].queue.toString() == queueName) /*& (!queueReady || contains(rowsA[item].queue, rowsB[0].queue))*/)
					B['yvalue'+i] = parseInt(rowsA[item].depth);

				queueReady = true;
			} else {
				if((queueName == 'all' || rowsA[item].queue.toString() == queueName) /*& (!queueReady || contains(rowsA[item].queue, rowsB[0].queue))*/)
					B['yvalue'+i] = parseInt(rowsA[item].depth);
			}
			rowsB.push(cloneObject(B));
		}

		var myJSON = new JSON();
		data 		= myJSON.toJSON(null, rowsB, false);
		fields 	= myJSON.generateFields(rowsB);

		Response.Write( '{ "metaData": { "totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');
	}
}catch (err)
{
 logData('diagram/mq_check.asp: ' + err.message);
  Response.Write("{success:false,error:'" + err.message + "'}");
}

%>