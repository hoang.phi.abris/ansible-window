<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->
	<!--#include file="../include/util.asp"-->


	<%
try{	
	var rec_count = Request("rec_count");
	var environment = Request("environment");
	var ctype = Request("ctype");
	var instance = "";
	var bcid = isset(Request("bcid")) ? String(Request("bcid")) : ""
	var base_time = String(Request("base_time"));
	var max_time;
	
	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_PORTAL_REALTIME, environment)) {
		var data = [];
		var instances = [];
		var rowsA = [];
		var rowsB = [];
		var configData = [];
		var config = [];
		
		getInstances = "select host, name from Configuration.Instances where environment = '" + environment + "'"
		instances = runSQL(getInstances,cAcc,"fulltable");
		
		for(var i=0; i < instances.length; i++) {
			getConfigSQL = "exec [observer].[getConfig.BROWSER.CONNECTIVITY] @host='" + instances[i].host + "', @instance='" + instances[i].name + "'"
			rowsA = runSQL(getConfigSQL,cAcc,"fulltable");
			for(var c=0; c < rowsA.length; c++) {
				configData.push(rowsA[c]);
			}
		}
		var myJSON = new JSON();		
		config 	= myJSON.toJSON(null, configData, false); 
		
		if ((configData.length > 0 ) && bcid == "")
		   bcid = configData[0].desc


		sql = "SELECT * FROM ("
		sql += "SELECT top " + rec_count + " id, "
		sql += "	timestamp, " 
		sql += "	status, "
		sql += "	o.severity, "
		sql += "	respdata, "
		sql += "	total, "
		sql += "	m.environment, "
		sql += "	m.instance, "
		sql += "	m.host, "
		sql += "	o.[description] "
		sql += "FROM "
		sql += "	observer.[BROWSER.CONNECTIVITY] o "
		sql += "	INNER JOIN measurements m ON o.measurement_id = m.id "
		sql += "	LEFT JOIN (select master_environment_name, name, multicountry from Configuration.Environments) e  on name = '" + environment + "' and e.multicountry = 1 "
		sql += "WHERE "
		sql += "	(environment = '" + environment + "' OR environment=e.master_environment_name) "
		sql += "    and m.observer = 'BROWSER.CONNECTIVITY'"
		sql += "	and o.[description] = '" + bcid + "' "
		sql += "ORDER BY timestamp DESC	) as belso "
		sql += "ORDER BY timestamp ASC	"
		
		

		rowsA = runSQL(sql,cAcc,"fulltable");

		if (rowsA) {
			if (rowsA[0]) {
				var sum = 0;
				for ( item in rowsA) {
					max_time = convertDateTime(rowsA[item].timestamp);
					rowsA[item].timestamp = formatDateTimeToHighChart(rowsA[item].timestamp);
					rowsA[item].respdata = eval('('+rowsA[item].respdata+')');
					sum += rowsA[item].total			
				}

				if(base_time!="" && base_time!="undefined"  && base_time!==undefined) {

					var arr = base_time.split(',');
					for(var index = 0; index<arr.length; index++) {
	
						if(arr[index].indexOf("d")==0) {	//Dynamic
	
								max_time_stamp = " and dateadd(day,"+arr[index].substring(1)+",timestamp) <= '"+max_time+"' ";
								sort = "desc";
	
						} else { //Static
	
								max_time_stamp = " and timestamp < '"+arr[index].substring(1)+"' ";
								sort = "desc";
						}
	
	
						sql = "SELECT * FROM ("
						sql += "SELECT top " + rec_count + " id, "
						sql += "	timestamp, " 
						sql += "	total "
						sql += "FROM "
						sql += "	observer.[BROWSER.CONNECTIVITY] o "
						sql += "	INNER JOIN measurements m ON o.measurement_id = m.id "
						sql += "	LEFT JOIN (select master_environment_name, name, multicountry from Configuration.Environments) e  on name = '" + environment + "' and e.multicountry = 1 "
						sql += "WHERE "
						sql += "	(environment = '" + environment + "' OR environment=e.master_environment_name) " + max_time_stamp 
						sql += "    and m.observer = 'BROWSER.CONNECTIVITY'"
						sql += "	and o.[description] = '" + bcid + "' "
						sql += "ORDER BY timestamp DESC	) as belso "
						sql += "ORDER BY timestamp ASC	"
	
						var column = 'yvalue' + (index+1)
						rowsB = runSQL(sql,cAcc,"fulltable");
						for(var j = 0; j<rowsA.length; j++)
							rowsA[j][column] = 0;
	
						for(var a=0, b=0; a<rowsA.length && b<rowsB.length; a++, b++)
							rowsA[a][column] = parseInt(rowsB[b].total);
	
	
	
					}
				}

				rowsA[0].avgtotal = ( sum /  rowsA.length )

				data 	= myJSON.toJSON(null, rowsA, false); 
				
			}
			else
				data = '[]'
		}
		else
			data = '[]'
		Response.Write( '{ "metaData": {"config":' + config +'}, "total":"' + rowsA.length + '","results":' + data + '}');
	}
}catch (err) 
{
 logData('diagram/BrowserCheck.asp: ' + err.message);
  Response.Write("{success:false,error:'" + err.message + "'}");
} 

%>