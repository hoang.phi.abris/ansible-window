<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
try{
	var rec_count = Request("rec_count");
	var environment = Request("environment");
	var aname = Request("aname");
	var host = Request("host");
	var port = Request("port");
	var tableSuffix = "";
	if (port == "-1") {
		tableSuffix = ".TOCF";
	}

	var min_time_stamp = isset(Request("min_time_stamp")) ? Request("min_time_stamp") : "";
	var max_time_stamp = isset(Request("max_time_stamp")) ? Request("max_time_stamp") : "";
	

	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_PORTAL_REALTIME, environment)) {

		var sort = "desc";

		if(min_time_stamp != ""){
			min_time_stamp = " and timestamp > '"+min_time_stamp+"' ";
			sort = "asc";
		}

		if(max_time_stamp != ""){
			max_time_stamp = " and timestamp < '"+max_time_stamp+"' ";
			sort = "desc";
		}

		sql  = "SELECT timestamp, cast (AvgMillis as int) as 'AvgMillis', cast (QueueSize as int) as 'QueueSize', cast (ToBeProcessed as int) as 'ToBeProcessed', cast (TotalTrans as int) as 'TotalTrans' "
		sql += "FROM observer.[TC.CHECK" + tableSuffix + "] tc INNER JOIN measurements m "
		sql += "ON tc.measurement_id = m.id "
		sql += "WHERE measurement_id IN ( "
		sql += "	SELECT top " + rec_count + " id "
		sql += "	FROM measurements left join (select master_environment_name, name, multicountry from Configuration.Environments) e  on name = '" + environment + "' and e.multicountry = 1"
		sql += "	WHERE (environment = '" + environment + "' OR environment=e.master_environment_name) " + max_time_stamp + min_time_stamp + " AND observer = 'TC.CHECK" + tableSuffix + "' "
		sql += "	ORDER BY timestamp "+sort+") "
		sql += "	AND tc.AdapterName = '" + aname + "' "
		if (port != "-1") {
			sql += "	AND tc.host = '" + host + "' "
			sql += "	AND tc.port = " + port + " "
		}
		else {
			sql += "	AND tc.servername = '" + host + "' "
		}
		sql += "ORDER BY timestamp ASC "
		
		
		var fields = []
		var data = []
		var rowsA = []

		rowsA = runSQL(sql,cAcc,"fulltable");

		for ( item in rowsA)
		{
			rowsA[item].timestamp = formatDateTimeToHighChart(rowsA[item].timestamp)
		}

		var myJSON = new JSON();
		data 		= myJSON.toJSON(null, rowsA, false); 
		fields 	= myJSON.generateFields(rowsA)

		//logData('({ "metaData": { "sql":"' + sql + '",	"totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '})')

		Response.Write( '{ "metaData": {"totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');
	}
}catch (err) 
{
 logData('diagram/tc_check.asp: ' + err.message);
  Response.Write("{success:false,error:'" + err.message + "'}");
} 
%>