<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
try{
	var rec_count = Request("rec_count");
	var environment = Request("environment");
	var type = Request("type");
	var name = Request("name");
	var host = Request("host");

	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_PORTAL_REALTIME, environment)) {
		var tableName = ''
		var objName = ''
		if (type == 'phantom') {
			objName = 'phantom_name'
			tableName = 'EB.PHANTOM';
		}
		else {
			objName = 'agent'
			tableName = 'TSA.SERVICE.MONITOR'
		}

		sql = "IF ((SELECT top 1 host FROM measurements left join (select master_environment_name, name, multicountry from Configuration.Environments) e  on name = '" + environment + "' and e.multicountry = 1 WHERE (environment = '" + environment + "' OR environment=e.master_environment_name) AND observer = '" + tableName + "' ORDER by timestamp desc) <> 'MULTISERVER') "
		    + "SELECT distinct timestamp, cast(isnull(int_db, 0) as int) as 'yvalue' FROM observer.[" + tableName + "] epo INNER JOIN measurements m ON epo.measurement_id = m.id WHERE measurement_id IN ( 	SELECT top " + rec_count + " id 	FROM measurements left join (select master_environment_name, name, multicountry from Configuration.Environments) e  on name = '" + environment + "' and e.multicountry = 1 	WHERE (environment = '" + environment + "' OR environment=e.master_environment_name) AND observer = '" + tableName + "' 	ORDER BY timestamp DESC) AND m.host = '" + host + "' 	AND epo." + objName + " = '" + name + "' ORDER BY timestamp ASC; "
		    + "ELSE "
			+ "SELECT distinct timestamp, cast(isnull(int_db, 0) as int) as 'yvalue' FROM observer.[" + tableName + "] epo INNER JOIN measurements m ON epo.measurement_id = m.id WHERE measurement_id IN ( 	SELECT top " + rec_count + " id 	FROM measurements left join (select master_environment_name, name, multicountry from Configuration.Environments) e  on name = '" + environment + "' and e.multicountry = 1 	WHERE(environment = '" + environment + "' OR environment=e.master_environment_name) AND observer = '" + tableName + "' 	ORDER BY timestamp DESC) AND epo.host = '" + host + "' 	AND epo." + objName + " = '" + name + "' ORDER BY timestamp ASC";


		var fields = []
		var data = []
		var rowsA = []

		rowsA = runSQL(sql,cAcc,"fulltable");

		for ( item in rowsA) {
			rowsA[item].timestamp = formatDateTimeToHighChart(rowsA[item].timestamp)

		}

		var myJSON = new JSON();
		data 		= myJSON.toJSON(null, rowsA, false);
		fields 	= myJSON.generateFields(rowsA)

		
		Response.Write( '{ "metaData": { "totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');
	}
}catch (err)
{
 logData('diagram/eb_phantom_int_db.asp: ' + err.message);
  Response.Write("{success:false,error:'" + err.message + "'}");
}
%>