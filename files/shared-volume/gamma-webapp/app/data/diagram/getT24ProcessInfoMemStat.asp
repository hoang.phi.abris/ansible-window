<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT" %>

<!--#include file="../include/JSON.asp"-->
<!--#include file="../include/get_db_data.asp"-->
<!--#include file="../include/util.asp"-->
<!--#include file="../security/security.asp"-->
<!--#include file="../diagram/statisticsUtil.asp"-->

<%
/* configre extra url request header properties */
try{
	if(CheckRight(SEC_ROLE_LOGIN, null) && FindRight(SEC_ROLE_PORTAL_STATISTICS)) {
		var timestamps = "";
		var host = Request("host");
		var env = Request("environment");
		var type = String(Request("type"));
		var observer = "mw42.analyser";
		var min_time_stamp = isset(Request("min_time_stamp")) ? Request("min_time_stamp") : "";
		var max_time_stamp = isset(Request("max_time_stamp")) ? Request("max_time_stamp") : "";
		var	rec_count = Request("rec_count");
		var	archive_db = Request("archive_db");
		var loadSteps = isset(Request("loadSteps")) ? parseInt(Request("loadSteps")) : 0;
		var useArchive = isset(Request("useArchive")) ? parseInt(Request("useArchive")) : 0;

		var sql  = "select top 1 host from measurements where  observer='mw42.analyser' and environment in (select '"+env+"' UNION (select master_environment_name from Configuration.Environments where name = '"+env+"' and multicountry = 1) ) order by timestamp desc";
		var isMulti = runSQL(sql,cAcc,"fulltable")[0]["host"]=="MULTISERVER";

		sql = "";

		var msql = "select top 1 CONVERT(VARCHAR(19),timestamp,120) as min_time from measurements where observer = '"+observer+"' order by timestamp asc";

		var db = "";
		var minDates = {liveDbMinTime: null, archDbMinTime: null};
		minDates = getMinDatesLiveAndArch(msql);
		var intarvalInArchive = false;
		if(loadSteps == 0){
			if(minDates.liveDbMinTime == null){
				useArchive = true;
			}
		}
		if(useArchive){
			db =  archive_db + ".";
		} else  if(minDates.achMinTime && getTimestampFromDate(String(min_time_stamp)) < minDates['liveDbMinTime']) {
			intarvalInArchive=true
		}

		var div_seconds = getDivSec(min_time_stamp, max_time_stamp, rec_count, useArchive,observer);


		avgWithCast = " CAST( [" + type + "] AS float) "
        avgWithCastSubStr = " CAST( SUBSTRING([" + type + "], 0, LEN([" + type + "])) AS float) "
		var caseStr = "ISNULL(AVG(CASE SUBSTRING([" + type + "], LEN([" + type + "]), 1) "
          caseStr+=  "WHEN 'K' THEN 1024 * CAST(SUBSTRING([" + type + "], 1, LEN([" + type + "])-1) AS FLOAT) "
          caseStr+=  "WHEN 'M' THEN 1048576 * CAST(SUBSTRING([" + type + "], 1, LEN([" + type + "])-1) AS FLOAT) "
          caseStr+=  "ELSE CAST([" + type + "] AS FLOAT) "
          caseStr+=  "END), 0) AS yvalue "

		if(type=="cpu") {
			caseStr = "ISNULL(AVG(CASE SUBSTRING([" + type + "], LEN([" + type + "]), 1) "
			  caseStr+=  "WHEN 'm' THEN 60 * CAST(SUBSTRING([" + type + "], 1, LEN([" + type + "])-1) AS FLOAT) "
			  caseStr+=  "WHEN 'h' THEN 3600 * CAST(SUBSTRING([" + type + "], 1, LEN([" + type + "])-1) AS FLOAT) "
			  caseStr+=  "ELSE CAST([" + type + "] AS FLOAT) "
			  caseStr+=  "END), 0) AS yvalue "
		}

		sql = "SELECT datestamp, "
		sql += caseStr
			+" 	FROM ("
			+" 		SELECT  dbo.Timestamp_to_interval_time( "
			+"			m.timestamp, "
			+"			'" + min_time_stamp + "', "
			+"			'" + max_time_stamp + "', "
			+"			" + div_seconds + ") AS 'datestamp', "
			+"		id, host FROM " + db + " dbo.measurements m"
			+" 		left join (select master_environment_name, name, multicountry from Configuration.Environments) e"
			+" 		on name = '" + env + "' and e.multicountry = 1"
			+" 		WHERE (environment = '" + env + "' OR environment=e.master_environment_name) and observer = 'mw42.analyser'"
			+"		and m.timestamp between '" + min_time_stamp + "' and '" + max_time_stamp + "'"
			+" 	  ) m2 "
			+" 	INNER JOIN " + db + "observer.[mw42.analyser] epo ON epo.measurement_id = m2.id "

			if(isMulti)
				sql += " 	WHERE m2.host = 'MULTISERVER' AND epo.host = '" + host + "'  and [" + type + "] is not null"
			else
				sql += " 	WHERE m2.host = '"+host+"' AND epo.host is null   and [" + type + "] is not null"
		sql += " 	GROUP BY datestamp"
			+" ORDER BY datestamp ASC";

		rowsA = runSQL(sql,cAcc,"fulltable");

		var rowsB = [];
		if(intarvalInArchive && rowsA.length < 100) {
			db =  archive_db + ".";
			sql = "SELECT datestamp, "
			sql += caseStr
				+" 	FROM ("
				+" 		SELECT  dbo.Timestamp_to_interval_time( "
				+"			m.timestamp, "
				+"			'" + min_time_stamp + "', "
				+"			'" + max_time_stamp + "', "
				+"			" + div_seconds + ") AS 'datestamp', "
				+"		id, host FROM " + db + " dbo.measurements m"
				+" 		left join (select master_environment_name, name, multicountry from Configuration.Environments) e"
				+" 		on name = '" + env + "' and e.multicountry = 1"
				+" 		WHERE (environment = '" + env + "' OR environment=e.master_environment_name) and observer = 'mw42.analyser'"
				+"		and m.timestamp between '" + min_time_stamp + "' and '" + max_time_stamp + "'"
				+" 	  ) m2 "
				+" 	INNER JOIN " + db + "observer.[mw42.analyser] epo ON epo.measurement_id = m2.id "

				if(isMulti)
					sql += " 	WHERE m2.host = 'MULTISERVER' AND epo.host = '" + host + "'"
				else
					sql += " 	WHERE m2.host = '"+host+"' AND epo.host is null "
			sql += " 	GROUP BY datestamp"
				+" ORDER BY datestamp ASC";


		}
		rowsB = runSQL(sql,cAcc,"fulltable");
		while(rowsB.length>0&&rowsA.unshift(rowsB.pop())<100);


		var fields = [{"name": "datestamp"},{"name": "yvalue"}]

		var myJSON = new JSON();

		for ( item in rowsA) {
			if(rowsA[item].datestamp != ""){
				rowsA[item].datestamp = getTimestampFromDate(rowsA[item].datestamp);
				rowsA[item].yvalue = parseFloat(rowsA[item].yvalue.toFixed(2));
			}
		}


		data = myJSON.toJSON(null, rowsA, false);
		fields = myJSON.toJSON(null, fields, false);

		Response.Write( '{ "metaData": { "timestamps": "'+timestamps+'", "totalProperty" : "total", 	"root" : "results", "id" : "id",  "liveDbMinTime" : "' + minDates.liveDbMinTime + '", "archDbMinTime" : "' + minDates.archDbMinTime + '", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');

	}
}catch (err)
{
 logData('diagram/getT24ProcessInfoMemStat.asp: ' + err.message);
  Response.Write( " {success:false,error:'" + err.message + "'} " + sql);
}
%>
