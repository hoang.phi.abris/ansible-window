<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
try{
	var environment = Request("environment");
	var host = Request("host");
	var rec_count = Request("rec_count");
	var min_time_stamp = isset(Request("min_time_stamp")) ? Request("min_time_stamp") : "";
	var max_time_stamp = isset(Request("max_time_stamp")) ? Request("max_time_stamp") : "";
	
	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_PORTAL_REALTIME, environment)) {

		var sort = "desc";

		if(min_time_stamp != ""){
			min_time_stamp = " and timestamp > '"+min_time_stamp+"' ";
			sort = "asc";
		}

		if(max_time_stamp != ""){
			max_time_stamp = " and timestamp < '"+max_time_stamp+"' ";
			sort = "desc";
		}
		
		sql  = "SELECT timestamp, ISNULL(current_locks,0) as current_locks, logged_on_users, cast (CASE WHEN m.host='MULTISERVER' THEN ISNULL(mo1.value,'0') ELSE pid_num END as int) as pid_num, "
		sql += "	cast(CASE WHEN m.host='MULTISERVER' THEN ISNULL(mo2.value,'0') ELSE port_num END as int) as port_num "
		sql += "FROM observer.[T24.ENVIRONMENT] geo "
		sql += "JOIN measurements m on geo.measurement_id = m.id "
		sql += "LEFT OUTER JOIN measurementsOptions mo1 ON mo1.measurement_id = m.id AND mo1.[key] = '" + host + "_pid_num' "
		sql += "LEFT OUTER JOIN measurementsOptions mo2 ON mo2.measurement_id = m.id AND mo2.[key] = '" + host + "_port_num' "
		sql += "WHERE m.id IN (SELECT top " + rec_count + " id FROM measurements left join (select master_environment_name, name, multicountry from Configuration.Environments) e  on name = '" + environment + "' and e.multicountry = 1 "
		sql += "  WHERE (environment = '" + environment + "' OR environment=e.master_environment_name) " + max_time_stamp + min_time_stamp + " AND observer = 'T24.ENVIRONMENT' AND host IN ('" + host + "', 'MULTISERVER') order by timestamp "+sort+") "
		sql += "ORDER BY timestamp ASC "
		
		rowsA = runSQL(sql,cAcc,"fulltable");
		var fields = [{"name": "timestamp"},{"name": "current_locks"},{"name": "logged_on_users"},{"name": "pid_num"},{"name": "port_num"}]
		var myJSON = new JSON();
		for(var i=0; i<rowsA.length;i++)
			rowsA[i].timestamp = formatDateTimeToHighChart(rowsA[i].timestamp)
			
		data = myJSON.toJSON(null, rowsA, false); 
		fields = myJSON.toJSON(null, fields, false); 
		Response.Write( '{ "metaData": { "totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');
	}
}catch (err) 
{
 logData('diagram/t24_environment.asp: ' + err.message);
  Response.Write("{success:false,error:'" + err.message + "'}");
} 
%>