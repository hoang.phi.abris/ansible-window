
<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT" %>

<!--#include file="../../include/JSON.asp"-->
<!--#include file="../../include/get_db_data.asp"-->
<!--#include file="../../include/util.asp"-->

<%
// Retrieve the given limits (or any type of singleValue config) from the configXml
//<script>
try {
    var host = Request("host");
    var env = Request("environment");
    var observer = Request("observer");
    var limitNames = String(Request("limits"));
    var limitArray = limitNames.split(',');
    var sql = 'SELECT ';

    for(var i=0; i<limitArray.length; i++) {
        sql += (i==0 ? '' : ',')+' observerConfig.value(\'(/OBSERVER/CONFIG/@'+limitArray[i]+')[1]\', \'NVARCHAR(50)\') AS '+limitArray[i];
    }

    sql+=' FROM (SELECT host, CAST(thefile as XML).query(\'(/GAMMA/INSTANCES/INSTANCE[@environment_name="'+env+'"]/OBSERVER[@name="' + observer + '" and @enabled="true"])[1]\') as observerConfig'
    sql+=' FROM Configuration.ConfigFiles '
    sql+=' WHERE status = 0 and filetype_id = 1 and host = \''+host+'\') observerConfigs;'

    rowsA = runSQL(sql,cAcc,"fulltable");

    var myJSON = new JSON();
    Response.Write(myJSON.toJSON(null,  rowsA[0], false))

} catch (err) {
    Response.Write("{success:failed, error:'" + err.message +  "' } " );

}
%>
