<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
try{
	if(CheckRight(SEC_ROLE_LOGIN, null) && FindRight(SEC_ROLE_GAMMA_MANAGER_VIEW)) {

		var refresh	= (isset(Request("refresh"))) ? true : false
		var parent	= String(Request("parent"));
		var child = String(Request("child"));

		sql  = "SELECT "
		sql += "	e.name as environment "
		sql += "	,e.description as envdesc "
		sql += "	,i.name as instance "
		sql += "	,i.status as status "
		sql += "	,i.changed as changed "
		sql += "	,i.progress as progress "
		sql += "	,h.name as host "
		sql += "	,h.description as hostdesc "
		sql += "	,DATEDIFF(minute, i.changed, GETDATE()) as sqlNow "
		sql += "FROM "
		sql += "	configuration.environments e "
		sql += "	LEFT join configuration.Instances i on e.name = i.environment "
		sql += "	LEFT join configuration.Hosts h on i.host = h.name  "
	

//Response.Write(sql)


		
		rowsA = runSQL(sql,cAcc,"fulltable");
		
		if (rowsA[0].error == "ERROR")	{
			Response.Write( '({ "success":"false"})')
		}
		else{
			
			var myJSON = new JSON();
			
			var host = {}
			//var rowsResult = [{'root':'root'}]
			var rowsResult = {}
			var id = 0
			for (i=0; i<rowsA.length;i++)	{
				if (rowsA[i][parent]) {
					if (!host[rowsA[i][parent]]) {
						host[rowsA[i][parent]] = {
							name		: rowsA[i][parent]
							,status		: rowsA[i].status
							,time		: (parent=='host') ? rowsA[i].changed : ""
							,instance	: ""
							,progress	: ""
							,icon		: 'resources/img/' + parent +'.png'
							,children   : []
						}  
//						rowsResult.push(rowsA[i].status)
					}
					if (rowsA[i][child]) {
						host[rowsA[i][parent]].children.push({
							name		: rowsA[i][child]
							,status		: rowsA[i].status
							,host		: rowsA[i].host
							,environment: rowsA[i].environment
							,time		: (child=='host') ? rowsA[i].changed : ""
							,instance	: rowsA[i].instance
							,progress	: (rowsA[i].progress) ? (rowsA[i].sqlNow<3) ? rowsA[i].progress : null : rowsA[i].progress
							,icon		: 'resources/img/' + child +'.png'
							,leaf		: "true"
						})
												
					}
				}
			}
			

			for (var s in host) {
				host[s].id = id++				
				rowsResult[host[s].id] = {status: host[s].status, progress:host[s].progress,time:''}
				host[s].progress = null
				for (var i in host[s].children) {					
					host[s].children[i].id = id++
					rowsResult[host[s].children[i].id] = host[s].children[i].status
					rowsResult[host[s].children[i].id] = {status: host[s].children[i].status, progress: host[s].children[i].progress, time: host[s].children[i].time}
					host[s].status = (host[s].children[i].status!=host[s].status) ? 'play_pause' : host[s].status
					host[s].progress = (host[s].children[i].progress) ? host[s].children[i].progress : host[s].progress
					host[s].time = host[s].time
					rowsResult[host[s].id] = {status: host[s].status, progress:host[s].progress, time:host[s].time}
				}
			}
			
			
			var id = 1
			var result = {
				 name 		: "GAMMA " + parent + " view"
				,status		: "root"
				,time		: ""
				,icon		: 'resources/img/icons/16/earth.png'
				,expanded	: true
				,children	: []
			}
			
			for (var s in host) {
				result.children.push(host[s])
			}
			if (refresh) 
				data 	=  myJSON.toJSON(null, rowsResult, false); 
		
			else
			
				data 	=  myJSON.toJSON(null, result, false); 
				
				
		}
		
		if (refresh)
				Response.Write('{"success":"true","parent":"' + parent + '","results": ' + data + '}');   
		  else
		   Response.Write('{"text":".","children": [' + data + ']}');
	}
}catch (err) {
  Response.Write('Error: ' + err.message);
  logData('controll/GAMMAControll.asp: ' + err.message);
}
	%>