
<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

<!--#include file="../include/JSON.asp"-->
<!--#include file="../include/get_db_data.asp"-->
<!--#include file="../security/security.asp"-->
<!--#include file="../include/util.asp"-->
<%

try {
	var parameter = String(Request("param"));
	var modified_by = String(Request("modified_by"));
	var env = String(Request("env"));
	var host = String(Request("host"));
	var logType = Request("logType");
    var commandId = Request("commandId");
    var runByServer = String(Request("runByServer"));
	
	if (runByServer  == "undefined")
		runByServer = 0;

    var commandEnvRole = sendCommandRoles[commandId];
	// Response.Write(String(commandEnvRole)=="undefined")
	if(CheckRight(SEC_ROLE_LOGIN, null) && (!commandEnvRole || CheckRight(commandEnvRole, env) ) ) {
		var result = SendCommand(modified_by, env, host,commandId, parameter, runByServer);
		var myJSON = new JSON();
		data = myJSON.toJSON(null, result[0], false); 
        Response.Write( data );

		if ((String(logType)!='undefined') && result[0].result=="True") {
			var parameterArray = parameter.split(" ");
			var auditSql = "INSERT INTO Audit.LogDetails ([user], log_type, timestamp, env, param1, param2) VALUES('" + modified_by + "', " + logType + " ,GETDATE(),'" + env + "', 1, '" + parameter + "' )";
			runSQL(auditSql,cAcc,"cmd");
		}
	}
} 
catch (err) {
	 logData('controll/sendCommand.asp: ' + err.message);
   Response.Write('{success:false,error:"' + err.message + '"}');  
}

	%>
