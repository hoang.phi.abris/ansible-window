<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

  <!--#include file="include/JSON.asp"-->
  <!--#include file="include/get_db_data.asp"-->

  <%
  try {
    var inputterid = Request("inputterid");
    var commandid = Request("commandid");
    var host = Request("host");
    var typeid = Request("typeid");
    var thefile = ''
    var result = null;

    var adCmdStoredProc = 4
    var adParamInput = 1
    var adParamOutput = 2
    var adInteger = 3
    var adBStr = 8
    var adVarWChar = 202
    var adLongVarWChar = 203
    var adStateClosed = 0

    var cmd = Server.CreateObject('ADODB.Command');
    cmd.ActiveConnection = cAcc;
    cmd.CommandType = adCmdStoredProc
    cmd.CommandText = "Configuration.GetServerFile"

    cmd.Parameters.Append(cmd.CreateParameter("@inputter", adInteger, adParamInput, -1, inputterid));
    cmd.Parameters.Append(cmd.CreateParameter("@commandid", adInteger, adParamInput, -1, commandid));
    cmd.Parameters.Append(cmd.CreateParameter("@host", adVarWChar, adParamInput, -1, host));

    var rsTEMP = cmd.Execute();
    if(rsTEMP.State != adStateClosed)
      result = rsTEMP.Fields(0).Value;
    else {
      if (typeid != '') {
        var sql = "SELECT name From Configuration.ConfigFileTypes WHERE id = " + typeid;
        var rowsA = runSQL(sql,cAcc,"fulltable");
        if (rowsA.length > 0) {
          var fs,f,t,x
          fs=Server.CreateObject("Scripting.FileSystemObject")
          var currentdirectorypath = Server.MapPath(".")
          t=fs.OpenTextFile(currentdirectorypath + "\\defaults\\" + rowsA[0].name, 1, false)
  
          result = t.ReadAll()
          t.close()
        }
      }  
    }
    
    if (result == null) 
      Response.Write("Error: Failed while loading the file");
    else
      Response.Write(result);
  } 
  catch (err) {
    Response.Write('Error: ' + err.message);
     logData('get_server_file.asp: ' + err.message);
  }
%>