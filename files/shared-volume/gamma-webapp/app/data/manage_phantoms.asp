<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="include/JSON.asp"-->
	<!--#include file="include/get_db_data.asp"-->
	<!--#include file="security/security.asp"-->


	<%

	var environment = Request("environment");
	if(CheckRight(SEC_ROLE_LOGIN, null)) {
		var conn = '';
		try {
			var modified_by = String(Request("modified_by"));							
			var param = String(Request("param"));

			var sql = "select h.name from Configuration.Environments e INNER JOIN Configuration.Hosts h ON h.id = e.default_host WHERE e.name = '" + environment + "'";
			rowsA = runSQL(sql,cAcc,"fulltable");
			if (rowsA.length)  {
				var defaultHost = rowsA[0].name;
				sql = "select name from Configuration.Instances where host = '" + defaultHost + "' and environment = '" + environment + "'";
				rowsA = runSQL(sql,cAcc,"fulltable");
				if (rowsA.length)  {
					var instance = rowsA[0].name;

					conn = beginTransaction(cAcc) 	
					sql = "INSERT INTO Communication (inputter_id, command_id, host, timestamp, environment, instance, parameter) VALUES(1, 23, '" + defaultHost + "', GETDATE(), '" + environment+ "', '" + instance + "', '" + param + "')";
					runSQL(sql,conn,"cmd");
					
					var log_type = 0;
					var params = param.split(',')
					switch (params[0]) {
						case "START":
							log_type = 72;
							break;
						case "STOP":
							log_type = 73;
							break;
						case "LOGOFF":
							log_type = 74;
							break;
						case "STOPALL":
							log_type = 75;
							break;
					}
					var phantom = ''
					if (params.length > 1)
						phantom = params[1];
					var sql2 = "INSERT INTO Audit.LogDetails ([user], log_type, timestamp, env, param1, param2) VALUES('" + modified_by + "'," + log_type + ",GETDATE(),'" + environment + "', NULL, '" + phantom + "')";
					runSQL(sql2,conn,"cmd");

					commitTransaction(conn);
					Response.Write("Command sent");
				}
			}
		} 
		catch (err) {
			if (conn)
			  rollbackTransaction(conn)
			Response.Write('Error: ' + err.message);
			   logData('manage_phantoms.asp: ' + err.message);
		}
	}
%>