<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="include/JSON.asp"-->
	<!--#include file="include/get_db_data.asp"-->
	<!--#include file="include/controller_groups.asp"-->
	<!--#include file="security/security.asp"-->
	<!--#include file="include/json2.asp"-->

	<%
try {
	var environment_id = Number(Request("environment_id"));
	if(CheckRight(SEC_ROLE_LOGIN, null) && FindRights([SEC_ROLE_SECMAN_USERS, SEC_ROLE_SECMAN_GROUPS, SEC_ROLE_SECMAN_ASSIGN]))
	{
		var fileName = 'SecurityConfig.sec'

		var securityData = {}
		
		securityData.PortalProfiles = []
		var sql= "SELECT id, user_name, name, is_global FROM Configuration.PortalProfile where is_global = 1" 
		var rowsA = runSQL(sql,cAcc,"fulltable");
		for(var i=0; i<rowsA.length; i++) {
			rowsA[i].items = []
			securityData.PortalProfiles.push(rowsA[i])
		}
		
		sql= 'SELECT pi.id as "id", profile_id, e.id as "environment_id", position, diagram_id FROM Configuration.PortalProfileItem pi INNER JOIN Configuration.Environments e ON pi.env = e.name'
		var rowsA = runSQL(sql,cAcc,"fulltable");
		for(var i=0; i<rowsA.length; i++) {
			for(var j=0; j<securityData.PortalProfiles.length; j++) {
				if (rowsA[i].profile_id == securityData.PortalProfiles[j].id) {
					securityData.PortalProfiles[j].items.push(rowsA[i])
					break;
				}
			}
		}
		
		securityData.Environments = []
		var sql= "SELECT id, name FROM Configuration.Environments"
		var rowsA = runSQL(sql,cAcc,"fulltable");
		for(var i=0; i<rowsA.length; i++) {
			securityData.Environments.push(rowsA[i])
		}

		securityData.Users = []
		var sql= "SELECT id, ad_id, name, details, status, profile_id, environment_id, popup, sessiontimeout FROM Security.[User] WHERE id > 0"
		var rowsA = runSQL(sql,cAcc,"fulltable");
		for(var i=0; i<rowsA.length; i++) {
			securityData.Users.push(rowsA[i])
		}
		
		securityData.Roles = []
		var sql= "SELECT id, name, description, type, sec_id, environment_id FROM Security.[Role]"
		var rowsA = runSQL(sql,cAcc,"fulltable");
		for(var i=0; i<rowsA.length; i++) {
			securityData.Roles.push(rowsA[i])
		}
		
		securityData.UserGroups = []
		var sql= "SELECT id, name, description FROM Security.[UserGroup]"
		var rowsA = runSQL(sql,cAcc,"fulltable");
		for(var i=0; i<rowsA.length; i++) {
			securityData.UserGroups.push(rowsA[i])
		}
		
		securityData.RoleGroups = []
		var sql= "SELECT id, name, description FROM Security.[RoleGroup]"
		var rowsA = runSQL(sql,cAcc,"fulltable");
		for(var i=0; i<rowsA.length; i++) {
			securityData.RoleGroups.push(rowsA[i])
		}
		
		securityData.UserRoles = []
		var sql= "SELECT id, user_id, role_id FROM Security.[UserRoles]"
		var rowsA = runSQL(sql,cAcc,"fulltable");
		for(var i=0; i<rowsA.length; i++) {
			securityData.UserRoles.push(rowsA[i])
		}
 
		securityData.UserRoleGrps = []
		var sql= "SELECT id, user_id, rolegroup_id FROM Security.[UserRoleGrps]"
		var rowsA = runSQL(sql,cAcc,"fulltable");
		for(var i=0; i<rowsA.length; i++) {
			securityData.UserRoleGrps.push(rowsA[i])
		}

		securityData.UserUserGrps = []
		var sql= "SELECT id, user_id, usergroup_id FROM Security.[UserUserGrps]"
		var rowsA = runSQL(sql,cAcc,"fulltable");
		for(var i=0; i<rowsA.length; i++) {
			securityData.UserUserGrps.push(rowsA[i])
		}
 
		securityData.UserGrpUserGrps = []
		var sql= "SELECT id, usergroup1_id, usergroup2_id FROM Security.[UserGrpUserGrps]"
		var rowsA = runSQL(sql,cAcc,"fulltable");
		for(var i=0; i<rowsA.length; i++) {
			securityData.UserGrpUserGrps.push(rowsA[i])
		}
  
		securityData.RoleUserGrps = []
		var sql= "SELECT id, role_id, usergroup_id FROM Security.[RoleUserGrps]"
		var rowsA = runSQL(sql,cAcc,"fulltable");
		for(var i=0; i<rowsA.length; i++) {
			securityData.RoleUserGrps.push(rowsA[i])
		}
 
		securityData.RoleRoleGrps = []
		var sql= "SELECT id, role_id, rolegroup_id FROM Security.[RoleRoleGrps]"
		var rowsA = runSQL(sql,cAcc,"fulltable");
		for(var i=0; i<rowsA.length; i++) {
			securityData.RoleRoleGrps.push(rowsA[i])
		}
 
		securityData.RoleGrpRoleGrps = []
		var sql= "SELECT id, rolegroup1_id, rolegroup2_id FROM Security.[RoleGrpRoleGrps]"
		var rowsA = runSQL(sql,cAcc,"fulltable");
		for(var i=0; i<rowsA.length; i++) {
			securityData.RoleGrpRoleGrps.push(rowsA[i])
		}
 
		securityData.UserGrpRoleGrps = []
		var sql= "SELECT id, usergroup_id, rolegroup_id FROM Security.[UserGrpRoleGrps]"
		var rowsA = runSQL(sql,cAcc,"fulltable");
		for(var i=0; i<rowsA.length; i++) {
			securityData.UserGrpRoleGrps.push(rowsA[i])
		}

		var data = JSON.stringify(securityData, null, 2);

		Response.ContentType = "text/plain";
		Response.AddHeader("Content-disposition", "attachment; filename=" + fileName);

		Response.Write(data);
	}
}
catch (err) {
  Response.Write('Error: ' + err.message);
   logData('get_user_config.asp: ' + err.message);
}
%>