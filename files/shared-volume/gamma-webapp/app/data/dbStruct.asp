<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="include/JSON.asp"-->
	<!--#include file="include/get_db_data.asp"-->
	<!--#include file="security/security.asp"-->

	<%				

try 
{
	if(CheckRight(SEC_ROLE_LOGIN, null) && FindRight(SEC_ROLE_ALERT_SOLVE)) {

		var conn = beginTransaction(cAcc) 
	
		var sql = "INSERT INTO Communication (inputter_id, command_id,     host, timestamp, run_by_server) ";
			sql += " VALUES						       (1, 		   32, 'SERVER', GETDATE(),            1 )";
		runSQL(sql,conn,"cmd");
		commitTransaction(conn);
		
		Response.Write( '{success:true}');
	}
}
catch (err) 
{
	if (conn)
		rollbackTransaction(conn) 
	Response.Write("{success:false,error:'" + err.message + "'}");
	logData('dbStruct.asp: ' + err.message);
}
%>