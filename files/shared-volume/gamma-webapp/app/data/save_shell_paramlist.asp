<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

<!--#include file="include/JSON.asp"-->
<!--#include file="include/get_db_data.asp"-->
<!--#include file="include/util.asp"-->
<!--#include file="security/security.asp"-->
<!--#include file="include/json2.asp"-->
<!--#include file="include/contoller_shell_paramlist.asp"-->

<%
	var conn = ''
	var sql = ''
	try
	{
		var environment = String(Request("environment"))
		var data_s = String(Request("data"))
		var data = eval('(' + data_s + ')');

		var count = 0;

		if(CheckRight(SEC_ROLE_LOGIN, null) && (CheckRight(SEC_ROLE_CONTROLLER_VIEW, environment) || CheckRight(SEC_ROLE_CONTROLLER_CONFIG, environment)))
		{
			conn = beginTransaction(cAcc);

			if (data.command.param_list_id != "") {
				handleParamList(data.command, data.host_id, data.paramLists, conn);
			}
			if (data.command.cmd_class.indexOf('ShellCommand') > 0) {
				handleShell(data.command, "cmd_class_param", data.host_id, data.shells, conn);
			}
			if (data.command.check_cmd_class.indexOf('ShellCheckCommand') > 0) {
				handleShell(data.command, "check_cmd_class_param", data.host_id, data.shells, conn);
			}
			if (data.command.status_cmd_class != undefined && data.command.status_cmd_class.indexOf('ShellStatusCommand') > 0) {
				handleShell(data.command, "status_cmd_class_param", data.host_id, data.shells, conn);
			}

			commitTransaction(conn);
			var response = JSON.stringify(data.command, null, 2);
			Response.Write(response);
		}
	}
	catch(err) {
		if (conn) {
			rollbackTransaction(conn)
		}
		Response.Write("Error: " + err.message);
		logData('save_shell_paramlist.asp: ' + err.message);
	}
%>
