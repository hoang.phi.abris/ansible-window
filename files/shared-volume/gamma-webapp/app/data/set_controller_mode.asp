<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="include/JSON.asp"-->
	<!--#include file="include/get_db_data.asp"-->
	<!--#include file="security/security.asp"-->

	<%
try {
	if(CheckRight(SEC_ROLE_LOGIN, null)) {
		var function_id = String(Request("function_id"));
		var mode = String(Request("mode")) == "true" ? 1 : 0;
		var userID = Session("username");

		sql = "UPDATE Configuration.ControllerFunction SET mode = " + mode + " WHERE id = " + function_id
		if (mode == 1) {
			sql += " AND 0 = (SELECT count(*) from Configuration.ControllerGroup g";
			sql += " JOIN Configuration.ControllerAction a ON a.group_id = g.id";
			sql += " WHERE g.function_id = " + function_id + " AND ((g.startable = 1 and g.status NOT IN (1, 2, 8, 9)) OR a.status IN (3, 4, 5, 6, 9, 12, 13)))"
		} else {
			sql += " AND 0 = (SELECT count(*) from Configuration.ControllerGroup g ";
 			sql += " INNER JOIN Configuration.ControllerAction a ON a.group_id = g.id ";
 			sql += " INNER JOIN Configuration.ControllerFunction f ON g.function_id = f.id ";
 			sql += " INNER JOIN Configuration.Environments e ON f.environment_id = e.id ";
 			sql += " INNER JOIN Configuration.ControllerCommand c ON a.run_command = c.id AND ((a.host_id = 0 AND e.default_host = c.host_id) OR a.host_id = c.host_id) ";
 			sql += " WHERE g.function_id = " + function_id + " AND c.record_status <> 0) ";
		}
		
		runSQL(sql,cAcc,"cmd");
		
		var sql= "SELECT mode FROM Configuration.ControllerFunction WHERE id = " + function_id;
		rowsA = runSQL(sql,cAcc,"fulltable");
		
		var myJSON = new JSON();
		var data = ''
		var fields = ''
		if (rowsA.length)  {
			data = myJSON.toJSON(null, rowsA, false); 
			fields = myJSON.generateFields(rowsA)
		}
		else {
			data = '[]'
			fields = '[]'
		}

		//logData(rowsA[0].mode + ' ' + mode)
		if (rowsA[0].mode == mode) {
			var stopGoStatus = (mode == 1 ? 12 : 3);
			sql = "UPDATE Configuration.ControllerAction SET status = " + stopGoStatus + ", modified_by = '" + userID + "' WHERE command_type = 'STATUS' AND group_id IN "
			sql += "(SELECT id FROM Configuration.ControllerGroup WHERE function_id IN "
			sql += "(SELECT id FROM Configuration.ControllerFunction WHERE id = " + function_id + "))"
			runSQL(sql,cAcc,"cmd");
		}

		var modified_by = Session("username");
		sql = "INSERT INTO Audit.LogDetails ([user], log_type, timestamp, env, param1, param2) SELECT '" + modified_by + "', 58, GETDATE(), e.name, " + mode + ", cf.name "
		sql += "FROM Configuration.ControllerFunction cf INNER JOIN Configuration.Environments e ON cf.environment_id = e.id WHERE cf.id = " + function_id 
		runSQL(sql,cAcc,"cmd");			
				
		Response.Write( '{ "metaData": {	"totalProperty" : "total", "root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '", "results":' + data + '}');
	}
} 
catch (err) {
  Response.Write('Error: ' + err.message);
   logData('set_controller_mode.asp: ' + err.message);
}

%>