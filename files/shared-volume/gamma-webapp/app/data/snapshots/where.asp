<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->
	<!--#include file="../include/util.asp"-->
	<script language="javascript" runat="server">

		try {
			var inputter = String(Request("inputter"));
			//var environment = String(Request("environment"));
			var parameters = String(Request("parameters"));

			var host = isset(Request("host")) ? " ,@host = '" + String(Request("host")) + "'" : ""

			if (CheckRight(SEC_ROLE_LOGIN, null)) {

				sql = "execute dbo.getSnapshot @stype = '2', @environment='" + env + "' " + host


				rowsA = runSQL(sql, cAcc, "fulltable");
				var myJSON = new JSON();
				if (rowsA.length == 0) {
					Response.Write('({ "metaData": {	"totalProperty" : "total", "root" : "results", "id" : "id"}, "total":"0", "results":[]})');
				}
				else {
					var resultGrid = [];
					var resArr = eval(rowsA[0].value)



					for (i = 0; i < resArr.length; i++) {
						resultGrid.push({
							"Port": resArr[i].port
							, "Device": resArr[i].device
							, "Account": resArr[i].user
							, "Pid": resArr[i].pid
							, "Command": resArr[i].commands.join('<br>')
							, "sessionid": resArr[i].sessionId
							, "inappserver": resArr[i].inWebServer
						})
					}

					data = myJSON.toJSON(null, resultGrid, false);
					fields = myJSON.generateFields(resultGrid);
					count = resultGrid.length;
					Response.Write('{ "metaData": {	"totalProperty" : "total", "root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + count + '", "results":' + data + '}');
				}
			}
		}
		catch (err) {
			logData('snapshots/where.asp: ' + err.message);
			Response.Write('Error: ' + err.message);
		}


		function encodeMyHtml(v) {
			encodedHtml = escape(v);
			encodedHtml = encodedHtml.replace(/\//g, "%2F");
			encodedHtml = encodedHtml.replace(/\?/g, "%3F");
			encodedHtml = encodedHtml.replace(/=/g, "%3D");
			encodedHtml = encodedHtml.replace(/&/g, "%26");
			encodedHtml = encodedHtml.replace(/@/g, "%40");
			return encodedHtml;
		}
	</script>