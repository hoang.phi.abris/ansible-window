<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->
	<!--#include file="../include/util.asp"-->

	<%
try {
	if(CheckRight(SEC_ROLE_LOGIN, null)) {
		sql = "SELECT "
		sql += "	host "
		sql += "	,(	SELECT "
		sql += "			name "
		sql += "		FROM "
		sql += "			configuration.Hosts "
		sql += "		WHERE id = (select default_host from Configuration.Environments WHERE name = '" + env + "') "
		sql += "	) as 'default' "
		sql += " from configuration.Instances WHERE environment = '" + env + "'" 

		rowsA = runSQL(sql,cAcc,"fulltable");
		var myJSON = new JSON();
		
		data = myJSON.toJSON(null, rowsA, false); 	
		fields = myJSON.generateFields(rowsA);
		count = rowsA.length;
		Response.Write( '{ "metaData": {	"totalProperty" : "total", "root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + count + '", "results":' + data + '}');
	}
} 
catch (err) {
logData('snapshots/getEnvironmentHost.asp: ' + err.message);
  Response.Write('Error: ' + err.message);
}

%>