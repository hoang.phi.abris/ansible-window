<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

<!--#include file="include/JSON.asp"-->
<!--#include file="include/get_db_data.asp"-->
<!--#include file="include/util.asp"-->
<!--#include file="security/security.asp"-->
<!--#include file="include/contoller_shell_paramlist.asp"-->

<%
//<script>
  var conn = ''
  var sql = ''
  try
  {
    var environment_id = String(Request("environment_id"))
    var data = String(Request("data"))
    var overwrite = String(Request("overwrite"))
    var eraseExisting = String(Request("eraseExisting"))

	var userID = Session("username");
  	var d = new Date(new Date().getTime()+2000)
  		while(new Date()<d) {}
	var count = 0;
	if(CheckRight(SEC_ROLE_LOGIN, null))
	{
		var sql = "SELECT name, default_host From Configuration.Environments WHERE id = " + environment_id;
		var rowsA = runSQL(sql,cAcc,"fulltable");
		var commands = []
		if (rowsA.length > 0) {
    		if(rowsA[0].default_host)
				default_host = rowsA[0].default_host;

			conn = beginTransaction(cAcc)
			commands = getControllerCommands(conn);

			if(eraseExisting == 1) {
				var sql = "SELECT id FROM Configuration.ControllerFunction WHERE environment_id = " + environment_id;
				var rowsB = runSQL(sql,conn,"fulltable");
				for(var i=0; i<rowsB.length; i++) {
					var sql = "exec Configuration.DeleteControllerFunction " + rowsB[i].id;
					runSQL(sql,conn,"cmd");
				}

				sql = "DELETE FROM Configuration.ControllerFunction WHERE environment_id = " + environment_id;
				runSQL(sql,conn,"cmd");

				sql = "DELETE FROM Configuration.ControllerScheduler WHERE environment_id = " + environment_id;
				runSQL(sql,conn,"cmd");
			}

			var warn_count = 0;
			var funct_list = eval('(' + data + ')');
			for(var i=0; i<funct_list.length; i++) {
				var funct = funct_list[i]

				if (i == 0) {
					for(var j=0; j<funct.schedulers.length; j++) {
						var sched = funct.schedulers[j];
						sql = "SELECT id FROM Configuration.ControllerScheduler WHERE name = '" + sched.name + "' AND environment_id = " + environment_id
						rowsC = runSQL(sql,conn,"fulltable");

						if(rowsC.length == 0) {
						   /** insert new hardday for the scheduler */
						   if(sched.stype == 'harddays') {

							   var rowsD = runSQL("SET NOCOUNT ON;" +
								   " INSERT INTO Configuration.[COB.HardDays](name, description, value, icon)" +
								   "   VALUES ('" + sched.name + "','Imported hardday'," +
								   "   (SELECT ISNULL(MAX(value), 1) * 2 FROM Configuration.[COB.HardDays]), 'cob-closed');" +
								   " SELECT MAX(value) as [id] FROM Configuration.[COB.HardDays]", conn, "fulltable");
								   sched.param = rowsD[0].id;
						   }
							sql = "SET NOCOUNT ON; INSERT INTO Configuration.ControllerScheduler (name, startdate, frequency, type, param, is_automatic, start_at, timeout, environment_id, day_type, t24_state, date_type) "
							sql += "VALUES ('" + sched.name + "', '" + sched.startdate + "', " + sched.frequency + ", '" + sched.stype + "', '" + sched.param + "', " + (sched.is_automatic?1:0) + ", '" + sched.start_at + "', " + sched.timeout + ", " + environment_id + ", '"+ sched.day_type + "', '" + sched.t24_state + "', '" + sched.date_type + "')"
							sql += "; SELECT SCOPE_IDENTITY() AS [id]"

							rowsC = runSQL(sql,conn,"fulltable");
						}
						else
							warn_count++;

						if (rowsC.length > 0) {
							for(var k=0; k<funct_list.length; k++) {
								for(var l=0; l<funct_list[k].groups.length; l++) {
									var group = funct_list[k].groups[l];
									if (group.scheduler_name == sched.name) {
										group.scheduler_name = rowsC[0].id;
									}
									for(var m=0; m<group.actions.length; m++) {
										var action = group.actions[m];
										if (action.scheduler_name == sched.name) {
											action.scheduler_name = rowsC[0].id;
										}
									}
								}
							}
						}
					}
				}

				if (funct.checked) {
					if (overwrite == 1 && eraseExisting != 1) {
						var sql = "SELECT id FROM Configuration.ControllerFunction WHERE environment_id = " + environment_id + " AND name = '" + funct.name + "'";
						var rowsB = runSQL(sql,conn,"fulltable");
						if (rowsB.length>0) {
							var sql = "exec Configuration.DeleteControllerFunction " + rowsB[0].id;
							runSQL(sql,conn,"cmd");
						}

						sql = "DELETE FROM Configuration.ControllerFunction WHERE environment_id = " + environment_id + " AND name = '" + funct.name + "'";
						runSQL(sql,conn,"cmd");
					}

					var changedCommands = [];
					var mode = 0
					for(var j=0; j<funct.groups.length; j++) {
						for(var k=0; k<funct.groups[j].actions.length; k++) {
							var action = funct.groups[j].actions[k]
							var orig_host_id = ((action.orig_host_id != undefined) ? action.orig_host_id : 0);
							var command = getCommand(funct.commands, orig_host_id, action.run_command);
							if (command == null) {
								action.error_status = 1;
								mode = 1;
							} else {
								var host_id = ((action.host_id == 0) ? default_host : action.host_id);
								var command_id = checkCommand(commands, host_id, command.name);
								if (command_id < 0) {
									command_id = addNewCommand(conn, command, host_id, funct);
								}
								if (command_id < 0) {
									action.error_status = 1;
									mode = 1;
								} else {
									commands.push({
										id : command_id,
										name : command.name,
										host_id : host_id
									});
									changedCommands.push({
										old_id : action.run_command,
										old_host : orig_host_id,
										new_id : command_id,
										new_host : host_id
									});
									action.error_status = 0;
								}
							}
						}
					}

					for(var j=0; j<funct.groups.length; j++) {
						for(var k=0; k<funct.groups[j].actions.length; k++) {
							var action = funct.groups[j].actions[k]
							if (!action.changed) {
								var orig_host_id = ((action.orig_host_id != undefined) ? action.orig_host_id : 0);
								var change = getCommandChange(changedCommands, orig_host_id, action.run_command);
								action.changed = true;
								if (change) {
									action.run_command = change.new_id;
									if (change.new_host == default_host) {
										action.host_id = 0;
									}
								}
							}
						}
					}

					warn_count+=mode
					var is_cob = funct.is_cob ? 1 : 0;
					sql = "SET NOCOUNT ON; INSERT INTO Configuration.ControllerFunction (name, environment_id, position, mode, is_cob) "
					sql += "VALUES ('" + funct.name + "', " + environment_id + ", " + (i + 1) + ", " + mode + ", " + is_cob + " ); SELECT SCOPE_IDENTITY() AS id"


					rowsA = runSQL(sql,conn,"fulltable");

					var sqlF = "exec Configuration.NewControllerFunction " + rowsA[0].id;
					runSQL(sqlF,conn,"cmd");

					funct.id = rowsA[0].id
					while(funct.groups.length > 0) {
						var gindex = 0;
						for(var j=0; j<funct.groups.length; j++) {
							var found = 0;
							for(var k=j+1;k<funct.groups.length;k++) {
								if (funct.groups[j].dependent_id == funct.groups[k].id) {
									found = 1;
									break;
								}
							}
							if (!found) {
								gindex = j;
								break;
							}
						}
						var group = funct.groups[gindex]
						sql = "SET NOCOUNT ON; INSERT INTO Configuration.ControllerGroup (name, function_id, startable, status, modified_by, modified_at, position, email, dependent_id, group_scheduler_id, autostart)  "
						sql += "VALUES ('" + group.name + "', " + funct.id + ", " + (group.startable?1:0) + ", 1, '" + userID + "', GETDATE(), " + (j + 1) + ", '" + group.email + "', " + group.dependent_id + ", " + (group.scheduler_name == "" ? 0 : group.scheduler_name) + ", " + (group.autostart?1:0) +")"
						sql += "; SELECT SCOPE_IDENTITY() AS id"

						rowsA = runSQL(sql,conn,"fulltable");


						var sqlG = "exec Configuration.NewControllerGroup " + rowsA[0].id;
						runSQL(sqlG,conn,"cmd");

						var newgroup_id = rowsA[0].id
						for(var k=0; k<funct.groups.length; k++)
							if (funct.groups[k].dependent_id == group.id)
								funct.groups[k].dependent_id = newgroup_id;

						for(var j=0; j<group.actions.length; j++)
							group.actions[j].position = j+1;

						while(group.actions.length > 0) {
							var index = 0;
							for(var j=0; j<group.actions.length; j++) {
								var found = 0;
								for(var k=j+1;k<group.actions.length;k++) {
									if (group.actions[j].dependent_id == group.actions[k].id) {
										found = 1;
										break;
									}
								}
								if (!found) {
									index = j;
									break;
								}
							}

							var action = group.actions[index]
							sql = "SET NOCOUNT ON; INSERT INTO Configuration.ControllerAction (group_id, name, run_command, dependent_id, position, status, modified_by, modified_at, error_status, command_type, host_id, email_onsuccess, email_onerror, re_runnable, stoppable, scheduler_id, [cob_time_type]) "
							sql += "VALUES (" + newgroup_id + ", '" + action.name + "', '" + action.run_command + "', " + action.dependent_id + ", " + action.position + ", 1, '" + userID + "', GETDATE(), " + action.error_status + ", '" + action.command_type + "', " + action.host_id + ", " + (action.email_onsuccess?1:0) + ", " + (action.email_onerror?1:0) + ", " + (action.re_runnable?1:0) + ", " + (action.stoppable?1:0) + ", " + (action.scheduler_name==""?0:action.scheduler_name) + ", " + action.cob_time_type + ")"
							sql += "; SELECT SCOPE_IDENTITY() AS id"
							rowsA = runSQL(sql,conn,"fulltable");

							var sqlA = "exec Configuration.NewControllerAction " + rowsA[0].id;
							runSQL(sqlA,conn,"cmd");

							for(var l=0; l<group.actions.length; l++)
								if (group.actions[l].dependent_id == action.id)
									group.actions[l].dependent_id = rowsA[0].id

							group.actions.splice(index, 1);
						}
						funct.groups.splice(gindex, 1);
					}
					count++;
				}
			}
			commitTransaction(conn)
			Response.Write(count + " function" + (count>1?"s were":" was") + " successfully saved")
			if (warn_count)
				Response.Write(" with " + warn_count + " warning" + (warn_count>1?"s":""))
		}
		else
			Response.Write("Error: unknown environment given");
    }
  }
  catch(err) {
	logData(sql)
    if (conn)
      rollbackTransaction(conn)
    Response.Write("Error: " + err.message)
	 logData('save_functions.asp: ' + err.message);
  }

 function checkCommand(commands, host_id, name) {
	for(var i=0; i<commands.length; i++) {
		if (commands[i].host_id == host_id && commands[i].name == name)
			return commands[i].id;
	}
	return -1;
 }

 function getControllerCommands(conn) {
	var sql = "SELECT id, host_id, name FROM Configuration.ControllerCommand";
	return runSQL(sql,conn,"fulltable");
 }

 function getCommand(commands, orig_host_id, command_id) {
	for(var i=0; i<commands.length; i++) {
		if (commands[i].id == command_id && commands[i].host_id == orig_host_id) {
			return commands[i];
		}
	}
	return null;
 }

 function addNewCommand(conn, command, host_id, funct) {
	if (command.param_list_id != "") {
		handleParamList(command, host_id, funct.paramLists, conn);
	}
	if (command.cmd_class.indexOf('ShellCommand') > 0) {
		handleShell(command, "cmd_class_param", host_id, funct.shells, conn);
	}
	if (command.check_cmd_class.indexOf('ShellCheckCommand') > 0) {
		handleShell(command, "check_cmd_class_param", host_id, funct.shells, conn);
	}
	if (command.status_cmd_class != undefined && command.status_cmd_class.indexOf('ShellStatusCommand') > 0) {
		handleShell(command, "status_cmd_class_param", host_id, funct.shells, conn);
	}

	var	sql = "SET NOCOUNT ON;  INSERT INTO Configuration.ControllerCommand (id, host_id, version, type, name, description, check_timeout, run_in_paused_state, cmd_class, cmd_class_param, command_line, param_list_id, stop_command_line, ";
	sql += "command_param, check_cmd_class, check_cmd_class_param, check_cmd_line, check_cmd_line_param, ";
	sql += "status_cmd_class, status_cmd_class_param, status_cmd_line, status_cmd_line_param, record_status, ";
	sql += "template_id, email_to, email_cc, email_subject, email_body, email_attachment, email_auto, command_line_start)  ";
	sql += "VALUES((SELECT max(id)+1 FROM Configuration.ControllerCommand), " + host_id + ", 1, '" + command.type + "', ";
	sql += "'" + command.name + "', ";
	sql += "'" + command.description + "', ";
	sql += (command.check_timeout?command.check_timeout:'NULL') + ", ";
	sql += (command.run_in_paused_state == "true" ? 1 : 0) + ", ";
	sql += "'" + command.cmd_class + "', ";
	sql += "'" + command.cmd_class_param + "', ";
	sql += "'" + command.command_line + "', ";
	sql += "'" + command.param_list_id + "', ";
	sql += "'" + command.stop_command_line + "', ";
	sql += "'" + command.command_param + "', ";
	sql += "'" + command.check_cmd_class + "', ";
	sql += "'" + command.check_cmd_class_param + "', ";
	sql += "'" + command.check_cmd_line + "', ";
	sql += "'" + command.check_cmd_line_param + "', ";
	sql += "'" + command.status_cmd_class + "', ";
	sql += "'" + command.status_cmd_class_param + "', ";
	sql += "'" + command.status_cmd_line + "', ";
	sql += "'" + command.status_cmd_line_param + "', ";
	sql += "2, ";
	sql += "'" + command.template_id + "', ";
	sql += "'" + command.email_to + "', ";
	sql += "'" + command.email_cc + "', ";
	sql += "'" + command.email_subject + "', ";
	sql += "'" + command.email_body + "', ";
	sql += "'" + command.email_attachment + "',"+ (command.email_auto == "true" ? 1 : 0)+","+ (command.command_line_start == "true" ? 1 : 0) + ")";
	sql += "; SELECT max(id) as id FROM Configuration.ControllerCommand";

	rows = runSQL(sql,conn,"fulltable");
	return rows.length > 0 ? rows[0].id : -1;
 }

 function getCommandChange(changedCommands, orig_host_id, run_command) {
	for(var i=0; i<changedCommands.length; i++) {
		if (changedCommands[i].old_host == orig_host_id && changedCommands[i].old_id == run_command) {
			return changedCommands[i];
		}
	}
	return null;
 }
%>
