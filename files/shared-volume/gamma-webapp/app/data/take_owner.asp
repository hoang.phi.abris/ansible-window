
<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

<!--#include file="include/JSON.asp"-->
<!--#include file="include/get_db_data.asp"-->
<!--#include file="security/security.asp"-->

<%
var measurement_id = String(Request("measurement_id"));							
var owner = String(Request("owner"));							

try {
	if(CheckRight(SEC_ROLE_LOGIN, null) && FindRight(SEC_ROLE_ALERT_TAKEOWNER)) {
		var sql= "UPDATE Alerts SET owner = '" + owner + "', owner_timestamp = GETDATE() WHERE measurement_id = '" + measurement_id + "'"
		runSQL(sql,cAcc,"cmd");
		Response.Write( 'Ownership was successfully taken');
	}
}
catch (err) {
  Response.Write('Error: ' + err.message);
   logData('take_owner.asp: ' + err.message);
}

%>
