<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

<!--#include file="include/JSON.asp"-->
<!--#include file="include/get_db_data.asp"-->
<!--#include file="include/util.asp"-->
<!--#include file="security/security.asp"-->

<%
//<script>
	var tran = '';

	try {
		var action = Request("action");
		var table = Request("table");
		var sProc = String(Request("sProc"));
		var columns = String(Request("columns"));
		var extra_result_columns = String(Request("extra_result_columns"));
		var id_cols = String(Request("id_cols"));
		var insertProc = String(Request("insertProc"));
		var updateProc = String(Request("updateProc"));
		var deleteProc = String(Request("deleteProc"));
		var where = (String(Request("environment")) != "undefined") ? " WHERE environment = '" + String(Request("environment")) + "'" : "";
		if(String(Request("where_host_id")) != "undefined") {
			where = " WHERE host_id = '" + String(Request("where_host_id")) + "'";
		}
		var order = (String(Request("order")) != "undefined") ? " ORDER BY " + String(Request("order")) : "";
		var formatters = String(Request("sqlReadFormatters"));
        // creates a map for formatters from parameter string
        if(formatters != "undefined") {
            var _formatters = {};
            for(var index in formatters.split(";")) {
                _formatters[formatters.split(";")[index].split(":")[0]] = formatters.split(";")[index].split(":")[1];
            }
            formatters = _formatters;
        } else {
            formatters = {};
		}

		var rowsA = [];
		if(CheckDbTableRights(table, action)) {
			if(action == "read") {
				if (sProc != "undefined") {
					var sql= "EXEC " + sProc;
				}
				else {
					var columnList = columns.split(",");
					for(var index in columnList) {
						// replace columns with its formatter
                        if(formatters[columnList[index]])
                            columns = columns.replace(columnList[index], formatters[columnList[index]]+" as "+columnList[index]);
					}
					var sql= "SELECT " + columns.replace('order','[order]').replace('column', '[column]') + " FROM " + table + where + order.replace('order','[order]');
				}

				rowsA = runSQL(sql, cAcc, "fulltable");

			} else {
				if(table == "Configuration.AlertActions" || table == "Configuration.AlertClassActions" || table == "Configuration.HostObserver" || table == "Configuration.ObserverAlertFilter" || table == "Configuration.ObserverClass") {
					var sql2 = "INSERT INTO Communication (inputter_id, command_id, host, timestamp, run_by_server, parameter) VALUES(1, 34, 'SERVER', GETDATE(), 1, '')";
					runSQL(sql2, cAcc, "cmd");
				}
				var firstComaPos = columns.indexOf(',');
				var idColumn = columns.substr(0, firstComaPos);
				var columnsWithoutID = columns.substr(firstComaPos+1);

				if(id_cols=="2") {
					 idColumn += ","+columnsWithoutID.substr(0, columnsWithoutID.indexOf(','));
					 columnsWithoutID = columnsWithoutID.substr(columnsWithoutID.indexOf(',')+1);
				}


				var record = eval('('+getData()+')');
				if (record.data.length == undefined) {
					rowsA = doOperation(action, record.data, cAcc, idColumn, columnsWithoutID);
				}
				else {
					tran = beginTransaction(cAcc);
					rowsA = new Array();
					for (var i = 0; i < record.data.length; i++) {
						var result = doOperation(action, record.data[i], tran, idColumn, columnsWithoutID);
						if (result.length > 0) {
							rowsA[rowsA.length] = result[0];
						} 
					}
					commitTransaction(tran);
				}
			}
			var myJSON = new JSON();
			var data = '';
			var fields = '';
			
			if (rowsA.length) {
				data = myJSON.toJSON(null, rowsA, false);
				fields = myJSON.generateFields(rowsA);
			}
			
			else {
				var columnsarray = columns.split(',');
				fields = '[';
				for (var i = 0; i < columnsarray.length; i++) {
					fields += (i ? ',' : '') + '{"name": "' + columnsarray[i] + '"}';
				}
				fields += ']';
				data = '[]';
			}
			var responseArray = Response.Status.split(' ');
			var statusCode = responseArray[0];
			
			if(statusCode != '500'){
				Response.Write('{"metaData": {"totalProperty" : "total", "root" : "results", "id" : "id"}, "total":"' + rowsA.length + '", "results":' + data + '}');
			}
			
		}
	} catch (err) {
		if (tran) {
			rollbackTransaction(tran);
		}
//		Response.Write('Error: ' + err.message)
		logData('db_table_store.asp: ' + err.message);
	}

	function doOperation(action, data, conn, idColumn, columnsWithoutID) {

		try {
			var columnsarray = columns.split(',');
			var sql = '';

			if (action == 'create') {

				var values = '';

				for(var k = 0; k<idColumn.split(',').length; k++) {
					var colName = idColumn.split(',')[k];
				}

				for (var i = 0; i < columnsWithoutID.split(',').length; i++) {
					var colName = columnsWithoutID.split(',')[i];
					values += (i  ? ', ' : '') + (data[colName] == null ? "null" : "'" + data[colName] + "'");
				}

				sql = "SET NOCOUNT ON; INSERT INTO " + table + " (" + columnsWithoutID.replace('order','[order]').replace('column', '[column]') + ") " +  " VALUES(" + values + "); ";
				sql += "SELECT SCOPE_IDENTITY() AS ["+idColumn.split(',')[0]+"]";

				var result = runSQL(sql, conn ,"fulltable");
				for(var k = 1; k<idColumn.split(',').length; k++) {
					var colName = idColumn.split(',')[k];
					result[0][colName] = data[colName];

				}

				for (var i = 0; i < columnsWithoutID.split(',').length; i++) {
					var colName = columnsWithoutID.split(',')[i];
					result[0][colName] = data[colName];
				}

				if (insertProc != "undefined" && result.length > 0) {
					RunStoredProcWithId(result[0][idColumn.split(',')[0]], conn, insertProc, result);
				}

			} else if (action == 'update') {

				var outputs = ''
				var sets = '';
				var updateWhere = '';
				for(var k = 0; k<idColumn.split(',').length; k++) {
					updateWhere += ( k ? "AND " : "" ) + idColumn.split(',')[k] + " = '" + data[idColumn.split(',')[k]] + "'";
					outputs += (k ? ', ' : '') + '[' + idColumn.split(',')[k]+']';
				}


				for (var i = idColumn.split(',').length; i < columnsarray.length; i++) {
					var colName = columnsarray[i];

                    if(formatters[colName]) {
						// replace columns with its formatter
                        outputs += ', ' + formatters[colName] + ' as [' + colName+']';
                    } else {
                        outputs += ', ' + '[' + colName+']';
					}

					if(data[colName] != null){
						sets += '['+colName+'] = ' + "'" + data[colName] + "',";
					}
				}

				if(sets.length > 0){
					sets = sets.slice(0, -1);
				}

				if(extra_result_columns != 'undefined' && extra_result_columns != null && extra_result_columns != '')
					for(var k = 0; k<extra_result_columns.split(',').length; k++) {
						outputs += ', [' + extra_result_columns.split(',')[k]+']';
					}

				sql = "SET NOCOUNT ON; UPDATE " + table + " SET " + sets + " WHERE " + updateWhere;

				sql += ";SELECT "+ outputs+" FROM "+table+"  "+ (where ? where + ' AND ': 'WHERE ') +" ["+idColumn.split(',')[0]+"] = '"+data[idColumn.split(',')[0]]+"'";
					Response.Write(sql)
				result = runSQL(sql, conn, "fulltable");

				if (updateProc != "undefined") {
					RunStoredProcWithId(data[idColumn], conn, updateProc);
				}

			} else if (action == 'destroy') {
				var destroyWhere = "";
				for(var k = 0; k<idColumn.split(',').length; k++) {
					destroyWhere += ( k ? "AND [" : "[" ) + idColumn.split(',')[k] + "] = '" + data[idColumn.split(',')[k]] + "'";
				}

					var result = new Array();
					var cols = new Object();
					for (var i = 0; i < columnsarray.length; i++) {
						var colName = columnsarray[i];
						cols[colName] = data[colName];
					}
					if (deleteProc != "undefined") {
						RunStoredProcWithId(data[idColumn], conn, deleteProc);
					}
	
					sql = "DELETE FROM " + table + " WHERE " + destroyWhere;
					runSQL(sql, conn, "cmd");

					result[0] = cols;
			}

			else if (action == 'destroyunic') {
				var result = new Array();
				var cols = new Object();
				for (var i = 0; i < columnsarray.length; i++) {
					var colName = columnsarray[i];
					cols[colName] = data[colName];
				}
				if (deleteProc != "undefined") {
					RunStoredProcWithId(data[idColumn], conn, deleteProc)
				}

				sql = "DELETE FROM " + table + " WHERE " + idColumn + " = '" + data[idColumn] + "'";
				runSQL(sql, conn, "cmd");
				
				result[0] = cols;
			}

			return result;
		}
		catch (err) {
			//Response.Write('Error: ' + err.message);
			logData('db_table_store.asp: ' + err.message);
		}
	}

	function getData() {
		var sOut = '';

		if(Request.TotalBytes > 0) {
			var lngBytesCount = Request.TotalBytes;

			var stream = Server.CreateObject("Adodb.Stream");
			stream.type = 1;
			stream.open;
			stream.write(Request.BinaryRead(lngBytesCount));
			stream.position = 0;
			stream.type = 2;
			stream.charset = "iso-8859-1";
			sOut = stream.readtext();
			stream.close;
		}

		return sOut;
	}
%>
