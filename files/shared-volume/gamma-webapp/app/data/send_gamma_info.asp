<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="include/JSON.asp"-->
	<!--#include file="include/get_db_data.asp"-->
	<!--#include file="security/security.asp"-->
	<!--#include file="include/util.asp"-->

	<%
try {
	var inputter = String(Request("inputter"));
	var environment = String(Request("environment"));
	var parameters = String(Request("parameters"));
	var download = String(Request("download"));
	var modified_by = String(Request("modified_by"));							

	if(CheckRight(SEC_ROLE_LOGIN, null))
	{
		var result = SendGammaInfo(inputter, environment, parameters, download);
		var myJSON = new JSON();
		data = myJSON.toJSON(null, result, false); 
		fields = myJSON.generateFields(result);
		count = result.length;

		Response.Write( '{ "metaData": {	"totalProperty" : "total", "root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + count + '", "results":' + data + '}');

		var log_type = 69 + (download == "1" ? 1 : 0);
		var sql2 = "INSERT INTO Audit.LogDetails ([user], log_type, timestamp, env, param1, param2) VALUES('" + modified_by + "', " + log_type + " ,GETDATE(),'" + environment + "', 0, NULL)";
		runSQL(sql2,cAcc,"cmd");
	}
} 
catch (err) {
  Response.Write('Error: ' + err.message);
   logData('send_gamma_info.asp: ' + err.message);
}

%>