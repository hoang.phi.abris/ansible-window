<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../include/util.asp"-->
	<!--#include file="../security/security.asp"-->
	<!--#include file="../include/contoller_shell_paramlist.asp"-->

	<%
//<script language="javascript">

  var conn = ''
  var sql = ''
  try
  {
    var data = String(Request("data"));
    var operation = String(Request("operation"));
    var userID = String(Request("user_id"));
    var userAD = Session("username");
	var host_id = String(Request("host_id"));
	if (CheckRight(SEC_ROLE_LOGIN, null)) {
		conn = beginTransaction(cAcc);

			if (operation == "auth") {
				var sql = "select top 1 name,environment,host from Configuration.Instances where host = (select name from Configuration.Hosts where id = " + host_id + ")"
				rows = runSQL(sql, cAcc, "fulltable");

				if (rows.length > 0) {
					var sql = "insert into communication (inputter_id, command_id, host, environment, timestamp, instance) VALUES (" + userID + ", 33, '" + rows[0].host + "', '" + rows[0].environment + "', GETDATE(), '" + rows[0].name + "')"
					runSQL(sql, cAcc,"cmd");
				}
			} else if (operation == "import") {

				if (data != undefined && data != null && data != "") {
					var gcc_list = eval('(' + data + ')');
				}
				for (var i = 0; i < gcc_list.commands.length; i++) {
					var command = getCommand(gcc_list.commands[i]);
					if (command <= 0) {
						var command_id = addNewCommand(conn, gcc_list.commands[i], host_id, gcc_list);
					}
				}

				for(var i=0; i< gcc_list.shells.length; i++) {
					var shell = gcc_list.shells[i];
					if (shell != null) {
						var id = getShellIdFromDb(conn, host_id, shell.name);
						if (id < 0) {
							id = addNewShell(conn, host_id, shell);
						}
					}
				}

			} else if (operation == "delete") {
				var sql = "select * from ( select default_host host from Configuration.Environments where id IN (select environment_id from Configuration.ControllerFunction where id  IN (select function_id from Configuration.ControllerGroup where id IN (select group_id from Configuration.ControllerAction where run_command = "+data+" and host_id = 0))) union ALL select host_id host from Configuration.ControllerAction where run_command = "+data+" and host_id <> 0) t where host = " + host_id;
				rows = runSQL(sql, cAcc, "fulltable");
				Response.Write(rows.length);
			} else if(operation == "get_templates") {
				var rowsB = [];
				var sql = 'select distinct t.name, template_id, param_id from Configuration.CommandTemplate t, ' +
					'Configuration.CommandTemplateConn c  where c.template_id = t.id'
				rows = runSQL(sql, cAcc, "fulltable");
				sql = 'select * from Configuration.CommandTemplateVars'
				rowsB = runSQL(sql, cAcc, "fulltable");
				var myJSON = new JSON();
				Response.Write('{"templates":'+myJSON.toJSON(null, rows, false)+', "variables": ' +myJSON.toJSON(null, rowsB, false)+'}');
      		}
		}
		commitTransaction(conn);
    }
    catch (err) {
	logData(sql)
    if (conn)
      rollbackTransaction(conn)
    Response.Write("Error: " + err.message)
	 logData('save_functions.asp: ' + err.message);
  }


 function getCommand(command) {
	var sql = "SELECT * FROM Configuration.ControllerCommand where host_id = '"+host_id+"' and name = '"+command.name+"'";
	rows = runSQL(sql, cAcc,"fulltable");
	return rows.length;
}

 function addNewCommand(conn, command, host_id, funct) {

	if (command.param_list_id != "") {
		handleParamList(command, host_id, funct.paramLists, conn);
	}

	if (command.cmd_class.indexOf('ShellCommand') > 0) {
		handleShell(command, "cmd_class_param", host_id, funct.shells, conn);
	}
	if (command.check_cmd_class != undefined && command.check_cmd_class.indexOf('ShellCheckCommand') > 0) {
		handleShell(command, "check_cmd_class_param", host_id, funct.shells, conn);
	}
	if (command.status_cmd_class != undefined && command.status_cmd_class.indexOf('ShellStatusCommand') > 0) {
		handleShell(command, "status_cmd_class_param", host_id, funct.shells, conn);
	}
	var	sql = "INSERT INTO Configuration.ControllerCommand (id, template_id, host_id, version,type, name, description, check_timeout, run_in_paused_state, cmd_class, cmd_class_param, command_line, param_list_id, stop_command_line, ";
	sql += "command_param, check_cmd_class, check_cmd_class_param, check_cmd_line, check_cmd_line_param, ";
	sql += "status_cmd_class, status_cmd_class_param, status_cmd_line, status_cmd_line_param, record_status, input_user_id, wm_status, ";
	sql += "email_to, email_cc, email_subject, email_body, email_attachment, email_auto, command_line_start) OUTPUT INSERTED.id ";
	sql += "VALUES((SELECT max(id)+1 FROM Configuration.ControllerCommand), "+command.template_id+", " + host_id + ", 1, '" + command.type + "', ";
	sql += "'" + command.name + "', ";
	sql += "'" + command.description + "', ";

	sql += (command.check_timeout||10)  + ", ";
	sql += (command.run_in_paused_state == "true" ? 1 : 0) + ", ";
	sql += "'" + command.cmd_class + "', ";
	sql += "'" + command.cmd_class_param + "', ";
	sql += "'" + command.command_line + "', ";
	sql += "'" + command.param_list_id + "', ";
	sql += "'" + command.stop_command_line + "', ";
	sql += "'" + command.command_param + "', ";
	sql += "'" + (command.check_cmd_class||"") + "', ";
	sql += "'" + (command.check_cmd_class_param||"") + "', ";
	sql += "'" + (command.check_cmd_line||"") + "', ";
	sql += "'" + (command.check_cmd_line_param||"") + "', ";
	sql += "'" + (command.status_cmd_class||"") + "', ";
	sql += "'" + (command.status_cmd_class_param||"") + "', ";
	sql += "'" + (command.status_cmd_line||"") + "', ";
	sql += "'" + (command.status_cmd_line_param||"") + "', ";
	sql += "2, ";
	sql += userID+",";
	sql += "'" + (command.wm_status || "") + "', ";
	sql += "'" + (command.email_to || "") + "', ";
	sql += "'" + (command.email_cc || "") + "', ";
	sql += "'" + (command.email_subject || "") + "', ";
	sql += "'" + (command.email_body || "") + "', ";
	sql += "'" + (command.email_attachment||"") + "', "+(command.email_auto == "true" ? 1 : 0)+"," + (command.command_line_start == "true" ? 1 : 0) + ")";
  	sql = sql.replace(/''/g,'null');



	rows = runSQL(sql, cAcc,"fulltable");
	return rows.length > 0 ? rows[0].id : -1
 }

%>