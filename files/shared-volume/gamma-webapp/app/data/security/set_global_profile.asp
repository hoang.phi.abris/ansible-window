<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="security.asp"-->

	<%
	var profile_id = String(Request("profile_id"));							
	var is_global = String(Request("is_global"));							

	try {
		if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_PROTAL_GLOBAL_PROFILE, null)) {
			var sql= "UPDATE Configuration.PortalProfile SET is_global = " + is_global + " WHERE profile_id = " + profile_id;
			runSQL(sql,cAcc,"cmd");
		}
	}
	catch (err) {
	  Response.Write('Error: ' + err.message);
	  logData('security/set_global_profile.asp: ' + err.message);
	}
%>