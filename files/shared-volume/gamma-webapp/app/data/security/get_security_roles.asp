<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

<!--#include file="../include/JSON.asp"-->
<!--#include file="../include/get_db_data.asp"-->

<script runat="server" language="JavaScript">
    try {
        var userID = Session('username');

        var sql = "select id, sec_id as secId, envname as environmentName, rolename, [type] from Security.UsersRoles('" + userID + "')";
        var rowsA = runSQL(sql, cAcc, 'fulltable');

        var loginType = 8; //login failed
        for (var i = 0; i < rowsA.length; i++) {
            if (rowsA[i].secId == 1) loginType = 7; //login enabled
            Session(rowsA[i].secId + '-' + rowsA[i].environmentName) = 'ok';
        }
        if (loginType != 7) Session('username') = '';

        var myJSON = new JSON();
        var data = myJSON.toJSON(null, rowsA, false);
        var fields = myJSON.generateFields(rowsA);

        Response.Write(
            '{ "metaData": {	"totalProperty" : "total", "root" : "results", "id" : "id", 	"fields" : ' +
                fields +
                '}, "total":"' +
                rowsA.length +
                '", "results":' +
                data +
                '}'
        );

        var sql2 = "INSERT INTO Audit.LogDetails ([user], log_type, timestamp) VALUES('" + userID + "'," + loginType + ',GETDATE() )';
        runSQL(sql2, cAcc, 'cmd');
    } catch (err) {
        logData('security/get_security_roles.asp: ' + err.message);
        Response.Write("{success:failed, error:'" + err.message + "'}");
    }
</script>
