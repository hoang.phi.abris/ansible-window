<%

var SEC_ROLE_LOGIN = 1
var SEC_ROLE_ENVMAN_CONFIG = 2
var SEC_ROLE_ENVMAN_VIEW = 3
var SEC_ROLE_SECMAN_VIEW = 4
var SEC_ROLE_SECMAN_USERS = 5
var SEC_ROLE_SECMAN_GROUPS = 6
var SEC_ROLE_SECMAN_ASSIGN = 7
var SEC_ROLE_ALERTMAN_CONFIG = 8
var SEC_ROLE_ALERTMAN_VIEW = 9
var SEC_ROLE_DIAGRAM_CONFIG = 10
var SEC_ROLE_PROTAL_GLOBAL_PROFILE = 11
var SEC_ROLE_GAMMA_MANAGER_VIEW = 12
var SEC_ROLE_EVENTS_VIEW = 13
var SEC_ROLE_EVENTS_CONFIG = 14
var SEC_ROLE_PORTAL_CONFIG = 15
var SEC_ROLE_CHECK_T24_STATE = 16
var SEC_ROLE_SEND_T24_STATE = 17
var SEC_ROLE_DOWNLOAD_T24_STATE = 18
var SEC_ROLE_SLA_VIEW = 19
var SEC_ROLE_SLA_CONFIG = 20
var SEC_ROLE_GAMMA_MANAGER_CONTROL = 21
var SEC_ROLE_AUDIT_VIEW = 22
var SEC_ROLE_MAINTENANCE_VIEW = 23
var SEC_ROLE_ARCHIVE_CONFIG = 24
var SEC_ROLE_ARCHIVE_SCHEDULER_CONFIG = 25
var SEC_ROLE_ADMIN_WINDOW = 26
var SEC_ROLE_ICONS_VIEW = 27
var SEC_ROLE_ICONS_CONFIG = 28

var SEC_ROLE_FILECONFIG = 1001
var SEC_ROLE_FILEVIEW = 1002
var SEC_ROLE_GAMMACONFIG = 1003
var SEC_ROLE_GAMMAVIEW = 1004
var SEC_ROLE_SERVICECONFIG = 1005
var SEC_ROLE_OCMCONFIG = 1007
var SEC_ROLE_OCMVIEW = 1008

var SEC_ROLE_ALERT_TAKEOWNER = 1013
var SEC_ROLE_ALERT_SOLVE = 1014
var SEC_ROLE_CONTROLLER_CONFIG = "1015"
var SEC_ROLE_CONTROLLER_VIEW = "1016"
var SEC_ROLE_CONTROLLER_OPERATION = 1017
var SEC_ROLE_CONTROLLER_OPERATION_VIEW = 1018
var SEC_ROLE_CONTROLLER_SKIP_ACTION = 1019

var SEC_ROLE_PORTAL_REALTIME = 1021
var SEC_ROLE_PORTAL_STATISTICS = 1022
var SEC_ROLE_PORTAL_SLAREPORTS = 1023
var SEC_ROLE_PORTAL_INCIDENTREPS = 1024
var SEC_ROLE_PORTAL_MANAGEMENT = 1025
var SEC_ROLE_OCM_SAVECOBCOMMENT = 1026
var SEC_ROLE_COBAN_VIEW = 1027
var SEC_ROLE_COBAN_COBDETAILS = 1028
var SEC_ROLE_COBAN_JOBSTATISTICS = 1029
var SEC_ROLE_COBAN_TOP20RUNTIME = 1030
var SEC_ROLE_COBAN_JOBLIST = 1031
var SEC_ROLE_COBAN_COMPARE = 1032
var SEC_ROLE_COBAN_CHARTS = 1033
var SEC_ROLE_COBAN_ALERTS = 1034
var SEC_ROLE_PH_AGENT_STATUS_CHANGE = 1035
var SEC_ROLE_PH_STOPALL = 1038
var SEC_ROLE_PORTAL_COB_MONITOR = 1039
var SEC_ROLE_SET_AGENT_THREAD_CNT = 1040
var SEC_ROLE_COBAN_CONFIG = 1041
var SEC_ROLE_PROTOCOL_VIEW = 1042
var SEC_ROLE_ORD_VIEW = 1043
var SEC_ROLE_LOGOFF = 1044
var SEC_ROLE_ARCHIVE_VIEW = 1045
var SEC_ROLE_VIEW_SENSITIVE_DATA = 1046
var SEC_ROLE_CONTROLLER_SKIP_GROUP = 1047
var SEC_ROLE_CONTROLLER_PAUSE_GROUP = 1048
var SEC_ROLE_SCHEDULER_CONFIG = 1049
var SEC_ROLE_ONLINE_REPORT_CONFIG = 1050
var SEC_ROLE_T24_JOB_EDIT = 1051

var sendCommandRoles = {
	29: SEC_ROLE_PH_AGENT_STATUS_CHANGE,
	25: SEC_ROLE_SET_AGENT_THREAD_CNT,
	30: SEC_ROLE_LOGOFF,
	24: SEC_ROLE_CONTROLLER_OPERATION,
	35: SEC_ROLE_T24_JOB_EDIT
}

function CheckRight(right, environment) 
{
	if (Session("username") == undefined || Session("username") == "")
	{
		var path = String(Request.ServerVariables ("PATH_INFO")).split("/app/data/")[0];
		Server.Execute(path+"/app/data/security/get_logged_in_user.asp")
		Response.Clear();
		if (Session("username") != "") {
			Server.Execute(path+"/app/data/security/get_security_roles.asp")
			Response.Clear();
		}
	}
	
	var id = right + "-" + environment;
	var res = (Session(id) == "ok");
	if(!res)
		Response.Write("Security violation: " + id);
	return res;
}

function HasRight(right, environment) 
{
	if (Session("username") == undefined || Session("username") == "")
	{
		var path = String(Request.ServerVariables ("PATH_INFO")).split("/app/data/")[0];
		Server.Execute(path+"/app/data/security/get_logged_in_user.asp")
		Response.Clear();
		if (Session("username") != "") {
			Server.Execute(path+"/app/data/security/get_security_roles.asp")
			Response.Clear();
		}
	}
	
	var id = right + "-" + environment;
	var res = (Session(id) == "ok");
	if(!res)
		return false;
	else
	return true;
}

function FindRight(right) {
	var s = ''; 
	for(s = new Enumerator(Session.Contents); !s.atEnd(); s.moveNext()) 
	{ 
		var sn = s.item(); 
		if (sn.split("-")[0] == right)
			return true;
	}
	Response.Write("Security violation: " + right);
	return false;
}

function FindRights(rights) {
	var s = ''; 
	for(s = new Enumerator(Session.Contents); !s.atEnd(); s.moveNext()) 
	{ 
		var sn = s.item(); 
		var  i = 0;
		for(i = 0;i<rights.length;i++)
			if (sn.split("-")[0] == rights[i])
				return true;
	}
	Response.Write("Security violation: " + rights);
	return false;
}

function CheckDbTableRights(tableName, action) {
	var result = CheckRight(SEC_ROLE_LOGIN, null);
	if (!result)
		return false;
	
	var table = String(tableName).toLowerCase();
	table = table.replace(/\[/g, '');
	table = table.replace(/\]/g, '');
	switch (table) {
		case "configuration.instances":
		case "configuration.hosts":
		case "configuration.environments":
			if (action != "read")
				result = CheckRight(SEC_ROLE_ENVMAN_CONFIG, null);
			break;
		case "configuration.alertactions":
		case "configuration.alertclassactions":
		case "configuration.hostobserver":
		case "configuration.observeralertFilter":
		case "configuration.observerclass":
			if (action == "read")
				result = CheckRight(SEC_ROLE_ALERTMAN_VIEW, null);
			else
				result = CheckRight(SEC_ROLE_ALERTMAN_CONFIG, null);
			break;
		case "configuration.controllerfunction":
			//needed for menu creation for every user
			break;
		case "configuration.controlleraction":
		case "configuration.controllergroup":
			if (action == "read")
				result = FindRight(SEC_ROLE_CONTROLLER_VIEW)
			else
				result = FindRight(SEC_ROLE_CONTROLLER_CONFIG)
			break;
		case "security.user":
			if (action == "read")
				result = CheckRight(SEC_ROLE_SECMAN_VIEW, null);
			else
				result = CheckRight(SEC_ROLE_SECMAN_USERS, null);
			break;
		case "security.rolegroup":
		case "security.usergroup":
			if (action == "read")
				result = CheckRight(SEC_ROLE_SECMAN_VIEW, null);
			else
				result = CheckRight(SEC_ROLE_SECMAN_GROUPS, null);
			break;
		case "security.rolegrptolegrps":
		case "security.rolerolegrps":
		case "security.roleusergrps":
		case "security.usergrprolegrps":
		case "security.usergrpusergrps":
		case "security.userrolegrps":
		case "security.userroles":
		case "security.userusergrps":
			if (action == "read")
				result = CheckRight(SEC_ROLE_SECMAN_VIEW, null);
			else
				result = CheckRight(SEC_ROLE_SECMAN_ASSIGN, null);
			break;
		case "security.roleview":
			result = CheckRight(SEC_ROLE_SECMAN_VIEW, null);
			break;
		case "configuration.diagramparameter":
			result = FindRights([SEC_ROLE_PORTAL_REALTIME, SEC_ROLE_PORTAL_STATISTICS]);
			break;
		case "configuration.diagramparametertype":
			result = CheckRight(SEC_ROLE_DIAGRAM_CONFIG, null);
			break;
		case "configuration.environmentsview":
			//read-only, login right is enough
			break;
		case "configuration.portalprofile":
			result = CheckRight(SEC_ROLE_SECMAN_VIEW, null) || FindRights([SEC_ROLE_PORTAL_REALTIME, SEC_ROLE_PORTAL_STATISTICS, SEC_ROLE_PORTAL_SLAREPORTS, SEC_ROLE_PORTAL_INCIDENTREPS, SEC_ROLE_PORTAL_MANAGEMENT, SEC_ROLE_PORTAL_COB_MONITOR]);
			break;
		case "configuration.portalprofileitem":
			result = FindRights([SEC_ROLE_PORTAL_REALTIME, SEC_ROLE_PORTAL_STATISTICS, SEC_ROLE_PORTAL_SLAREPORTS, SEC_ROLE_PORTAL_INCIDENTREPS, SEC_ROLE_PORTAL_MANAGEMENT, SEC_ROLE_PORTAL_COB_MONITOR]);
			break;
	}
	return result;
}

function getParamValue(param) {
	var queryStrings = String(Request.ServerVariables ("QUERY_STRING")).split("&");
	for(var i=0;i<queryStrings.length;i++) {
		var nameAndValue = queryStrings[i].split("=");
		if (nameAndValue[0] == param)
			return nameAndValue[1];
	}
}
%>