<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="security.asp"-->

	<script runat=server language=JavaScript>

		try {
			if (CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_AUDIT_VIEW, null)) {
				var environment = String(Request("environment"));
				var user = String(Request("user"));
				var from = String(Request("from"));
				var to = String(Request("to"));
				var lpt = String(Request("lpt"));

				var sql = "SELECT id, isnull(env,'') as env, uname, CONVERT(VARCHAR,timestamp, 120) as timestamp, action, details FROM Audit.LogDetailsView WHERE"
				sql += " timestamp between '" + from + "' AND '" + to + "'";
				if (environment != "All") {
					if (environment == "Global")
						sql += " AND env is null ";
					else
						sql += " AND env = '" + environment + "'";

				}
				if (user != "All") {
					sql += " AND uname = '" + user + "'";
				}
				if (lpt != "0") {
					sql += " AND logtype = " + lpt;
				}
				var rowsA = runSQL(sql, cAcc, "fulltable");

				var myJSON = new JSON();
				var data = myJSON.toJSON(null, rowsA, false);
				var fields = myJSON.generateFields(rowsA);

				Response.Write('{ "metaData": {	"totalProperty" : "total", "root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '", "results":' + data + '}');
			}
		}
		catch (err) {
			logData('security/get_audit_logdetview.asp: ' + err.message);
			Response.Write("{success:failed, error:'" + err.message + "'}");
		}

	</script>