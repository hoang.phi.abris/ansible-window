<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="../include/jwt.all.asp" -->
<%

username = Session("username")

Dim fs: Set fs = Server.CreateObject("Scripting.FileSystemObject") 
Set stream = fs.OpenTextFile(Server.MapPath("private.pem"), 1, false)
privateKey = stream.ReadAll
stream.close

Response.Write getJwt(username, timeInSeconds() + (3600*24*7), privateKey) ' one week expiration

%>

<script language="javascript" runat="server">
function getJwt(username, timestamp, privateKey){
  var token = new jwt.WebToken('{"sub": "' + username + '", "exp": ' + timestamp + '}', '{"typ":"JWT", "alg":"RS256"}');
  var signed = token.serialize(privateKey);
  return signed;
}
function timeInSeconds() {
  return Math.floor(new Date().getTime() / 1000);
}
</script>
