<%
dim domainController : domainController = "yourdc.company.com"
dim ldapPort : ldapPort = 389
dim startOu : startOu = "DC=company,DC=com"

Function Base64Decode(vCode)
    Dim oXML, oNode

    Set oXML = CreateObject("Msxml2.DOMDocument.3.0")
    Set oNode = oXML.CreateElement("base64")
    oNode.dataType = "bin.base64"
    oNode.text = vCode
    Base64Decode = Stream_BinaryToString(oNode.nodeTypedValue)
    Set oNode = Nothing
    Set oXML = Nothing
End Function

Function Stream_StringToBinary(Text)
  Const adTypeText = 2
  Const adTypeBinary = 1

  'Create Stream object
  Dim BinaryStream 'As New Stream
  Set BinaryStream = CreateObject("ADODB.Stream")

  'Specify stream type - we want To save text/string data.
  BinaryStream.Type = adTypeText

  'Specify charset For the source text (unicode) data.
  BinaryStream.CharSet = "us-ascii"

  'Open the stream And write text/string data To the object
  BinaryStream.Open
  BinaryStream.WriteText Text

  'Change stream type To binary
  BinaryStream.Position = 0
  BinaryStream.Type = adTypeBinary

  'Ignore first two bytes - sign of
  BinaryStream.Position = 0

  'Open the stream And get binary data from the object
  Stream_StringToBinary = BinaryStream.Read

  Set BinaryStream = Nothing
End Function

'Stream_BinaryToString Function
'2003 Antonin Foller, http://www.motobit.com
'Binary - VT_UI1 | VT_ARRAY data To convert To a string 
Function Stream_BinaryToString(Binary)
  Const adTypeText = 2
  Const adTypeBinary = 1

  'Create Stream object
  Dim BinaryStream 'As New Stream
  Set BinaryStream = CreateObject("ADODB.Stream")

  'Specify stream type - we want To save binary data.
  BinaryStream.Type = adTypeBinary

  'Open the stream And write binary data To the object
  BinaryStream.Open
  BinaryStream.Write Binary

  'Change stream type To text/string
  BinaryStream.Position = 0
  BinaryStream.Type = adTypeText

  'Specify charset For the output text (unicode) data.
  BinaryStream.CharSet = "us-ascii"

  'Open the stream And get text/string data from the object
  Stream_BinaryToString = BinaryStream.ReadText
  Set BinaryStream = Nothing
End Function

Function CheckLogin( szUserName, szPassword)
  CheckLogin = False

  szUserName = trim( "" &  szUserName)

  dim oCon : Set oCon = Server.CreateObject("ADODB.Connection")
  oCon.Provider = "ADsDSOObject"

  dim rootDSE : set rootDSE = GetObject("LDAP://RootDSE")
  
  oCon.Properties("User ID") = szUserName
  oCon.Properties("Password") = szPassword
  oCon.Open "ADProvider"
  dim oCmd : Set oCmd = Server.CreateObject("ADODB.Command")
  Set oCmd.ActiveConnection = oCon
    
  ' let's look for the mail address of a non exitsting user

  dim cmd : cmd = "SELECT cn FROM 'LDAP://" & rootDSE.Get("defaultNamingContext")  & "' WHERE objectClass='user'"
  oCmd.CommandText = cmd
  oCmd.Properties("Page Size") = 100
  on error resume next
  dim rs : Set rs = oCmd.Execute
  if err.Number = 0 then
    CheckLogin = true
    call rs.Close()
    set rs = nothing
  end if
  on error goto 0
  set oCmd = nothing
End Function

' perform test
dim res : res = CheckLogin( request("strUserName"), Base64Decode(request("strPassword")))

if res then
  Response.Write( "{ ""success"": true }")
  Session("wholeUsername") = request("strUserName")
else
  Response.Write( "{ ""success"": false }")
end if

%>