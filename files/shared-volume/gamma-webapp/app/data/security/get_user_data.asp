<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="security.asp"-->

	<%
try {
	if(CheckRight(SEC_ROLE_LOGIN, null))
	{
		var userID = Session("username");
		var sql = "select * from Configuration.PortalProfileItem ppi where ppi.profile_id = (select profile_id from Security.[User] u where u.ad_id = '" + userID + "')"
		var rowsA = runSQL(sql,cAcc,"fulltable");

		var myJSON = new JSON();
		var data = myJSON.toJSON(null, rowsA, false); 
		var fields = myJSON.generateFields(rowsA)

		
		sql = "select u.id, popup, sessiontimeout, e.name, controller_view from Security.[User] u LEFT JOIN Configuration.Environments e on u.environment_id = e.id where u.ad_id = '" + userID + "'"

		rowsA = runSQL(sql,cAcc,"fulltable");
		var environment_name = "", notification = "", sessionTimeout = 0;
		var sec_id = -1;
		if (rowsA.length > 0) {
			environment_name = rowsA[0].name;
			notification = rowsA[0].popup;
			sec_id = rowsA[0].id;
			sessionTimeout = rowsA[0].sessiontimeout;
			controller_view = rowsA[0].controller_view;
		}

		Response.Write( '{ "metaData": {	"totalProperty" : "total", "root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '", "results":' + data + ', "environment_name" : "' + environment_name + '", "notification" : "' + notification + '", "sec_id" : ' + sec_id + ', "sessionTimeout" : ' + sessionTimeout + ', "controller_view" : ' + controller_view + '}'); 
	}
}
catch(err) {
    logData('security/get_user_data.asp: ' + err.message);
	Response.Write('Error: ' + err.message);
}
%>