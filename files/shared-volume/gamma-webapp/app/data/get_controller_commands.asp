<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

<!--#include file="include/JSON.asp"-->
<!--#include file="include/get_db_data.asp"-->
<!--#include file="security/security.asp"-->
<!--#include file="include/util.asp"-->

<%
try {
	var inputter = String(Request("inputter"));
	var environment = String(Request("environment"));

	if(CheckRight(SEC_ROLE_LOGIN, null) && (CheckRight(SEC_ROLE_CONTROLLER_VIEW, environment) || CheckRight(SEC_ROLE_CONTROLLER_CONFIG, environment)))
	{
		var commands = GetControllerCommands(inputter, environment);
		var myJSON = new JSON();
		data = myJSON.toJSON(null, commands, false); 
		fields = myJSON.generateFields(commands);
		count = commands.length;

		Response.Write( '{ "metaData": {	"totalProperty" : "total", "root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + count + '", "results":' + data + '}');
	}
} 
catch (err) {
logData(err.message)
  Response.Write('Error: ' + err.message);
}

%>
