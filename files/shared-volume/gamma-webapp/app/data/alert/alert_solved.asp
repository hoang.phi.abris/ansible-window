<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

<!--#include file="../include/JSON.asp"-->
<!--#include file="../include/get_db_data.asp"-->
<!--#include file="../security/security.asp"-->

<%
var measurement_id = String(Request("measurement_id"));							
var actions_taken = String(Request("actions_taken"));							
var solution = String(Request("solution"));							
var resolved_by = String(Request("resolved_by"));						

try 
{
	if(CheckRight(SEC_ROLE_LOGIN, null) && FindRight(SEC_ROLE_ALERT_SOLVE)) {

		var conn = beginTransaction(cAcc) 	
		var sql= "UPDATE Alerts SET actions_taken = '" + actions_taken + "', solution = " + solution + ", owner = ISNULL(owner, '" + resolved_by + "'), owner_timestamp = ISNULL(owner_timestamp, GETDATE()), resolved_by = '" + resolved_by + "', solved = GETDATE() , status = 7 WHERE measurement_id = '" + measurement_id + "'"
		runSQL(sql,conn,"cmd");
		
		var sql = "INSERT INTO Communication (inputter_id, command_id, host, timestamp, run_by_server, parameter) VALUES(1, 31, 'SERVER', GETDATE(), 1, '" + measurement_id + "')";
		runSQL(sql,conn,"cmd");
		commitTransaction(conn);
		
		Response.Write( '{"success":true}');
	}
}
catch (err) 
{
	if (conn)
		rollbackTransaction(conn) 
	Response.Write("{success:false,error:'" + err.message + "'}");
	logData('alert_solved.asp: ' + err.message);
}
%>
