
<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

<!--#include file="../include/JSON.asp"-->
<!--#include file="../include/get_db_data.asp"-->
<!--#include file="../security/security.asp"-->

<%
/* configre extra url request header properties */

try{
	if(CheckRight(SEC_ROLE_LOGIN, null)) {

		var pageSize = parseFloat(isset(Request("pageSize")) ? Request("pageSize") : "0")  
		var withoutOwner = ((String(Request("withoutOwner")))=="undefined" ? 0 : String(Request("withoutOwner")) );
		var withOwner = ((String(Request("withOwner")))=="undefined" ? 0 : String(Request("withOwner")) );
		var severityfilter = isset(Request("severity")) ? String(Request("severity")) : "";
		var start = parseFloat(isset(Request("start")) ? Request("start") : "0")
		var observer = isset(Request("observer")) ? String(Request("observer")) : ""
		var host = isset(Request("host")) ? String(Request("host")) : ""
		var tfilter = isset(Request("tfilter")) ? String(Request("tfilter")) : ""
		var untouched = String(Request("untouched"));
		var showOwnedBy = String(Request("showOwnedBy"));
		var solved = String(Request("solved"));
		var closed = String(Request("closed"));

		if (solved == "undefined" || solved == "" || solved == false || solved == "false"){
			solved = 0;
		}
		if (withOwner == "undefined" || withOwner == "" || withOwner == false || withOwner == "false"){
			withOwner = 0;
		}
		if (withoutOwner == "undefined" || withoutOwner == "" || withoutOwner == false || withoutOwner == "false"){
			withoutOwner = 0;
		}
		if (closed == "undefined" || closed == "" || closed == false || closed == "false"){
			closed = 0;
		}
		if (untouched == "undefined" || untouched == "" || untouched == false || untouched == "false"){
			untouched = 0;  
		}
		if (showOwnedBy  == "undefined" || showOwnedBy == "" || showOwnedBy == false || showOwnedBy == "false"){
			showOwnedBy = "";
		}
		if (severityfilter  == "undefined"  || severityfilter == ""){
			severityfilter = "''";
		}




		sql= "exec getAlerts '" + env + "',  @showOwnedBy = '" + showOwnedBy +"', @untouched = " + withoutOwner +", @solved = " + solved + ", @closed = " + closed + ", @severity = " + severityfilter + ", @withOwner = " + withOwner + ", @observer = '" + observer + "', @host = '" + host + "', @tfilter = '" + tfilter + "'";

		debugPrint(1,'sql: ' + sql)

		rowsA = runSQL(sql,cAcc,"fulltable");
		ind				= 0;
		pagedRows = [];
		pageSize 	= (pageSize==0) ? rowsA.length : start + pageSize;
		pageSize 	= (pageSize > rowsA.length) ? rowsA.length : pageSize;
		if (rowsA.length >0)
		{
			for(ii=start;ii<pageSize; ii++)
			{
				pagedRows[ind++] = rowsA[ii];
			}
		}
		else
		{ pagedRows = rowsA;}

		var myJSON = new JSON();
		data 		= myJSON.toJSON(null, pagedRows, false); 
		fileds 	= myJSON.generateFields(pagedRows)
//		Response.Write( '({ sql:"' + sql + '",  "metaData": {	"totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fileds + '}, "total":"' + rowsA.length + '","results":' + data + '})');
		Response.Write( '{ "metaData": {	"totalProperty" : "total", 	"root" : "results", "id" : "id"}, "total":"' + rowsA.length + '", "results":' + data + '}');
	}
}catch (err) {
  Response.Write('Error: ' + err.message);
  logData('alerts.asp: ' + err.message);
} 
	

	

%>
