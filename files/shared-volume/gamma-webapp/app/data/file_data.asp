<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="include/JSON.asp"-->
	<!--#include file="include/get_db_data.asp"-->
	<!--#include file="include/util.asp"-->
	<!--#include file="security/security.asp"-->
	<%
  var conn = ''
  try
  {
    var funct = String(Request("funct"))
    var xmlx, host, typeid
    host = String(Request("host"))
    typeid = String(Request("typeid"))

    switch(funct) {
      case "load":
		if(CheckRight(SEC_ROLE_LOGIN, null))
		{
			var res = ''
			var sql = "SELECT theFile From Configuration.ConfigFiles WHERE host = '" + host + "' AND STATUS = 0 AND filetype_id = " + typeid;
			var rowsA = runSQL(sql,cAcc,"fulltable");
			if (rowsA.length > 0) //false &&  ki kell venni!
				res = rowsA[0].theFile;
			else {
				var sql = "SELECT name From Configuration.ConfigFileTypes WHERE id = " + typeid;

				var rowsA = runSQL(sql,cAcc,"fulltable");
				if (rowsA.length > 0) {
					var fs,f,t,x
					fs=Server.CreateObject("Scripting.FileSystemObject")
					var currentdirectorypath = Server.MapPath(".")
					t=fs.OpenTextFile(currentdirectorypath + "\\defaults\\" + rowsA[0].name, 1, false)
					res = t.ReadAll()
					t.close()
				}
			}
        }
        Response.write(res)
        break;
      case "save":
		if(CheckRight(SEC_ROLE_LOGIN, null))
		{
			var modified_by = Session("username");

			xmlx = String(Request("xmlx"))

			var instToDelete = String(Request("instToDelete"))
			var toDelete = instToDelete.split(String.fromCharCode(252));
			var instToUpdate = String(Request("instToUpdate"))
			var toUpdate = instToUpdate.split(String.fromCharCode(252));
			var instToInsert = String(Request("instToInsert"))
			var toInsert = instToInsert.split(String.fromCharCode(252));
			var obsToDelete = String(Request("obsToDelete"))
			var toDelete2 = obsToDelete.split(String.fromCharCode(252));
			var obsToUpdate = String(Request("obsToUpdate"))
			var toUpdate2 = obsToUpdate.split(String.fromCharCode(252));
			var obsToInsert = String(Request("obsToInsert"))
			var toInsert2 = obsToInsert.split(String.fromCharCode(252));
			var defaultHosts = String(Request("defaultHosts"))
			var toDefault = defaultHosts.split(String.fromCharCode(252));
			var copyAlertSettings = String(Request("copyAlertSettings"))
			var copyAlerts = copyAlertSettings.split(String.fromCharCode(252));
			var hostobservers = String(Request("hostobservers"))
			var hostobs = hostobservers.split(String.fromCharCode(252));
			var alertactions = String(Request("alertactions"))
			var aas = alertactions.split(String.fromCharCode(252));
			var alertactionsupdate = String(Request("alertactionsupdate"))
			var aasu = alertactionsupdate.split(String.fromCharCode(252));
			var ms_instances = String(Request("ms_instances"))
			var s_instances = String(Request("s_instances"))


			conn = beginTransaction(cAcc)

			for (var i=0;i<toDelete2.length-1;i++) {
				var id = toDelete2[i];

				var sql = "INSERT INTO Audit.LogDetails ([user], log_type, timestamp, env, param1, param2) SELECT '" + modified_by + "', 16, GETDATE(), environment, 0, class_name + ' from host " + host + "' FROM Configuration.HostObserver ho "
				sql += "LEFT OUTER JOIN Configuration.Instances i on ho.instance_id = i.id AND ho.host = i.host WHERE ho.observer_id = " + id + " AND ho.host = '" + host + "'"
				runSQL(sql,conn,"cmd");
			}

			for (var i=0;i<toDelete.length-1;i++) {
				var data = toDelete[i].split('/');
				var sql = "INSERT INTO Audit.LogDetails ([user], log_type, timestamp, env, param1, param2) SELECT '" + modified_by + "', 13, GETDATE(), environment, 0, name + ' from host " + host + "' FROM Configuration.Instances where id = " + data[0] + " AND host = '" + host + "'"
				runSQL(sql,conn,"cmd");

				sql = "DELETE FROM Configuration.Instances WHERE id = " + data[0] + " AND host = '" + host + "'"
				runSQL(sql,conn,"cmd");
			}

			for (var i=0;i<toUpdate.length-1;i++) {
				var data = toUpdate[i].split('/');
				var sql = "UPDATE Configuration.Instances SET name = '" + data[0] + "', is_multi_server = " + (data[2]=="true"?1:0) + " WHERE id = " + data[1] + " AND host = '" + host + "'"
				runSQL(sql,conn,"cmd");

				sql = "INSERT INTO Audit.LogDetails ([user], log_type, timestamp, env, param1, param2) SELECT '" + modified_by + "', 12, GETDATE(), environment, 0, name + ' on host " + host + "' FROM Configuration.Instances where id = " + data[1] + " AND host = '" + host + "'"
				runSQL(sql,conn,"cmd");
			}

			for (var i=0;i<toInsert.length-1;i++) {
				var data = toInsert[i].split('/');
				var sql = "INSERT INTO Configuration.Instances (id, environment, host, name, is_multi_server) VALUES(" + data[0] + ", '" + data[1] + "', '" + host + "', '" + data[2] + "', " + (data[3]=="true"?1:0) + ")"
				runSQL(sql,conn,"cmd");

				sql = "INSERT INTO Audit.LogDetails ([user], log_type, timestamp, env, param1, param2) VALUES('" + modified_by + "', 11, GETDATE(), '" + data[1] + "', 0, '" + data[2] + " on host " + host + "')"
				runSQL(sql,conn,"cmd");
			}

			for (var i=0;i<toDelete2.length-1;i++) {
				var id = toDelete2[i];

				sql = "DELETE FROM Configuration.HostObserver WHERE observer_id = " + id + " AND host = '" + host + "'";
				runSQL(sql,conn,"cmd");
				sql = "DELETE FROM Configuration.HostObserver WHERE parent_id = " + id + " AND host = '" + host + "'";
				runSQL(sql,conn,"cmd");
				sql = "DELETE FROM Configuration.AlertActions WHERE observer_id = " + id + " AND host = '" + host + "'";
				runSQL(sql,conn,"cmd");
			}

			for (var i=0;i<toUpdate2.length-1;i++) {
				var data = toUpdate2[i].split('/');
				sql = "INSERT INTO Audit.LogDetails ([user], log_type, timestamp, env, param1, param2) SELECT '" + modified_by + "', 15, GETDATE(), environment, 0, class_name + ' on host " + host + "' FROM Configuration.HostObserver ho "
				sql += "LEFT OUTER JOIN Configuration.Instances i on ho.instance_id = i.id AND ho.host = i.host WHERE ho.observer_id = " + data[0] + " AND ho.host = '" + host + "'"
				runSQL(sql,conn,"cmd");
			}

			for (var i=0;i<toInsert2.length-1;i++) {
				var data = toInsert2[i].split('/');
				var parent_id = data[3]
				var sql = "INSERT INTO Configuration.HostObserver (observer_id, instance_id, host, class_name, obs_cba, obs_mail, obs_sms, obs_interface, obs_message_group, parent_id, host_observer_id) VALUES('" + data[0] + "', " + data[2] + ", '" + host + "', '" + data[1] + "', 0, 0, 0, 0, 0, " + (parent_id == "" ? "NULL": parent_id) + ",'" + host + "_" + data[0] +"')"
				runSQL(sql,conn,"cmd");

				sql = "INSERT INTO Audit.LogDetails ([user], log_type, timestamp, env, param1, param2) SELECT '" + modified_by + "', 14, GETDATE(), environment, 0, class_name + ' on host " + host + "' FROM Configuration.HostObserver ho "
				sql += "LEFT OUTER JOIN Configuration.Instances i on ho.instance_id = i.id AND ho.host = i.host WHERE ho.observer_id = " + data[0] + " AND ho.host = '" + host + "'"
				runSQL(sql,conn,"cmd");
			}

			for (var i=0;i<toDefault.length-1;i++) {
				var defaults = toDefault[i].split('/');
				var sql = "UPDATE [Configuration].[Environments] SET default_host = (SELECT id FROM [Configuration].[Hosts] WHERE name = '" + defaults[1] + "') WHERE name = '" + defaults[0] + "'"
				runSQL(sql,conn,"cmd");

				sql = "INSERT INTO Audit.LogDetails ([user], log_type, timestamp, env, param1, param2) VALUES('" + modified_by + "', 17, GETDATE(),'" + defaults[0] + "', 0, '" + defaults[1] + "')";
				runSQL(sql,conn,"cmd");
			}

			var tempSql = ''
			for (var i=0;i<copyAlerts.length-1;i++) {
				var data = copyAlerts[i].split('/');

				var sql = "INSERT INTO Configuration.AlertActions (observer_id, age, repeat_after, mail_target, mail_to, sms_target, sms_to, host, interface) SELECT " + data[0] + ", age, repeat_after, mail_target, mail_to, sms_target, sms_to, '" + host + "', interface FROM Configuration.AlertActions WHERE observer_id = " + data[1] + " AND host = '" + data[2] + "'"
				tempSql += sql;
				runSQL(sql,conn,"cmd");

				sql = "UPDATE ho1 SET ho1.obs_cba = ho2.obs_cba, ho1.critical_block_alert = ho2.critical_block_alert, ho1.obs_mail = ho2.obs_mail, ho1.mail_subject = ho2.mail_subject, "
				sql += "ho1.obs_sms = ho2.obs_sms, ho1.sms_text = ho2.sms_text, ho1.obs_interface = ho2.obs_interface, ho1.interface_text = ho2.interface_text, ho1.obs_message_group = ho2.obs_message_group, ho1.message_group = ho2.message_group, "
				sql += "ho1.obs_action = ho2.obs_action FROM Configuration.HostObserver ho1 "
				sql += "INNER JOIN Configuration.HostObserver ho2 ON ho2.observer_id = " + data[1] + " AND ho2.host = '" + data[2] + "' "
				sql += "WHERE ho1.observer_id = " + data[0] + " AND ho1.host = '" + host + "'"
				runSQL(sql,conn,"cmd");
			}

			for(var i=0; i<hostobs.length-1; i++) {
				var data = hostobs[i].split('/');
				var sql = "UPDATE Configuration.HostObserver SET obs_cba = " + (data[1] == "true"?1:0) + ", critical_block_alert = " + data[2] + ", obs_mail = " + (data[3] == "true"?1:0) + ", mail_subject = '" + data[4]
				sql += "', obs_sms = " + (data[5] == "true"?1:0) + ", sms_text = '" + data[6] + "', obs_interface = " + (data[7] == "true"?1:0) + ", interface_text = '" + data[8]
				sql += "', obs_message_group = " + (data[9] == "true"?1:0) + ", message_group = '" + data[10] + "', obs_action = " + (data[11] == "true"?1:0)
				sql += " WHERE observer_id = " + data[0] + " AND host = '" + host + "'"
				runSQL(sql,conn,"cmd");
			}

			for(var i=0; i<aas.length-1; i++) {
				var data = aas[i].split('/');
				var sql = "INSERT INTO Configuration.AlertActions (observer_id, age, repeat_after, mail_target, mail_to, sms_target, sms_to, host, interface, int_target) "
				sql += " VALUES (" + data[0] + ", " + data[1] + ", " + data[2] + ", " + (data[3] == "true"?1:0) + ", '" + data[4] + "', " + (data[5] == "true"?1:0) + ", '" + data[6] + "', '" + host + "', '" + data[7]+ "," +data[8]+")"
				runSQL(sql,conn,"cmd");
			}
			for(var i=0; i<aasu.length-1; i++) {
				var data = aasu[i].split('/');
				var sql = "UPDATE Configuration.AlertActions SET age = " + data[1] + ", repeat_after = " + data[2] + ", mail_target = " + (data[3] == "true"?1:0) + ", mail_to = '" + data[4]
				sql += "', sms_target = " + (data[5] == "true"?1:0) + ", sms_to = '" + data[6] + "', interface = '" + data[7] + ", int_target= "+data[8]+"' WHERE observer_id = " + data[0] + " AND host = '" + host + "'";
				runSQL(sql,conn,"cmd");
			}

			if (ms_instances != "undefined" && ms_instances != "") {
				sql = "INSERT INTO Communication (inputter_id, command_id, host, timestamp, environment, instance) ";
				sql += "SELECT 1, 9, host, GETDATE(), environment, i1.name FROM Configuration.Instances i1 ";
				sql += "	INNER JOIN Configuration.Hosts h1 ON h1.name = i1.host ";
				sql += "	INNER JOIN Configuration.Environments e1 ON e1.name = i1.environment ";
				sql += "	WHERE environment IN ( ";
				sql += "		SELECT environment FROM Configuration.Instances i2 ";
				sql += "			INNER JOIN Configuration.Hosts h2 ON h2.name = i2.host ";
				sql += "			INNER JOIN Configuration.Environments e2 ON e2.name = i2.environment ";
				sql += "		WHERE i2.host = '" + host + "' AND i2.id in ( " + ms_instances + " ) AND e2.default_host = h2.id) ";
				sql += "	 AND (i1.is_multi_server = 1 OR (i1.host = '" + host + "' AND i1.id in ( " + ms_instances + " ) AND e1.default_host = h1.id)) ";

				runSQL(sql,conn,"cmd");

				sql = "INSERT INTO Communication (inputter_id, command_id, host, timestamp, environment, instance) ";
				sql += "SELECT 1, 9, host, GETDATE(), environment, i.name FROM Configuration.Instances i "
				sql += "	INNER JOIN Configuration.Hosts h ON h.name = i.host "
				sql += "    INNER JOIN Configuration.Environments e ON e.name = i.environment "
				sql += "	WHERE i.host = '" + host + "' AND i.id in ( " + ms_instances + " ) AND e.default_host <> h.id"

				runSQL(sql,conn,"cmd");
			}
			if (s_instances != "undefined" && s_instances != "") {
				sql = "INSERT INTO Communication (inputter_id, command_id, host, timestamp, environment, instance) ";
				sql += "SELECT 1, 9, host, GETDATE(), environment, name FROM Configuration.Instances i"
				sql += "	WHERE i.host = '" + host + "' AND i.id in ( " + s_instances + " )"
				runSQL(sql,conn,"cmd");
			}

			StoreConfigFile (host, xmlx, typeid, conn);

			commitTransaction(conn);

			Response.write("File has been successfully saved");
		}
        break;
      case "save-simple":
        xmlx = String(Request("xmlx"));
        StoreConfigFile (host, xmlx, typeid, cAcc);
        Response.write("File has been successfully saved");
        break;
    }
  }
  catch(err) {
    if (conn)
      rollbackTransaction(conn)
    Response.Write("Error: " + err.message)
	  logData('file_data.asp: ' + err.message);
  }
%>