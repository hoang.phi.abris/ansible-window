<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="include/JSON.asp"-->
	<!--#include file="include/get_db_data.asp"-->
	<!--#include file="security/security.asp"-->

	<%
try {
//	if(FindRights([SEC_ROLE_CONTROLLER_OPERATION_VIEW, SEC_ROLE_CONTROLLER_OPERATION])) {
		var type_id = String(Request("type_id"));
		var environment_id = String(Request("environment_id"));

		var sql = "";
		switch(type_id) {
			case "1":
				sql = "select distinct application_name as 'name' from observer.[LOCK.CHECK] o INNER JOIN measurements m ON o.measurement_id = m.id where application_name is not null";
				break;
			case "2":
				sql = "select distinct logical_file_name  as 'name' from observer.[LOCK.CHECK] o INNER JOIN measurements m ON o.measurement_id = m.id where logical_file_name is not null";
				break;
			case "3":
				sql = "select distinct AdapterName + '|' + o.host + '|' + o.port as 'name' from observer.[TC.CHECK] o INNER JOIN measurements m ON o.measurement_id = m.id";
				sql += " WHERE AdapterName is not null and environment = (SELECT name FROM Configuration.Environments WHERE id = " + environment_id + ")";
				sql += " UNION ";
				sql += "select distinct AdapterName + '|' + o.servername 'name' from observer.[TC.CHECK.TOCF] o INNER JOIN measurements m ON o.measurement_id = m.id";
				sql += " where AdapterName is not null"
				break;
			case "4":
				sql = "select distinct ofs_source  as 'name' from observer.[OFS.REQUEST.DETAIL] o INNER JOIN measurements m ON o.measurement_id = m.id where ofs_source is not null";
				break;
			case "5":
				sql = "select distinct channel  as 'name' from observer.[MQ.CHECK] o INNER JOIN measurements m ON o.measurement_id = m.id where channel is not null";
				break;
			case "6":
				sql = "select distinct phantom_name as name from observer.[EB.PHANTOM] o INNER JOIN measurements m ON o.measurement_id = m.id where phantom_name <> 'All phantoms'";
				break;
			case "7":
				sql = "select distinct servlet_name as name from observer.[TOCF.BROWSER.SERVLET] o INNER JOIN measurements m ON o.measurement_id = m.id where servlet_name is not null";
				break;
			case "8":
				sql = "select distinct m.host + ':' + port as name from observer.[TOCF.QUEUE.CHECK] o INNER JOIN measurements m ON o.measurement_id = m.id where m.host is not null";
				break;
			case "9":
				sql = "select distinct cc.name as name from Configuration.[ControllerCommand] cc where cc.name is not null";
				break;
		}
		if(type_id!="9" && environment_id!=""){
			sql += " AND environment = (SELECT name FROM Configuration.Environments WHErE id = " + environment_id + ")"
		}
		sql += " EXCEPT select name from Configuration.DiagramParameter where type_id = " + type_id + " and environment_id = " + environment_id;
		rowsA = runSQL(sql,cAcc,"fulltable");

		var myJSON = new JSON();
		var data = ''
		var fields = ''
		if (rowsA.length)  {
			data = myJSON.toJSON(null, rowsA, false);
			fields = myJSON.generateFields(rowsA)
		}
		else {
			data = '[]'
			fields = '[]'
		}

		Response.Write( '{ "metaData": {	"totalProperty" : "total", "root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '", "results":' + data + '}');
//	}
}
catch (err) {
  Response.Write('Error: ' + err.message);
    logData('get_diag_param.asp: ' + err.message);
}

%>
