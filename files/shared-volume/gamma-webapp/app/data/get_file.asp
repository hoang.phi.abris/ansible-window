<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

  <!--#include file="security/security.asp"-->

  <%
  var currentdirectorypath = Server.MapPath(".")
  try
  {
    if(CheckRight(SEC_ROLE_LOGIN, null))
    {
      var fileName = String(Request("fileName"))
      var fs,f,t,x
      fs=Server.CreateObject("Scripting.FileSystemObject")
      t=fs.OpenTextFile(currentdirectorypath + "\\defaults\\" + fileName, 1, false)
      
      res = t.ReadAll()
      t.close()
      Response.write(res)
    }
  }
  catch(err) {
    Response.Write("Error: " + err.message + ' ' + currentdirectorypath)
  }
%>