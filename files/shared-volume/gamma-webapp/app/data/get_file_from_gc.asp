<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

<!--#include file="include/JSON.asp"-->
<!--#include file="include/get_db_data.asp"-->
<!--#include file="security/security.asp"-->

<%
try {
	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_DOWNLOAD_T24_STATE, null)) {
		var communication_id = String(Request("communication_id"));

		var sql= "SELECT thefile FROM observer.[GAMMA.CONTROLLER] g where communication_id = " + communication_id
		rowsA = runSQL(sql,cAcc,"fulltable");

		if (rowsA.length)  {
			Response.ContentType = "text/plain";             
			Response.AddHeader("Content-disposition", "attachment; filename=GAMMA.COLLECTED.INFO");		

			var sql2= "UPDATE observer.[GAMMA.CONTROLLER] SET thefile = NULL where communication_id = " + communication_id
			runSQL(sql2,cAcc,"cmd"); 

			Response.Write( rowsA[0].thefile .replace(/&amp;/g, "&")
				.replace(/&lt;/g, "<")
				.replace(/&gt;/g, ">")
				.replace(/&apos;/g, "'")
				.replace(/&quot;/g, '"')
			);
		}
		else {
			data = '[]'
			fields = '[]'
			Response.Write( '{ "metaData": {	"totalProperty" : "total", "root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '", "results":' + data + '}');
		}
	}
} 
catch (err) {
  Response.Write('Error: ' + err.message);
   logData('get_file_from_gc.asp: ' + err.message);
}


%>
