<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="include/JSON.asp"-->
	<!--#include file="include/get_db_data.asp"-->
	<!--#include file="include/controller_groups.asp"-->
	<!--#include file="security/security.asp"-->
	<!--#include file="include/json2.asp"-->

	<%
try {
	var fid = Number(Request("fid"));
	var fname = String(Request("fname"));
	if(CheckRight(SEC_ROLE_LOGIN, null) && FindRights([SEC_ROLE_CONTROLLER_VIEW, SEC_ROLE_CONTROLLER_CONFIG]))
	{
		var fileName = ''
		var funct = {}
		funct.id = fid;
		if (fname != "undefined") {
			funct.name = fname;
			fileName = fname + '.gcf'
		}
		
		getIs_cob(funct, fid);
		
		getGroups(funct, fid)
		getControllerScheds(funct, fid)
		getControllerCommands(funct, fid)
		var data = JSON.stringify(funct, null, 2);
	
		if (fileName != '') {
			Response.ContentType = "text/plain";             
			Response.AddHeader("Content-disposition", "attachment; filename=" + fileName);		
		}
		Response.Write(data);
	}
} 
catch (err) {
  Response.Write('Error: ' + err.message);
}

%>