<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="include/JSON.asp"-->
	<!--#include file="include/get_db_data.asp"-->
	<!--#include file="security/security.asp"-->

	<%
try {
	if(CheckRight(SEC_ROLE_LOGIN, null) && FindRights([SEC_ROLE_ENVMAN_CONFIG, SEC_ROLE_ENVMAN_VIEW]))
		var environment_id = Number(Request("environment_id"));

		var sql= "SELECT i.id as i_id, i.name as i_name, h.id as h_id, h.name as h_name, e.id as e_id, e.name as e_name FROM Configuration.Instances i "
		sql += "INNER JOIN Configuration.Hosts h ON i.host = h.name "
		sql += "INNER JOIN Configuration.Environments e ON i.environment = e.name "
		sql += "WHERE e.id = " + environment_id

		rowsA = runSQL(sql,cAcc,"fulltable");

		var myJSON = new JSON();
		var data = ''
		var fields = ''
		if (rowsA.length)  {
			data = myJSON.toJSON(null, rowsA, false); 
			fields = myJSON.generateFields(rowsA)
		}
		else {
			data = '[]'
			fields = '[]'
		}

		Response.Write( '{ "metaData": {	"totalProperty" : "total", "root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '", "results":' + data + '}');
}
catch (err) {
  Response.Write('Error: ' + err.message);
   logData('get_instance.asp: ' + err.message);
}

%>