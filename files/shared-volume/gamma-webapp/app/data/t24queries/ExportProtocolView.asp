<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

    <%
var fileName = isset(Request("myFn")) ? String(Request("myFn")) : "Protocol_Export"
Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader("Content-disposition", "attachment; filename=" + fileName +".xls");
%>
        <?xml version="1.0"?>
        <?mso-application progid="Excel.Sheet"?>
            <Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel"
                xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40">
                <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
                    <Title>GAMMA export</Title>
                    <Author>GAMMA</Author>
                    <Created>
                        <%=Response.Write(Date())%>
                    </Created>
                    <Version>1.0</Version>
                </DocumentProperties>
                <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
                    <AllowPNG/>
                </OfficeDocumentSettings>
                <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
                    <WindowHeight>9000</WindowHeight>
                    <WindowWidth>20750</WindowWidth>
                    <ProtectStructure>False</ProtectStructure>
                    <ProtectWindows>False</ProtectWindows>
                </ExcelWorkbook>
                <Styles>
                    <Style ss:ID="Hyperlink" ss:Name="Hyperlink">
                        <Font ss:FontName="Helvetica" ss:Size="12" ss:Color="#0000D4" ss:Underline="Single"/>
                    </Style>
                    <Style ss:ID="defaultcenter">
                        <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/><Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/><Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/></Borders><Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#215867"/><Interior ss:Color="#FAFAFA" ss:Pattern="Solid"/>
                    </Style>
                    <Style ss:ID="MeasurementTitle">
                        <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1" ss:Indent="0"/><Borders><Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#FAFAFA"/></Borders><Font ss:Bold="1" ss:Color="#616a76" ss:FontName="Tahoma" ss:Size="8"/><Interior ss:Color="#dddddd" ss:Pattern="Solid"/>
                    </Style>
                    <Style ss:ID="MeasurementText">
                        <Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/><Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#dcddde"/></Borders><Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#948B54"/><Interior ss:Color="#eeeeee" ss:Pattern="Solid"/>
                    </Style>
                </Styles>
                <Worksheet ss:Name='ProtocolExport'>
                    <WorksheetOptions xmlns='urn:schemas-microsoft-com:office:excel'>
                        <PageSetup>
                            <Header x:Data='&amp;B&amp;S04+000GAMMA&amp;S000000 &amp;S06-023export&#10;Measurements&amp;K&amp;S04+000modelbank'
                            />
                        </PageSetup>
                        <Unsynced/>
                        <Print>
                            <ValidPrinterInfo/>
                            <PaperSizeIndex>9</PaperSizeIndex>
                            <HorizontalResolution>600</HorizontalResolution>
                            <VerticalResolution>600</VerticalResolution>
                        </Print>
                        <Selected/>
                        <Panes>
                            <Pane>
                                <Number>3</Number>
                                <ActiveRow>4</ActiveRow>
                                <ActiveCol>1</ActiveCol>
                            </Pane>
                        </Panes>
                        <ProtectObjects>False</ProtectObjects>
                        <ProtectScenarios>False</ProtectScenarios>
                    </WorksheetOptions>


                    <!--#include file="../include/JSON.asp"-->
                    <!--#include file="../include/get_db_data.asp"-->
                    <!--#include file="../security/security.asp"-->


                    <%
try
{
    if (CheckRight(SEC_ROLE_LOGIN, null) && FindRight(SEC_ROLE_PROTOCOL_VIEW))
    {
        var size = (Request("size"));

        var environment = String(Request("environment"));
        var user = String(Request("user"));
        var application = String(Request("application"));
        var level_function = String(Request("level_function"));
        var companyId = String(Request("companyId"));
        var remark = String(Request("remark"));
        var terminal_id = String(Request("terminal_id"));
        var from = String(Request("from"));
        var to = String(Request("to"));
        var archive_db = String(Request("archive_db"));
        var selection_criteria = String(Request("selection_criteria"));
    		var classification = String(Request("classification"));
        var overR16 = String(Request("overR16"));
        var start = parseInt(Request("start"));
        var limit = parseInt(Request("limit"));

        var sql_start = "SELECT protocol_id, process_date, date_version, time, time_msecs, terminal_id, phantom_id, company_id, [user] as us, application, level_function, o.id as oid, remark, client_ip_address, selection_criteria, local_date_time, pw_activity_txn_id, channel, classification FROM "
        sql = " o INNER JOIN measurements m ON o.measurement_id = m.id WHERE o.timestamp between '" + from + "' AND '" + to + "'";
        sql += " AND environment in (select '" + environment + "' UNION select master_environment_name from Configuration.Environments where name = '" + environment + "') ";

        if (user != "All users")
            sql += " AND [user] = '" + user + "'";

        if (application != "All applications")
            sql += " AND application = '" + application + "'";

        if (level_function != "All levels")
            sql += " AND level_function = '" + level_function + "'";

        if (companyId != "All companies")
            sql += " AND company_id = '" + companyId + "'";

        if (remark != "")
            sql += " AND remark like '%" + remark + "%'";

        if (terminal_id != "")
            sql += " AND terminal_id like '%" + terminal_id + "%'";


    		if(classification) {
    			sql += " AND classification = '"+classification+"'";
    		}

    		if(selection_criteria) {
    			sql += " AND UPPER(selection_criteria) LIKE UPPER('"+selection_criteria+"')";
    		}

        var fin_sql = sql_start + " [observer].[PROTOCOL.STORE] " + sql;
        if (archive_db != "null")
        {
            fin_sql += " UNION " + sql_start + archive_db + ".[observer].[PROTOCOL.STORE] " + sql;
        }

        fin_sql += " ORDER BY process_date, time "

        var rowsA = runSQL(fin_sql, cAcc, "fulltable");

        var columnNum = 16;
        if(overR16=='true')  {
          columnNum = 21;
        }
        Response.Write('\n<Table x:FullRows="1" x:FullColumns="1" ss:ExpandedColumnCount="' + (columnNum + 2) + '" ss:ExpandedRowCount="' + (rowsA.length+2) + '">');
        var width = 120;

        for (var i = 0; i < columnNum; i++)
        {
            switch (i)
            {
                case 0:
                    width = 120;
                    break;
                case 3:
                    width = 50;
                    break;
                default:
                    width = 100;
            }
            Response.Write('\n<Column ss:Width= "' + width + '"/>');
        }


      Response.Write('\n<Row ss:AutoFitHeight="0" ss:Height="22">');
			Response.Write('\n<Cell ss:StyleID="MeasurementTitle"><Data ss:Type="String">Protocol Id</Data></Cell>');
			Response.Write('\n<Cell ss:StyleID="MeasurementTitle"><Data ss:Type="String">Process Date</Data></Cell>');
			Response.Write('\n<Cell ss:StyleID="MeasurementTitle"><Data ss:Type="String">Date Version</Data></Cell>');
			Response.Write('\n<Cell ss:StyleID="MeasurementTitle"><Data ss:Type="String">Time</Data></Cell>');
			Response.Write('\n<Cell ss:StyleID="MeasurementTitle"><Data ss:Type="String">Time (msec) reference</Data></Cell>');
			Response.Write('\n<Cell ss:StyleID="MeasurementTitle"><Data ss:Type="String">Terminal Id</Data></Cell>');
			Response.Write('\n<Cell ss:StyleID="MeasurementTitle"><Data ss:Type="String">Phantom Id</Data></Cell>');
			Response.Write('\n<Cell ss:StyleID="MeasurementTitle"><Data ss:Type="String">Company Id</Data></Cell>');
			Response.Write('\n<Cell ss:StyleID="MeasurementTitle"><Data ss:Type="String">User</Data></Cell>');
			Response.Write('\n<Cell ss:StyleID="MeasurementTitle"><Data ss:Type="String">Application</Data></Cell>');
			Response.Write('\n<Cell ss:StyleID="MeasurementTitle"><Data ss:Type="String">Level function</Data></Cell>');
			Response.Write('\n<Cell ss:StyleID="MeasurementTitle"><Data ss:Type="String">Id</Data></Cell>');
			Response.Write('\n<Cell ss:StyleID="MeasurementTitle"><Data ss:Type="String">Remark</Data></Cell>');
			Response.Write('\n<Cell ss:StyleID="MeasurementTitle"><Data ss:Type="String">Client ip address</Data></Cell>');
      if(overR16=='true') {
        Response.Write('\n<Cell ss:StyleID="MeasurementTitle"><Data ss:Type="String">Selection Criteria</Data></Cell>');
        Response.Write('\n<Cell ss:StyleID="MeasurementTitle"><Data ss:Type="String">Local Datetime</Data></Cell>');
        Response.Write('\n<Cell ss:StyleID="MeasurementTitle"><Data ss:Type="String">PW Activity txn Id</Data></Cell>');
        Response.Write('\n<Cell ss:StyleID="MeasurementTitle"><Data ss:Type="String">Channel</Data></Cell>');
        Response.Write('\n<Cell ss:StyleID="MeasurementTitle"><Data ss:Type="String">Classification</Data></Cell>');
      }

      Response.Write('\n</Row>');

        for (var i = 0; i < rowsA.length; i++)
        {
            Response.Flush();
            Response.Write('\n<Row ss:AutoFitHeight="0" ss:Height="22">');
				Response.Write('\n<Cell ss:StyleID="defaultcenter"><Data ss:Type="String">' + isNull(rowsA[i].protocol_id) + '</Data></Cell>');
				Response.Write('\n<Cell ss:StyleID="defaultcenter"><Data ss:Type="String">' + isNull(rowsA[i].process_date) + '</Data></Cell>');
				Response.Write('\n<Cell ss:StyleID="defaultcenter"><Data ss:Type="String">' + isNull(rowsA[i].date_version) + '</Data></Cell>');
				Response.Write('\n<Cell ss:StyleID="defaultcenter"><Data ss:Type="String">' + isNull(rowsA[i].time) + '</Data></Cell>');
				Response.Write('\n<Cell ss:StyleID="defaultcenter"><Data ss:Type="String">' + isNull(rowsA[i].time_msecs) + '</Data></Cell>');
				Response.Write('\n<Cell ss:StyleID="defaultcenter"><Data ss:Type="String">' + isNull(rowsA[i].terminal_id) + '</Data></Cell>');
				Response.Write('\n<Cell ss:StyleID="defaultcenter"><Data ss:Type="String">' + isNull(rowsA[i].phantom_id) + '</Data></Cell>');
				Response.Write('\n<Cell ss:StyleID="defaultcenter"><Data ss:Type="String">' + isNull(rowsA[i].company_id) + '</Data></Cell>');
				Response.Write('\n<Cell ss:StyleID="defaultcenter"><Data ss:Type="String">' + isNull(rowsA[i].us) + '</Data></Cell>');
				Response.Write('\n<Cell ss:StyleID="defaultcenter"><Data ss:Type="String">' + isNull(rowsA[i].application) + '</Data></Cell>');
				Response.Write('\n<Cell ss:StyleID="defaultcenter"><Data ss:Type="String">' + isNull(rowsA[i].level_function) + '</Data></Cell>');
				Response.Write('\n<Cell ss:StyleID="defaultcenter"><Data ss:Type="String">' + isNull(rowsA[i].oid) + '</Data></Cell>');
				Response.Write('\n<Cell ss:StyleID="defaultcenter"><Data ss:Type="String">' + isNull(rowsA[i].remark) + '</Data></Cell>');
				Response.Write('\n<Cell ss:StyleID="defaultcenter"><Data ss:Type="String">' + isNull(rowsA[i].client_ip_address) + '</Data></Cell>');
        if(overR16=='true')  {
          Response.Write('\n<Cell ss:StyleID="defaultcenter"><Data ss:Type="String">' + isNull(rowsA[i].selection_criteria) + '</Data></Cell>');
          Response.Write('\n<Cell ss:StyleID="defaultcenter"><Data ss:Type="String">' + toTime(rowsA[i].local_date_time) + '</Data></Cell>');
          Response.Write('\n<Cell ss:StyleID="defaultcenter"><Data ss:Type="String">' + isNull(rowsA[i].pw_activity_txn_id) + '</Data></Cell>');
          Response.Write('\n<Cell ss:StyleID="defaultcenter"><Data ss:Type="String">' + isNull(rowsA[i].channel) + '</Data></Cell>');
          Response.Write('\n<Cell ss:StyleID="defaultcenter"><Data ss:Type="String">' + isNull(rowsA[i].classification) + '</Data></Cell>');
        }
			  Response.Write('\n</Row>');
        }
        function toTime(data) {
          if(data) {
            return data.substring(2,4)+'/'+data.substring(4,6)+'/20'+data.substring(0,2) +' '+data.substring(6,8) +':'+data.substring(8,10)+':'+data.substring(10,12);
          } else {
            return "";
          }
        }
        function isNull(data)
        {
            if (data == null)
            {
                return "";
            }
            else
            {
                return data;
            }

        }

        Response.Write('</Table>')
        Response.Write('</Worksheet>')
        Response.Write('</Workbook>')

    }
}
catch (err)
{
    Response.Write("\n{success:failed, error:'" + err.message + "'}");
    logData('t24queries/ExportProtocolView.asp: ' + err.message);
}
%>