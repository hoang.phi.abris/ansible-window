<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

    <!--#include file="../include/JSON.asp"-->
    <!--#include file="../include/get_db_data.asp"-->
    <!--#include file="../security/security.asp"-->

    <script runat=server language=JavaScript>
        try
        {
            if (CheckRight(SEC_ROLE_LOGIN, null) && FindRight(SEC_ROLE_PROTOCOL_VIEW))
            {
                var environment = String(Request("environment"));
                var user = String(Request("user"));
                var application = String(Request("application"));
                var version = String(Request("version"));
                var functionc = String(Request("functionc"));
                var company = String(Request("company"));
                var status = String(Request("status"));
                var trans_reference = String(Request("trans_reference"));
                var msg_in = String(Request("msg_in"));
                var msg_out = String(Request("msg_out"));
                var recd_proc = (String(Request("recd_proc")) == "true");
                var from = String(Request("from"));
                var to = String(Request("to"));
                var archive_db = String(Request("archive_db"));
                var ord_id = String(Request("ord_id"));

                var start = parseInt(Request("start"));
                var limit = parseInt(Request("limit"));


                var date_field = "date_time_recd";
                if (!recd_proc)
                    date_field = "date_time_proc";

                var sql_start = "SELECT ord_id, application, version, [function], trans_reference, user_name, company, date_time_recd, date_time_queue, date_time_proc, status, msg_in, msg_out, action, gts_control, no_of_auth FROM ";
                sql = " o INNER JOIN measurements m ON o.measurement_id = m.id";
                sql += " WHERE " + date_field + " between convert(datetime, '" + from + "') AND convert(datetime, '" + to + "')";
                sql += " AND environment in (select '" + environment + "' UNION select master_environment_name from Configuration.Environments where name = '" + environment + "') ";

                if (user != "All users")
                {
                    if (user == '{Empty}')
                        sql += " AND user_name = ''";
                    else
                        sql += " AND user_name = '" + user + "'";
                }

                if (application != "All applications")
                {
                    if (application == '{Empty}')
                        sql += " AND application = ''";
                    else
                        sql += " AND application = '" + application + "'";
                }

                if (version != "All versions")
                {
                    if (version == '{Empty}')
                        sql += " AND version = ''";
                    else
                        sql += " AND version = '" + version + "'";
                }

                if (functionc != "All functions")
                {
                    if (functionc == '{Empty}')
                        sql += " AND [function] = ''";
                    else
                        sql += " AND [function] = '" + functionc + "'";
                }

                if (company != "All companies")
                {
                    if (company == '{Empty}')
                        sql += " AND company = ''";
                    else
                        sql += " AND company = '" + company + "'";
                }

                if (status != "All statuses")
                {
                    if (status == '{Empty}')
                        sql += " AND status = ''";
                    else
                        sql += " AND status = '" + status + "'";
                }

                if (trans_reference != "")
                    sql += " AND trans_reference = '" + trans_reference + "'";

                if (msg_in != "")
                    sql += " AND msg_in like '%" + msg_in + "%'";

                if (msg_out != "")
                    sql += " AND msg_out like '%" + msg_out + "%'";

                if (ord_id != "")
                    sql += " AND ord_id = '" + ord_id + "'";

                var fin_sql = sql_start + " [observer].[OFS.REQUEST.DETAIL.STORE] " + sql;
                if (archive_db != "null")
                {
                    fin_sql += " UNION " + sql_start + archive_db + ".[observer].[OFS.REQUEST.DETAIL.STORE] " + sql;
                }

                fin_sql += " ORDER BY date_time_recd ";

                var rowsA = runSQL(fin_sql, cAcc, "fulltable");
                var result = [];

                if (rowsA.length < start)
                {
                    start = 0;
                    limit = 50;
                }

                for (var i = start; i < rowsA.length && i < (start + limit); i++)
                    result.push(rowsA[i])

                var myJSON = new JSON();
                var data = myJSON.toJSON(null, result, false);
                var fields = myJSON.generateFields(rowsA);

                Response.Write('{ "metaData": {	"totalProperty" : "total", "root" : "results", "id" : "id"}, "total":"' + rowsA.length + '", "results":' + data + '}');
            }
        }
        catch (err)
        {
            Response.Write("{success:failed, error:'" + err.message + "'}");
            logData('t24queries/get_ordview.asp: ' + err.message);
        }
    </script>