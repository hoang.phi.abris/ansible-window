<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

<!--#include file="../include/JSON.asp"-->
<!--#include file="../include/get_db_data.asp"-->
<!--#include file="../security/security.asp"-->

<%
/* configre extra url request header properties */
try{
	if(CheckRight(SEC_ROLE_LOGIN, null)) 
	{
		var environment = String(Request("environment"));

		var rowsA = []
		var myJSON = new JSON();
		var result = {
			users : [],
			applications : [],
			level_functions : [],
			company_ids : []
		}

		sql = "select value from Configuration.DistinctValue where type = 11 Order by value ASC"
		rowsA = runSQL(sql,cAcc,"fulltable");
		for(var i=0;i<rowsA.length;i++)
		{
			if([rowsA[i].value] == '')
				result.users.push(['{Empty}']);
			else
				result.users.push([rowsA[i].value]);
		}
			
			
		sql = "select value from Configuration.DistinctValue where type = 12 Order by value ASC"
		rowsA = runSQL(sql,cAcc,"fulltable");
		for(var i=0;i<rowsA.length;i++)
		{
			if([rowsA[i].value] == '')
				result.applications.push(['{Empty}']);
			else
				result.applications.push([rowsA[i].value]);
		}


		sql = "select value from Configuration.DistinctValue where type = 13 Order by value ASC"
		rowsA = runSQL(sql,cAcc,"fulltable");
		for(var i=0;i<rowsA.length;i++)
		{
			if([rowsA[i].value] == '')
				result.level_functions.push(['{Empty}']);
			else
				result.level_functions.push([rowsA[i].value]);
		}


		sql = "select value from Configuration.DistinctValue where type = 14 Order by value ASC"
		rowsA = runSQL(sql,cAcc,"fulltable");
		for(var i=0;i<rowsA.length;i++)
		{
			if([rowsA[i].value] == '')
				result.company_ids.push(['{Empty}']);
			else
				result.company_ids.push([rowsA[i].value]);
		}

		result = myJSON.toJSON(null, result, false); 

		Response.Write(result);
	}
}
catch(err) {
	Response.Write( "{success:failed, error:'" + err.message + "'}");
	logData('t24queries/get_protocol_dist.asp: ' + err.message);
}
%>
