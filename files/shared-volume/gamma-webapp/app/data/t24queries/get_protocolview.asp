<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<script runat=server language=JavaScript>

		try {
			if (CheckRight(SEC_ROLE_LOGIN, null) && FindRight(SEC_ROLE_PROTOCOL_VIEW)) {
				var environment = String(Request("environment"));
				var user = String(Request("user"));
				var application = String(Request("application"));
				var level_function = String(Request("level_function"));
				var companyId = String(Request("companyId"));
				var remark = String(Request("remark"));
				var terminal_id = String(Request("terminal_id"));
				var from = String(Request("from"));
				var to = String(Request("to"));
				var archive_db = String(Request("archive_db"));
				var selection_criteria = String(Request("selection_criteria"));
				var classification = String(Request("classification"));
				var start = parseInt(Request("start"));
				var limit = parseInt(Request("limit"));

				var sql_start = "SELECT protocol_id, process_date, date_version, time, time_msecs, terminal_id, phantom_id, company_id, [user], application, level_function, o.id, remark, client_ip_address, selection_criteria, local_date_time, pw_activity_txn_id, channel, classification FROM "
				sql = " o INNER JOIN measurements m ON o.measurement_id = m.id WHERE o.timestamp between '" + from + "' AND '" + to + "'";
				sql += " AND environment in (select '" + environment + "' UNION select master_environment_name from Configuration.Environments where name = '" + environment + "') ";

				if (user != "All users") {
					if (user == '{Empty}')
						sql += " AND [user] = ''";
					else
						sql += " AND [user] = '" + user + "'";
				}


				if (application != "All applications") {
					if (application == '{Empty}')
						sql += " AND application = ''";
					else
						sql += " AND application = '" + application + "'";
				}

				if (classification) {
					sql += " AND classification = '" + classification + "'";
				}

				if (selection_criteria) {
					sql += " AND UPPER(selection_criteria) LIKE UPPER('" + selection_criteria + "')";
				}

				if (level_function != "All levels") {
					if (level_function == '{Empty}')
						sql += " AND level_function = ''";
					else
						sql += " AND level_function = '" + level_function + "'";
				}


				if (companyId != "All companies") {
					if (companyId == '{Empty}')
						sql += " AND company_id = ''";
					else
						sql += " AND company_id = '" + companyId + "'";
				}


				if (remark != "")
					sql += " AND remark like '%" + remark + "%'";

				if (terminal_id != "")
					sql += " AND terminal_id like '%" + terminal_id + "%'";


				var fin_sql = sql_start + " [observer].[PROTOCOL.STORE] " + sql;
				if (archive_db != "null") {
					fin_sql += " UNION " + sql_start + archive_db + ".[observer].[PROTOCOL.STORE] " + sql;
				}

				fin_sql += " ORDER BY process_date, time "

				var rowsA = runSQL(fin_sql, cAcc, "fulltable");
				var result = [];

				if (rowsA.length < start) {
					start = 0;
					limit = 50;
				}

				for (var i = start; i < rowsA.length && i < (start + limit); i++)
					result.push(rowsA[i])

				var myJSON = new JSON();
				var data = myJSON.toJSON(null, result, false);
				var fields = myJSON.generateFields(rowsA);

				Response.Write('{ "metaData": {	"totalProperty" : "total", "root" : "results", "id" : "id"}, "total":"' + rowsA.length + '", "results":' + data + '}');
			}
		}
		catch (err) {
			Response.Write("{success:failed, error:'" + err.message + "', sql: '" + fin_sql + "'}");
			logData('t24queries/get_protocolview.asp: ' + err.message);
		}
	</script>