<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<script runat=server language=JavaScript>

		try {
			if (CheckRight(SEC_ROLE_LOGIN, null) && FindRight(SEC_ROLE_PROTOCOL_VIEW)) {

				var sql = "SELECT dataIndex from Configuration.[Observer.Columns] WHERE sensitive = 1 AND observer = 'OFS.REQUEST.DETAIL.STORE'";


				var rowsA = runSQL(sql, cAcc, "fulltable");
				var result = [];
				for (i = 0; i < rowsA.length; i++)
					result.push(rowsA[i])

				var myJSON = new JSON();
				var data = myJSON.toJSON(null, result, false);
				var fields = myJSON.generateFields(rowsA);

				Response.Write('{ "metaData": {	"totalProperty" : "total", "root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '", "results":' + data + '}');
			}
		}
		catch (err) {
			Response.Write("{success:failed, error:'" + err.message + "'}");
			logData('t24queries/get_ordview_sensitive.asp: ' + err.message);
		}

	</script>