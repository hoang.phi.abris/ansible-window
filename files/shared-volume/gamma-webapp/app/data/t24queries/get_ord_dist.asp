<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
/* configre extra url request header properties */
try{
if(CheckRight(SEC_ROLE_LOGIN, null)) 
{
	var environment = String(Request("environment"));

	var rowsA = []
	var myJSON = new JSON();
	var result = {
		user_names : [],
		applications : [],
		functions : [],
		companies : [],
		versions : [],
		statuses : []
	}

	sql = "select value from Configuration.DistinctValue where type = 1  Order by value ASC"
	rowsA = runSQL(sql,cAcc,"fulltable");
	for(var i=0;i<rowsA.length;i++)
	{
		if([rowsA[i].value] == '')
			result.user_names.push(['{Empty}']);
		else
			result.user_names.push([rowsA[i].value]);
	}

	
	sql = "select value from Configuration.DistinctValue where type = 2  Order by value ASC"
	rowsA = runSQL(sql,cAcc,"fulltable");
	for(var i=0;i<rowsA.length;i++)
	{
		if([rowsA[i].value] == '')
			result.applications.push(['{Empty}']);
		else
			result.applications.push([rowsA[i].value]);
	}

	sql = "select value from Configuration.DistinctValue where type = 3  Order by value ASC"
	rowsA = runSQL(sql,cAcc,"fulltable");
	for(var i=0;i<rowsA.length;i++)
	{
		if([rowsA[i].value] == '')
			result.functions.push(['{Empty}']);
		else
			result.functions.push([rowsA[i].value]);
	}


	sql = "select value from Configuration.DistinctValue where type = 4  Order by value ASC"
	rowsA = runSQL(sql,cAcc,"fulltable");
	for(var i=0;i<rowsA.length;i++)
	{
		if([rowsA[i].value] == '')
			result.companies.push(['{Empty}']);
		else
			result.companies.push([rowsA[i].value]);
	}


	sql = "select value from Configuration.DistinctValue where type = 5  Order by value ASC"
	rowsA = runSQL(sql,cAcc,"fulltable");
	for(var i=0;i<rowsA.length;i++)
	{
		if([rowsA[i].value] == '')
			result.versions.push(['{Empty}']);
		else
			result.versions.push([rowsA[i].value]);
	}


	sql = "select value from Configuration.DistinctValue where type = 6  Order by value ASC"
	rowsA = runSQL(sql,cAcc,"fulltable");
	for(var i=0;i<rowsA.length;i++)
	{
		if([rowsA[i].value] == '')
			result.statuses.push(['{Empty}']);
		else
			result.statuses.push([rowsA[i].value]);
	}


	result = myJSON.toJSON(null, result, false); 

	Response.Write(result);
}
} 
catch (err) {
logData('t24queries/get_ord_dist.asp: ' + err.message);
  Response.Write('Error: ' + err.message);
}
/*
- DATE.TIME.RECD or DATE.TIME.PROC (mandatory because of the size of the tables)
 - USER.NAME: combo - All or one selected user
 - APPLICATION: combo - All or one selected application
 - VERSION: combo - All or one selected version
 - FUNCTION: combo - All or one selected function
 - STATUS: combo - All or one selected status
- TRANS.REFERENCE: edit - specified search string
- MSG.IN: edit - specified search string
- MSG.OUT: edit - specified search string
*/

%>