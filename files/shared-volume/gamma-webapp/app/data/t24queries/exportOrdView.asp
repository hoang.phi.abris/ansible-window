
<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

<%
var fileName = isset(Request("myFn")) ? String(Request("myFn")) : "ORD_View_Export"
Response.ContentType = "application/vnd.ms-excel" 
Response.AddHeader("Content-disposition", "attachment; filename=" + fileName +".xls");	
%><?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
	<Title>GAMMA export</Title>
	<Author>GAMMA</Author> 
	<Created><%=Response.Write(Date())%></Created>
  <Version>1.0</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
   <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>9000</WindowHeight>
  <WindowWidth>20750</WindowWidth>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Hyperlink" ss:Name="Hyperlink">
   <Font ss:FontName="Helvetica" ss:Size="12" ss:Color="#0000D4" ss:Underline="Single"/>
</Style>
<Style ss:ID="defaultcenter">
	<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
	<Borders>
		<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/>
		<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/>
	</Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#215867"/>
	<Interior ss:Color="#FAFAFA" ss:Pattern="Solid"/>
</Style>
<Style ss:ID="encrypted">
	<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
	<Borders>
		<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/>
		<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#E8EAEC"/>
	</Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#215867"/>
	<Interior ss:Color="#FAFAFA" ss:Pattern="Solid"/>
</Style>
<Style ss:ID="MeasurementTitle">
	<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1" ss:Indent="0"/>
	<Borders>
		<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#FAFAFA"/>
	</Borders>
	<Font ss:Bold="1" ss:Color="#616a76" ss:FontName="Tahoma" ss:Size="8"/>
	<Interior ss:Color="#dddddd" ss:Pattern="Solid"/>
</Style>
<Style ss:ID="MeasurementText">
	<Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/>
	<Borders>
		<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#dcddde"/>
	</Borders>
	<Font ss:FontName="Tahoma" x:CharSet="238" x:Family="Swiss" ss:Size="8" ss:Color="#948B54"/>
	<Interior ss:Color="#eeeeee" ss:Pattern="Solid"/>
</Style>
 </Styles>
 <Worksheet ss:Name='ORDViewExport'>
<WorksheetOptions xmlns='urn:schemas-microsoft-com:office:excel'> <PageSetup><Header x:Data='&amp;B&amp;S04+000GAMMA&amp;S000000 &amp;S06-023export&#10;Measurements&amp;K&amp;S04+000modelbank'/></PageSetup><Unsynced/> <Print><ValidPrinterInfo/><PaperSizeIndex>9</PaperSizeIndex><HorizontalResolution>600</HorizontalResolution> <VerticalResolution>600</VerticalResolution></Print><Selected/><Panes><Pane><Number>3</Number><ActiveRow>4</ActiveRow><ActiveCol>1</ActiveCol></Pane></Panes><ProtectObjects>False</ProtectObjects>   <ProtectScenarios>False</ProtectScenarios>  </WorksheetOptions>


<!--#include file="../include/JSON.asp"-->
<!--#include file="../include/get_db_data.asp"-->
<!--#include file="../security/security.asp"-->


<%
try {
	var environment = String(Request("environment"));
	var has_sensitive_right = HasRight(SEC_ROLE_VIEW_SENSITIVE_DATA, environment)

	if(CheckRight(SEC_ROLE_LOGIN, null) && FindRight(SEC_ROLE_PROTOCOL_VIEW) )
	{
		var size = (Request("size"));

		var ord_id = isset(Request("ord_id")) ? String(Request("ord_id"))  : '';

		var environment = String(Request("environment"));
		var user = String(Request("user"));
		var application = String(Request("application"));
		var version = String(Request("version"));
		var functionc = String(Request("functionc"));
		var company = String(Request("company"));
		var status = String(Request("status"));
		var trans_reference = String(Request("trans_reference"));
		var msg_in = String(Request("msg_in"));
		var msg_out = String(Request("msg_out"));
		var recd_proc = (String(Request("recd_proc")) == "true");
		var from = String(Request("from"));
		var to = String(Request("to"));
		var archive_db = String(Request("archive_db"));
		var hidden_fields = isset(Request("hidden_fields")) ? String(Request("hidden_fields")) : '';

		var start = parseInt(Request("start"));
		var limit = parseInt(Request("limit"));


		var date_field = "date_time_recd"
		if (!recd_proc)
			date_field = "date_time_proc"

		var sql_start = "SELECT ord_id, application, version, [function] as fun, trans_reference, user_name, company, date_time_recd, date_time_queue, date_time_proc, status, msg_in, msg_out, action, gts_control, no_of_auth FROM "
		sql = " o INNER JOIN measurements m ON o.measurement_id = m.id"
		sql += " WHERE " + date_field + " between convert(datetime, '" + from + "') AND convert(datetime, '" + to + "')"
		sql += " AND environment in (select '" + environment + "' UNION select master_environment_name from Configuration.Environments where name = '" + environment + "') ";

		if (ord_id != "")
			sql += " AND ord_id = '" + ord_id + "'";

		if (user != "All users")
			sql += " AND user_name = '" + user + "'";

		if (application != "All applications")
			sql += " AND application = '" + application + "'";

		if (version != "All versions")
			sql += " AND version = '" + version + "'";

		if (functionc != "All functions")
			sql += " AND [function] = '" + functionc + "'";

		if (company != "All companies")
			sql += " AND company = '" + company + "'";

		if (status != "All statuses")
			sql += " AND status = '" + status + "'";

		if (trans_reference != "")
			sql += " AND trans_reference = '" + trans_reference + "'";

		if (msg_in != "")
			sql += " AND msg_in like '%" + msg_in + "%'";

		if (msg_out != "")
			sql += " AND msg_out like '%" + msg_out + "%'";

		var fin_sql = sql_start + " [observer].[OFS.REQUEST.DETAIL.STORE] " + sql;
		if (archive_db != "null") {
			fin_sql += " UNION " + sql_start + archive_db + ".[observer].[OFS.REQUEST.DETAIL.STORE] " + sql;
		}

		fin_sql += " ORDER BY date_time_recd "

		var rowsA = [];
		rowsA = runSQL(fin_sql,cAcc,"fulltable");


		Response.Write('\n<Table x:FullRows="1" x:FullColumns="1" ss:ExpandedColumnCount="' + (16 + 2 ) + '" ss:ExpandedRowCount="' + (rowsA.length+2) + '">');
		var width = 120;
		for (var i=0; i<16;i++)	{
			switch (i)
			{
			   case 0:
					width = 120;
					break;
			   case 3:
					width = 50;
					break;
				case 4:
					width = 150;
					break;
			   case 11 :
				    width = 450;
					break;
				case 12:
				    width = 450;
					break;
			   default:
				   width = 100;
			}
			Response.Write('\n<Column ss:Width= "' + width + '"/>');
		}

		var titles = ["ORD Id","Application","Version","Function","Trans reference","User","Company","Data Received","Data Queue","Data Processed","Status","MSG In","MSG Out","Action","Gts Control","No Of Auth"];
		var columns = ["ord_id","application","version","fun","trans_reference","user_name","company","date_time_recd","date_time_queue","date_time_proc","status","msg_in","msg_out","action","gts_control","no_of_auth"];

		Response.Write('\n<Row ss:AutoFitHeight="0" ss:Height="22">');
			for (var c = 0; c < columns.length; c++) {
				if (isVisibleColumn(columns[c])) {
					Response.Write('\n<Cell ss:StyleID="MeasurementTitle"><Data ss:Type="String">' + titles[c] + '</Data></Cell>');
				}
			}
		Response.Write('\n</Row>');


		for(var i=0;i<rowsA.length; i++)
		{
			Response.Flush();
			Response.Write('\n<Row ss:AutoFitHeight="0" ss:Height="22">');
				for (var r = 0; r < columns.length; r++) {
					if (isVisibleColumn(columns[r])) {
						var style = getStyleId(columns[r]);
						var value = isNull(rowsA[i][columns[r]]);
						Response.Write('\n<Cell ss:StyleID="' + style + '"><Data ss:Type="String">' + value + '</Data></Cell>');
					}
				}
			Response.Write('\n</Row>');
		}

		function isNull(data) {
			if(data == null)
			{
				return "";
			}
			else
			{
				return data;
			}

		}

		function isVisibleColumn(col) {
			if (hidden_fields.indexOf(col) < 0 || has_sensitive_right) {
				return true;
			}
			else {
				return false;
			}
		}

		function getStyleId(col) {
			if (hidden_fields.indexOf(col) > -1	&& has_sensitive_right) {
				return "encrypted";
			}
			else {
				return "defaultcenter";
			}
		}
		
		Response.Write('</Table>')
		Response.Write('</Worksheet>')
		Response.Write('</Workbook>')
		
	}
}
catch(err) {
	Response.Write( "\n{success:failed, error:'" + err.message + "'}");
	logData('t24queries/ExportOrdView.asp: ' + err.message);
}
%>
