<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="include/JSON.asp"-->
	<!--#include file="include/get_db_data.asp"-->
	<!--#include file="security/security.asp"-->

	<%
try {
	//takes token regardless of the user it has (called after timeout)
	if(CheckRight(SEC_ROLE_LOGIN, null)) {
		var token_type = String(Request("token_type"));
		var sub_type = String(Request("sub_type"));
		var browser_id = String(Request("browser_id"));
		var userID = Session("username");

		var sql = "UPDATE Configuration.TokenData SET user_name = '" + userID + "', browser_id = '" + browser_id + "', timestamp = GETDATE(), status = 0 WHERE token_type = " + token_type + " AND sub_type = " + sub_type;
		runSQL(sql,cAcc,"cmd");

		sql= "SELECT user_name, browser_id, status FROM Configuration.TokenData WHERE token_type = " + token_type + " AND sub_type = " + sub_type;
		rowsA = runSQL(sql,cAcc,"fulltable");
		
		var myJSON = new JSON();
		var data = ''
		var fields = ''
		if (rowsA.length)  {
			data = myJSON.toJSON(null, rowsA, false); 
			fields = myJSON.generateFields(rowsA)
		}
		else {
			data = '[]'
			fields = '[]'
		}

		Response.Write( '{ "metaData": {	"totalProperty" : "total", "root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '", "results":' + data + '}');
	}
} 
catch (err) {
  Response.Write('Error: ' + err.message);
   logData('fget_config_token.asp: ' + err.message);
}

%>