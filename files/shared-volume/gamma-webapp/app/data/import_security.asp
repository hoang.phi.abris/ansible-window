<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="include/JSON.asp"-->
	<!--#include file="include/get_db_data.asp"-->
	<!--#include file="include/util.asp"-->
	<!--#include file="security/security.asp"-->
	<%
	var conn = ''
	if(CheckRight(SEC_ROLE_LOGIN, null)) {
		try
		{
			conn = beginTransaction(cAcc)
			var result = String(Request("result"))
			var sql = "";
			result = eval('('+result+')');

			for(var i=0; i<result.portalProfiles.length; i++) {
				var profile = result.portalProfiles[i];
				
				if (profile.id_in_target == -1) {
					sql = "INSERT INTO Configuration.PortalProfile (user_name, name, is_global) OUTPUT INSERTED.id "
					sql += "VALUES ('" + profile.user_name + "', '" + profile.name + "', 1)"    
					rowsA = runSQL(sql, conn, "fulltable"); 				
					var profile_id = rowsA[0].id;
					for(var j=0; j<profile.items.length; j++) {
						var item = profile.items[j];
						sql = "INSERT INTO Configuration.PortalProfileItem (profile_id, env, position, diagram_id) "
						sql += "VALUES (" + profile_id + ", '" + item.env_id_in_target + "', " + item.position + ", " + item.diagram_id + ")"    
						runSQL(sql, conn, "cmd"); 				
					}
					for(var j=0; j<result.users.length; j++) {
						var user = result.users[j];
						if (user.profile_id == profile.id && user.portal_id_in_target == -1) {
							user.portal_id_in_target = profile_id;
						}
					}
				}
				else {
					sql = "UPDATE Configuration.PortalProfile set user_name = '" + profile.user_name + "' WHERE id = " + profile.id_in_target 
					rowsA = runSQL(sql, conn, "cmd"); 				
					sql = "DELETE FROM Configuration.PortalProfileItem WHERE profile_id = " + profile.id_in_target;
					rowsA = runSQL(sql, conn, "cmd"); 				
					for(var j=0; j<profile.items.length; j++) {
						var item = profile.items[j];
						sql = "INSERT INTO Configuration.PortalProfileItem (profile_id, env, position, diagram_id) "
						sql += "VALUES (" + profile.id_in_target + ", '" + item.env_id_in_target + "', " + item.position + ", " + item.diagram_id + ")"    
						runSQL(sql, conn, "cmd"); 				
					}
				}
			}
			
			for(var i=0; i<result.users.length; i++) {
				var user = result.users[i];
				
				if (user.id_in_target == -1) {
					var env_id = "NULL"
					if (user.env_id_in_target != undefined)
						env_id = user.env_id_in_target;

					
					sql = "INSERT INTO Security.[user] (ad_id, name, details, status, profile_id, environment_id, sessiontimeout) OUTPUT INSERTED.id "
					sql += "VALUES ('" + user.ad_id + "', '" + user.name + "', '" + user.details + "', " + (user.status?1:0) + ", " + user.portal_id_in_target + "," + env_id + "," + user.sessiontimeout+ ")"
					
					rowsA = runSQL(sql, conn, "fulltable"); 				

					setKnownIds(result, rowsA[0].id, "userRoles", user, "user_id", 0);
					setKnownIds(result, rowsA[0].id, "userUserGrps", user, "user_id", 0);
					setKnownIds(result, rowsA[0].id, "userRoleGrps", user, "user_id", 0);					
				}
				else {
					sql = "UPDATE Security.[user] set name = '" + user.name + "', details = '" + user.details + "', status = '" + (user.status?1:0) + "' WHERE ad_id = '" + user.ad_id + "'"
					rowsA = runSQL(sql, conn, "cmd"); 				
				}	
			}

			for(var i=0; i<result.userGroups.length; i++) {
				var userGroup = result.userGroups[i];
				
				if (userGroup.id_in_target == -1) {
					sql = "INSERT INTO Security.[UserGroup] (name, description) OUTPUT INSERTED.id "
					sql += "VALUES ('" + userGroup.name + "', '" + userGroup.description + "')"    
					rowsA = runSQL(sql, conn, "fulltable"); 				

					//logData(userGroup.name + " new id " + rowsA[0].id)

					setKnownIds(result, rowsA[0].id, "userUserGrps", userGroup, "usergroup_id", 1);
					setKnownIds(result, rowsA[0].id, "roleUserGrps", userGroup, "usergroup_id", 1);
					setKnownIds(result, rowsA[0].id, "userGrpRoleGrps", userGroup, "usergroup_id", 0);
					setKnownIds(result, rowsA[0].id, "userGrpUserGrps", userGroup, "usergroup1_id", 0);
					setKnownIds(result, rowsA[0].id, "userGrpUserGrps", userGroup, "usergroup2_id", 1);
				}
				else {
					sql = "UPDATE Security.[UserGroup] set description = '" + userGroup.description + "' WHERE id = " + userGroup.id_in_target 
					rowsA = runSQL(sql, conn, "cmd"); 				
				}	
			}

			for(var i=0; i<result.roleGroups.length; i++) {
				var roleGroup = result.roleGroups[i];
				
				if (roleGroup.id_in_target == -1) {
					sql = "INSERT INTO Security.[RoleGroup] (name, description) OUTPUT INSERTED.id "
					sql += "VALUES ('" + roleGroup.name + "', '" + roleGroup.description + "')"    
					rowsA = runSQL(sql, conn, "fulltable"); 				

					setKnownIds(result, rowsA[0].id, "userRoleGrps", roleGroup, "rolegroup_id", 1);
					setKnownIds(result, rowsA[0].id, "roleRoleGrps", roleGroup, "rolegroup_id", 1);
					setKnownIds(result, rowsA[0].id, "userGrpRoleGrps", roleGroup, "rolegroup_id", 1);
					setKnownIds(result, rowsA[0].id, "roleGrpRoleGrps", roleGroup, "rolegroup1_id", 0);
					setKnownIds(result, rowsA[0].id, "roleGrpRoleGrps", roleGroup, "rolegroup2_id", 1);
				}
				else {
					sql = "UPDATE Security.[RoleGroup] set description = '" + roleGroup.description + "' WHERE id = " + roleGroup.id_in_target 
					//logData(sql)
					rowsA = runSQL(sql, conn, "cmd"); 				
				}	
			}

			insertLink(result, "userRoles", "user_id", "role_id", "Security.[UserRoles]")
			insertLink(result, "userUserGrps", "user_id", "usergroup_id", "Security.[UserUserGrps]")
			insertLink(result, "userRoleGrps", "user_id", "rolegroup_id", "Security.[UserRoleGrps]")
			insertLink(result, "roleUserGrps", "role_id", "usergroup_id", "Security.[RoleUserGrps]")
			insertLink(result, "roleRoleGrps", "role_id", "rolegroup_id", "Security.[RoleRoleGrps]")
			insertLink(result, "userGrpRoleGrps", "usergroup_id", "rolegroup_id", "Security.[UserGrpRoleGrps]")
			insertLink(result, "userGrpUserGrps", "usergroup1_id", "usergroup2_id", "Security.[UserGrpUserGrps]")
			insertLink(result, "roleGrpRoleGrps", "rolegroup1_id", "rolegroup2_id", "Security.[RoleGrpRoleGrps]")
			
			commitTransaction(conn);
		}
		catch(err) {
			if (conn)
				rollbackTransaction(conn)
			Response.Write("Error: " + err.message)
			 logData('import_security.asp: ' + err.message);
		}
	}
	
	function setKnownIds(result, id, listName, item, item_id, pos) {
		for(var i=0;i<result[listName].length;i++) {
//			var text = ''
//			for(var o in result[listName][i]) {
//				var t = o + ':' + result[listName][i][o];
//				text += t + '; ';
//			}	
//			logData(listName + " " + i +" " + text + " " + item.id + " <- " + id)
			if(!result[listName][i].done[pos] && item.id == result[listName][i][item_id]) {
//				logData(listName + " " + item.id + " <- " + id)
				result[listName][i][item_id] = id;
				result[listName][i].done[pos] = true;
			}
		}
	}

	function insertLink (result, listName, id1, id2, tableName) {
		for(var i=0; i<result[listName].length; i++) {
			var item = result[listName][i];
			sql = "INSERT INTO " + tableName + " ( " + id1 + ", " + id2 + " ) "
			sql += "VALUES (" + item[id1] + ", " + item[id2] + ")"    
			logData(sql)
			rowsA = runSQL(sql, conn, "cmd"); 				
		}
	}
%>