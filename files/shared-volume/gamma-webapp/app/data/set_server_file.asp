<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

  <!--#include file="include/JSON.asp"-->
  <!--#include file="include/get_db_data.asp"-->

  <%
  try {
    var inputterid = Request("inputterid");
    var commandid = Request("commandid");
    var host = Request("host");
    var filetypeid = Request("filetypeid");
    var thefile = Request("thefile");

    var adCmdStoredProc = 4
    var adParamInput = 1
    var adParamOutput = 2
    var adInteger = 3
    var adBStr = 8
    var adVarWChar = 202
    var cmd = Server.CreateObject('ADODB.Command');
    cmd.ActiveConnection = cAcc;
    cmd.CommandType = adCmdStoredProc
    cmd.CommandText = "Configuration.SetServerFile"

    cmd.Parameters.Append(cmd.CreateParameter("@inputter", adInteger, adParamInput, -1, inputterid));
    cmd.Parameters.Append(cmd.CreateParameter("@commandid", adInteger, adParamInput, -1, commandid));
    cmd.Parameters.Append(cmd.CreateParameter("@host", adVarWChar, adParamInput, -1, host));
    cmd.Parameters.Append(cmd.CreateParameter("@filetypeid", adInteger, adParamInput, -1, filetypeid));
    cmd.Parameters.Append(cmd.CreateParameter("@theFile", adBStr, adParamInput, -1, thefile));
    cmd.Parameters.Append(cmd.CreateParameter("@status", adVarWChar, adParamOutput, 5));

    cmd.Execute();
 
    var result = String(cmd.Parameters("@status").Value);
    if (result == 'ready')
      Response.Write('File has been successfully saved');
    else
      Response.Write('Error: Failed while saving the file'+result);
  } 
  catch (err) {
    Response.Write('Error: ' + err.message);
  }
%>