<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../include/util.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
try{
var tran = ''

var action = Request("action");
var table = Request("table");
var columns = String(Request("columns"));
var where = (String(Request("environment")) != "undefined")  ? " WHERE environment = '" + String(Request("environment")) + "'" : "";
var order = (String(Request("order")) != "undefined")  ? " ORDER By " + String(Request("order"))  : "";

if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_OCMVIEW,env)) {	

	if(action == "read") {
		var sql= "exec ocm.getCOBComment @env='" + env + "'" ;
		rowsA = runSQL(sql,cAcc,"fulltable");
	}
	else {
		var firstComaPos = columns.indexOf(',');
		var idColumn = columns.substr(0, firstComaPos);
		var columnsWithoutID = columns.substr(firstComaPos+1);
		var record = eval('('+getData()+')');
		if (record.data.length == undefined) {
			rowsA = doOperation(action, record.data, cAcc, idColumn, columnsWithoutID);
		}
	}

	var myJSON = new JSON();
	var data = ''
	var fields = ''
	if (rowsA)  {
		for (i=0; i<rowsA.length;i++)	{			
			rowsA[i].text = rowsA[i].text.replace(/%0A/g, "<br>") 				
		}
		data = myJSON.toJSON(null, rowsA, false); 
		fields = myJSON.generateFields(rowsA)
	}
	else {
		var columnsarray = columns.split(',');
		fields = '['
		for(var i=0;i<columnsarray.length;i++) 
			fields += (i ? ',' : '') + '{"name": "' + columnsarray[i] + '"}'
		fields += ']'
		data = '[]'
	}
	Response.Write( '{ "results":' + data + '}');
}
}catch (err) 
	{
 		 logData('ocm/cobComment.asp: ' + err.message);
 		 Response.Write("{success:false,error:'" + err.message + "'}");
	} 	
	
	
function doOperation(action, data, conn, idColumn, columnsWithoutID) {  
	var sql = ''
	var result = [];
	if(CheckRight(SEC_ROLE_OCM_SAVECOBCOMMENT, env) && CheckRight(SEC_ROLE_OCMVIEW,env)) {	
		if (action == 'create') {  
			sql = "exec OCM.setCOBComment @state='"+ data["state"] +"', @comment='" + data["text"] + "', @env='" + env + "', @user_id = '" + data["userid"] + "'"
			var result = runSQL(sql,conn,"cmd");   
		}
	}
  return result;
}

function getData() {
 var sOut = ''
 if(Request.TotalBytes > 0){
    var lngBytesCount = Request.TotalBytes

    var stream = Server.CreateObject("Adodb.Stream")
    stream.type = 1
    stream.open
    stream.write(Request.BinaryRead(lngBytesCount))
    stream.position = 0
    stream.type = 2
    stream.charset = "iso-8859-1"
    sOut = stream.readtext()
    stream.close
  }
  return sOut;
}	
%>