<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../include/util.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
//<script>
try {
var bankdate = String(Request("bankdate"));
var tran = ''

var rowsB,result = new Array();
		
if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_OCMVIEW,env)) {
	var sql= "exec OCM.COBProcessChart @env='" + env + "'";
	rowsA = runSQL(sql,cAcc,"fulltable");			
	if(bankdate.split("-").length<3) {
        bankdate = convertDateTime(bankdate);
	}
	
	var sql= "exec OCM.COBProcessChart_avg @env='" + env + "',@bankdate='" + bankdate + "'";
	rowsB = runSQL(sql,cAcc,"fulltable");			
	
	var myJSON = new JSON();
	var data = ''
	var fields = ''

	
	if (rowsA){
		if(rowsB.length==0) {
			rowsB.push({});
			rowsB[0].B_avg = rowsA[0].B_start;
			rowsB[0].A_avg = rowsA[0].A_start;
			rowsB[0].S_avg = rowsA[0].S_start;
			rowsB[0].R_avg = rowsA[0].R_start;
			rowsB[0].D_avg = rowsA[0].D_start;
			rowsB[0].O_avg = rowsA[0].O_start;
			rowsB[0].N_avg = rowsA[0].N_start;
		}
		result.push({"stage":"Pre Batch",runtime:rowsA[0].B_start,avg_runtime:rowsB[0].B_avg})
		result.push({"stage":"Application",runtime:rowsA[0].A_start,avg_runtime:rowsB[0].A_avg})
		result.push({"stage":"System Wide",runtime:rowsA[0].S_start,avg_runtime:rowsB[0].S_avg})
		result.push({"stage":"Reporting",runtime:rowsA[0].R_start,avg_runtime:rowsB[0].R_avg})
		result.push({"stage":"Start of Day",runtime:rowsA[0].D_start,avg_runtime:rowsB[0].D_avg})
		result.push({"stage":"Online",runtime:rowsA[0].O_start,avg_runtime:rowsB[0].O_avg})
		result.push({"stage":"Pre Online",runtime:rowsA[0].N_start,avg_runtime:rowsB[0].N_avg})
	}
	data = myJSON.toJSON(null, result, false); 
	fields = myJSON.generateFields(result)

	Response.Write( '{ "metaData": { "totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');
}
}catch (err) 
	{
 		 logData('ocm/cobRuntimeChart.asp: ' + err.message);
 		 Response.Write("{success:false,error:'" + err.message + "'}");
	} 	
%>