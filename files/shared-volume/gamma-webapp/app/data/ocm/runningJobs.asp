<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../include/util.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
var tran = ''
try {
	var action = Request("action");
	var table = Request("table");
	var columns = String(Request("columns"));
	var bankdate = String(Request("bankdate"));
	var state = String(Request("state"));
	var where = (String(Request("environment")) != "undefined")  ? " WHERE environment = '" + String(Request("environment")) + "'" : "";
	var order = (String(Request("order")) != "undefined")  ? " ORDER By " + String(Request("order"))  : "";
	var myJSON = new JSON();
	var pstr;
	var actualArray = {"pre_online":"PO","pre_batch":"PB"};

	var rowsA = [];
	
	if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_OCMVIEW,env)) {		
		if(action == "read") {
			var process = "";
			if ((state == "pre_batch") || (state == "pre_online"))  {
				var sql= "SELECT * FROM ocm.runningJobs_pre('" + env + "','" + state.toUpperCase() +"')";
				rowsA = runSQL(sql,cAcc,"fulltable");
			}
			if(state == "batch") {
				var sql= "SELECT * FROM ocm.runningJobs_batch('" + env + "','" + bankdate + "')";
				rowsA = runSQL(sql,cAcc,"fulltable");
			}										
		}
			
		var data = '';
		var fields = '';

		if (rowsA.length)  {
			data = myJSON.toJSON(null, rowsA, false); 
			fields = myJSON.generateFields(rowsA)
		}
		else {
			var columnsarray = columns.split(',');
			fields = '[';
			for(var i=0;i<columnsarray.length;i++) 
				fields += (i ? ',' : '') + '{"name": "' + columnsarray[i] + '"}'
			fields += ']';
			data = '[]'
		}
		Response.Write( '{ "metaData": { "totalProperty" : "total", "root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '",  "results":' + data + '}');
	}
} 
catch (err) {
	if (tran)
		rollbackTransaction(tran);
	logData('ocm/runningJobs.asp: ' + err.message);
	Response.Write('Error: ' + err.message)
}

%>