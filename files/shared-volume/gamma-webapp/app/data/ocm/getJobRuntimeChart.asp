<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
try{
/* configre extra url request header properties */
var jobid = String(Request("jobid"));
var state 	= String(Request("state"));

if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_OCMVIEW,env)) {

	if ((state=="pre_online") || (state=="pre_batch"))
		sql= " exec OCM.getActionRuntimeChart @env='" + env + "', @action_id_int='" + jobid + "'"
	else
		sql= " exec OCM.getJobRuntimeChart @env='" + env + "', @job_id='" + jobid + "'"
	var fields = []
	var data = []
	var rowsA = []

	rowsA = runSQL(sql,cAcc,"fulltable");

	for ( item in rowsA)
	{
		rowsA[item].closed_day = formatDateTimeToHighChart(rowsA[item].closed_day)
	}

	var myJSON = new JSON();
	data 		= myJSON.toJSON(null, rowsA, false);
	fields 	= myJSON.generateFields(rowsA)

	Response.Write( '{ "metaData": { "totalProperty" : "total", 	"root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '","results":' + data + '}');
}
}catch (err)
{
 	logData('ocm/getJobRuntimeChart.asp: ' + err.message);
 	Response.Write("{success:false,error:'" + err.message + "'}");
}

%>