<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
/* configre extra url request header properties */
try{
var task 	= String(Request("task"));
var jobid 	= String(Request("jobid"));
var bankdate 	= String(Request("bankdate"));
var state 	= String(Request("state"));

		
if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_COBAN_VIEW, env)) {
	var cobid 	= isset(Request("cobid")) ? Request("cobid") : '';
	var jobdes = ""
	vprint(1,"JSON: processing task:"+task+":");

	if ((state!="pre_online") || (state!="pre_batch")) {
		var pre_config_data = [
		{
			"name"		: "batch_name",
			"text"		: "Batch name",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"name"		: "job_name",
			"text"		: "Job name",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"name"		: "run_time_avg",
			"text"		: "AVG runtime",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"name"		: "run_time_max",
			"text"		: "Max runtime",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"name"		: "run_time_min",
			"text"		: "Min runtime",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"name"		: "run_time_avg_cob",
			"text"		: "AVG runtime",
			"value"		: "",
			"editor"	: "",
			"group"		: "Filtered COB property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"name"		: "run_time_max_cob",
			"text"		: "Max runtime",
			"value"		: "",
			"editor"	: "",
			"group"		: "Filtered COB property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"name"		: "run_time_min_cob",
			"text"		: "Min runtime",
			"value"		: "",
			"editor"	: "",
			"group"		: "Filtered COB property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		}	]
	}
	else {
		var pre_config_data = [
		{
			"name"		: "stage",
			"text"		: "Stage",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"name"		: "batch_name",
			"text"		: "Batch name",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"name"		: "job_order",
			"text"		: "Order",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"name"		: "job_name",
			"text"		: "Job name",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"name"		: "use_first",
			"text"		: "First run",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: "'onlyDate'"
		},{
			"name"		: "run_time_avg",
			"text"		: "AVG runtime",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"name"		: "run_time_max",
			"text"		: "Max runtime",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"name"		: "run_time_min",
			"text"		: "Min runtime",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			 "name"		: "jobtext"
			,"text"		: "Job Comment"
			,"value"	: ""
			,"editor"	: ""
			,"group"	: "General property"
			,"editable"	: false
			,"status"	: false				
		},{
			"name"		: "run_time_avg_cob",
			"text"		: "AVG runtime",
			"value"		: "",
			"editor"	: "",
			"group"		: "Filtered COB property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"name"		: "run_time_max_cob",
			"text"		: "Max runtime",
			"value"		: "",
			"editor"	: "",
			"group"		: "Filtered COB property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"name"		: "run_time_min_cob",
			"text"		: "Min runtime",
			"value"		: "",
			"editor"	: "",
			"group"		: "Filtered COB property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		}	]
	}
				

	//job_properties_sql = "SELECT * FROM COB.getJobProperties('" + job + "')"
	
	if ((state!="pre_online") || (state!="pre_batch")){				
		general_properties_sql = "exec COB.actionProperty @action_id='" + jobid + "'"
		filteredCOBs_properties_sql = "exec COB.actionPropertyAtCOB @action_id='" + jobid + "', @cob_id='11111111-2222-3333-4444-555555555555', @env='" + env + "', @bankdate='" + bankdate + "'"
	}
	else {		
		general_properties_sql = "exec COB.jobProperty @job_id='" + jobid + "'"        // "exec Online.jobProperty @job_id='" + jobid + "'"
		filteredCOBs_properties_sql = "exec COB.jobPropertyAtCOB @job_id='" + jobid + "', @cob_id='11111111-2222-3333-4444-555555555555', @env='" + env + "', @bankdate='" + bankdate + "'"
	}
	var rowsA = new Array();
	rowsA = runSQL(general_properties_sql,cAcc,"fulltable");
	var jobdes	=  rowsA[0]["description"]
	buildPropertyGridElement(rowsA);
	buildPropertyGridElement(runSQL(filteredCOBs_properties_sql,cAcc,"fulltable"));
	var myJSON = new JSON();
	data 	=  myJSON.toJSON(null, pre_config_data, false); 
	Response.Write( '{ success:true, "metaData": { "description":"jobdesc", "totalProperty" : "total", 	"root" : "results", "id" : "id", 	fields:[{"name":"name"},{"name":"text"},{"name":"value"},{"name":"editor"},{"name":"group"},{"name":"editable"},{"name":"status"},{"name":"renderer"}]}, "total":"' + pre_config_data.length + '","jobdesc":"' + jobdes + '","results":' + data	 + '}');
}
}catch (err) 
{
 	logData('ocm/get_online_properties.asp: ' + err.message);
 	Response.Write("{success:false,error:'" + err.message + "'}");
} 

		
function buildPropertyGridElement(arr)
{
	for (i=0; i<arr.length;i++)
	{
	
		for (item in arr[i])
		{
			//rowsA.push({"name":item,"value":arr[i][item]});
			for (j=0; j<pre_config_data.length;j++)
			{
				if (pre_config_data[j]["name"] == item)
				{					
					pre_config_data[j]["value"] =  arr[i][item];
//					if (item=='jobtext')
//						pre_config_data[j]["value"] =  unescape(arr[i][item]);
					//Response.Write( '====' + pre_config_data[j]["name"] + 'item: ' + item + ' value ' + arr[i][item] + '<br>');
				}
			}
			
		}
	}
}

/*
******************* ha több sql lekérdezésbõl egy szép nagy rekordot szeretnél csinálni, akkor használf esztet
data 		=  myJSON.toJSON(null, rowsA1, false); 
data 		=  data + "," + myJSON.toJSON(null, rowsA2, false); 
fileds = myJSON.generateFields(rowsA1)
fileds 	= fileds + "," + myJSON.generateFields(rowsA2)
data 	= data.replace(/\[/g, "")
data 		=	data.replace(/]/g, "")
data= data.replace(/{/g, "")
data 	= data.replace(/}/g, "")
fileds 	= fileds.replace(/\[/g, "")
fileds 	= fileds.replace(/]/g, "")
data = "[{" + data + "}]" 
fileds = "[" + fileds + "]" 
*/
%>