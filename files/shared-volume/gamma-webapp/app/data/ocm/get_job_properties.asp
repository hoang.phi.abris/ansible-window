<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="../include/JSON.asp"-->
	<!--#include file="../include/get_db_data.asp"-->
	<!--#include file="../security/security.asp"-->

	<%
/* configre extra url request header properties */
try{
var task 	= String(Request("task"));
var jobid 	= String(Request("jobid"));
var bankdate 	= String(Request("bankdate"));
var state 	= String(Request("state"));


if(CheckRight(SEC_ROLE_LOGIN, null) && CheckRight(SEC_ROLE_COBAN_VIEW, env)) {
	var cobid 	= isset(Request("cobid")) ? Request("cobid") : '';
	vprint(1,"JSON: processing task:"+task+":");

	if ((state=="pre_online") || (state=="pre_batch")) {
		var pre_config_data = [
		{
			"id"		: "batch_name",
			"name"		: "Batch name",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"id"		: "job_name",
			"name"		: "Job name",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"id"		: "run_time_avg",
			"name"		: "AVG runtime",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"id"		: "run_time_max",
			"name"		: "Max runtime",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"id"		: "run_time_min",
			"name"		: "Min runtime",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"id"		: "run_time_avg_cob",
			"name"		: "AVG runtime",
			"value"		: "",
			"editor"	: "",
			"group"		: "Filtered COB property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"id"		: "run_time_max_cob",
			"name"		: "Max runtime",
			"value"		: "",
			"editor"	: "",
			"group"		: "Filtered COB property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"id"		: "run_time_min_cob",
			"name"		: "Min runtime",
			"value"		: "",
			"editor"	: "",
			"group"		: "Filtered COB property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		}	]
	}
	else {
		var pre_config_data = [
		{
			"id"		: "stage",
			"name"		: "Stage",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"id"		: "batch_name",
			"name"		: "Batch name",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"id"		: "job_order",
			"text"		: "Order",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"id"		: "job_name",
			"name"		: "Job name",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"id"		: "use_first",
			"name"		: "First run",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"id"		: "run_time_avg",
			"name"		: "AVG runtime",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"id"		: "run_time_max",
			"name"		: "Max runtime",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"id"		: "run_time_min",
			"name"		: "Min runtime",
			"value"		: "",
			"editor"	: "",
			"group"		: "General property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			 "id"		: "jobtext"
			,"name"		: "Job Comment"
			,"value"	: ""
			,"editor"	: ""
			,"group"	: "General property"
			,"editable"	: false
			,"status"	: false
		},{
			"id"		: "run_time_avg_cob",
			"name"		: "AVG runtime",
			"value"		: "",
			"editor"	: "",
			"group"		: "Filtered COB property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"id"		: "run_time_max_cob",
			"name"		: "Max runtime",
			"value"		: "",
			"editor"	: "",
			"group"		: "Filtered COB property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		},{
			"id"		: "run_time_min_cob",
			"name"		: "Min runtime",
			"value"		: "",
			"editor"	: "",
			"group"		: "Filtered COB property",
			"editable"	: false,
			"status"	: false,
			"renderer"	: ""
		}	]
	}


	//job_properties_sql = "SELECT * FROM COB.getJobProperties('{" + job + "}')"

	if ((state=="pre_online") || (state=="pre_batch")){
		general_properties_sql = "exec COB.actionProperty @action_id=" + jobid
		filteredCOBs_properties_sql = "exec COB.actionPropertyAtCOB @action_id=" + jobid + ", @cob_id='{11111111-2222-3333-4444-555555555555}', @env='" + env + "', @bankdate='" + bankdate + "'"
	}
	else {
		general_properties_sql = "exec COB.jobProperty @job_id='{" + jobid + "}'"
		filteredCOBs_properties_sql = "exec COB.jobPropertyAtCOB @job_id='{" + jobid + "}', @cob_id='{11111111-2222-3333-4444-555555555555}', @env='" + env + "', @bankdate='" + bankdate + "'"
	}
	var rowsA = new Array();

	rowsA = runSQL(general_properties_sql,cAcc,"fulltable");
	var jobdes = '';
	if(rowsA.length > 0){
	    jobdes = rowsA[0]["description"];
	}
	buildPropertyGridElement(rowsA);
	buildPropertyGridElement(runSQL(filteredCOBs_properties_sql,cAcc,"fulltable"));
	var myJSON = new JSON();
	data 	=  myJSON.toJSON(null, pre_config_data, false);
	Response.Write('{"success": true, "metaData": {"description":"jobdesc","totalProperty" : "total","root" : "results","id" : "id", "fields": [{"name":"id"},{"name":"name"},{"name":"value"},{"name":"editor"},{"name":"group"},{"name":"editable"},{"name":"status"},{"name":"renderer"}]},"total":"' + pre_config_data.length + '","jobdesc":"' + jobdes + '","results":' + data + '}');

}
}catch (err)
{
 	logData('ocm/get_job_properties.asp: ' + err.message);
 	Response.Write("{success:false,error:'" + err.message + "','sql':'"+general_properties_sql+"'}");
}

function buildPropertyGridElement(arr)
{
	for (i=0; i<arr.length;i++)
	{

		for (item in arr[i])
		{
			for (j=0; j<pre_config_data.length;j++)
			{
				if (pre_config_data[j]["id"] == item)
				{
					pre_config_data[j]["value"] =  arr[i][item];
				}
			}

		}
	}
}

/*
******************* ha több sql lekérdezésb?l egy szép nagy rekordot szeretnél csinálni, akkor használd esztet
data 		=  myJSON.toJSON(null, rowsA1, false);
data 		=  data + "," + myJSON.toJSON(null, rowsA2, false);
fileds = myJSON.generateFields(rowsA1)
fileds 	= fileds + "," + myJSON.generateFields(rowsA2)
data 	= data.replace(/\[/g, "")
data 		=	data.replace(/]/g, "")
data= data.replace(/{/g, "")
data 	= data.replace(/}/g, "")
fileds 	= fileds.replace(/\[/g, "")
fileds 	= fileds.replace(/]/g, "")
data = "[{" + data + "}]"
fileds = "[" + fileds + "]"
*/
%>