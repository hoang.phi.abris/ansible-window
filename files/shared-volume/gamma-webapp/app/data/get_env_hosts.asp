<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

<!--#include file="include/JSON.asp"-->
<!--#include file="include/get_db_data.asp"-->
<!--#include file="security/security.asp"-->

<%
try {
//	if(FindRights([SEC_ROLE_CONTROLLER_OPERATION_VIEW, SEC_ROLE_CONTROLLER_OPERATION])) {
		var environment = String(Request("environment"));

		sql = "SELECT h.id, h.name FROM Configuration.Instances i "
		sql += "INNER JOIN Configuration.Hosts h ON i.host = h.name "
		sql += "WHERE i.environment = '" + environment + "'" 
		rowsA = runSQL(sql,cAcc,"fulltable");

		var myJSON = new JSON();
		var data = ''
		var fields = ''
		if (rowsA.length)  {
			data = myJSON.toJSON(null, rowsA, false); 
			fields = myJSON.generateFields(rowsA)
		}
		else {
			data = '[]'
			fields = '[]'
		}

		Response.Write( '{ "metaData": {	"totalProperty" : "total", "root" : "results", "id" : "id", 	"fields" : ' + fields + '}, "total":"' + rowsA.length + '", "results":' + data + '}');
//	}
} 
catch (err) {
  Response.Write('Error: ' + err.message);
    logData('get_env_hosts.asp: ' + err.message);
}

%>
