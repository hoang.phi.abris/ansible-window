<%@CODEPAGE=65001 LANGUAGE="JAVASCRIPT"%>

	<!--#include file="include/JSON.asp"-->
	<!--#include file="include/get_db_data.asp"-->
	<!--#include file="security/security.asp"-->

	<%
var host_name = String(Request("host_name"));							
var env_name = String(Request("env_name"));							
var modified_by = Session("username");

try {
	if(FindRight(SEC_ROLE_GAMMACONFIG)) {
		var conn = beginTransaction(cAcc) 	
		
		var sql = "UPDATE Configuration.Environments SET default_host = (SELECT id FROM Configuration.Hosts WHERE name = '" + host_name + "') WHERE name = '" + env_name + "'";
		runSQL(sql,conn,"cmd");
		
		sql = "INSERT INTO Audit.LogDetails ([user], log_type, timestamp, env, param1, param2) VALUES('" + modified_by + "', 17, GETDATE(),'" + env_name + "', 0, '" + host_name + "')";
		logData(sql)
		runSQL(sql,conn,"cmd");

		commitTransaction(conn);
	}
}
catch (err) {
  Response.Write('Error: ' + err.message);
  logData('save_default_host.asp: ' + err.message);
}

%>